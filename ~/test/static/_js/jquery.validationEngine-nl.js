﻿

(function($) {
	$.fn.validationEngineLanguage = function() {};
	$.validationEngineLanguage = {
		newLang: function() {
			$.validationEngineLanguage.allRules = 	{"required":{    			// Hier uw regex-bepalingen toevoegen- u kunt uw telefoon als voorbeeld gebruiken
						"regex":"geen",
						"alertText":"* Dit veld is verplicht",
						"alertTextCheckboxMultiple":"* Kiest u een optie a.u.b.",
						"alertTextCheckboxe":"* Dit selectievakje is verplicht"},
					"length":{
						"regex":"geen",
						"alertText":"*Tussen ",
						"alertText2":" en ",
						"alertText3": " tekens toegestaan"},
					"maxCheckbox":{
						"regex":"geen",
						"alertText":"* Toegestane controles overschreden"},	
					"minCheckbox":{
						"regex":"geen",
						"alertText":"* Selecteren a.u.b. ",
						"alertText2":" opties"},	
					"equals":{
						"regex":"geen",
						"alertText":"* Uw wachtwoorden komen niet overeen"},		
					"telephone":{
						"regex":/^[0-9\-\(\)\ ]+$/,
						"alertText":"* Ongeldig telefoonnummer"},	
					"email":{
						"regex":/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.(?:[A-Z]{2}|COM|ORG|NET|EDU|GOV|MIL|BIZ|INFO|MOBI|NAME|AERO|ASIA|JOBS|MUSEUM)$/i,
						//regex":/^[a-zA-Z0-9_\.-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9\.]{2,5}$/,
						"alertText":"* Ongeldig e-mailadres"},	
					"date":{
                         "regex":/^[0-9]{4}\-\[0-9]{1,2}\-\[0-9]{1,2}$/,
                         "alertText":"* Ongeldige datum, moet volgende datumnotatie hebben"},
					"onlyNumber":{
						"regex":/^[0-9\ ]+$/,
						"alertText":"* Uitlsuitend numerieke waarden"},	
					"noSpecialCharacters":{
						"regex":/^[0-9a-zA-Z]+$/,
						"alertText":"* Geen speciale tekens toegestaan"},	
					"ajaxUser":{
						"file":"validateUser.php",
						"extraData":"",
						"alertTextOk":"* Deze gebruiker is beschikbaar",	
						"alertTextLoad":"* Laden..., een ogenblik geduld a.u.b.",
						"alertText":"* Deze gebruikersnaam is reeds in gebruik"},	
					"ajaxName":{
						"file":"validateUser.php",
						"alertText":"* Deze naam is reeds in gebruik",
						"alertTextOk":"* Deze naam is beschikbaar",	
						"alertTextLoad":"* Laden..., een ogenblikje geduld a.u.b."},		
					"onlyLetter":{
						"regex":/^[a-zA-Z\ \']+$/,
						"alertText":"* Uitsluitend letters"}
            };
            
        }
    };
    $.validationEngineLanguage.newLang();
})(jQuery);


