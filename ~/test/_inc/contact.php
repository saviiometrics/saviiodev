
		<?php
			$countries = $controlObj->listCountries();
		?>
		
		<script type="text/javascript">
			$(function() {
				$('.goContact').click(function() {
					var valid = $("#contactForm").validationEngine('validate');
	  			if(valid == true) {
						 $.post("/AjaXFreeTrial.php", {accName:$('#company').val(), sPhone:$('#phone').val(), contactWhen:$('#contactmein').val(), sEmail:$('#email').val(), sFirstName:$('#first_name').val(), sLastName:$('#last_name').val(), countryid:$('#countryid').val(), sCountry:$('#countryid :selected').text(), contactMe:true},
						   function(data){
							   	$('#contactHeader').html('Thanks for getting in touch!');
									$('#maincontentPad').html('<p>Thanks for your interest in Saviio, we will contact you as soon as possible.</p><br />');
						   });
					}
			  });
				$("#contactForm").validationEngine('attach');
			});
		</script>
		
     <div id="innerContent">
	      <h1 class="MWxHeader">Contacting us...</h1>
	      <div class="headerDark-Grey-pro subheadtext" id="contactHeader">There's nothing better than working with great people.</div>
	    	<div id="maincontentPad">
		      <p>If you're asking yourself any of the questions below and want to bounce some ideas around the answers - get in touch.</p>
					<ul>
						<li>How do I achieve better quality with fewer resources?</li>
						<li>How can I increase revenues without increasing head-count?</li>
						<li>Where is my focus when coaching less-effective performers?</li>
						<li>How should I reorganise my teams to maximise value?</li>
						<li>How can I define our employee brand to attract the right people?</li>
					</ul>
		 			<form id="contactForm">
			 			<table border="0" cellpadding="6" cellspacing="0" class="contactTable">
			 				<tr>
			 					<td class="contactFormTitle">
			 						<span class="headerDark-Grey-pro">First Name</span><br />
			 						<span class="small-required">Required</span>
			 					</td>
			 					<td class="contactFormInput"><input id="first_name" maxlength="40" name="first_name" size="20" type="text" class="validate[required,length[3,32]] textfield" /></td>
			 			  </tr>
			 			 	<tr>
			 					<td class="contactFormTitle">
			 						<span class="headerDark-Grey-pro">Family Name</span><br />
			 						<span class="small-required">Required</span>
			 					</td>
			 					<td class="contactFormInput"><input id="last_name" maxlength="80" name="last_name" size="20" type="text" class="validate[required,length[3,32]] textfield" /></td>
			 			  </tr>
			 			  <tr>
			 					<td class="contactFormTitle">
			 						<span class="headerDark-Grey-pro">Email Address</span><br />
			 						<span class="small-required">Required</span>
			 					</td>
			 					<td class="contactFormInput"><input id="email" maxlength="80" name="email" size="20" type="text" class="validate[required,length[3,32],custom[email]] textfield" /></td>
			 			  </tr>
			 			  	<tr>
			 					<td class="contactFormTitle">
			 						<span class="headerDark-Grey-pro">Organisation</span><br />
			 						<span class="small-required">Required</span>
			 					</td>
			 					<td class="contactFormInput"><input id="company" maxlength="40" name="company" size="20" type="text" class="validate[required,length[3,32]] textfield" /></td>
			 			  </tr>
			 			  <tr>
			 					<td class="contactFormTitle">
			 						<span class="headerDark-Grey-pro">Phone Number</span><br />
			 						<span class="small-required">Required</span>
			 					</td>
			 					<td class="contactFormInput"><input id="phone" maxlength="40" name="phone" size="20" type="text" class="validate[required,length[3,32],custom[onlyNumberSp]] textfield" /></td>
			 			  </tr>
			 			  <tr>
			 					<td class="contactFormTitle">
			 						<span class="headerDark-Grey-pro">Country List</span><br />
			 						<span class="small-required">Required</span>
			 					</td>
			 					<td class="contactFormInput">
			 						<select name="countryid" id="countryid" class="listfield">
				 						<?php
										foreach($countries as $cid => $country) {
											if($cid == 242) {
												$selected = ' selected="selected"';
											} else {
												$selected = '';
											}
											echo '<option value="' . $cid . '"' . $selected. '>' . $country . '</option>' . "\n";
										}
										?>
								 </select>
								</td>
			 			  </tr>
			 			   <tr>
			 					<td class="contactFormTitle">
			 						<span class="headerDark-Grey-pro">Contact me:</span><br />
			 						<span class="small-required">Required</span>
			 					</td>
			 					<td class="contactFormInput">
			 						<select name="contactmein" id="contactmein" class="listfield">
											<option value="ASAP">ASAP</option>
											<option value="Today">Today</option>
											<option value="This Week">This Week</option>
								 </select>
								</td>
			 			  </tr>
			 			</table>	
		 			</form>
		 			<br /><br />
					<a href="javascript:void(0);" class="goContact"><img src="/_images/contact_us_tab.png" width="166" height="34" /></a><br /><br />
		 	 </div>
  	</div>
  	