<?php
$compinfo = $adminobj->getCompanyDetails($_SESSION['uid']);
?>

<input type="hidden" value="<?=ADMIN_ACCOUNT_PATH;?>" name="adminaccpath" id="adminaccpath" />
<input type="hidden" value="<?=ACCOUNT_PATH;?>" name="accpath" id="accpath" />
<!-- Overlay Box for Adding / Editing Companies -->
<div id="companybox" class="accountsBox">
	<div class="addpad">
		<div class="cicon" style="padding:10px;"><img src="<?=ADMIN_ACCOUNT_PATH;?>_images/_icons/IC_AddCompany.png" width="16" height="15" /></div>
		<h2>Add Account</h2>
		<p>
			<?=("Please fill in the form below to add an account to the system.");?>
		</p>
		<div id="companyboxheader"><?=_('Account Details');?></div>
		<form id="companyform" name="companyform">
			<table border="0" cellpadding="3" cellspacing="2" width="664">
				<?php
				if($superAccess['GODADMIN'] == 'TRUE') { 
					$accList = $accobj->getAccounts();
					?>
					<tr>
						<td><?=_('Select Account (ENTERPRISE LEVEL ETC)');?></td>
						<td align="right">
							<select name="godAccid" id="godAccid">
								<option value="0">--Please Select--</option>
								<?php
								foreach($accList as $accid => $accArr) {
									echo '<option value="' . $accid . '">' . $accArr['accountname'] . '</option>' . "\n";
								}
								?>
							</select>	
						</td>
					</tr>
				<?php } ?>
				<tr class="ctyperow">
					<td><?=_('Account Type');?></td>
					<td align="right">
						<select name="ctype" id="ctype">
							<?php 
							$companyAccess = $adminobj->getLevelAccess('company');
							//everyone apart from sub account admins can add a main account.
							if($companyAccess['VIEW'] != 'SA') {
								echo '<option value="head" selected="selected">' . _("Main Account") . '</option>' . "\n";
							}
							echo '<option value="sub">' . _("Associated Account") . '</option>' . "\n";
							?>
						</select>
					</td>
				</tr>
					<tr class="compheadlist">
						<td><?=("Select Main Account");?></td>
						<td align="right">
							<select name="compheadid" id="compheadid">
								<option value="0">--Please Select--</option>
								<?php
								$headcomps = $adminobj->getCompanies("head");
								foreach($headcomps as $compid => $infoarr) {
									echo '<option value="' . $compid . '">' . $infoarr['companyname'] . '</option>' . "\n";
								}
								?>
							</select>	
						</td>
					</tr>
				<tr>
					<td><?=_('Account Name');?></td>
					<td align="right"><input type="text" name="companyN" id="companyN" class="validate[required,length[3,32]] textfield" size="30" /></td>
				</tr>
				<tr>
					<td><?=_('Account Description');?></td>
					<td align="right"><input type="text" name="companyD" id="companyD" class="validate[required,length[3,32]] textfield" size="30" /></td>
				</tr>
				<tr>
					<td><?=_('Account Country');?></td>
					<td align="right">
						<select name="CCountry" id="CCountry">
							<?php
							$countries = $adminobj->getCountries();
							foreach($countries as $cid => $country) {
								echo '<option value="' . $cid . '">' . $country . '</option>' . "\n";
							}
							?>
						</select>	
							<input type="hidden" name="editCompID" id="editCompID" />
							<input type="hidden" name="xRTG" id="xRTG" value="<?php if($companyAccess['VIEW'] != 'SA') { echo 'Y'; } else { echo 'N'; }?>" />
							<input type="hidden" name="headID" id="headID" value="<?=$adminobj->headid; ?>" />
					</td>
				</tr>
			</table>
		</form>
		<p>
			<?=_('To save the new Account click the "Save" Button, else click "close" or press ESC to close the dialog box.');?>
		</p>
		<!-- yes/no buttons -->
		<p>
			<button class="savecompany"> <?=_('Save');?> </button>
			<button class="close"> <?=_('Close');?> </button>
		</p>

	</div>
</div>


<div id="sharecompanybox" class="accountsBox">
	<div class="addpad">
		<div class="cicon" style="padding:10px;"><img src="<?=ADMIN_ACCOUNT_PATH;?>_images/_icons/IC_AddCompany.png" width="16" height="15" /></div>
		<h2>Share Account</h2>
		<p>
			<?=_("From here you can share accounts with other administrators. You can select the users you wish to share a particular account with and grant them certain access rights. You can select which access rights to give them by simply checking the checkboxes provided.");?>
		</p>
		<div id="companyboxheader"><?=_('Administrator List');?></div>
		<div id="shareAccBox">
			<select id="shareAcc" class="multiselect" multiple="multiple" name="shareAcc[]"></select>
		</div>
		<div id="editSHAREbox"></div>
		<div id="shareuserlist"></div>
		<div class="cleaner"></div>
		<p>
			<?=_('Once you have selected the administrators you wish to share this account with, please click the "Next Stage" link below.');?>
		</p>
		<!-- yes/no buttons -->
		<p id="showRightsLink"><a href="javascript:void(0);" class="selectRights orangetab">Next Stage</a></p>
		<p id="submitRightsLink"><a href="javascript:void(0);" class="submitShare orangetab">Save Access Rights</a></p>
		<p id="updateRightsLink"><a href="javascript:void(0);" class="updateShare orangetab">Update Access Rights</a></p>
		<p>
			<button class="close"> <?=_('Close');?> </button>
		</p>

	</div>
</div>

<div id="requestpullbox" class="accountsBox">
	<div class="addpad">
		<div class="cicon" style="padding:10px;"><img src="<?=ADMIN_ACCOUNT_PATH;?>_images/_icons/IC_AddCompany.png" width="16" height="15" /></div>
		<h2>Request Access to Account</h2>
		<p><?php echo _("Please use the search box below to type in the name of the account you wish to request access to. Alternatively if you don't know the name of the account but know the owner, type in the owners name and all of their accounts will be listed.");?></p>
		<div id="pullAccListBox">
			<select id="pullAccList" class="multiselect" multiple="multiple" name="pullAccList[]"></select>
		</div>
		<p>
		<?php echo _('Please type in your comments below and let the owner know why you would like access to the account.');?>
		</p>
		<p>
			<form name="pull_comments" id="pull_comments">
				<textarea name="req_comments_pull" id="req_comments_pull" class="validate[required,length[3,500]]"></textarea>
			</form>
		</p>
		<p>
			<button class="sendPullReq"> <?=_('Send Request');?> </button>
			<button class="close"> <?=_('Close');?> </button>
		</p>

	</div>
</div>

<div id="showrequestsbox" class="accountsBox">
	<div class="addpad">
		<div class="cicon" style="padding:10px;"><img src="<?=ADMIN_ACCOUNT_PATH;?>_images/_icons/IC_AddCompany.png" /></div>
		<h2>Your Account Requests</h2>
		<p><?php echo _("This window helps you track requests that people have made for your accounts, or requests you have made to other users' accounts.");?></p>

		<div id="sharereqs">
				
			<p style="font-size:14px; font-weight:bold;">Recieved Requests</p>
			
			<table cellspacing="1" width="676" cellpadding="3" border="0">
				<thead>
					<tr>
						<th align="left"><strong><?=_('Company Name');?></strong></th>
						<th align="left"><strong><?=_('Requester');?></strong></th>
						<th align="left"><strong><?=_('Date Requested');?></strong></th>
						<th align="left"><strong><?=_('Admin Panel');?></strong></th>
					</tr>
				</thead>
				<tbody>
					<?php
					$req_arr = $adminobj->getShareRequests('owner');
					if(count($req_arr) == 0) {
						echo '<tr><td colspan="4">' . _("You have no pending requests") . '</td></tr>' . "\n";
					} else {
						foreach($req_arr as $srid => $reqinfo) {
							switch($reqinfo['status']) {
								case 1:
								$status = _('Pending');
								$status_style = 'req_pending';
								break;
								case 2:
								$status = _('Approved');
								$status_style = 'req_approved';
								break;
								case 3:
								$status = _('Declined');
								$status_style = 'req_declined';
								break;
							}
							echo '<tr>' . "\n";
							echo '<td class="reqComp" id="reqcid_' . $reqinfo['reqcid'] . '">' . $reqinfo['companyname'] . '</td>' . "\n";
							echo '<td>' . $reqinfo['firstname'] . ' ' . $reqinfo['surname'] . '</td>' . "\n";
							echo '<td>' . date('d/m/Y', $reqinfo['request_time']) . '</td>' . "\n";
							echo '<td><a href="javascript:void(0);" id="requid_' . $reqinfo['requid'] . '" class="reqapprove">Approve</a> | <a href="javascript:void(0);" id="srid_' . $srid . '" class="reqdecline">Decline</a></td>' . "\n";
							echo '</tr>' . "\n";
						}
					}
					?>
				</tbody>
			</table>

			<p style="font-size:14px; font-weight:bold;">Sent Requests</p>
	
			<table cellspacing="1" width="676" cellpadding="3" border="0">
				<thead>
					<tr>
						<th align="left"><strong><?=_('Company Name');?></strong></th>
						<th align="left"><strong><?=_('Account Owner');?></strong></th>
						<th align="left"><strong><?=_('Date Requested');?></strong></th>
						<th align="left"><strong><?=_('Status');?></strong></th>
					</tr>
				</thead>
				<tbody>
					<?php
					$req_arr = $adminobj->getShareRequests('requested');
					if(count($req_arr) == 0) {
						echo '<tr><td colspan="4">' . _("You have no pending requests") . '</td></tr>' . "\n";
					} else {
						foreach($req_arr as $srid => $reqinfo) {
							switch($reqinfo['status']) {
								case 1:
								$status = _('Pending');
								$status_style = 'req_pending';
								break;
								case 2:
								$status = _('Approved');
								$status_style = 'req_approved';
								break;
								case 3:
								$status = _('Declined');
								$status_style = 'req_declined';
								break;
							}
							echo '<tr>' . "\n";
							echo '<td>' . $reqinfo['companyname'] . '</td>' . "\n";
							echo '<td>' . $reqinfo['firstname'] . ' ' . $reqinfo['surname'] . '</td>' . "\n";
							echo '<td>' . date('d/m/Y', $reqinfo['request_time']) . '</td>' . "\n";
							echo '<td><span class="' . $status_style . '">' . $status . '</span></td>' . "\n";
							echo '</tr>' . "\n";
						}
					}
					?>
				</tbody>
			</table>
		</div>
		<div id="reqshareadd"></div>
		<br />
		<div id="reqaddlink"><a href="javascript:void(0);" class="requestAdd orangetab"><?=('Save Access Rights');?></a></div>
		<p>
			<button class="close"> <?=_('Close');?> </button>
		</p>

	</div>
</div>

<div id="structurebar"><div class="iconfloat"><img src="<?=ADMIN_ACCOUNT_PATH;?>_images/_icons/IC_Structure.png" width="15" height="13" /></div> <div class="floatleft"><?=_('Structure');?></div></div>
<div class="titlebar_shadow"></div>
<div id="content_pad">
<h1 class="MWxHeader"><?=_("Manage Accounts")?></h1>
<p><?=_('In this section you can create and edit main accounts and associated accounts. Additionally you can also share your own accounts by clicking on the "Share Account" icon in the Admin panel column of the account you wish to share. Alternatively if you wish to request access to an account, click on the "Request Account Share" icon of the account you wish to have access too. If you cannot see the account you want access too, simply click the "Request Access" button on the right of the page, and search for the account you want access too and follow the on screen instructions.');?></p>
<p><strong><?=_("Here is a key for the icons in the \"Access Controls\" column, simply click these icons to access the features.");?></strong></p>
<p>
<?php
echo '<img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_EditApplication.png" alt="' . _("Edit Account Details") . '" title="' . _("Edit Account Details") . '" /> - ' . _("Edit Account Details") . '<br />' . "\n";
echo '<img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_ShareAccount.png" alt="' . _("Share Account") . '" title="' . _("Share Account") . '" /> - ' . _("Share Account") . '<br />' . "\n";
?>
</p>
<div style="margin-left:10px;" class="acc_sharetab"><a href="javascript:void(0);" class="showrequests">Show Requests</a></div><div class="acc_sharetab"><a href="javascript:void(0);" class="reqpull">Request Access</a></div>
<div style="clear:right;"></div>
<?php
$companies = $adminobj->getCompanies();
$countries = $adminobj->getCountries();
?>
<div id="tableHolder">
	<table cellspacing="1" class="tablesorter">
		<thead>
			<tr>
				<th><?=_('Account Name');?></th>
				<th><?=_('Country');?></th>
				<th><?=_('Main Account');?></th>
				<th><?=_('Master Account');?></th>
				<th><?=_('Type');?></th>
				<th><?=_('Created By');?></th>
				<th><?=_('Admin Panel');?></th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<th><?=_('Account Name');?></th>
				<th><?=_('Country');?></th>
				<th><?=_('Main Account');?></th>
				<th><?=_('Master Account');?></th>
				<th><?=_('Type');?></th>
				<th><?=_('Created By');?></th>
				<th><?=_('Admin Panel');?></th>
			</tr>
		</tfoot>
		<tbody>
			<?php 
			$timenow = time();
			$access = $adminobj->getLevelAccess('company');
			$shareaccess = $adminobj->getAccountShareAccess('comp');
			function checkAccess($acctype, $html, $companyARR, $adminid, $headid, $companyid, $checktype, $sharerights, $currcompid) {
				echo $companyARR['companyid'];
				switch($acctype) {
					case 'ALL':
					$adminstring = $html;
					break;
					case 'CREATED':
					if($companyARR['uid'] == $adminid) {
						$adminstring = $html;
					} else {
						if($sharerights[$currcompid][$checktype] == 'TRUE') {
							$adminstring = $html;
						}
					}
					break;
					case 'MA':
					if($companyARR['headid'] == $headid) {
						$adminstring = $html;
					} else {
						if($sharerights[$currcompid][$checktype] == 'TRUE') {
							$adminstring = $html;
						}
					}
					break;
					case 'SA':
					if($companyARR['companyid'] == $companyid) {
						$adminstring = $html;
					} else {
						if($sharerights[$currcompid][$checktype] == 'TRUE') {
							$adminstring = $html;
						}
					}
					break;
				}
				return $adminstring;
			}
			foreach($companies as $companyid => $companyARR) {
				$adminstring = '';
				$editstringHTML = '<a href="javascript:void(0);" class="editcompany" id="editcom_' . $companyid . '"><img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_EditApplication.png" width="16" height="15" /></a>&nbsp;&nbsp;' . "\n";
				$sharestringHTML = '<a href="javascript:void(0);" class="sharecompany" id="sharecom_' . $companyid . '"><img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_ShareAccount.png" width="16" height="16" /></a>&nbsp;&nbsp;' . "\n";
				$requeststringHTML = '<a href="javascript:void(0);" class="requestshare" id="reqshare_' . $companyid . '"><img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_RequestShare.png" width="16" height="15" /></a>' . "\n";
				$adminstring .= checkAccess($access['EDIT'], $editstringHTML, $companyARR, $adminobj->uid, $adminobj->headid, $adminobj->companyid, 'EDIT', $shareaccess, $companyid);
				$adminstring .= checkAccess($access['SHARE'], $sharestringHTML, $companyARR, $adminobj->uid, $adminobj->headid, $adminobj->companyid, 'SHARE', $shareaccess, $companyid);
				//$adminstring .= checkAccess('ALL', $requeststringHTML, $companyARR, $adminobj->uid, $adminobj->headid, $adminobj->companyid, 'SHARE', $shareaccess, $companyid);
				
				if($adminstring == '') {
					$adminstring = _('Read Only');
				}
				
				if($userARR['level'] == 1 && $adminobj->level != 1) {
					$adminstring = _('Read Only');
				}
				
				echo '<tr>' . "\n";
				echo '<td>' . $companyARR['companyname'] . '</td>' . "\n";
				echo '<td><img src="..' . ACCOUNT_PATH . '_images/_flags/' . $companyARR['countryid'] . '.gif" alt="' . $countries[$companyARR['countryid']] . '" title="' . $countries[$companyARR['countryid']] . '" /></td>' . "\n";
				$compobj = $adminobj->getCompany($companyARR['headid']);
				echo '<td>' . $compobj->companyname . '</td>' . "\n";
				echo '<td>' . $compobj->accountname . '</td>' . "\n";
				if($companyARR['type'] == 'head') {
					$typeshow = _('Main Account');
					$comptype = 'headquarters';
				} else {
					$typeshow = _('Sub Account');
					$comptype = 'childcompany';
				}
				echo '<td class="' . $comptype . '">' . $typeshow . '</td>' . "\n";
				echo '<td>' . $companyARR['fullname'] . '</td>' . "\n";
				echo '<td><div class="makerelative">' . $adminstring . '</div></td>' . "\n";
				echo '</tr>' . "\n";
			}
			?>
		</tbody>
	</table>

 </div>
 	<div class="cleaner"></div>
</div>
