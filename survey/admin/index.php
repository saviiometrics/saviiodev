<?php
if(!isset($_SESSION)) { session_start(); }
$phpsessid = session_id();
//ini_set("display_errors", 1);
//include localization and site config files
require_once("../site.config.php");
include CONTENT_PATH . '/_classes/db-class.php';
include CONTENT_PATH . '/_classes/account-class.php';

$accobj = new Account($_SESSION['accid']);

//include other classes
include CONTENT_PATH . '/_classes/user-class.php';
include CONTENT_PATH . '/_classes/admin-class.php';
include CONTENT_PATH . '/_classes/MAP-class.php';
include CONTENT_PATH . '/_classes/prettyDate-class.php';

	if(isset($_GET['logout'])) {
		session_destroy();
		$_SESSION = array();
	}
	
	function in_multi_array( $needle,$haystack ) {
		$in_multi_array = false;
		if( in_array($needle,$haystack) ) {
			$in_multi_array = true;
		} else {
			for( $i=0; $i<sizeof($haystack); $i++ )	{
				if( is_array($haystack[$i]) ) {
					if( in_multi_array($needle, $haystack[$i]) ) {
						$in_multi_array = true;
						break;
					}
				}
			}
		}
		return $in_multi_array;
	}

	$userobj = new User();
	//first login
	if(isset($_POST['firstlogin'])) {
		//do the check here for surveys expiration?
		//$userobj->checkExpiration();
		if(!$userobj->checkLogin(addslashes($_POST['savuser']), md5($_POST['savpass']))) {
			$errorstr = _("I'm sorry your username and password were incorrect");
		} else {
			session_regenerate_id(true);
			$_SESSION['username'] = addslashes($_POST['savuser']);
			$_SESSION['passw'] = md5($_POST['savpass']);
			$_SESSION['dashpass'] = $_POST['savpass'];
			$userobj->getUservUN($_POST['savuser']);
			$_SESSION['uid'] = $userobj->uid;
			$_SESSION['level'] = $userobj->level;
			$_SESSION['accid'] = $userobj->accid;
			$accobj = new Account($userobj->accid);
		}
	}

	require_once(CONTENT_PATH . "/localization.php");
	require_once(CONTENT_PATH . "/scripts.php");

	if(isset($_SESSION['username'])) {
		if(!$userobj->checkLogin($_SESSION['username'], $_SESSION['passw'])) {
			session_destroy();
			$_SESSION = array();
		} else {
			//5184000 is 60 days!
			$freeTrialExp = false;
			//service ID 3 is freetrial
			if($accobj->serviceid == 3) {
		  	$calculation = (($accobj->time_added + 5184000) - time(void));
		  	if($calculation < 0) {
		  		$freeTrialExp = true;
		  	} else {
		  		$freeTrialExp = false;
		  	}
		  }

			$adminobj = new Admin($userobj->uid);
			//get admin levels
			$adminlevels = $adminobj->getLevels();
			$levelAccess = intval($adminlevels[$adminobj->level]["levelAccess"]);
			//$adminlevels = array_reverse($adminlevels, true);
			
			$adminaccess = $adminobj->getLevelAccess('siteaccess');
			//access for god admins
			$superAccess = $adminobj->getLevelAccess('admin');
			//reset this for every page load, is populated if needed below in switch statement.
			$jsMinFile = '';
			if($adminaccess['ADMIN'] == 'TRUE') {
				switch($actionurl) {
					case '?':
					$pageviewed = 'Welcome';
					break;
					case '?admin=main':
					$pageviewed = 'Welcome';
					break;
					case '?admin=tmaps&tmap=talentmap':
					$jsMinFile = 'tmaps-min.js';
					break;
					case '?admin=tmaps&tmap=teammap':
					$pageviewed = 'Team MAPs';
					$jsMinFile = 'tmaps-min.js';
					break;
					case '?admin=companies':
					$pageviewed = 'Companies';
					$jsMinFile = 'companies-min.js';
					break;
					case '?admin=users':
					$pageviewed = 'Users';
					$jsMinFile = 'users-min.js';
					break;
					case '?admin=accesslog':
					$pageviewed = 'Access Log';
					$jsMinFile = 'accesslog-min.js';
					break;
					case '?admin=emails':
					$pageviewed = 'Emails';
					$jsMinFile = 'email-min.js';
					break;
					case '?admin=archive':
					$pageviewed = 'Archive';
					break;
					case '?admin=cp':
					$pageviewed = 'Control Panel';
					$jsMinFile = 'cp-min.js';
					break;
					case '?admin=lms':
					$pageviewed = 'Learning Management System';
					break;
					case '?admin=feedback':
					$pageviewed = 'Feedback';
					break;
					case '?admin=idealmaps':
					$pageviewed = 'IdealMAPs';
					$jsMinFile = 'idealmaps-min.js';
					break;
					case '?admin=adminpanel':
					$pageviewed = 'Admin Panel';
					$jsMinFile = 'adminpanel-src.js';
					break;
					case '?admin=faqs':
					$pageviewed = 'FAQs';
					break;
					default:
					$pageviewed = 'Welcome';
					break;
				}
				$adminobj->runTracker('Viewed Page', $pageviewed, 0, 'PAGEVIEW');
				$_SESSION['level'] = $adminobj->level;
				$MODAccessIDs = $adminobj->getModuleAccess();
				$MODArr = $adminobj->getModules($MODAccessIDs);
				//less than 6 is admin.. 6 is survey taker
		
					//build template file
					require (ADMIN_PATH . "/header.php");
					require (ADMIN_PATH . "/menu.php");
					
					//get the whole string after the ?admin= to check against db and get modid then see if that modid is in access ids for this user!
					$checkstr = substr($actionurl, 7);
					$mid = $adminobj->getMid($checkstr);
	
					if( !isset($_GET['admin'])) {
						require (ADMIN_CONTENT_PATH .  "/main.php");
					} else {
						/*
						if(in_array($mid, $MODAccessIDs)) {
							if(!require (ADMIN_CONTENT_PATH . '/' . $_GET['admin'] . '.php')) {
								echo "I'm sorry this page doesn't exist";
							}
						} else {
							echo _("Sorry you don't have access to this page!");
						}
						*/
						require (ADMIN_CONTENT_PATH . '/' . $_GET['admin'] . '.php');
					}
					require (ADMIN_PATH . "/footer.php");
			} else if ($freeTrialExp == true) {
				$errorstr = _("I'm sorry your 60 day Free Trial has expired. Please contact accounts@saviio.com to upgrade your account!");
				require (ADMIN_PATH . "/login.php");
			} else {
				$errorstr = _("I'm sorry you do not have access rights to view the admin section");
				require (ADMIN_PATH . "/login.php");
			}
		}
	} else {
		require (ADMIN_PATH . "/login.php");
	}

?>

