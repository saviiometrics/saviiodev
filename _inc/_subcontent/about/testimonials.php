		<div id="innerContent">
			<div id="contentMenuBar">
	      <div id="contentSiFR">
	      	<h2 class="MWxBox">Don't just take our word for it. Check out the testimonials below to see what Saviio MAPs has done for other high profile organisations.</h2>
	      </div>
	      <div class="cleaner"></div>
	     </div>
	     <div id="contentSideGFX">
	     	<img src="/_images/_sidebar/<?=$sideGFX;?>" width="194" height="150" />
	     </div>
	     <div class="cleaner"></div>
				<div class="infoLeft">
					<span class="contentHeader">We Focus on Business Issues</span>
					We work on concrete business issues, not general organisational climate. 
					<p><blockquote><span class="testMonHead">Coors Brewers</span><br />"Saviio did a terrific job for us. It was the only company I felt really listened to our business needs, and worked with us to address these through a tailored management development programme. I am already witnessing both improved behaviours and skill levels in the management team, which is clearly impacting significantly on business performance."<br /><br /><strong>Director, Coors Brewers Ltd</strong><br /><br /></blockquote></p>
					<span class="contentHeader">Each Solution is Customised</span>
					Our tailor-made programs combine diagnostics, consulting, teaching, coaching and team facilitation. We emphasise on-going performance measurement to ensure that the learning experiences are translated into concrete actions and results. 
					<p><blockquote><span class="testMonHead">Nestle</span><br />"One of the benefits of working with Saviio, is that they listen and tailors their involvement to your needs. Unlike many consultancy firms, there's something very practical and relevant about what they do." <br /><br /><strong>MD, Nestle UK Beverages Division</strong><br /><br /></blockquote></p>
					<span class="contentHeader">We Go Deep</span>
					We work beyond the level of techniques and behavioral skills, addressing the emotional and thought processes, and the values and beliefs at levels.
					<p><blockquote><span class="testMonHead">Butchers Pet Care</span><br />"Saviio has a refreshing way of getting to the root cause of an issue. They don't hide things in a thick report, and when they need to deliver a difficult message, they're not afraid of doing it.  They don't drop a problem on the table without suggesting two or three solutions."<br /><br /><strong>CEO, Butchers Pet Care</strong><br /><br /></blockquote></p>
					<span class="contentHeader">We're good at what we do</span>
					We are a group of business people formally trained in soft and hard disciplines committed to helping our clients improve their effectiveness and organisational performance. (We're a diverse bunch - musician, engineer, mathematician, choregrapher, psychologist, drummer...) 
					<p><blockquote><span class="testMonHead">BT</span><br /> "We wanted someone inspirational for a team building workshop and Saviio came highly recommended. Saviio's knowledge and expertise, their ability to analyse group dynamics and their gravitas ensured that it was an excellent learning experience for the team."<br /><br /><strong>HR Manager, BT</strong><br /><br /></blockquote></p>
				</div>	
    	
				<div class="infoLeft">
					<span class="contentHeader">Don't just take our word for it!</span>
					<p>Still not convinced why you need Saviio to help your organisation? Check out the testimonials below to see how we've made other clients happy, really happy!</p>
					<p><blockquote><span class="testMonHead">Sitel</span><br />Since implementing Saviio in five of our major outsourced clients' campaigns we have managed to reduce the annual attrition rate by an average of over 10% - equivalent to an annual saving in excess of six figures. I have also noticed that our team leaders and campaign managers have created a "buzz about the place" by better understanding the needs of their teams and being able to coach them accordingly.<br /><br /><strong>Director, Corporate Development, Sitel</strong><br /><br /></blockquote></p>
					<p><blockquote><span class="testMonHead">Office Depot</span><br />"I have worked with Saviio in a number of totally different companies. Saviio has a unique ability to fit perfectly with the culture, ethos and objectives of those businesses."<br /><br /><strong>HR Director, Office Depot</strong><br /><br /></blockquote></p>
					<p><blockquote><span class="testMonHead">SAM Headhunting</span><br />"We use Saviio MAPs to improve the quality of our service to our clients and candidates. We have now made Saviio MAPs a strategic tool in our offer and look forward to a long relationship with Saviio and future applications which they will provide for us." <br /><br /><strong>Board Member, SAM Headhunting, Denmark</strong><br /><br /></blockquote></p>
					<p><blockquote><span class="testMonHead">Staples</span><br />" Thanks so much for all your support and challenge that's been instrumental in what I believe to have been a remarkable gathering for Staples UK & Ireland. Unique to date and one of our best. I have a feeling we will work together again and, for me, a special connection and friendship has emerged with you that I have already placed great value in."<br /><br /><strong>HR Director, Staples UK & Ireland</strong><br /><br /></blockquote></p>
				</div>	
		</div>
	
 
 













 





















