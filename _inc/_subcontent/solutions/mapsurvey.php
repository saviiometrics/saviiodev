
		<div id="innerContent">
			<div id="contentMenuBar">
	      <div id="contentSiFR">
	      	<h2 class="MWxBox">Saviio MAPs improves individual, team and organisational performance by providing continuous insight into the unique combination of human factors that predict success.</h2>
	      </div>
	      <div class="cleaner"></div>
	     </div>
	     <div id="contentSideGFX">
	     	<img src="/_images/_sidebar/sidebar_maps.jpg" width="194" height="150" />
	     </div>
	     <div class="cleaner"></div>
	     <div class="infoLeft">
				<span class="contentHeader">The Saviio MAPs Survey</span>
				<p>The heart of the tool comprises 60 bipolar statements that inform 12 bipolar preference sets - organised into 4 main Factors. When completed by the respondent, a MAP (Motivations and Preferences) is created. The MAP may be viewed in several ways:</p>
				<ol>
				 <li>By itself - without comparing against other individuals, teams or job profiles.</li>
				 <li>Comparison against a previous MAP from the same individual</li>
				 <li>Comparison against another individual, team or job profile.</li>
				</ol>
				<br />
				<span class="contentHeader">Benchmark Reporting:</span>
				<p>A benchmark can be established by generating responses from within an organisation and as such does not depend on normative data to generate comparisons. MAPs may be combined to produce benchmarks against which teams, talent, managers and individuals can all be compared for 'match' of 'fit'. When used for recruitment and selection, an individuals' MAP may be compared with a talentMAP (a job profile) that has been created for a specific job vacancy.</p><br />
				<span class="contentHeader">Theory:</span>
				<p>The underlying theory supporting <em>Saviio MAPs </em>is based on NLP (Neuro Linguistic Programming) Meta Programs. Meta Programs are filters that determine how you perceive the world around you and have a major influence on how you communicate with others and the behaviours you manifest. <em>Meta </em>means over, beyond, above, or on a different level - i.e. operating at an unconscious level. Meta Programs are deep-rooted mental programs, which automatically filter our experience and guide and direct our thought processes, resulting in significant differences in behaviour from person to person. They define typical patterns in the strategies or thinking styles of an individual, group, company or culture.</p>
				<p>Saviio MAPs are used in assessment, recruitment, selection and coaching.<br />
				 The results set out in the reports have a usefulness of between six to nine months. In most cases patterns of individual preferences change only slowly over time, but in times of rapid change in your personal or working life the <em>Saviio MAPs </em>profile may develop more quickly.<br />
				 <em>Saviio MAPs </em>is only part of the overall assessment process alongside other essential elements:</p>
				 <ol>
				 	<li>Face to face meeting and dialogue.</li>
				  <li>CV with accompanying evidence of achievement and reference sources provided.</li> 
				  <li>Competency Assessment (in relation to the role).</li>
				  <li><em>Saviio MAPs </em>- Motivations and Preferences of the individual in <em>relation </em>to the role, the team and the manager.</li>
				 </ol>
				 <p>Please note that the results set out reflect preferences rather than abilities. <em>Saviio MAPs </em>will help you to understand <em>how </em>an individuals prefer to operate - for example when making decisions. It will <strong><em>not </em></strong>assess their <em>ability </em>(for example, how <strong><em>good </em></strong>they are at making decisions).</p>
			 </div>
  	</div>
  	  