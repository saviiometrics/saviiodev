		<div id="innerContent">
			<div id="contentMenuBar">
	      <div id="contentSiFR">
	      	<h2 class="MWxBox">Every organisation experiences some degree of staff attrition. But what is sometimes ignored is the fact that the reasons why employees leave can help attract and retain high quality people in the future.</h2>
	      </div>
	      <div class="cleaner"></div>
	     </div>
	     <div id="contentSideGFX">
					<img src="/_images/_sidebar/<?=$sideGFX;?>" width="194" height="150" />
	     </div>
	     <div class="cleaner"></div>
	     <div class="featureHighlights">
	     	<div class="contentPadTop">
	     		<p>	Saviio works with clients to source and make best use of this information through a wide range of tools and practices including:</p>
      	</div>
      	<ul>
         <li><strong>Exit surveys</strong><p>	We develop tailored exit questionnaires that help organisations pinpoint and remedy any failings in procedures, processes or culture.</p></li>
         <li><strong>Employee surveys</strong><p>We also construct ongoing employee surveys that can spot and address such failings before they lead to staff moving on.</p></li>
         <li class="cleaner"><strong>	Outplacement</strong><p>	Where redundancies become necessary Saviio can provide a full outplacement service to help departing employees re-establish careers elsewhere.</p></li>
         <li><strong>Keep-in touch programmes</strong><p>If an employee's departure is managed effectively they can become a valuable potential source of new talent or new business. Saviio can assist in the development of highly effective alumni programmes to make this a reality.</p></li>
        </ul>
      </div>
		</div>
		