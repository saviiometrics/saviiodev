<?php
/**
 * Groups configuration for default Minify implementation
 * @package Minify
 */

/** 
 * You may wish to use the Minify URI Builder app to suggest
 * changes. http://yourdomain/min/builder/
 **/
 
if(!isset($_SESSION)) { session_start(); }
$phpsessid = session_id();

//dont really use multi accounts for different style right now so just fix the acc path here
define(ACCOUNT_PATH, "_accounts/_saviio/");

return array(
    'static_sale' => array(
    	'//_js/swfobject.js',
      '//_js/jquery.validationEngine.js',
      '//_js/jquery.cycle.all.min.js',
      '//_js/jquery.easing.1.3.js',
    	'//_js/jquery.twitter.search.js', 
    	'//_js/jquery.validationEngine-en.js',
    	'//_js/sale/header-min.js'
    ),
    'static_css_sale' => array(
			'//_css/validationEngine.jquery.css',
			'//_css/scrollable-buttons.css',
			'//_css/scrollable-horizontal.css',
			'//_css/jquery.jscrollpane.css'
    ),
    'static' => array(
      '//_js/plugins/localisation/jquery.localisation.min.js',
      '//_js/jquery.jgrowl.js',
      '//_js/ajaxupload.js',
      '//_js/jquery.validationEngine.js',
    	'//_js/jquery.confirm-1.3.js', 
    	'//_js/jquery.timers.1.1.3.js',
    	'//_js/jquery.cycle.all.min.js',
    	'//_js/jquery.tmpl.1.1.1.js',
    	'//_js/jquery.blockUI.js',
    	'//_js/ui.multiselect.js',
    	'//_js/digitialspaghetti.password.min.js',
    	'//_js/jquery.stylish-select.js',
    	'//_js/jquery.floatobject-1.4.js',
    	'//_js/swfobject.js',
    	'//_js/jquery.mousewheel.js',
    	'//_js/jquery.jscrollpane.min.js',
    	'//_js/jquery.watermark.js',
    	'//_js/jquery.qtip-1.0.0-rc3.min.js',
    	new Minify_Source(array(
    		'filepath' => '//_js/jquery.tools.min.js',
    		'minifier' => '',
    	))
    ),
    'admin' => array(
			'//_js/jquery.dataTables.min.js',
    	'//_js/admin/header-min.js',
    	'//_js/highcharts.js', 
    	'//_js/hc_exporting.js'
    )
);