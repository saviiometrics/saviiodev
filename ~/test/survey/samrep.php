<?php
$month = isset($_GET["month"]) ? $_GET["month"] : null;
if(!is_null($month)) {
	$year = $_GET["year"];
	if($month == 12) {
		$endmonth = 1;
		$endyear = $year + 1;
	} else {
		$endmonth = $month + 1;
		$endyear = $year;
	}
	
	$start = mktime(0,0,1,$month,1,$year);
	$end = mktime(0,0,1,$endmonth,1,$endyear) - 1;
	$out = array();
	if(APPLICATION_ENV == "development") {
		$mySQLConnection = new mysqli('192.168.33.11', 'saviiomaps', 'sp1derm0nkey', 'savmaps', 3306);
	} else {
		$mySQLConnection = new mysqli('localhost', 'saviiomaps', 'Ch7g$#%G23', 'savmaps', 3306);
	}
	$stmt = $mySQLConnection->prepare('SELECT uid, email, firstname, surname FROM sav_users WHERE accid = 17 AND username like "%sam-int.com" AND email like "%sam-int.com"');
	//$stmt = $mySQLConnection->prepare('SELECT uid, email, firstname, surname FROM sav_users WHERE accid = 9');
	$stmt->execute();
	$stmt->bind_result($uid, $email, $firstname, $surname);
	while($stmt->fetch()) {
		$out[] = array('uid'=>$uid, 'email'=>$email, 'firstname'=>utf8_decode($firstname), 'lastname'=>utf8_decode($surname), 'Activity'=>array());
	}
	$stmt->close();
	$stmt = $mySQLConnection->prepare('SELECT action, result, timestamp FROM sav_log WHERE uid = ? AND timestamp > ' . $start .' AND timestamp < ' . $end);
	foreach($out as $id=>$user) {
		$uid = $user['uid'];
		$stmt->bind_param('s', $uid);
		$stmt->execute();
		$stmt->bind_result($action, $result, $time);
		while($stmt->fetch()) {
			$out[$id]['Activity'][] = array('action'=>utf8_decode($action), 'result'=>utf8_decode($result), 'time'=>date("H:i:s d M y",$time));
		}
	}
	$stmt->close();
	$pages = array();
	$noPages = array();
	$maps = array();
	$noMaps = array();
	foreach ($out as $uid=>$user) {
		$count = 0;
		$pvs = 0;
		foreach ($user["Activity"] as $aid=>$activity) {
			if($activity['action'] == "Added User") {
				$count ++;
			} elseif($activity['action'] == "Viewed Page") {
				$pvs ++;
			}
		}
		if($count > 0) {
			$maps[] = $user;
		} else {
			$noMaps[] = $user;
		}
		if($pvs > 0) {
			$pages[] = $user;
		} else {
			$noPages[] = $user;
		}
	}
	
		?>
	<table border="1">
  <tr>
    <th>Firstname</th>
    <th>Lastname</th>
    <th>Email</th>
    <th>MAPs Issued</th>
  </tr>

	<?php 
	foreach ($maps as $user) {
		$count = 0;
		foreach ($user["Activity"] as $activity) {
			if($activity['action'] == "Added User") {
				$count ++;
			}
		}
		?>
		 <tr>
		    <td><?= $user['firstname']; ?></td>
		    <td><?= $user['lastname']; ?></td>
		   	<td><?= $user['email']; ?></td>
		   	<td><?= $count; ?></td>
		  </tr>
		<?php
		
	}
	foreach ($noMaps as $user) {
		$count = 0;
		foreach ($user["Activity"] as $activity) {
			if($activity['action'] == "Added User") {
				$count ++;
			}
		}
		?>
		 <tr>
		    <td><?= $user['firstname']; ?></td>
		    <td><?= $user['lastname']; ?></td>
		   	<td><?= $user['email']; ?></td>
		   	<td><?= $count; ?></td>
		  </tr>
		<?php
		
	}
	?>
	</table>
	<?php 
	
	?>
	<table border="1">
  <tr>
    <th>Firstname</th>
    <th>Lastname</th>
    <th>Email</th>
    <th>Page viewed</th>
    <th>Date</th>
  </tr>

	<?php 
	foreach ($pages as $user) {
		?>
		 <tr>
		    <td><?= $user['firstname']; ?></td>
		    <td><?= $user['lastname']; ?></td>
		   	<td><?= $user['email']; ?></td>
		   	<td>&nbsp;</td>
		   	<td>&nbsp;</td>
		  </tr>
		<?php
		foreach ($user["Activity"] as $activity) {
			if($activity['action'] == "Viewed Page") {
		?>
		 <tr>
		 	<td>&nbsp;</td>
		   	<td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td><?= $activity['result']; ?></td>
		   	<td><?= $activity['time']; ?></td>
		  </tr>
		<?php 
			}
	}
	}
	foreach ($noPages as $user) {
		?>
		 <tr>
		    <td><?= $user['firstname']; ?></td>
		    <td><?= $user['lastname']; ?></td>
		   	<td><?= $user['email']; ?></td>
		   	<td>&nbsp;</td>
		   	<td>&nbsp;</td>
		  </tr>
		<?php
		foreach ($user["Activity"] as $activity) {
			if($activity['action'] == "Viewed Page") {
		?>
		 <tr>
		 	<td>&nbsp;</td>
		   	<td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td><?= $activity['result']; ?></td>
		   	<td><?= $activity['time']; ?></td>
		  </tr>
		<?php 
			}
	}
	}
	?>
	</table>
	<?php 
}
