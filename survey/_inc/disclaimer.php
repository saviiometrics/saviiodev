<div class="contentPadding">
  <h2 class="MWxMedium"><?= _("Legal Disclaimer");?></h2>
  <p>The information contained in this website is provided for general information purposes only. Bespoke legal advice must be taken for each individual query as every situation is different. While we do our best to ensure that the content of this site is accurate, errors or omissions may occur, and we do not accept any liability in respect of them. SAVIIO LTD. accepts no responsibility for loss which may arise from any reliance which is placed on any information contained in this site or from access to, or inability to access, this site.</p>
  <p>Any links provided to external web sites are provided for convenience and we are not responsible for the content or operation of such sites.</p>
  <p>&copy; The content of this website is the property of SAVIIOMETRICS LTD. unless there is an indication to the contrary. The name SAVIIO and the SAVIIO logo are trade marks.</p>
 Permission is given for the downloading of the content of this website for the purpose of viewing on a personal computer or monitor. No further reproduction of the content is permitted without the approval of SAVIIOMETRICS LTD
<p class="MWxMedium">&nbsp;</p>
</div>