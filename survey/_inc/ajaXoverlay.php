	  <?php
	  if(!isset($_SESSION)) { session_start(); }
		$phpsessid = session_id();
		$start_time=microtime(true);

		//include localization and site config files
		require_once("../site.config.php");
		//include DB AND ACCOUNT INFO CLASSES
		include CONTENT_PATH . '/_classes/db-class.php';
		include CONTENT_PATH . '/_classes/account-class.php';
		$accobj = new Account($_SESSION['accid']);
	
		//include other classes
		include FULL_PATH . '/_inc/_classes/question-class.php';
		include FULL_PATH . '/_inc/_classes/MAP-class.php';
		include FULL_PATH . '/_inc/_classes/user-class.php';
		
		$userobj = new User();
		$MAPobj = new MAP();
		require_once(FULL_PATH . "/_inc/localization.php");
		require_once(FULL_PATH . "/_inc/scripts.php");


		$fids = $MAPobj->getfids();
    $fgs = $MAPobj->getFactorGroups();
    $cids = $MAPobj->getClusters();

		$maptype = 'MAP';

		//rankLink

		$mapid = $_GET['mapid'];
		//$mapid = 1;
		$tmapobj = $MAPobj->getMAPvid($mapid);
		$mapname = $tmapobj->firstname . ' ' . $tmapobj->surname;
		$leftmapUID = $tmapobj->uid;
		//get multigen left map select0r
		$leftswitchHTML = GenSelectHTML($leftmapUID, 'left', $MAPobj, $l_mapids, $mapname);

		//set session var for lmapids
		$_SESSION['mapid'] = $mapid;
		$mapscores = $MAPobj->getScores($mapid);
	
		$arrFactorScores = array();
		$arrDeviations = array();

		loopScoreSingle($mapscores, $mapid, $mapname, $leftmapUID, $mapid);
		
		$rankid = 0;
		$WsConstant = 17.8;
		$overlay_arr = array();
		foreach($fgs as $fg => $clusterid) {
			$intScoreMin = min($arrFactorScores[$rankid][$fg]['lscoreall']);
			$intScoreMax = max($arrFactorScores[$rankid][$fg]['lscoreall']);
			
			$WsConstant = 18;
			$clusterscore += $permatch;
			
			$intScoreWidth = round((($intScoreMax - $intScoreMin) * $WsConstant),0);
	  	$lf_total = number_format($arrFactorScores[$rankid][$fg]['lscore'],2,'.','');
		  $fac_total = number_format($arrFactorScores[$rankid][$fg]['num'],2,'.','');
		  $roundnum_l = $lf_total/$fac_total;
		  $engnum_l = number_format($roundnum_l, 2, '.', '');
		  $finalval = number_format((($engnum_l * $WsConstant)-11),2,'.','');
		  $overlay_arr[$fg] = array('position' => $finalval, 'barleft' => round(($WsConstant * $intScoreMin), 0), 'barwidth' => $intScoreWidth);
		}
		
		echo json_encode($overlay_arr);
		
    ?>