<?php

//include DB class for future user
include_once('db-class.php');

class PageController {
		public $pagename;
		public $countriestable;
	
		public function __construct() {
			$this->countriestable = 'system_countries';
		}
		
		public function getContent($pagename, $submenu=false) {
			/*
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			*/
		

		}//end getContent
		
		public function getSideGFX($saviioTag=false) {
		
		$GFXArray = array("sidebar_analytics.jpg",
											"sidebar_graph.jpg",
											"sidebar_manstacked.jpg",
											"sidebar_maps.jpg",
											"sidebar_multiperson.jpg",
											"sidebar_popup.jpg"
								);

		return $GFXArray;

		}//end getSideGFX	
		
		
		public function generateMenu($saviioTag) {
   		/* SUB MENU ARRAY BUILDER THINGY */
    	switch($saviioTag) {
    		case 'about':
    		$menuArray = array("The First 100 Days" => array("title" => "The First 100 days", "link" => "first100days", "submenu" => false),
    											 "Our Main Activities" => array("title" => "Our Main Activities", "link" => "mainactivities", "submenu" => false),
    											 "Our Design Concept" => array("title" => "Our Design Concept", "link" => "designConcept", "submenu" => false),
    											 "oneSaviio" => array("title" => "One Saviio Foundation", "link" => "onesaviio", "submenu" => false),
    											 "Testimonials" => array("title" => "Testimonials", "link" => "testimonials", "submenu" => false),
    											 "Case Studies" => array("title" => "Case Studies", "subgroup" => "casestudies", "link" => false,
    											    "submenu" => array(
    														"Sales or Safety?" => array("title" => "Sales or Safety?", "link" => "cs_attract"),
    													  "Service or Stress?" => array("title" => "Service or Stress?", "link" => "cs_engage"),
    														"Luck or Design?" => array("title" => "Luck or Design?", "link" => "cs_salesperformance")
    													)
    											 )
    		);
    		break;
    		case 'resources':
    		$menuArray = array("Employee Engagement" => array("title" => "Employee Engagement", "subgroup" => "employee_engage", "link" => false, 
    													"submenu" => array(
    														"Strategic narrative" => array("title" => "Strategic narrative", "link" => "attract"),
    													  "Engaging Managers" => array("title" => "Engaging Managers", "link" => "engage"),
    														"Employee Voice" => array("title" => "Employee Voice", "link" => "develop"),
    														"Integrity" => array("title" => "Integrity", "link" => "retain")
    													)
    											 ),
    											 "Transition Acceleration" => array("title" => "Transition Acceleration", "subgroup" => "transaccel", "link" => false,
    													"submenu" => array(
    														"Promote Yourself" => array("title" => "Promote Yourself", "link" => "attract"),
    													  "Accelerate Your Learning" => array("title" => "Accelerate Your Learning", "link" => "engage"),
    														"Match Strategy to Situation" => array("title" => "Match Strategy to Situation", "link" => "develop"),
    														"Secure Early Wins" => array("title" => "Secure Early Wins", "link" => "retain"),
    														"Negotiate Success" => array("title" => "Negotiate Success", "link" => "attract"),
    													  "Achieve Alignment" => array("title" => "Achieve Alignment", "link" => "engage"),
    														"Build Your Team" => array("title" => "Build Your Team", "link" => "develop"),
    														"Create Coalitions" => array("title" => "Create Coalitions", "link" => "retain")
    													)
    											 ),
    											 "The Psychological Contract" => array("title" => "The Psychological Contract", "subgroup" => "pyschcontract", "link" => false,
    											    "submenu" => array(
    														"Breaking the contract?" => array("title" => "Breaking the contract?", "link" => "attract"),
    													  "Taking the contract seriously" => array("title" => "Taking the contract seriously", "link" => "engage"),
    														"Employer Brand" => array("title" => "Employer Brand", "link" => "develop"),
    														"The importance of communication" => array("title" => "The importance of communication", "link" => "retain"),
    														"Strategic implications of the contract" => array("title" => "Strategic implications of the contract", "link" => "salesperformance")
    													)
    											 ),
    											 "FAQs" => array("title" => "FAQs", "subgroup" => "faqs", "link" => false,
    											    "submenu" => array(
    														"What kind of leader do you want?" => array("title" => "What kind of leader do you want?", "link" => "attract"),
    													  "Psychological basis for Saviio MAPs" => array("title" => "Psychological basis for Saviio MAPs", "link" => "engage"),
    														"Tell me more about Saviio MAPs" => array("title" => "Tell me more about Saviio MAPs", "link" => "develop"),
    														"How do you construct a great team?" => array("title" => "How do you construct a great team?", "link" => "retain"),
    														"Personality Profiling" => array("title" => "Personality Profiling", "link" => "salesperformance"),
    														"NLP - What is it?" => array("title" => "NLP - What is it?", "link" => "retain"),
    														"NLP Meta Programs" => array("title" => "NLP Meta Programs", "link" => "salesperformance")
    													)
    											 )
    		);
    		break;
    		case 'solutions':
    		$menuArray = array("Saviio MAPs" => array("title" => "Saviio MAPs", "subgroup" => "saviiomaps", "link" => false,
    													"submenu" => array(
    														"The Survey" => array("title" => "The Survey", "link" => "mapsurvey"),
    														"What is Saviio MAPs?" => array("title" => "What is Saviio MAPs", "link" => "whatissaviiomaps"),
    														//"Features" => array("title" => "Features", "link" => "saviiomaps_features"),
    														"Understanding the Report" => array("title" => "Understanding the Report", "link" => "understanding_report")
    													)
    											 ),
    											 "Consulting" => array("title" => "Consulting", "subgroup" => "consulting", "link" => false,
    													"submenu" => array(
    														"Consulting" => array("title" => "Consulting", "link" => "consulting"),
    														"Attract" => array("title" => "Attract", "link" => "workforce_attract"),
    													  "Engage" => array("title" => "Engage", "link" => "workforce_engage"),
    														"Develop" => array("title" => "Develop", "link" => "workforce_develop"),
    														"Retain" => array("title" => "Retain", "link" => "workforce_retain")
    													)
    											 )
    		);
    		break;
    	}

			return $menuArray;

		}//end generateMenu
		
		public function listCountries() {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}

			$sql = "SELECT * from $this->countriestable ORDER BY Name ASC";

			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error());
				
			$i = 0;
			$countries = array();
			while( $row = mysql_fetch_object( $result ) ) {
				$countries[$row->countryid] = $row->Name;
				$i = $i + 1;
			}//end while
			
			RETURN $countries;

			$db->mysqlclose();
		}
		
}//end class

?>
