
<div class="contentPadding">
	 <?php if(isset($_SESSION['username']) && $_SESSION['level'] < 5) { /*
	 <p style="font-size:14px; color:red;">Please note the site has been upgraded to a new version (v2.24). Please clear your browser cache to make sure you are running the latest version of the code. If after you have tried this and are experiencing any problems please use the feedback form in the admin section</p>
	 <p>For a list of changes please see the <a href="http://<?=$_SERVER['SERVER_NAME'];?>/changelog.txt" target="_blank">Change Log</a></p>
	 */} ?>
	<div class="largeHeader"><?= _("Welcome to Saviio MAPs");?></div>
  <h2 class="MWxMedium"><?= _("About the Survey:");?></h2>
  <p><?php printf(_("%s is a survey to help individuals to better understand their Motivations And Preferences (MAP) when forming relationships, thinking and planning, making decisions and getting things done."), ACC_NAME); ?></p>
  <p><?= _("Your results are confidential and will only be used for the purpose intended.  Your name and email address will NOT be sold or distributed to any third party.");?></p><br />
  <h2 class="MWxMedium"><?= _("How to complete the survey:");?></h2>
 	<p><?= _("This survey is about you and your work role. Please have this in mind when completing the survey.");?></p>
  <p><?= _("You will be presented with 60 statement-pairs, one at a time.");?></p>
  <p><?= _("Once you have started the survey, continue through to the end in your own time without interruption. There is no time limit, however allocate yourself approximately 20 minutes of uninterrupted time.");?></p>
  <p><?= _("Consider both statements in the pair and move the slider anywhere along the line to the place that you feel best fits your response.  Then click 'Next' for the next statement pair, until you have responded to all 60 statement-pairs.");?></p>
  <p><?= _("You may choose the centre position of each statement-pair if you feel that both statements are equally applicable; however, to do this, you must move the slider away from the centre position first and then back again so that we know this is your deliberate choice.");?></p>
  <p><?php printf(_("There are no right or wrong answers and you are free to work at your own natural pace. Your %s consultant will let you know what happens next when you have completed the survey."), ACC_NAME);?></p>
  <p><?= _("Click \"Start the Survey\" to begin the survey.");?></p>
  <p>&nbsp;</p>
  <?php if(isset($_SESSION['username'])) { ?>
  	<div id="startsurvey"><a href="?saviio=map"><?=_("Start the Survey");?></a></div>
  <?php } else { ?>
  	<div style="font-weight:bold;"><?=_("I'm sorry you must login before you can continue to take the survey. Please login at the top of the screen with the login details you were provided in order to gain access to the survey.");?></div>
	<?php } ?>
</div>