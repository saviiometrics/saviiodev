<?php

require 'vendor/autoload.php';

$domain = 'http://survey.saviio.com/';

$hostname = 'survey.saviio.com';

$to_email = array('david.foggin@saviio.com','sudhir@porwalconsulting.com');

$from_email = 'sudhir@porwalconsulting.com';

$from_name = 'Sitecheck';

$error_subject = 'ALERT - '.$hostname.' might be down.';

$limit_subject = 'ALERT - '.$hostname.' might be working slow.';

$limit = 15;

$client = new GuzzleHttp\Client();

$mandrill = new Mandrill('d38ZyLlZhPO73qJK_CGmbQ');

function sendEmail($html,$subject,$from_email,$from_name,$to_email)
{
  return  $message = array(
            'html' => $html,
            'subject' => $subject,
            'from_email' => $from_email,
            'from_name' => $from_name,
            'to' => array(
                array(
                    'email' => $to_email[0],
                    'type' => 'to'
                ),array(
                    'email' => $to_email[1],
                    'type' => 'to'
                )
        ));
}

try
{
    $time1 = new DateTime('now');
    
    $res = $client->request('GET', $domain);
    
    $time2 = new DateTime('now');
    
    $interval = $time1->diff($time2);
    
    $sec = $interval->format('%s');
    
//    $milisec = $sec * 1000;
    
    if($sec > $limit)
    {
        $message = sendEmail('<p>Host Name : '.$domain.'</p><p>Time to load the home page = '.$interval->format('%s second(s)').'</p>',$limit_subject,$from_email,$from_name,$to_email);
    
        $result = $mandrill->messages->send($message);
    }
}
catch(GuzzleHttp\Exception\ClientException $e)
{   
    $message = sendEmail('<p>Host Name : '.$domain.'</p>',$error_subject,$from_email,$from_name,$to_email);
    
    $result = $mandrill->messages->send($message);
    
    echo $e->getResponse()->getStatusCode();
}
catch(GuzzleHttp\Exception\ClientErrorResponseException $e)
{
    $message = sendEmail('<p>Host Name : '.$domain.'</p>',$error_subject,$from_email,$from_name,$to_email);
    
    $result = $mandrill->messages->send($message);  
}
catch(GuzzleHttp\Exception\ConnectException $e)
{
    $message = sendEmail('<p>Host Name : '.$domain.'</p>',$error_subject,$from_email,$from_name,$to_email);
    
    $result = $mandrill->messages->send($message);
}
catch(GuzzleHttp\Exception\RequestException $e)
{
    $message = sendEmail('<p>Host Name : '.$domain.'</p>',$error_subject,$from_email,$from_name,$to_email);

    $result = $mandrill->messages->send($message);
}


?>
