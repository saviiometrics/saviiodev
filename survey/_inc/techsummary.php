<?php
	//include localization and site config files
	//include DB AND ACCOUNT INFO CLASSES
	if(!isset($_SESSION)) { session_start(); }
define ("MAIL_HOSTNAME","ssl://smtp.gmail.com:465");
define ("MAIL_USERNAME","adwords@saviio.com");
define ("MAIL_PASSWORD","daniel1709");

define ("ROOT_URL","http://".$_SERVER['HTTP_HOST'] . "");
define ("ROOT_PATH","/");
define ("FULL_PATH",$_SERVER['DOCUMENT_ROOT'] . '/');
define ("CONTENT_PATH",$_SERVER['DOCUMENT_ROOT'] . '/_inc');
define ("SURVEY_CONTENT_PATH",$_SERVER['DOCUMENT_ROOT'] . '/survey/_inc');
	
	$accobj = new Account($_SESSION['accid']);
	
	//include other classes
	include_once FULL_PATH . '/_inc/_classes/question-class.php';
	include_once FULL_PATH . '/_inc/_classes/MAP-class.php';
	include_once FULL_PATH . '/_inc/_classes/user-class.php';
	include_once FULL_PATH . '/_inc/_classes/admin-class.php';
	include_once FULL_PATH . '/_inc/_classes/pdf-class.php';
	
	//$_GET['pdflocale'] = 'da_DK';
	require_once(FULL_PATH . "/_inc/localization.php");
	require_once(FULL_PATH . "/_inc/scripts.php");
	
	$dispPDFObj = new DisplayPDF();
	$fontn = $dispPDFObj->getFont($_SESSION['locale']);
	$_SESSION['fontn'] = $fontn;
	if(!isset($_REQUEST['orientation']) || $_REQUEST['orientation'] == 'landscape') {
		$PDF_PAGE_ORIENTATION = 'L';
		$height = 16.8;
		$width = 55.5;
		$clusttoptenwidth1 = "3.3%";
		$clusttoptenwidth2 = "90.7%";
		$matchedStatements = "90.7%";
		$clusttoptenwidth3 = "5.7%";
		$clusttoptenwidth4 = "0.3%";
		$clusterBar1 = "92.7%";
		$clusterBar2 = "0.8%";
		$clusterBar3 = "5.7%";
	} else {
		$PDF_PAGE_ORIENTATION = 'P';
		$height = 16.8;
		$width = 55.5;
		$clusttoptenwidth1 = "30";
		$clusttoptenwidth2 = "540";
		$matchedStatements = "550";
		$clusttoptenwidth3 = "52.5";
		$clusttoptenwidth4 = "0";
		$clusterBar1 = "582";
		$clusterBar2 = "4";
		$clusterBar3 = "52.5";
	}
	$clusterPredictorCount = array();
		$clusterPredictorCount[1] = 0;
		$clusterPredictorCount[2] = 0;
		$clusterPredictorCount[3] = 0;
		$clusterPredictorCount[4] = 0;
	// Extend the TCPDF class to create custom Header and Footer
	
	// create new PDF document
	$pdf = new SaviioTCPDF($PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false, 16.8, 55.5);
	
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor(ACC_NAME);
	$pdf->SetTitle(ACC_NAME . ' Report');
	$pdf->SetSubject('Analytics Summary Report');
	$pdf->SetKeywords(ACC_NAME . ',analytics, summary, report');
	
	// set default header data
	//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
	
	// set header and footer fonts
	$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
	
	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	
	//set margins
	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
	
	//set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	
	//set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
	
	$userobj = new Admin($_SESSION['uid']);
	$MAPobj = new MAP();
	$fids = $MAPobj->getfids();
	$fgs = $MAPobj->getFactorGroups();
	$cids = $MAPobj->getClusters();
	$stquestions = $_SESSION['stquestions'];
	
	$access = $userobj->getLevelAccess('tmap');
	if($access['EDIT'] == 'MA') {
		$compid = $userobj->headid;
	} else {
		$compid = $userobj->companyid;
	}
	
	if(!isset($_SESSION['fdescs'])) {
		$fdescs = $MAPobj->getFactDescs();
		$_SESSION['fdescs'] = $fdescs;
	} else {
		$fdescs = $_SESSION['fdescs'];
	}
	//$rankid = $_GET['rankID'];
	if(!isset($_SESSION['rankID'])) {
		$rankid = 0;
	} else {
		$rankid = $_SESSION['rankID'];
	}
	$_SESSION['rightname'] = $arrFactorScores[$rankid]['rightname'];
	$_SESSION['leftname'] = $arrFactorScores[$rankid]['leftname'];
	$_SESSION['rightemail'] = $arrFactorScores[$rankid]['rightemail'];
	$_SESSION['leftemail'] = $arrFactorScores[$rankid]['leftemail'];
	$_SESSION['rightissuedata'] = $arrFactorScores[$rankid]['rightissuedata'];
	$_SESSION['leftissuedata'] = $arrFactorScores[$rankid]['leftissuedata'];
	
	
	$currentUsername = $_SESSION['cuFname'] . ' ' . $_SESSION['cuLname'];
	
	$grey_rbg = $dispPDFObj->getGrey(true);
	
		
	$excel = $dispPDFObj->getExcellentColour();
	$good = $dispPDFObj->getGoodColour();
	$average = $dispPDFObj->getAverageColour();
	$poor = $dispPDFObj->getPoorColour();
	$clus_1 = $dispPDFObj->getCluster1Colour();
	$clus_2 = $dispPDFObj->getCluster2Colour();
	$clus_3 = $dispPDFObj->getCluster3Colour();
	$clus_4 = $dispPDFObj->getCluster4Colour();
	$top10bg = $dispPDFObj->getTop10BackgroundColour();

  // ---------------------------------------------------------
	
	// set font
	if($_SESSION['locale'] == 'ar_SA') {
		$pdf->SetFont($fontn, '', 18);
	} else {
		$pdf->SetFont($fontn, '', 16);
	}
	
	// add a page
	$pdf->AddPage();
	$timenow = time();
	$headerpagehtmlArray = $dispPDFObj->getFirstPage($PDF_PAGE_ORIENTATION, $currentUsername, $_SESSION['username'], $_SESSION['leftTypeDisp'], $_SESSION['leftname'], $_SESSION['leftemail'], $_SESSION['rightTypeDisp'], $_SESSION['rightname'], $_SESSION['rightemail'], $_SESSION['leftissuedata'], $_SESSION['rightissuedata']);
	
	// encode HTML
	// output the HTML content
	$pdf->writeHTMLCell(0, 0, '', 30, $headerpagehtmlArray['main'], 0, 1, 0, true, 'center');
	$pdf->SetFont($fontn, '', 10);
	$pdf->writeHTMLCell(0, 0, '', 130, $headerpagehtmlArray['mapdata'], 0, 1, 0, true, 'center');
	
		// add a page
		$pdf->AddPage();
		
		// create some HTML content
		$namereplace = '<em>' . ACC_NAME . '</em>';
		$searchfor = array('%s', '%emS', '%strS', '%emE', '%strE');
		$replacewith = array('<em>' . ACC_NAME. '</em>', '<em>', '<strong>', '</em>', '</strong>');
		$htmlcontent = str_replace($searchfor, $replacewith, '<h1>' . _("Introduction to the %s reports") . '</h1>
		<strong>' . _("Purpose") . '</strong>:<br />
		' . _("Our online survey tools dramatically increase the probability of people succeeding in their role at work.") . "<ul>
		<li>" . _("We help improve employee engagement.") . "</li>
		<li>" . _("We help cut the cost of recruitment.") . "</li>
		<li>" . _("We help organisations make more profit.") . "</li>
		</ul>" .
		
		 "<p><strong>" ._("How?") . "</strong><br />" .
		_("By providing fresh insight to people at work that what really matters is how to create a working climate that plays to each persons' strengths.") . "</p>" .
		
		_("So, how is this approach different from other profiling tools?") . "<br />" .
		_("Most psychometric profiling tools assess the individual and compare the person with a normed benchmark - usually comprising of a collection of anonymous people who have completed the same survey in the past. Somewhere else.") . "<br />" .
		_("Not Saviio.") . "<br />" .
		_("We compare the individual with their manager, their team and their colleagues. Oh - and with people who are rather good in a similar role too. In the here and now - not the there and then.") . "<br />" .
		_("What matters most is providing insight to both manager AND employee so that they can improve mutual understanding around each other's motivations and preferences.") . '<br />
		<br />
		<strong>' . _("The Survey") . '</strong>:<br />
		' . _("The heart of the tool comprises 60 bipolar statements that inform 12 bipolar preference sets - organised into 4 main factors. When completed by the respondent, a MAP (Motivations and Preferences) is created. The MAP may be viewed in several ways:") . '
		<ol start="1" type="1">
		<li>' . _("By itself - without comparing against other individuals, teams or job profiles.") . '</li>
		<li>' . _("Comparison against a previous MAP from the same individual") . '</li>
		<li>' . _("Comparison against another individual, team or job profile.") . '</li>
		</ol>
		<p><strong>' . _("Benchmark Reporting") . '</strong>:<br />
		' . _("A benchmark can be established by generating responses from within an organisation and as such does not depend on normative data to generate comparisons. MAPs may be combined to produce benchmarks against which teams, talent, managers and individuals can all be compared for 'match' of 'fit'. When used for recruitment and selection, an individuals' MAP may be compared with a talentMAP (a job profile) that has been created for a specific job vacancy.") . '<br />
		<br />
		<strong>' . _("Theory:") . '</strong><br />
		' . _("The underlying theory supporting %s is based on NLP (Neuro Linguistic Programming) Meta Programs. Meta Programs are filters that determine how you perceive the world around you, and they have a major influence on how you communicate with others and the behaviours you manifest. <em>Meta</em> means over, beyond, above, or on a different level - i.e. operating at an unconscious level. Meta Programs are deep-rooted mental programs, which automatically filter our experience and guide and direct our thought processes, resulting in significant differences in behaviour from person to person. They define typical patterns in the strategies or thinking styles of an individual, group, company or culture.") . '</p>
		<p><strong>' . _("Report Summary") . '</strong><br />
		' . _("This report is presented as an independent assessment using survey results of the individual respondent named in this report. It has been prepared in good faith, based on the information provided by the individual through psychometric assessment. The survey responses provided by the respondent have been used to produce either an <em>individual</em> MAP or they have been compared with <em>another</em> MAP as a benchmark (e.g. a teamMAP or a talent MAP).") . '</p>
		<p><strong>' . _("To the Survey respondent:") . '</strong><br />
		' . _("The accuracy of this report will reflect the frankness of your responses as well as, to some extent, your self-awareness. You may receive personal feedback of the results contained in this report. Whoever gives this feedback to you is trained to interpret %s surveys and to use the feedback session to validate your profile. The %s advisor should also be able to answer any questions and provide more detailed interpretation and guidance on what the results mean to you.") . '</p>
		<p>' . _("The content of this report is based on the information that was supplied by you on your preferred ways of thinking, understanding information and decision-making when you completed %s. It reflects your preferences in the context stated when you completed the questionnaire - usually this is a work context.") . '</p>
		<p>' . _("The results set out in this report have a usefulness of between six to nine months. In most cases, patterns of individual preferences change only slowly over time, but in times of rapid change in your personal or working life your %s profile may develop more quickly.") . '</p>
		' . _("%s is only part of the overall assessment process alongside other essential elements:") . '
		<ol>
		<li>' . _("Face to face meeting and dialogue.") . '</li>
		<li>' . _("CV with accompanying evidence of achievement and reference sources provided.") . '</li>
		<li>' . _("Competency assessment (in relation to the role).") . '</li>
		<li>' . _("%s - Motivations and Preferences of the individual in <em>relation</em> to the role, the team and the manager.") . '</li>
		</ol>
		<p>' . _("Remember that the results set out here reflect preferences rather than abilities. %s will help you to understand <em>how</em> you prefer to operate - for example when making decisions. It will <strong><em>not</em></strong> assess your <em>ability</em> (for example, how <strong><em>good</em></strong> you are at making decisions).") . '</p>
		<p><strong>' . _("Liability") . '</strong><br />
		' . _("As motivations and preferences in an individual can change, the accuracy of the information contained in this report should be considered to be up to approximately nine months, depending upon the respondent's work role and personal circumstances. No liability is accepted for outcomes resulting from the use of this information contained within the report.") . '</p>
		');
		
		// output the HTML content
		echo $htmlcontent;
		
		//new page here?
		$pdf->AddPage();
		$htmlcontent = str_replace($searchfor, $replacewith, '<h1>' . _("What is %s?") . '</h1>
		<p><strong>' . _("%s - Motivation and Preferences that predict success") . '</strong><br />
		' . _("%s improves your individual, team and organisational performance by providing continuous insight into the unique combination of human factors that predict success.") . '<br />
		<br />
		<strong><em>%s MAPs</em></strong><br />
		' . _("%s MAPs (Motivation And Preferences) is an online \"Software as a Service\" (SaaS) that enables organisations to better understand how people are uniquely motivated in four major areas:") . '<br />
		  <ol>
		  	<li>' . _("Relationships") . '</li>
			<li>' . _("Thinking & Planning") . '</li>
			<li>' . _("Making Decisions") . '</li>
			<li>' . _("Getting Things Done") . '</li>
		  </ol>
		  ' . _("This insight allows individuals, managers and their teams to work more productively and succeed in their roles.") . '</p>
		<strong>' . _("How does %s work?") . '</strong><br />'. _("People make unconscious and conscious judgments about the people they meet during the assessment and selection process.") . '<br />');
		
		// output the HTML content
		echo $htmlcontent;
		
		//to indent had to change it to this.. also add different colour to block.. only way I think for now!
		$cellcontent = sprintf('<span color="#45525f">' . _("%s helps individuals to positively influence these judgments using predictive data to support 'gut feel'") . '</span><br />',$namereplace);
		echo $cellcontent;
		
		//question / answer 2
		$htmlcontent = _("People want to feel that they will be doing a job that matters to them and where they will have 'significance' - it matters to others too.") . '<br />';
		echo $htmlcontent;
		
		$cellcontent = sprintf('<span color="#45525f">' . _("%s helps managers to understand the motivational preferences of team members and respond to meet those needs - people that feel understood feel valued and are more likely to stay. People generally leave managers - not organisations.") . '</span><br />',$namereplace);
		echo $cellcontent;
		
		//question / answer 3
		$htmlcontent = _("People want to be part of something and feel that they \"fit in\" and belong.") . '<br />';
		echo $htmlcontent;
		
		$cellcontent = sprintf('<span color="#45525f">' . _("%s helps inform the dialogue between manager, coach and individual in ways that help people feel that they belong to something more than just a means to provide income.") . '</span><br />',$namereplace);
		echo $cellcontent;
		
		//question / answer 4
		$htmlcontent = _("Once people feel that they 'fit in' and feel part of something that matters to them, they gain more intrinsic motivation from their job, feel more engaged, and are less open to changing their situation.") . '<br />';
		echo $htmlcontent;
		
		$cellcontent = sprintf('<span color="#45525f">' . _("%s helps managers to secure and develop people within their organisation.") . '</span><br />',$namereplace);
		echo $cellcontent;
		
		//new page here?
		
		
		$htmlcontent = str_replace($searchfor, $replacewith, '<h1>' . _("Understanding This Report - <em>Factors & Preferences</em>") . '</h1>
		<p>' . _("%s displays the results of the survey you have completed in 12 sets of preferences (opposites) organised into four main factors or groups.") . '</p>
		  ' . $dispPDFObj->getRelationshipsTable());
			
		echo $htmlcontent;
		if($PDF_PAGE_ORIENTATION == 'L') {
			$pdf->AddPage();
		}
		
		echo $dispPDFObj->getThinkingAndPlanningTable();
		$pdf->AddPage();
		
		echo $dispPDFObj->getmakingDecisionsTable();
		if($PDF_PAGE_ORIENTATION == 'L') {
			$pdf->AddPage();
		}
		
	echo $dispPDFObj->getgettingThingsDoneTable();
		$pdf->AddPage();
		
		$interpretingHTML = '<h1>' . _("Understanding This Report - <em>Interpreting The Symbols</em>") . '</h1>
		<p><strong>' . _("Behavioural Range Line") . '</strong><br/>
		' . _("According to NLP, behavioural flexibility is the 'ability to vary one's own behaviour in order to elicit, or secure, a response from another person'.") . ' ' . _("Behavioural flexibility can refer to the development of an entire range of responses to any given stimulus as opposed to having habitual, and therefore limiting responses, which would inhibit performance potential.") . '</p>
		<p>' . _("The behavioural flexibility bar that appears in the Saviio report represents how flexible the behaviour of the individual is within the twelve preference pairs.") . ' ' . _("If the line is short then it suggests that the behavioural flexibility within that preference is very small (less flexible), and if the line is long then the behavioural flexibility of that individual within that preference is large (more flexible).") . '</p>
		<p>' . _("In this example the line is very short, and sits on the scale next to 'perfectionist'.") . ' ' . _("This suggests that this particular person will always be a perfectionist no matter what their mood or environment (i.e. home or work).") . ' ' . _("This is because they have answered always towards perfectionist in the five statements that make up that preference pair.") . '</p>
		<p>' . _("This same person has a broader behavioural range when it comes to having concern for task or concern for people.") . '</p>
		<div align="center"><img height="75" src="' . FULL_PATH . ACCOUNT_PATH . '_images/_pdf/GettingThingsDone.png" width="550"><br />
		<img height="75" src="' . FULL_PATH . ACCOUNT_PATH . '_images/_pdf/Relationships.png" width="550"><br />&nbsp;</div>
		<p>' . _("This means that their behaviour in this area will be more flexible and may change depending on what mood they are in or what situation they're in.") . ' ' . _("For example on one day they may be concerned about their task, and then another day or another situation they may show more concern for people, however, as the figure is more on the side of concern for people, they are more likely to show concern for people.") . '</p>
		<p>' . _("If a survey takers output report shows that they have a very narrow behavioural range that sits in the middle of a preference pair, for example on 'perfectionist/pragmatist', this suggests that their behaviour will always be somewhere in the middle, i.e., they will never be a total perfectionist and will never be a complete pragmatist but will always behave somewhere between the two."). '</p>';
		
		echo $interpretingHTML;
		$pdf->AddPage();
		
		
		$htmlcontent = '<h1>' . _("Left & Right Brain Attributes by Factor") . '</h1>';
		$htmlcontent .= '<table width="100%" border="0" cellspacing="15" cellpadding="2">
		  <tr>
		    <td width="20%" rowspan="2" bgcolor="' . $dispPDFObj->getRelationshipsClusterColour() . '" align="center"><br />&nbsp;<h3 color="' . $dispPDFObj->getWhite() . '">' . _("Relationships") . '</h3></td>
		    <td width="40%" bgcolor="' . $dispPDFObj->getRelationshipsClusterColour() . '" align="center"><strong color="' . $dispPDFObj->getWhite() . '">' . _("Left Brain Inventory") . '</strong></td>
		    <td width="40%" bgcolor="' . $dispPDFObj->getRelationshipsClusterColour() . '" align="center"><strong color="' . $dispPDFObj->getWhite() . '">' . _("Right Brain Inventory") . '</strong></td>
		  </tr>
		  <tr>
		    
		    <td bgcolor="' . $dispPDFObj->getClusterTableDescriptionColour() . '">
	        <ul>
	            <li>' . _("Verbal, focusing on words, symbols, numbers") . '</li>
	            <li>' . _("Likely to follow rules without questioning them") . ' </li>
	            <li>' . _("Spelling and mathematical formula easily memorised") . ' </li>
	            <li>' . _("Enjoy observing") . ' </li>
	            <li>' . _("Listen to what is being said") . ' </li>
	            <li>' . _("Rarely use gestures when talking") . ' </li>
	      	</ul>
		    </td>
		    <td bgcolor="' . $dispPDFObj->getClusterTableDescriptionColour() . '">
		    <ul>
		        <li>' . _("Visual, focusing on images, patterns") . '</li>
		        <li>' . _("Like to know why you're doing something or why rules exist (reasons)") . ' </li>
		        <li>' . _("May have trouble with spelling and finding words to express yourself") . ' </li>
		        <li>' . _("Enjoy touching and feeling actual objects (sensory input)") . ' </li>
		        <li>' . _("Listen to how something is being said") . ' </li>
		        <li>' . _("Talk with your hands") . ' </li>
		    </ul>
		    </td>
		  </tr>
		  <tr>
		    <td width="20%" rowspan="2" bgcolor="' . $dispPDFObj->getThinkingAndPlanningClusterColour() . '" align="center"><br />&nbsp;<h3 color="' . $dispPDFObj->getWhite() . '">' . _("Thinking & Planning") . '</h3></td>
		    <td width="40%" bgcolor="' . $dispPDFObj->getThinkingAndPlanningClusterColour() . '" align="center"><strong color="' . $dispPDFObj->getWhite() . '">' . _("Left Brain Inventory") . '</strong></td>
		    <td width="40%" bgcolor="' . $dispPDFObj->getThinkingAndPlanningClusterColour() . '" align="center"><strong color="' . $dispPDFObj->getWhite() . '">' . _("Right Brain Inventory") . '</strong></td>
		  </tr>
		  <tr>
		    <td bgcolor="' . $dispPDFObj->getClusterTableDescriptionColour() . '">
		      <ul>
		          <li>' . _("Words used to remember things, remember names rather than faces") . ' </li>
		          <li>' . _("Make logical deductions from information") . ' </li>
		          <li>' . _("Like making lists and planning") . ' </li>
		          <li>' . _("Likely to follow rules without questioning them") . ' </li>
		          <li>' . _("Good at keeping track of time") . ' </li>
		          <li>' . _("Enjoy observing") . ' </li>
		          <li>' . _("Plan ahead") . ' </li>
		      </ul>
		    </td>
		    <td bgcolor="' . $dispPDFObj->getClusterTableDescriptionColour() . '">
		        <ul>
		            <li>' . _("\"Mind photos\" used to remember things, writing things down or illustrating them helps you remember") . ' </li>
		            <li>' . _("Make lateral connections from information") . '</li>
		            <li>' . _("Free association") . ' </li>
		            <li>' . _("Like to know why you're doing something or why rules exist (reasons)") . ' </li>
		            <li>' . _("No sense of time") . ' </li>
		            <li>' . _("Enjoy touching and feeling actual objects (sensory input)") . ' </li>
		            <li>' . _("Trouble prioritising, so often late, impulsive") . ' </li>
		        </ul>
		    </td>
		  </tr>
		  </table>';
			// output the HTML content
			echo $htmlcontent;
			$pdf->AddPage();
			
			$htmlcontent = '<h1>' . _("Left & Right Brain Attributes by Factor continued..") . '</h1>';
			$htmlcontent .= '<table width="100%" border="0" cellspacing="15" cellpadding="2">
		  <tr>
		    <td width="20%" rowspan="2" bgcolor="' . $dispPDFObj->getMakingDecisionsClusterColour() . '" align="center"><br />&nbsp;<h3 color="' . $dispPDFObj->getWhite() . '">' . _("Making Decisions") . '</h3></td>
		    <td width="40%" bgcolor="' . $dispPDFObj->getMakingDecisionsClusterColour() . '" align="center"><strong color="' . $dispPDFObj->getWhite() . '">' . _("Left Brain Inventory") . '</strong></td>
		    <td width="40%" bgcolor="' . $dispPDFObj->getMakingDecisionsClusterColour() . '" align="center"><strong color="' . $dispPDFObj->getWhite() . '">' . _("Right Brain Inventory") . '</strong></td>
		  </tr>
		  <tr>
		    <td bgcolor="' . $dispPDFObj->getClusterTableDescriptionColour() . '">
		     <ul>
		      <li>' . _("Analytical, led by logic") . '</li>
		      <li>' . _("Process ideas sequentially, step by step") . '</li>
		      <li>' . _("Words used to remember things, remember names rather than faces") . ' </li>
		      <li>' . _("Highly organised") . ' </li>
		      <li>' . _("Good at keeping track of time") . ' </li>
		      <li>' . _("Enjoy observing") . ' </li>
		     </ul>
		    </td>
		    <td bgcolor="' . $dispPDFObj->getClusterTableDescriptionColour() . '">
		    <ul>
		        <li>' . _("Intuitive, led by feelings") . ' </li>
		        <li>' . _("Process ideas simultaneously") . ' </li>
		        <li>' . _("\"Mind photos\" used to remember things, writing things down or illustrating them helps you remember") . ' </li>
		        <li>' . _("Organization tends to be lacking") . ' </li>
		        <li>' . _("No sense of time") . ' </li>
		        <li>' . _("Enjoy touching and feeling actual objects (sensory input)") . ' </li>
		    </ul>
		    </td>
		  </tr>
		   <tr>
		    <td width="20%" rowspan="2" bgcolor="' . $dispPDFObj->getGettingThingsDoneClusterColour() . '" align="center"><br />&nbsp;<h3 color="' . $dispPDFObj->getWhite() . '">' . _("Getting Things Done") . '</h3></td>
		    <td width="40%" bgcolor="' . $dispPDFObj->getGettingThingsDoneClusterColour() . '" align="center"><strong color="' . $dispPDFObj->getWhite() . '">' . _("Left Brain Inventory") . '</strong></td>
		    <td width="40%" bgcolor="' . $dispPDFObj->getGettingThingsDoneClusterColour() . '" align="center"><strong color="' . $dispPDFObj->getWhite() . '">' . _("Right Brain Inventory") . '</strong></td>
		  </tr>
		  <tr>
		    <td bgcolor="' . $dispPDFObj->getClusterTableDescriptionColour() . '">
		    <ul>
		      <li>' . _("Process ideas sequentially, step by step") . '</li>
		      <li>' . _("Work up to the whole step by step, focusing on details, information organised") . ' </li>
		      <li>' . _("Highly organised") . ' </li>
		      <li>' . _("Good at keeping track of time") . ' </li>
		      <li>' . _("Enjoy observing") . ' </li>
		      <li>' . _("Likely read an instruction manual before trying") . ' </li>
		    </ul>
		    </td>
		    <td bgcolor="' . $dispPDFObj->getClusterTableDescriptionColour() . '">
		    <ul>
		        <li>' . _("Process ideas simultaneously") . ' </li>
		        <li>' . _("\"Mind photos\" used to remember things, writing things down or illustrating them helps you remember") . ' </li>
		        <li>' . _("See the whole first, then the details") . ' </li>
		        <li>' . _("Organization tends to be lacking") . ' </li>
		        <li>' . _("No sense of time") . ' </li>
		        <li>' . _("Enjoy touching and feeling actual objects (sensory input)") . ' </li>
		        <li>' . _("Unlikely to read instruction manual before trying") . ' </li>
		    </ul>
		    </td>
		  </tr>
		</table>';
		
	echo $htmlcontent;
		
	
	/**********************************************************************
	*																																			*
	*																																			*
	*																	LEGENDS															*
	*																																			*
	*																																			*
	**********************************************************************/
	
	$pdf->AddPage();
	$legendHTML = ' <h1>' . _("Legend and Icon Descriptions") . '</h1>';
  $legendHTML .= '<table width="100%" border="0" cellspacing="0" cellpadding="6">
	  <tr>
	    <td colspan="5"><strong>Factors</strong></td>
	  </tr>
	  <tr>
	    <td width="4%"><img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_map/rel_icon.png" /></td><td width="21%">&nbsp;&nbsp;' . _("Relationships") . '</td>
	    <td width="4%"><img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_map/tap_icon.png" /></td><td width="21%">&nbsp;&nbsp;' . _("Thinking & Planning") . '</td>
	    <td width="4%"><img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_map/mak_icon.png" /></td><td width="21%">&nbsp;&nbsp;' . _("Making Decisions") . '</td>
	    <td width="4%"><img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_map/gtd_icon.png" /></td><td width="21%">&nbsp;&nbsp;' . _("Getting Things Done") . '</td>
	  </tr>
	  <tr><td colspan="8">' . _("The factors are sometimes represented by three letter acronyms (such as in the top 10 matched statements) these are:") . '</td></tr>
	  <tr>
	    <td width="4%">&nbsp;</td><td width="21%">&nbsp;&nbsp;' . _("REL") . '</td>
	    <td width="4%">&nbsp;</td><td width="21%">&nbsp;&nbsp;' . _("TAP") . '</td>
	    <td width="4%">&nbsp;</td><td width="21%">&nbsp;&nbsp;' . _("DEC") . '</td>
	    <td width="4%">&nbsp;</td><td width="21%">&nbsp;&nbsp;' . _("GTD") . '</td>
	  </tr>
	  <tr><td colspan="8">' . _("An asterisk (*) next to one of the above acronyms denotes that inspite of any high quality match the results from the two MAPs are on opposite sides of the scale") . '</td></tr>
	</table>
	<br />
	<table width="100%" border="0" cellspacing="0" cellpadding="6">
	  <tr>
	    <td colspan="4"><strong>MAP Predictor Strength</strong></td>
	  </tr>
	  <tr>
	    <td colspan="4">' . _("The MAP predictor strength shows how predicitive each statement in the team/talentMAP is based upon, the standard deviation of each of the questions between the participants in that team/talentMAP.") . '</td>
	  </tr>
	  <tr>
	    <td width="25%"><img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_map/excellent_bullet.png" />&nbsp;&nbsp;' . _("Excellent") . '</td>
	    <td width="25%"><img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_map/good_bullet.png" />&nbsp;&nbsp;' . _("Good") . '</td>
	    <td width="25%"><img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_map/poor_bullet.png" />&nbsp;&nbsp;' . _("Average") . '</td>
	    <td width="25%"><img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_map/bad_bullet.png" />&nbsp;&nbsp;' . _("Poor") . '</td>
	  </tr>
	</table>
	<br />
	<table width="100%" border="0" cellspacing="0" cellpadding="6">
	  <tr>
	    <td><strong>'._("Behavioural Flexibility") . '</strong></td>
	  </tr>
	  <tr>
	    <td>' . _("This bar shows the behavioural range for both of the MAPs. The top bar shows the 'master' or left hand side MAP and the bottom bar shows the behavioural range for the MAP on the right hand side.") . '</td>
	  </tr>
	  <tr>
	    <td><img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_map/behaviour_bar.png" width="61" height="7" alt="behaviour bar" /></td>
	  </tr>
	</table>
	<br />
	<table width="100%" border="0" cellspacing="0" cellpadding="6">
	  <tr>
	    <td colspan="4"><strong>Match Colour Key</strong></td>
	  </tr>
	  <tr>
	    <td colspan="4">' . _("The colours of the icons and percentages are based upon how well that preference or statement matches with the other MAP or talent/teamMAP. A low percentage (i.e. red) is not considered bad, it just means that the match is far apart.") . '</td>
	  </tr>
	  <tr>
	    <td width="25%"><img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_map/excellent_circle.png" />&nbsp;&nbsp;90-100%</td>
	    <td width="25%"><img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_map/good_circle.png" />&nbsp;&nbsp;70-90%</td>
	    <td width="25%"><img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_map/average_circle.png" />&nbsp;&nbsp;50-70%</td>
	    <td width="25%"><img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_map/poor_circle.png" />&nbsp;&nbsp;0-50%</td>
	  </tr>
	</table>';
  echo $legendHTML;