<?php 
/**
* @package  for DB connection
* @version  $id
* @author   gede.suartana
* @license  GNU GENERAL PUBLIC LICENSE
* @access   Public + Private
* @php      5 or higher
*
* */
include_once 'class.phpmailer.php';

class DbaseMySQL {

    // protectec member
    /*
     * Connection information
     */
    protected $mySQLUser;              // MySQL Username
    protected $mySQLPasswd;             // MySQL Password
    protected $mySQLHost;            // MySQL Host IP-Address or Domainname
    protected $mySQLPort;                 // MySQL Port option
    protected $mySQLName;            // MySQL Databasename    
    
    public $mySQLSelectDB;                         // MySQL Databasename
    public $mySQLConnection;                       // MySQL Databasename


    public function __construct(){
	    if(APPLICATION_ENV == "development") {
	        $this->mySQLUser    = "savmaps";              // MySQL Username
	        $this->mySQLPasswd  = 'sp1derm0nkey';         // MySQL Password
	        $this->mySQLHost    = "192.168.33.11";        // MySQL Host IP-Address or Domainname
	        $this->mySQLPort    = "3306";                 // MySQL Port option
	        $this->mySQLName    = "savmaps";			  // MySQL Databasename    
	    } else {
	    	$this->mySQLUser    = "saviiomaps";           // MySQL Username
	        $this->mySQLPasswd  = 'Ch7g$#%G23';           // MySQL Password
	        $this->mySQLHost    = "localhost";            // MySQL Host IP-Address or Domainname
	        $this->mySQLPort    = "3306";                 // MySQL Port option
	        $this->mySQLName    = "savmaps";			  // MySQL Databasename    
	    }
        return $this->mySQLConnect();
    }
    
    public function EscapeMe($input) {
			$safe_input = mysql_real_escape_string($input);
			return $safe_input;
		}

    public function mySQLConnect(){
        // set connection
        $this->mySQLConnection = mysql_connect($this->mySQLHost.":".$this->mySQLPort, $this->mySQLUser, $this->mySQLPasswd) or die("{connect} Database Error: ".mysql_errno()." : ".mysql_error());

        if($this->mySQLConnection){
            //select from databasename
            $this->mySQLSelectDB = mysql_select_db($this->mySQLName, $this->mySQLConnection) or die("{select_db} Database Error: ".mysql_errno()." : ".mysql_error());
            return true;
        }else{
            return false;
        }
    }
    public function mysqlclose()
    {
        // close connection
        $this->mySQLConnection = mysql_close();

        if($this->mySQLConnection){
            return true;
        }else {
            return false;
        }

    }
}
?>