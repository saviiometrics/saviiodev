﻿

(function($) {
	$.fn.validationEngineLanguage = function() {};
	$.validationEngineLanguage = {
		newLang: function() {
			$.validationEngineLanguage.allRules = 	{"required":{    			// Add your regex rules here, you can take telephone as an example
						"regex":"none",
						"alertText":"* هذا الحقل مطلوب",
						"alertTextCheckboxMultiple":"* يجب أن تختار حقل من القائمة",
						"alertTextCheckboxe":"* يجب أن تختار"},
					"length":{
						"regex":"none",
						"alertText":"*بين ",
						"alertText2":" و ",
						"alertText3": " المحارف المسموحة"},
					"maxCheckbox":{
						"regex":"none",
						"alertText":"* لقد اخترت أكثر من المسموح"},	
					"minCheckbox":{
						"regex":"none",
						"alertText":"* يجب أن تختار ",
						"alertText2":" على الأقل"},	
					"equals":{
						"regex":"none",
						"alertText":"* الحقول غير مطابقة"},		
					"telephone":{
						"regex":/^[0-9\-\(\)\ ]+$/,
						"alertText":"* صيغة رقم هاتف خاطئة"},	
					"email":{
						"regex":/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.(?:[A-Z]{2}|COM|ORG|NET|EDU|GOV|MIL|BIZ|INFO|MOBI|NAME|AERO|ASIA|JOBS|MUSEUM)$/i,
						"alertText":"* صيغة بريد إلكتروني خاطئة"},	
					"date":{
                         "regex":/^[0-9]{4}\-\[0-9]{1,2}\-\[0-9]{1,2}$/,
                         "alertText":"* صيغة تاريخ خاطئة يجب أن تكون YYYY-MM-DD"},
					"onlyNumber":{
						"regex":/^[0-9\ ]+$/,
						"alertText":"* أرقام فقط"},	
					"noSpecialCaracters":{
						"regex":/^[0-9a-zA-Z]+$/,
						"alertText":"* الرموز غير مسموحة"},	
					"ajaxUser":{
						"file":"validateUser.php",
						"extraData":"",
						"alertTextOk":"* هذا الإسم غير محجوز",	
						"alertTextLoad":"* برجى الإنتظار",
						"alertText":"* هذا الإسم محجوز"},	
					"ajaxName":{
						"file":"validateUser.php",
						"alertText":"* هذا الإسم محجوز",
						"alertTextOk":"* هذا الإسم غير محجوز",	
						"alertTextLoad":"* برجى الإنتظار"},		
					"onlyLetter":{
						"regex":/^[a-zA-Z\ \']+$/,
						"alertText":"* أحرف فقط"}
            };
            
        }
    };
    $.validationEngineLanguage.newLang();
})(jQuery);