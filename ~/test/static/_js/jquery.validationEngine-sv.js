

(function($) {
	$.fn.validationEngineLanguage = function() {};
	$.validationEngineLanguage = {
		newLang: function() {
			$.validationEngineLanguage.allRules = 	{"required":{    			// Lägg till dina regex-regler här, du kan använda telefonnr som exempel
						"regex":"inga",
						"alertText":"* Det här fältet är obligatoriskt",
						"alertTextCheckboxMultiple":"* Välj ett alternativ",
						"alertTextCheckboxe":"* Kryssrutan måste markeras"},
					"length":{
						"regex":"inga",
						"alertText":"*Mellan ",
						"alertText2":" och ",
						"alertText3": " tecken tillåtna"},
					"maxCheckbox":{
						"regex":"inga",
						"alertText":"* Antal tillåtna markeringar har överskridits"},	
					"minCheckbox":{
						"regex":"inga",
						"alertText":"* Välj ",
						"alertText2":" alternativ"},	
					"equals":{
						"regex":"inga",
						"alertText":"* Dina lösenord matchar inte varandra"},		
					"telephone":{
						"regex":/^[0-9\-\(\)\ ]+$/,
						"alertText":"* Ogiltigt telefonnummer"},	
					"email":{
						"regex":/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.(?:[A-Z]{2}|COM|ORG|NET|EDU|GOV|MIL|BIZ|INFO|MOBI|NAME|AERO|ASIA|JOBS|MUSEUM)$/i,
						//regex":/^[a-zA-Z0-9_\.-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9\.]{2,5}$/,
						"alertText":"* Ogiltig e-postadress"},	
					"date":{
                         "regex":/^[0-9]{4}\-\[0-9]{1,2}\-\[0-9]{1,2}$/,
                         "alertText":"* Ogiltigt datum, måste anges i formatet ÅÅÅÅ-MM-DD"},
					"onlyNumber":{
						"regex":/^[0-9\ ]+$/,
						"alertText":"* Endast siffor"},	
					"noSpecialCharacters":{
						"regex":/^[0-9a-zA-Z]+$/,
						"alertText":"* Inga specialtecken tillåtna"},	
					"ajaxUser":{
						"file":"validateUser.php",
						"extraData":"",
						"alertTextOk":"* Den här användaren är tillgänglig",	
						"alertTextLoad":"* Laddar, vänta",
						"alertText":"* Den här användaren är redan upptagen"},	
					"ajaxName":{
						"file":"validateUser.php",
						"alertText":"* Det här namnet är redan upptaget",
						"alertTextOk":"* Det här namnet är tillgängligt",	
						"alertTextLoad":"* Laddar, vänta"},		
					"onlyLetter":{
						"regex":/^[a-zA-Z\ \']+$/,
						"alertText":"* Endast bokstäver"}
            };
            
        }
    };
    $.validationEngineLanguage.newLang();
})(jQuery);


