<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Saviio</title>
<style type="text/css">
body,td,th {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #5F6060;
}
body {
	background-color: #FFF;
	margin-left: 0px;
	margin-top: 20px;
	margin-right: 0px;
	margin-bottom: 0px;
}
.mainTitle {
	color:#3499ad;
	font-size:36px;
	letter-spacing:0.4px;
	font-weight:normal;
	margin:0px;
	padding:0px;
}
.subTitle {
	color:#5f6060;
	font-size:24px;
	letter-spacing:0.4px;
	font-weight:normal;
}
.borderme {
	 border: 1px solid #cecece;
	 background-color:#ededed;
	 padding:8px 12px 18px 14px;
}

.mainimg {
	padding:6px 0px 0px 0px;	
}
.copytd {
	padding:0px 0px 0px 10px;	
}
.copytd img {
	padding:0px 12px 0px 0px;
}

</style>
</head>

<body>
<table width="800" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="147" align="center" valign="top" class="mainimg"><img src="http://static.saviio.com/_images/_email/saviio_left.jpg" width="102" height="138" alt="Saviio" longdesc="http://www.saviio.com" /></td>
    <td width="653" class="borderme"><span class="mainTitle">[EMAIL_HEADER]</span>
			[EMAIL_COPY]
		</td>
  </tr>
  <tr>
    <td height="66">&nbsp;</td>
    <td class="copytd"><img src="http://static.saviio.com/_images/_email/saviio_tiny.jpg" width="47" height="47" alt="copyright saviio" longdesc="http://www.saviio.com" align="left" />Copyright © Saviio Ltd. 2005-2011. All rights reserved.<br />
      5th Floor, 2 Wellington Place, Leeds, LS1 4AP<br />
    0113 366 2000 | Email: info@saviio.com | Twitter: @saviio</td>
  </tr>
</table>
</body>
</html>