	var oTable;

	$(function() {

		var currLocale;
		if($('#currLocale').val() == '') {
			currLocale = 'en_EN';
		} else {
			currLocale = $('#currLocale').val();
		}

		oTable = $('.tablesorter').dataTable({
			//"bProcessing": true,
			"iDisplayStart": 0,
			"aaSorting": [],
			"iDisplayLength": 25,
			"oLanguage": {
				"sUrl":  "dtblLocale/dataTables." + currLocale + ".txt"
			},
			"sPaginationType": "full_numbers",
			"sDom": '<"filter_container"fl><"top"ip>rt<"bottom"ip<"clear">',
			"bAutoWidth": false
		});

		$("#ctype").change(function() {
			if($(this).val() == 'sub') {
				$(".compheadlist").show();
			} else {
				$(".compheadlist").hide();
			};
		});

	 // select the overlay element - and "make it an overlay"
	  $('#addcompany, .editcompany').live("click", function(){
	  	if(this.id == "addcompany") {
				$("button.savecompany").html("Add Account").attr("id", "addCX");
	  		$("#companybox div > h2").html("Add a New Account");
	  	} else {
	  		$("button.savecompany").html("Save Details").attr("id", "editCX");
	  		$("#companybox div > h2").html("Edit Account");
	  	};
	  	compobj = $(this);
			var compapi = $("#companybox").overlay({
			    top: 'center',
			    mask: {
			        color: '#fff',
			        loadSpeed: 200,
			        opacity: 0.5
			    },
			    closeOnClick: false,
			    onBeforeLoad: function(event) {
			    	//reset values
			    	if(compobj.attr("id") == 'addcompany') {
			    		var editid = null;
			    		$(".ctyperow").show();
			    	} else {
			    		var editid = compobj.attr("id").substr(8);
			    		$("#editCompID").val(editid);
			    		//hide for edit
			    		$(".compheadlist").hide();
							$(".ctyperow").hide();
			    	};
			    	var xRTG = $("#xRTG").val();
			     	$("#companyN").val('');
	  				$("#companyD").val('');
	  				$("#CCountry").val('');
			    	if(editid != null) {
		    		$.get("AjaXCompany.php", { companyid: editid, getCompany: 'yes' },
						  function(data){
								var resultData = data.split('---');
								$("#companyN").val(jQuery.trim(resultData[0]));
					  		$("#companyD").val(jQuery.trim(resultData[1]));
					  		$("#CCountry").val(jQuery.trim(resultData[2]));
						  });
						};
			    },
			    onClose: function() {
			    	$("#companyform").validationEngine('hideAll');
			    	//$.validationEngine.closePrompt('.formError',true);
			    },
			    api: true
			});
			compapi.load();
	  });

	  $(".savecompany").click(function(){

	  	var valid = $("#companyform").validationEngine('validate');
	  	if(valid == true) {
		  	if(this.id == "addCX") {
		  		var Cname = $("#companyN").val();
		  		var Cdesc = $("#companyD").val();
		  		var CCid = $("#CCountry").val();
		  		var comptype = $("#ctype").val();
		  		var currAccid;
		  		var xRTG = $("#xRTG").val();
		  		if($("#godAccid").val() == '') {
		  			currAccid = $("#accid").val();
		  		} else {
		  			currAccid = $("#godAccid").val();
		  		}
		  		if(comptype == "head") {
		  			var CurrHeadid = "new";
		  		} else {
		  			//this is basically the value set if the person is a sub account admin. Because they can't add main accounts, it uses the headid of their current main account.
		  			if($("#xRTG").val() == 'Y') {
		  				var CurrHeadid = $("#compheadid").val();
		  			} else {
		  				var CurrHeadid = $("#headID").val();
		  			}
		  		};
					$.get("AjaXCompany.php", { headid:CurrHeadid, addCompany: 'yes', cname:Cname, cdesc:Cdesc, cid:CCid, accid:currAccid, type:comptype },
					  function(j){
					    if(j.error == false) {
								if(comptype == 'head') {
									var compclass = 'headquarters';
								} else {
									var compclass = 'childcompany';
								}
								//this adds data directly into the datatables DOM. The add class takes the current added row and adds the class to the 4th td along.
								var ai = oTable.fnAddData([
									Cname,
									'<img src="..' + $('#accpath').val() + '_images/_flags/' + CCid + '.gif" />',
									j.mainaccount,
									j.enterpriseAccount,
									j.comptype,
									j.createdby,
									j.adminString]);
									var n = oTable.fnSettings().aoData[ ai[0] ].nTr;
									$('td:eq(3)', n).addClass(compclass);
							}
						  $.jGrowl(j.message, {
								header:'<strong>System Message</strong>',
								closeTemplate:'<div class="jGrowlCloseMe">close (x)</div>',
								position:'bottom-right'
							});
						  $("#companybox").overlay().close();
						}, "json");
				} else if (this.id == "editCX") {
					var Cname = $("#companyN").val();
		  		var Cdesc = $("#companyD").val();
		  		var CCid = $("#CCountry").val();
		  		var editid = $("#editCompID").val();
		  		$.get("AjaXCompany.php", { companyid: editid, editCompany: 'yes', cname:Cname, cdesc:Cdesc, cid:CCid },
					  function(data){
							alert(data);
							$("#companybox").overlay().close();
					  });
				}
			}//end valid check
	  });

	  //set compid to global!
	  var compid = '';
	  // select the overlay element - and "make it an overlay"
	  $('.sharecompany').live("click", function(){
	  	shareobj = $(this);
			var shareapi = $("#sharecompanybox").overlay({
			    top: 30,
			    mask: {
			        color: '#fff',
			        loadSpeed: 200,
			        opacity: 0.5
			    },
			    fixed:false,
			    closeOnClick: false,
			    onBeforeLoad: function(event) {
			    	//reset values
			    	$("#shareAccBox").show();
			    	$("#shareuserlist").hide();
			    	compid = shareobj.attr("id").substr(9);
			  		//submit that shit!
				   	$.get("AjaXUser.php", { cid: compid, checkShareUsers: true },
						  function(j){
						  	var SUserL = '<div id="companyboxheader">Currently Shared With</div><br /><table border="0" cellpadding="0" cellspacing="0">';
						  	if(j.length == 0) {
						  		$("#editSHAREbox").html('Currently this account has not been shared with any users');
						  	} else {
							    $.each(j, function(uid, infoobj) {
							    	SUserL += '<tr><td width="600" height="17"><strong><div id="editname_' + uid + '">' + infoobj.firstname + ' ' + infoobj.surname + '</div></strong></td><td align="right" width="60"><div class="makerelative"><a href="javascript:void(0);" id="editshare_' + uid + '" class="editshare">Edit</a> | <a href="javascript:void(0);" id="deleteshare_' + uid + '" class="deleteshare">Delete</a></div></td></tr>';
							    });
							    SUserL += '</table>';
							    //add content and show/hide initial stuff
							    $("#editSHAREbox").html(SUserL);
							  	$("#editSHAREbox").show();
							  	$('.deleteshare').confirm({dialogShow:'fadeIn', wrapper:'<div class="confirmdialog"></span>', timeout:10000, msg:'Do you really want to unshare this account? &nbsp;', buttons: {ok:'Yes',cancel:'No', separator:' / '}});
							  }
						}, "json");
			    },
			    onLoad: function() {
    				$("#submitRightsLink").hide();
				  	$("#updateRightsLink").hide();
				  	$("#showRightsLink").show();
				    $("#shareAcc").multiselect({
				    	dividerLocation: 0.5,
							animated: 'fast',
							remoteUrl: "MSajaxUser.php",
							remoteParams: { maptype: 'MAP' }
						});
						$("#shareAcc").multiselect('selectNone');
			    },
			    onClose: function() {
			    	$("#companyform").validationEngine('hideAll');
			    },
			    api: true
			});
			shareapi.load();
	  });


	 	//this one is if you can't see the account at all. The function above for .requestshare is for when you can see the account but don't have access to any of the MAPs etc or its read only!
	  $('.reqpull').live("click", function() {
	  	reqpullobj = $(this);
			var reqpullapi = $("#requestpullbox").overlay({
			    top: 'center',
			    mask: {
			        color: '#fff',
			        loadSpeed: 200,
			        opacity: 0.5
			    },
			    closeOnClick: false,
			    onBeforeLoad: function(event) {
			    	//compid = reqobj.attr("id").substr(9);
			  		//$("#reqname").html(reqobj.closest('tr').find('td').html());
			    },
			    onLoad: function() {
			    	$("#pullAccList").multiselect({
				    	dividerLocation: 0.5,
							animated: 'fast',
							remoteUrl: "MSajaxAcc.php",
							remoteParams: { maptype: 'MAP' }
						});
						$("#pullAccList").multiselect('selectNone');
			    },
			    onClose: function() {
			    	$("#companyform").validationEngine('hideAll');
			    },
			    api: true
			});
			reqpullapi.load();
	  });

	 	//this one is if you can't see the account at all. The function above for .requestshare is for when you can see the account but don't have access to any of the MAPs etc or its read only!
	  $('.showrequests').live("click", function() {
	  	reqshowobj = $(this);
			var reqshowapi = $("#showrequestsbox").overlay({
			    top: 'center',
			    mask: {
			        color: '#fff',
			        loadSpeed: 200,
			        opacity: 0.5
			    },
			    closeOnClick: false,
			    onBeforeLoad: function(event) {
						$('#sharereqs').show();
						$('#reqshareadd').hide();
						$('#reqaddlink').hide();
			    },
			    onLoad: function() {

			    },
			    onClose: function() {
			    	$("#companyform").validationEngine('hideAll');
			    },
			    api: true
			});
			reqshowapi.load();
	  });

	  $(".sendPullReq").click(function(){
  		var comments = $('#req_comments_pull').val();
  		var closebox = '#requestpullbox';
  		var companylist = $("#pullAccList").multiselect('selectedValues').join(',');
  		if(companylist.length != '') {
			  var valid = $("#pull_comments").validationEngine('validate');
			  if(valid == true) {

					$.get("AjaXCompany.php", { shareRequest:true, commts:comments, compids:companylist },
					  function(data){
					    alert(data);
					    $(closebox).overlay().close();
					 });
				}
			} else {
				alert('I\'m sorry you must add at least one account to proceed');
			}

	  });

		var selbox = new Array();
  	selbox['map'] = new Array("map_view", false, false, false);
  	selbox['comp'] = new Array("comp_view", "comp_edit", false);
  	selbox['tmap'] = new Array("tmap_view", "tmap_edit", "tmap_archive");
  	selbox['user'] = new Array("user_view", "user_edit", false);
  	selbox['imap'] = new Array("imap_view", "imap_edit", false);
		var modulelist = new Array("MAPs", "Accounts", "talentMAPs &amp; teamMAPs", "Users", "idealMAPs");
		var modnames = new Array("map", "comp", "tmap", "user", "imap");

		//set edit uid outside of functions so it's global!
		var edituid = '';

		$('.deleteshare').live("click", function() {
			edituid = this.id.substr(12);
			$(this).closest('tr').hide();
			$.get("AjaXUser.php", { companyid: compid, uid:edituid, deleteShare:true },
			  function(data){
					alert(data);
			  });
	  });

	  $('.reqdecline').live("click", function() {
			var srid = this.id.substr(5);
			$.get("AjaXCompany.php", { srid: srid, declineShare:true },
			  function(data){
					alert(data);
			  });
	  });

	  $('.editshare').live("click", function() {
	  	edituid = this.id.substr(10);
			$.get("AjaXUser.php", { cid: compid, uid: edituid, getShareAccess: true },
			  function(j){

			  	//$("#shareAcc").multiselect('destroy');
			  	$("#shareAccBox").hide();
			  	$("#shareuserlist").html('');
			  	$("#shareuserlist").show();
			  	$("#submitRightsLink").hide();
					$("#updateRightsLink").show();
					$("#showRightsLink").hide();
			  	$("#editSHAREbox").hide();

			  	var usereditname = $("#editname_" + edituid).html();
			  	var htmlstr = '<table border="0" cellpadding="2" cellspacing="0"><tr><td width="160">Information</td><td width="70">View</td><td width="70">Edit</td><td width="70">Archive</td></tr></table>';
		  		htmlstr += '<div id="user_' + edituid + '"><table border="0" cellpadding="2" cellspacing="0"><tr><td width="160"><tr><td colspan="5"><strong>' + usereditname + '</strong></td></tr>';
					var i = 0;
		  		$.each(modulelist, function(modindex, modvalue) {
			  		htmlstr += '<tr><td width="160">' + modvalue + '</td>';
			  		var selarr = selbox[modnames[modindex]];
	  			  $.each(selarr, function(selindex, selboxname) {
			  			if(selboxname == false) {
			  				htmlstr += '<td width="70"></td>';
			  			} else {
			  				var selectme = '';
			  				$.each(j, function(index, infoobj) {
			  					var checkname = modnames[i] + '_' + infoobj.accesstype.toLowerCase();
			  					if(checkname == selboxname && modnames[i] == infoobj.accessgroup) {
			  						if(infoobj.allowaccess == 'TRUE') {
			  							selectme = ' checked="checked"';
			  						} else {
			  							selectme = '';
			  						}
			  					}
			  				});
			  				htmlstr += '<td width="70"><input type="checkbox" name="' + selboxname + '" class="' + selboxname + '" value="' + edituid + '"' + selectme + ' /></td>';
			  			}
			  		});
			  		htmlstr += '</tr>';
			  		i++;
		  		});
		  		htmlstr += '</table></div>';

				 	$("#shareuserlist").append(htmlstr);

				 	//loops through again (yes it has too because it doesn't get added to teh DOM straight away), could change i guess.. but w/e
				 	//It checks to see if the edit of the various boxes is set and if so disables the view item etc etc.
					var cldiv = $("#user_" + edituid);
					var i = 0;
				 	$.each(modulelist, function(modindex, modvalue) {
			  		var selarr = selbox[modnames[modindex]];
	  			  $.each(selarr, function(selindex, selboxname) {
	  			  	if(selboxname != false) {
	  			  		var disableView = false;
			  				var disableit = '';
			  				var editName = modnames[i] + '_edit';
			  				var viewName = modnames[i] + '_view';
				  			var editobj = cldiv.find("input[name='" + editName + "']");
				  			var viewobj = cldiv.find("input[name='" + viewName + "']");
		  			  	if(editobj.is(":checked")) {
				  				disableView = true;
				  			} else {
				  				disableView = false;
				  			}
			  				if(disableView == true) {
			  					viewobj.attr("disabled", "disabled");
			  				}
			  			}
		  			});
		  			i++;
		  		});

			  	$("#sharecompanybox #companyboxheader").html('Select Sharing Rights');

			}, "json");
	  });

	  var requid = '';
	  $('.selectRights, .reqapprove').live("click", function() {
	  	var clickLoc = '';
	  	if($(this).hasClass('reqapprove')) {
	  		clickLoc = 'req';
	  		compid = $(this).closest('tr').find('td:first').attr('id').substr(7);
	  		var userlist = new Array(this.id.substr(7));
	  		requid = this.id.substr(7);
	  	} else {
	  		clickLoc = 'share';
	  		var userlist = $("#shareAcc").multiselect('selectedValues');
	  	}
	  	if(userlist.length != 0) {
	  		if(clickLoc == 'share') {
			  	$("#shareAccBox").hide();
			  	$("#shareuserlist").html('');
			  	$("#shareuserlist").show();
			  } else {
			  	$("#sharereqs").hide();
			  	$('#reqshareadd').show();
					$('#reqaddlink').show();
			  }
		  	var htmlstr = '<table border="0" cellpadding="2" cellspacing="0"><tr><td width="160">Information</td><td width="70">View</td><td width="70">Edit</td><td width="70">Archive</td></tr></table>';
		  	var realname = '';
		  	if(clickLoc == 'req') {
		  	 realname = $(this).closest('tr').find('td:first').html() + ' with ' + $(this).closest('tr').find('td:nth-child(2)').html();
		  	}
		  	$.each(userlist, function(index, value) {
		  		if(clickLoc == 'share') {
		  			realname = $("#shareAcc option[value='" + value + "']").text();
		  		}
		  		htmlstr += '<div id="user_' + value + '"><table border="0" cellpadding="2" cellspacing="0"><tr><td width="160"><tr><td colspan="5"><strong>' + realname + '</strong></td></tr>';
		  		$.each(modulelist, function(modindex, modvalue) {
			  		htmlstr += '<tr><td width="160">' + modvalue + '</td>';
			  		var selarr = selbox[modnames[modindex]];
			  		$.each(selarr, function(selindex, selboxname) {
			  			if(selboxname == false) {
			  				htmlstr += '<td width="70"></td>';
			  			} else {
			  				if(selboxname == 'map_view') {
			  					var CND = ' checked="checked" disabled="disabled"';
			  				} else {
			  					var CND = '';
			  				}
			  				htmlstr += '<td width="70"><input type="checkbox" name="' + selboxname + '" class="' + selboxname + '" value="' + value + '" ' + CND + ' /></td>';
			  			}
			  		});
			  		htmlstr += '</tr>';
		  		});
		  		htmlstr += '</table></div>';
		  	});

				if(clickLoc == 'share') {
			  	$("#shareuserlist").append(htmlstr);
			  	$("#submitRightsLink").show();
			  	$("#showRightsLink").hide();
			  	$("#editSHAREbox").hide();
			  	$("#sharecompanybox #companyboxheader").html('Select Sharing Rights');
			  } else {
			  	$("#reqshareadd").html(htmlstr);
			  }
		  } else {
		  	alert('I\'m sorry you must select at least one person to share the company with!');
		  }
	  });

		$('.comp_edit, .tmap_edit, .user_edit, .tmap_archive, .imap_edit').live("click", function() {
			var ident = $(this).attr('class').substr(0,4);
  		var cldiv = $(this).closest("div");
  		var changeView = true;
  		if($(this).attr('class') == 'user_access') {
  			var editobj = cldiv.find("input[name='user_edit']");
  			if(editobj.is(":checked")) {
  				changeView = false;
  			}
  		} else if($(this).attr('class') == 'tmap_archive') {
  			var editobj = cldiv.find("input[name='tmap_edit']");
  			if(editobj.is(":checked")) {
  				changeView = false;
  			}
  		} else if($(this).attr('class') == 'tmap_edit') {
  			var editobj = cldiv.find("input[name='tmap_archive']");
  			if(!$(this).is(":checked")) {
  				if(editobj.is(":checked")) {
  					changeView = false;
  				}
  			}
  		} else if($(this).attr('class') == 'user_edit') {
  			var editobj = cldiv.find("input[name='user_access']");
  			if(!$(this).is(":checked")) {
  				if(editobj.is(":checked")) {
  					changeView = false;
  				}
  			}
  		}
  		if(changeView == true) {
	  		var viewobj = cldiv.find("input[name='" + ident + "_view']");
	  		if($(this).is(":checked")) {
	  			viewobj.attr("disabled", "disabled");
	  		} else {
	  			viewobj.attr("disabled", false);
	  		}
	  		if(!viewobj.is(":checked")) {
	  			viewobj.attr("checked", "checked");
	  		}
	  	}
  	});

	  $('.submitShare, .updateShare, .requestAdd').live("click", function() {
	  	if($(this).hasClass('submitShare')) {
	  		var userlist = $("#shareAcc").multiselect('selectedValues');
	  	} else if($(this).hasClass('updateShare')) {
	  		var userlist = new Array(edituid);
	  	} else if($(this).hasClass('requestAdd')) {
	  		//this is set when the "approve button / decline button is clicked. It is set above outside all functions so it acts as a global var!
	  		var userlist = new Array(requid);
	  	}
			var rightsObj = [];
			$.each(userlist, function(index, value) {
				var selnames = new Array();
		  	$("#user_" + value + " :checkbox").each(
				  function(cbindex, cbvalue) {
				  	userident = 'user' + $(this).attr("value");
				  	var checkname = $(this).attr("name");
				  	if($(this).is(":checked")) {
				  		selnames[checkname] = 'TRUE';
				   	} else {
				   		selnames[checkname] = 'FALSE';
				   	}
				  }
				);
				//zomg... multidimensional objects ftw.. well arrays! :)
				rightsObj.push({userid : value, map:{view:selnames['map_view']}, comp:{view:selnames['comp_view'], edit:selnames['comp_edit']}, tmap:{view:selnames['tmap_view'], edit:selnames['tmap_edit'], archive:selnames['tmap_archive']}, user:{view:selnames['user_view'], edit:selnames['user_edit']}, imap:{view:selnames['imap_view'], edit:selnames['imap_edit']}});
			});

  		//submit that shit!
  		if($(this).hasClass('submitShare')) {
  			$.post("AjaXUser.php", { accshareObj: rightsObj, companyid:compid, shareCompany: true },
			  function(data){
			  	$('#sharecompanybox').overlay().close();
	  			 $.jGrowl(data, {
						header:'<strong>System Message</strong>',
						closeTemplate:'<div class="jGrowlCloseMe">close (x)</div>',
						position:'bottom-right'
					});
			  });
	  	} else if($(this).hasClass('updateShare')) {
  			$.post("AjaXUser.php", { accshareObj: rightsObj, companyid:compid, updateShareAccess: true },
			  function(data){
			  	$('#sharecompanybox').overlay().close();
	  			 $.jGrowl(data, {
						header:'<strong>System Message</strong>',
						closeTemplate:'<div class="jGrowlCloseMe">close (x)</div>',
						position:'bottom-right'
					});
			  });
	  	} else if($(this).hasClass('requestAdd')) {
  			$.post("AjaXUser.php", { accshareObj: rightsObj, companyid:compid, shareCompany: true, reqApprove:true, requid:requid },
			  function(data){
			  	$('#showrequestsbox').overlay().close();
	  			 $.jGrowl(data, {
						header:'<strong>System Message</strong>',
						closeTemplate:'<div class="jGrowlCloseMe">close (x)</div>',
						position:'bottom-right'
					});
			  });
	  	};
	  });

	  $("#companyform").validationEngine('attach');

	});
