
			function removeByElement(arrayName,arrayElement) {
				  for(var i=0; i<arrayName.length;i++ ) { 
				  	if(arrayName[i]==arrayElement) {
				    	arrayName.splice(i,1);
				    }
					}
					return arrayName;
				}
				
				function array_diff(a1, a2) {
					var a=[], diff=[];
					for(var i=0;i<a1.length;i++) {
						a[a1[i]]=true;
					}
					for(var i=0;i<a2.length;i++) {
						if(a[a2[i]]) {
							delete a[a2[i]];
						} else {
							a[a2[i]]=true;
						}
					}
					for(var k in a) {
						diff.push(k);
					}
					return diff;
				}
				
				function oc(a)
				{
				  var o = {};
				  for(var i=0;i<a.length;i++)
				  {
				    o[a[i]]='';
				  }
				  return o;
				}

				$(function() {
					
					//this is really just for IE7 due to the retarded z-index bug. However doesn't hurt to leave it in for everything :)
					$('.leftfactor, .rightfactor, .infoDesc').live("hover", function() {
			  		var currObj = $(this);
			  		$('.cluster_container, .slider_con').each(function(index, value) {
			  			$(this).css("z-index", 10);
			  		});
			  		currObj.closest('.cluster_container').css("z-index", 11);
			  		currObj.closest('.slider_con').css("z-index", 11);
			  	});
					
					var PaneSettings = {
						autoReinitialise: false,
						showArrows: true,
						horizontalGutter: 10
					};
					
					var pane = $('#rightsortbox > .multiselect');
					pane.jScrollPane(PaneSettings);
					var paneApi = pane.data('jsp');

					$('#viewMoreButton').click(function() {
						getMoreMaps();
					});
					$('#MAPTextHeaderL .addmap').click(function() {
						clearResults();
						$('#SearchInput').val('');
						var maptype = $('#leftMAPselect').val();
						switch(maptype) {
							case 'MAP':
							$('#selectMAPsHeader').html($('#selMAP').val());
							break;
							case 'idealMAP':
							$('#selectMAPsHeader').html($('#selIdealMAP').val());
							break;
							case 'TalentMAP':
							$('#selectMAPsHeader').html($('#selTalentMAP').val());
							break;
							case 'TeamMAP':
							$('#selectMAPsHeader').html($('#selTeamMAP').val());
							break;
						}
						var target = 'leftsortbox';
						ViewMaps(target, maptype, true);
					});
					$('#MAPTextHeaderR .addmap').click(function() {
						clearResults();
						$('#SearchInput').val('');
						var maptype = $('#rightMAPselect').val();
						switch(maptype) {
							case 'MAP':
							$('#selectMAPsHeader').html($('#selMAP').val());
							break;
							case 'TalentMAP':
							$('#selectMAPsHeader').html($('#selTalentMAP').val());
							break;
							case 'TeamMAP':
							$('#selectMAPsHeader').html($('#selTeamMAP').val());
							break;
						}
						var target = 'rightsortbox';
						ViewMaps(target, maptype, false);
					});
					$('#ViewMode').change(function() {
						clearResults();
						getMoreMaps();
					});
					$('#leftsortbox .clearmaps').click(function() {
						clearSelected('leftsortbox');
					});
					$('#rightsortbox .clearmaps').click(function() {
						clearSelected('rightsortbox');
					});
					var timerid;
					$('#SearchInput').keyup(function() {
						clearTimeout(timerid);
						timerid = setTimeout(function() { 
							doSearch();
						}, 1000);
					});
					$('#rightMAPselect').change(function() {
						clearSelected('rightsortbox');
					});
					$('#leftMAPselect').change(function() {
						clearSelected('leftsortbox');
					});

					//this makes the box floatable before its even shown! so it doesn't do stupid animation :)
					var displayType = 'single';
					var floatBox = true;
					$("#leftresultbox").makeFloat({x:24,y:650,alwaysVisible:false});
					
					//this sets the timeout for all ajax requests on the page. 20 seconds. Default is like 300ms hah..
					$.ajaxSetup({
					  timeout: 20000
					});
					
					$('body').data("accountinfo", {path:$('#accpath').val()});	
					var accpath = $('body').data("accountinfo").path;
					var currlocale = $('#locale').val().substr(3);
					$.localise('ui-multiselect', {language: currlocale, path: '/_js/locale/'});
					
					//this is set in the usrobj and pasted into analytics.php in a hidden form field.
					var surveyDomain = $('#surveyDomain').val();
					
					$("#incREP").live("click", function () {
						var orient = $("#pdfOrientation").val();
						
					    if($(this).is(":checked")) {
								$('#detailedrep').attr('href', surveyDomain + 'pdfgen.php?pagetype=detailed&incR=yes&orientation=' + orient);
								$('#summaryrep').attr('href', surveyDomain + 'pdfgen.php?incR=yes&orientation=' + orient);
					    } else {
								$('#detailedrep').attr('href', surveyDomain + 'pdfgen.php?pagetype=detailed&orientation=' + orient);
								$('#summaryrep').attr('href', surveyDomain + 'pdfgen.php?orientation=' + orient);
					    }
					});
					
					$("#pdfOrientation").live("change", function() {
						var orient = $("#pdfOrientation").val();
					    if($("#incREP").is(":checked")) {
								$('#detailedrep').attr('href', surveyDomain + 'pdfgen.php?pagetype=detailed&incR=yes&orientation=' + orient);
								$('#summaryrep').attr('href', surveyDomain + 'pdfgen.php?incR=yes&orientation=' + orient);
					    } else {
								$('#detailedrep').attr('href', surveyDomain + 'pdfgen.php?pagetype=detailed&orientation=' + orient);
								$('#summaryrep').attr('href', surveyDomain + 'pdfgen.php?orientation=' + orient);
					    }
					    $('#quickview').attr('href', surveyDomain + 'pdfgen_quickview.php?orientation=' + orient);
					});
					
					//turn floating panel on or off!
					$('#changeFloat').live("click", function() {
						var newY = 650;
						if(floatBox == true) {
							$('#changeFloat').attr('src', accpath + '_images/_map/float_inactive.jpg');
							$("#leftresultbox").stopFloat();
							floatBox = false;
						} else {
							if(displayType == 'single') {
								newY = 650;
							} else {
								newY = 870;
							}
							$('#changeFloat').attr('src', accpath + '_images/_map/float_active.jpg');
							$("#leftresultbox").restartFloat({x:24,y:newY,alwaysVisible:false});
							floatBox = true;
						}
					});

					function setAvatars(leftAvatar, rightAvatar, avaLeftType, avaRightType) {
						if(leftAvatar != '') {
							$('#leftAvatarBox').html('<img src="' + leftAvatar + '" />');
						} else {
							switch(avaLeftType) {
								case 'MAP':
									$('#leftAvatarBox').html('<img src="' + accpath + '_images/_icons/mymap_icon.png" width="50" height="46" />');
									break;
								case 'TeamMAP':
									$('#leftAvatarBox').html('<img src="' + accpath + '_images/_icons/teammap_icon.png" width="50" height="46" />');
									break;
								case 'TalentMAP':
									$('#leftAvatarBox').html('<img src="' + accpath + '_images/_icons/talentmap_icon.png" width="50" height="46" />');
									break;
								case 'idealMAP':
									$('#leftAvatarBox').html('<img src="' + accpath + '_images/_icons/idealmap_icon.png" width="50" height="46" />');
									break;
							}
						}
						if(rightAvatar != '') {
							$('#rightAvatarBox').html('<img src="' + rightAvatar + '" />');
						} else {
							switch(avaRightType) {
								case 'MAP':
									$('#rightAvatarBox').html('<img src="' + accpath + '_images/_icons/mymap_icon.png" width="50" height="46"  />');
									break;
								case 'TeamMAP':
									$('#rightAvatarBox').html('<img src="' + accpath + '_images/_icons/teammap_icon.png" width="50" height="46" />');
									break;
								case 'TalentMAP':
									$('#rightAvatarBox').html('<img src="' + accpath + '_images/_icons/talentmap_icon.png" width="50" height="46" />');
									break;
								case 'idealMAP':
									$('#rightAvatarBox').html('<img src="' + accpath + '_images/_icons/idealmap_icon.png" width="50" height="46" />');
									break;
							}
						}
					}
					
					function setIcons(leftType, rightType) {
						if(leftType=='MAP') {
							if(rightType == 'MAP') {
								$("#scoreicon img").attr('src', accpath + '_images/_map/single_grey_alt.png');
							} else {
								$("#scoreicon img").attr('src', accpath + '_images/_map/single_grey.png');
							}
						} else if (leftType=='idealMAP') {
							if(rightType == 'MAP' || rightType == 'idealMAP') {
								$("#scoreicon img").attr('src', accpath + '_images/_map/ideal_grey_alt.png');
							} else {
								$("#scoreicon img").attr('src', accpath + '_images/_map/ideal_grey.png');
							}
						} else if(leftType == 'TalentMAP') {
							if(rightType == 'TalentMAP' || rightType == 'TeamMAP') {
								$("#scoreicon img").attr('src', accpath + '_images/_map/cross_grey_alt.png');
							} else {
								$("#scoreicon img").attr('src', accpath + '_images/_map/cross_grey.png');
							}
						} else if(leftType == 'TeamMAP') {
							if(rightType == 'TeamMAP') {
								$("#scoreicon img").attr('src', accpath + '_images/_map/cross_grey_alt.png');
							} else {
								$("#scoreicon img").attr('src', accpath + '_images/_map/cross_grey.png');
							}
						}
							
						if(rightType=='MAP') {
							if(leftType == 'MAP') {
								$("#benchicon img").attr('src', accpath + '_images/_map/single_grey.png');
							} else if(leftType == 'TalentMAP' || leftType == 'TeamMAP') {
								$("#benchicon img").attr('src', accpath + '_images/_map/single_grey.png');
							} else {
								$("#benchicon img").attr('src', accpath + '_images/_map/single_grey.png');
							}
						} else if(rightType=='idealMAP') {
								$("#benchicon img").attr('src', accpath + '_images/_map/ideal_grey.png');
						} else if(rightType == 'TalentMAP') {
							if(leftType == 'TeamMAP') {
								$("#benchicon img").attr('src', accpath + '_images/_map/cross_grey_alt.png');
							} else {
								$("#benchicon img").attr('src', accpath + '_images/_map/cross_grey.png');
							}
						} else if(rightType == 'TeamMAP') {
							$("#benchicon img").attr('src', accpath + '_images/_map/cross_grey.png');
						}
					}
					
					function checkMAP(event, ui) {
						if($(this).multiselect('selectedValues').length > 1) {
							alert('I\'m sorry you are only allowed to selected one teamMAP');
							$(this).multiselect('deselect', $(this).find('option[value='+$(ui.option).val()+']').text());
						}
					}
					
					//show MAP Comparison when rank icons clicked
					$(".RankMLink").live("click", function(){
						displayType = 'ranked';
						var currRightMAPs = $(this).attr('id').substr(6);
						//unset/set the selected class
						$("#rankscroll").find(".rankselected").removeClass("rankselected");
					  $(this).closest('.rankmapholder').addClass("rankselected");
						
						if($("#leftresultbox").is(':hidden')) {
							$("#leftresultbox").show("slide", { direction: "right" }, 1000, function() {
								if(floatBox == true) {
									$("#leftresultbox").restartFloat({x:24,y:870,alwaysVisible:false});
								}
							});
						}			
						$.get("/_inc/ajaXMAPRank.php", { rankID: currRightMAPs, rankLink:1 },
					  function(data){
					  		$('#compareBox').fadeIn('fast');
								var resultData = jQuery.trim(data).split("---");
								var currRightType = resultData[2];
								var currLeftType = resultData[1];
								
								setAvatars(resultData[7], resultData[8], currLeftType, currRightType);
									
								var leftswitchHTML = resultData[5];
								var rightswitchHTML = resultData[6];
								$("#sliders").html(resultData[0]);
								//check what righttype is from session set in ajaxGEN so can show correct icon!
								$("#leftresultsname").html(resultData[3]);
								setIcons(currLeftType, currRightType);						
								$("#rightresultsname").html(resultData[4]).effect("highlight", {color:"#f3a9c0"}, 1000);
							  $("#leftmapswitch").html(leftswitchHTML);
								$("#rightmapswitch").html(rightswitchHTML);
								goTips();
								enableChange(currLeftType, currRightType);
								if(currLeftType == 'MAP') {
									getleftMapPicture();
								}
								if(currRightType == 'MAP') {
									getrightMapPicture(currRightMAPs);
								}
					  });
						
					});
					//end ranklink
					//TODO complete ajax request, write ajax file and position image. so all of it really
					function getleftMapPicture() {
						//var image = getPictureAjax('left', null);
					}
					
					function getrightMapPicture(rankid) {
						//var image = getPictureAjax('right', rankid);
					}
					
					function getPictureAjax(direction, rankid) {
						$.get("/_inc/ajaXMAPPicture.php", { rankID: currRightMAPs, rankLink:1 },
								  function(data){
						});
					}
					
					//this is for cycle
					function onAfter(curr, next, opts) {
				    var index = opts.currSlide;
				    $('#prevSS')[index === 0 ? 'fadeOut' : 'fadeIn'](200);
				    $('#nextSS')[index == opts.slideCount - 1 ? 'fadeOut' : 'fadeIn'](200);
					}

				  function clearSelected(target){
				  	if(target == 'rightsortbox') {
				  		paneApi.getContentPane().html('<ul></ul>');
				  		paneApi.reinitialise();
				  	} else {
							$('#' + target +'>.multiselect').html('<ul></ul>');
						}
					}
					
					function clearResults() {
						$('#mapsDisplay>.Contents').html('');
						switch($('#ViewMode').val()) {
							case 'thumbs':
								$('#mapsDisplay>.Contents').html('<ul id="resultRow" class="thumbs"></ul>');
								break;
							default :
								$('#mapsDisplay>.Contents').html('<ul id="resultRow" class="list"></ul>');
								break;
						}
						$('#tooltips').html('');
						$('#Start').val(0);	
					}
					function addMapToResults(map) {
						var currentcol = $('#CurrentColumn').val();
						var image = '';
						var tooltip = '';
						var target = $('#Target').val();
						switch($('#MapType').val()) {
							case 'MAP':
								//TODO figure out user image
								image = map.avatar;
								tooltip = 'Company: '+ map.company + '<br />Email:' + map.email;
								break;
							case 'TeamMAP':
								tooltip = 'Company: '+ map.company;
								image = accpath + '_images/_icons/teammap_icon.png';
								break;
							case 'TalentMAP':
								tooltip = 'Company: '+ map.company;
								image = accpath + '_images/_icons/talentmap_icon.png';
								break;
							case 'idealMAP':
								tooltip = 'Company: '+ map.company;
								image = accpath + '_images/_icons/idealmap_icon.png';
								break;
						}
						var newImg = new Image();
						newImg.src = image;
						var size, minisize;
						if(newImg.width > newImg.height) { 
							size = 'width="50"';
							minisize = 'width="25"';
						} else {
							size = 'height="50"';
							minisize = 'height="25"';
						}
						switch($('#ViewMode').val()) {
						case 'thumbs':
							$('#resultRow').append('<li id="map' + map.mapid + '"><div style="height:50px;text-align:center; margin:0px 0px 4px 0px;"><img ' + size + ' src="' + image + '" /></div>' + map.name + '</li>');
							break;
						default:
							$('#resultRow').append('<li id="map' + map.mapid + '">' + map.name + '</li>');
							break;
						}
						if($('#Selected' + target + 'Map' + map.mapid).length != 0){
							$('#map' + map.mapid).addClass('selected');
						}
						addTooltip(tooltip, '#map' + map.mapid);
						$('#map' + map.mapid).click(function(){
							if($('#SingleSelect').val() == '1') {
								$('#' + target + '> .multiselect > ul').html('<li id="Selected' + target + 'Map' + map.mapid + '"><img ' + minisize + ' src="' + image + '" />' + map.name + '</li>');
								$('#map' + map.mapid).addClass('selected');
								addTooltip(tooltip, '#Selected' + target + 'Map' + map.mapid);
								$('#mapsDisplay').overlay().close();
							} else {
								if($('#map' + map.mapid).hasClass('selected')) {
									$('#Selected' + target + 'Map' + map.mapid).qtip('destroy');
									$('#Selected' + target + 'Map' + map.mapid).remove();
									$('#map' + map.mapid).removeClass('selected');
								} else {
									$('#' + target + '> .multiselect .jspPane > ul').append('<li id="Selected' + target + 'Map' + map.mapid + '" class="selectedMAPTab"><img ' + minisize + ' src="' + image + '" />' + map.name + '<a href="javascript:void(0);" class="removeMe">x</a></li>');
									$('#map' + map.mapid).addClass('selected');
									addTooltip(tooltip, '#Selected' + target + 'Map' + map.mapid);
									//delete item when clicked (only right side)
									$('.removeMe').click(function() {
										$(this).parent().qtip('destroy');
										$(this).parent().remove();
										paneApi.reinitialise();
									});
								}
							}
							paneApi.reinitialise();
						});
						var nextCol = parseInt(currentcol) + 1;
						$('#CurrentColumn').val('' + nextCol);
					}
					function addTooltip(tiptext, elementSelector) {
						$(elementSelector).qtip({
							content: tiptext,
							position: {
								corner: {
								target: 'topMiddle',
								tooltip: 'bottomLeft'
								}
							},
							style: {
								background: '#75ABD7',
								color: 'black',
								border: {
									width: 2,
									radius: 5,
									color: '#537A99'
								},
								tip: {
									corner: 'bottomLeft',
									color: '#537A99',
									size: {
										x: 15,
										y : 10
									}
								},
								name:'blue'
							}
						});
					}
					function getMoreMaps() {
						var maptype = $('#MapType').val();
						var search = $('#SearchInput').val();
						var start = $('#Start').val();
						if(search == 'Search MAPs') {
							search = '';
						}
						$.ajax({
							type:'POST',
							url:'/getMapsAjax.php',
							data:{maptype: maptype,
								search: search,
								start: start},
								success : function(dat) {
								$.each(dat.results, function(index, value){
									addMapToResults(value);
								});
								$('#Start').val(dat.start);
							},
							dataType: 'json',
							method: 'post'
						});
						
					}
					function doSearch() {
						clearResults();
						getMoreMaps();
					}
					function getSelectedMaps(target) {
						var out = new Array;
						var id = 0;
						var tarObj = '';
						if(target == "rightsortbox") {
							 tarObj = $("#"+ target +">.multiselect .jspPane > ul > li");
						} else {
							 tarObj = $("#"+ target +">.multiselect > ul > li");
						}
						tarObj.each(function() {
							var str = $(this).attr('id');
							var trimmedstr = str.replace('Selected' + target + 'Map','');
							out[id] = trimmedstr;
							id++;
						});
						return out;
					}
					function ViewMaps(target, maptype, singleSelect) {
						if($(window).height() < 600) {
							$(".Contents").height($(window).height() - 208);
						}
						$('#Target').val(target);
						$('#MapType').val('' + maptype);
						if(singleSelect) {
							$('#SingleSelect').val(1);
						} else {
							$('#SingleSelect').val(0);
						}
						getMoreMaps();
						var mapsOverlay = $('#mapsDisplay').overlay({
							top: 'center',
							onBeforeLoad: function() {
								$('#SearchInput').Watermark("Search MAPs");
							},
							api: true,
							mask: { 
								color: '#fff',  
								loadSpeed: 200, 
								opacity: 0.5,
								zIndex: 9999
							},
							closeOnClick: false
						});
						mapsOverlay.load();
					}
					
					//generate that MAP Comparison!!!!
					$("#generatemap").click(function () {
							//Get map ids from divs
							var currLeftMapArray = getSelectedMaps('leftsortbox');
							var currRightMapArray = getSelectedMaps('rightsortbox');
							var rightLen = currRightMapArray.length;
							var leftLen = currLeftMapArray.length;
							var currLeftMAP = currLeftMapArray.join(',');
							var currRightMAPs = currRightMapArray.join(',');
							var currLeftType = $('#leftMAPselect').getSetSSValue();
							var currRightType = $('#rightMAPselect').getSetSSValue();
							var currRankSort = $('#rankSort').val(); 
							$("#currLeft").val(currLeftType);
							$("#currRight").val(currRightType);
							displayType = 'single';
							if(floatBox == true) {
								$("#leftresultbox").restartFloat({x:24,y:650,alwaysVisible:false});
							}
							if(rightLen === 0 && leftLen !==0) {
								$('#compareBox').fadeOut('fast');
								$.get("/_inc/ajaXMAPShow.php", { leftMAP: currLeftMAP, leftType:currLeftType },
									function(data){
										//hide the rank report link as not usable in single view!
										$('#matchKeyHolder').hide();
										$('.rankReportBox').hide();
										$('#vsbox').hide();
										$('#benchicon').hide();
										var resultData = jQuery.trim(data).split("---");
										$("#sliders").html(resultData[0]);
										$("#leftresultsname").html(resultData[1]);
										if(currLeftType == 'MAP')	{
											$("#scoreicon img").attr('src', accpath + '_images/_map/single_grey.png');
										} else if(currLeftType == 'idealMAP') {
											$("#scoreicon img").attr('src', accpath + '_images/_map/ideal_grey.png');
										} else {
											$("#scoreicon img").attr('src', accpath + '_images/_map/cross_grey.png');
										}
										//$("#benchicon img").attr('src', '');
										$("#rightresultsname").html('');
										if(currLeftType != 'MAP' && currLeftType != 'idealMAP') {
											$("#leftmapswitch").html('<div class="shadeBox">' + resultData[2] + '</div>');
										} else {
											$("#leftmapswitch").html('');
										}
										$("#rightmapswitch").html('');
										if ($("#leftresultbox").is(':hidden')) {
											$("#leftresultbox").show("slide", { direction: "right" }, 1000, function() {
												//
											});
										}

										if ($("#rankholder").is(':visible')) {
											$("#rankholder").slideUp(600, function() {
												$("#rankholder").html('');
											});
										}
										//enable tips and drop down box for MAPs
										goTips(); 
										enableChange(currLeftType, currRightType);
									});
							} else if(leftLen === 0) {
								alert($('#populateError').val());
							} else {
								$('#matchKeyHolder').show();
								$('.rankReportBox').show();
								$('#vsbox').show(0);
								$('#benchicon').show(0);
								$.get("/_inc/ajaXMAPGen.php", { leftMAP: currLeftMAP, leftType:currLeftType, rightMAPs: currRightMAPs, rightType:currRightType, rankSort:currRankSort },
							  function(data){
									var resultData = jQuery.trim(data).split("---");
									if(rightLen > 1) {
										$('#compareBox').fadeOut('fast');
										displayType = 'ranked';
										if(floatBox == true) {
											$("#leftresultbox").restartFloat({x:24,y:870,alwaysVisible:false});
										}
										//reset sliders div
								   	$("#sliders").html('Please Click on a Rank Icon to view the MAP');
										if($("#rankholder").is(':hidden'))	{
							  			$("#rankholder").html(resultData[0]);
							  			//set the switch box data up
								  		$("#leftmapswitch").html(resultData[1]);
											$("#rightmapswitch").html(resultData[2]);
											
							  			$("#rankholder").slideDown(600, function() {
							  				if ($("#leftresultbox").is(':visible')) {
													$("#leftresultbox").hide("slide", { direction: "right" }, 1000, function() {
														//$("#leftresultbox").stopFloat();
													});
												}
								  			$('#rankscroll').cycle({ 
								  					next:   '#next', 
														prev:   '#prev',
												    fx: 'scrollHorz', 
												    delay: -1000,
												    speed:  800, 
														timeout: 0, 
												    pager: '#pagernav',
												    after: onAfter
												});
							  			});
							  		} else {
						  				if ($("#leftresultbox").is(':visible')) {
												$("#leftresultbox").hide("slide", { direction: "right" }, 1000, function() {
													//$("#leftresultbox").stopFloat();
												});
											}
							  			$("#rankholder").html(resultData[0]);
								  		$("#leftmapswitch").html(resultData[1]);
											$("#rightmapswitch").html(resultData[2]);
							  			$('#rankscroll').cycle({ 
							  					next:   '#next', 
													prev:   '#prev',
											    fx: 'scrollHorz',
											    delay: -1000,
											    speed:  800, 
													timeout: 0, 
											    after: onAfter
											});
							  		}
								  	//run if ONLY ONE OF EACH IS SELECTED
								  	//empty selected rank.. not needed on GEN
								  	$("#rankscroll").find(".rankselected").removeClass("rankselected");
							  	} else {
							  		$('#compareBox').fadeIn('fast');
							  		setAvatars(resultData[5], resultData[6], currLeftType, currRightType);
							  		displayType = 'single';
							  		$("#sliders").html(resultData[0]);
							  		$("#leftresultsname").html(resultData[1]);
							  		setIcons(currLeftType, currRightType);	
							  		$("#rightresultsname").html(resultData[2]).effect("highlight", {color:"#f3a9c0"}, 1000);
							  		$("#leftmapswitch").html(resultData[3]);
										$("#rightmapswitch").html(resultData[4]);
							  		if ($("#leftresultbox").is(':hidden')) {
										  	$("#leftresultbox").show("slide", { direction: "right" }, 1000, function() {
										  		if(floatBox == true) {
										  			$("#leftresultbox").restartFloat({x:24,y:650,alwaysVisible:false});
										  		}
										  	});
										}; 

										if ($("#rankholder").is(':visible')) {
								  		$("#rankholder").slideUp(600, function() {
								  			$("#rankholder").html('');
								  		});
							  		}
										goTips(); 
							  	}
									enableChange(currLeftType, currRightType);
							  });
							
							}//end if right 0 etc

					});

					$("#toggleObox").toggle(function() {
						$("#OboxContent").slideUp(400);
						$("#oboxswitch").attr('src', accpath + '_images/_map/optionsbox_expand.jpg');
						$("#leftresultbox").restartFloat({x:24,y:440,alwaysVisible:false});
					}, function() {
						$("#OboxContent").slideDown(400);
						$("#oboxswitch").attr('src', accpath + '_images/_map/optionsbox_collapse.jpg');
						$("#leftresultbox").restartFloat({x:24,y:650,alwaysVisible:false});
					});
					
					$('#leftMAPselect').sSelect().change(function(){
						var leftval = $('#leftMAPselect').getSetSSValue();
						$("#leftsort").html('');
						var mapicon = '';
						switch(leftval){
							case 'MAP':
						  mapicon = 'mymap_icon.png';
						  break;
							case 'TeamMAP':
						  mapicon = 'teammap_icon.png';
						  break;
							case 'TalentMAP':
						  mapicon = 'talentmap_icon.png';
						  break;
						  case 'idealMAP':
						  mapicon = 'idealmap_icon.png';
						  break;
						}
						
						$("#leftMAPheader > img").attr('src', accpath + '_images/_icons/' + mapicon);
					});
					
					$('#rightMAPselect').sSelect().change(function(){
						var rightval = $('#rightMAPselect').getSetSSValue();
						$("#rightsort").html('');
						var mapicon = '';
						switch(rightval){
							case 'MAP':
						  mapicon = 'mymap_icon.png';
						  break;
							case 'TeamMAP':
						  mapicon = 'teammap_icon.png';
						  break;
							case 'TalentMAP':
						  mapicon = 'talentmap_icon.png';
						  break;
						   case 'idealMAP':
						  mapicon = 'idealmap_icon.png';
						  break;
						}
						
						$("#rightMAPheader > img").attr('src', accpath + '_images/_icons/' + mapicon);

					});
					
					$(".RELHeader, .TAPHeader, .DECHeader, .GTDHeader, .RELHeaderS, .TAPHeaderS, .DECHeaderS, .GTDHeaderS").live("click", function(){
						//where talentmaps are used it uses different HTML structure
						if($(":nth-child(2)", this).hasClass('clusterClosed')) {
							$(":nth-child(2)", this).removeClass('clusterClosed').addClass('clusterOpen');
						} else {
							$(":nth-child(2)", this).removeClass('clusterOpen').addClass('clusterClosed');
						}
						
						//MAP vs MAP
						if($(":nth-child(1)", this).hasClass('clusterClosed')) {
							$(":nth-child(1)", this).removeClass('clusterClosed').addClass('clusterOpen');
						} else {
							if (!($(":nth-child(1)", this).hasClass("clusNameHolder"))) {
								$(":nth-child(1)", this).removeClass('clusterOpen').addClass('clusterClosed');
							}
						}
						$(this).next('.statementHolder').slideToggle();
					});
					
					$(".top10bar, .bottom10bar").live("click", function(){
						$(this).next('.statementHolder').slideToggle();
					});
					
					function enableChange(leftType, rightType) {	 
	   				$("#altleftmap, #altrightmap").change(function(){
	   					
	   					var leftie = 0;
	   					var rightie = 0;
	   					
	   					if(leftType == 'MAP') {
								var leftie = $("#altleftmap").val();
								$("#altleftmap option").each(function(i){
									if($(this).val() == leftie) {
										$(this).css("color","#398dca");
									} else {
										$(this).css("color","#000");
									}
       					});
							};
							if(rightType == 'MAP') {
								var rightie = $("#altrightmap").val();
								$("#altrightmap option").each(function(i){
									if($(this).val() == rightie) {
										$(this).css("color","#d47097");
									} else {
										$(this).css("color","#000");
									}
	       				});
							};
							
							//this checks to see if it is showing a single MAP. If so just need to send a var through to the recalc script to include the single view not the rank / compare view :)
							var currRightMapArray = getSelectedMaps('rightsortbox');
							var checkRightLen = currRightMapArray.length;
							var showSingle;
							if(checkRightLen == 0) {
								showSingle = true;
							} else {
								showSingle = false;
							}
							
							$.get("/_inc/ajaXMAPReCalc.php", { newLeftMapID:leftie, newRightMapID:rightie, Lt: leftType, Rt: rightType, showSingle:showSingle },
						  function(data){
						  		//just show the new data if it's single. No need to update the left box as nothing changes.
						  		if(showSingle == true) {
						  			$("#sliders").html(data);
						  		} else {
										var resultData = jQuery.trim(data).split("---");
										var startSlide = resultData[5];
										$("#rankholder").html(resultData[4]);
										$("#sliders").html(resultData[0]);
										//check what righttype is from session set in ajaxGEN so can show correct icon!
										var currRightType = resultData[1];
										$("#leftresultsname").html(resultData[2]);
										setIcons(leftType, rightType);
							  		$("#rightresultsname").html(resultData[3]).effect("highlight", {color:"#f3a9c0"}, 1000);
							  		//scroll it up baby
						  			$('#rankscroll').cycle({ 
						  					next:   '#next', 
		    								prev:   '#prev',
										    fx: 'scrollHorz', 
										    delay: -1000,
										    speed:  800, 
		    								timeout: 0, 
										    pager: '#pagernav',
										    after: onAfter, 
										    startingSlide:startSlide
										});
									}
						  	 //show tooltip
								 goTips();
						  });//end AJAX Call
						});
					};//end function!
					
					function goTips() {
						 //show tooltip
						 $(".rightfactor, .leftfactor").tooltip({
	 						 effect:'slide',
	 						 offset: [30, 0], 
	 						 relative: true, 
	 						 predelay:100
						 }).dynamic( { 
				        bottom: { 
			            direction: 'down', 
			            bounce: true
				        } 
				     });
				     
				     $(".ST_TT").tooltip({
	 						 effect:'slide',
	 						 offset: [0, 0], 
	 						 relative: true, 
	 						 predelay:100
						 }).dynamic({ 
				        bottom: { 
			            direction: 'down', 
			            bounce: true
				        } 
				     });
				     
				     $(".infoDesc").tooltip({
	 						 effect:'slide',
	 						 offset: [0, 0], 
	 						 relative: true, 
	 						 predelay:100
						 }).dynamic({ 
				        bottom: { 
			            direction: 'down', 
			            bounce: true
				        } 
				     });
				     
				     $(".matchhelp").tooltip({
			    		 tip: '#matchtip',  
	 						 effect:'slide', 
	 						 //offset: [0, -100],
	 						 position:'top left',
	 						 direction:'up',
	 						 bounce:true,
	 						 relative: true, 
	 						 predelay:100
						 }).dynamic({ 
				        bottom: { 
			            direction: 'down', 
			            bounce: true
				        } 
				     });
	   			};
	   			
					$('.edittmap').live("click", function() {
						$('.edittmap').data("parent", {id:$(this).parents('div:eq(1)').attr('id')});
						var mapids = '';
						$("#ETMapid").val($(this).attr('id').substr(5));
							var MAPapi = $("#TMAPbox").overlay({ 
								top: 'center', 
								mask: { 
									color: '#fff',  
									loadSpeed: 200, 
									opacity: 0.5,
									zIndex: 999999
								},
								closeOnClick: false, 
								onBeforeLoad: function(event) {
									//reset HTML and form values before loading
									$("#editTMAP").html('');
									$("#ntmapname").val('');
								},
								onLoad: function(event) {
									var nothing = '';
									var ETMapid = $("#ETMapid").val();
									$.get("/_inc/ajaXUpdate.php", { tmapid: ETMapid, getTMAP: 'yes' },
										function(data){
											var resultData = data.split('---');
											mapids = resultData[2];
											$('.edittmap').data("tmapname", {name:resultData[0]});
											
											//now complete map shiate!		
											$("#editTMAP").multiselect('destroy');
											$("#editTMAP").multiselect({ 
												dividerLocation: 0.5, 
												animated: 'fast',
												searchDelay: 1400,
												remoteUrl: "/MSajax.php",
												remoteParams: { newmapids: mapids, maptype: 'MAP' }
											});
											var currmapids = mapids.split(',');
											$("#editTMAP").multiselect('selectNone');
											//$("#editTMAP").multiselect('enabled', false);
											
											$('#editTMAP').everyTime(500, function(i) {
											  if($('#editTMAP').multiselect('isBusy') == false) {
												  $("#editTMAP").stopTime();
		
													$.each(currmapids, function(i, val) {
														$('#editTMAP').multiselect('select', $('#editTMAP').find('option[value=' + val + ']').text() );
													});
													
													//this is needed so that it isn't still selecting the options when it trying to do the array difference.
													$('#editTMAP').everyTime(100, function(i) {
														 if($('#editTMAP').multiselect('isBusy') == false) {
														 	  $("#editTMAP").stopTime();
																var diff_arr = array_diff($('#editTMAP').multiselect('selectedValues'),currmapids);
																//this is because ie is shit and returns the value "indexOf" from the diff_array function. So this removes it from the array!
																diff_arr = removeByElement(diff_arr,'indexOf');
																var oldmapids = diff_arr.join(',');
																if(diff_arr.length >= 1) {
												  				$.get("/_inc/ajaXUpdate.php", { oldmapids:oldmapids, getnewTMAPids:true },
												  					function(data){
												  						var updatedmapids = data.split(',');
												  						$.each(updatedmapids, function(i, val) {
												  							$('#editTMAP').multiselect('select', $('#editTMAP').find('option[value=' + val + ']').text() );
												  						});
												  						alert("Some of the respondents have retaken their MAPs since this talentMAP/teamMAP was made. The new MAPs have been automatically added for you. If you want to keep these changes click save, otherwise click cancel to keep the talentMAP/teamMAP in it's original state"); 
												  					});
											  				}
									  				}
								  				});
								  			}
								  		}); //end everytime 
										});
							},
							onClose: function() {
								$("#addMAPbox .search:input").val('');
								$("#editTMAP").multiselect('search', $("#addMAPbox .search:input").val());
							},
							api: true 
						});
						MAPapi.load();
					});

			   	$(".updatetmap, .newtmap").click(function(){
			   		var ETMapid = $("#ETMapid").val();
			   		var currtmapids = $('#editTMAP').multiselect('selectedValues').join(',');
			   		if(currtmapids.length != 0) {
				   		var whichside = $('.edittmap').data("parent").id;
				   		var leftType = $("#currLeft").val();
					  	var rightType = $("#currRight").val();
		   				if(whichside == 'leftmapswitch') {
					  		var leftid = currtmapids;
					  		var rightid = 0;
					  		var tmaptype = leftType;
					  	} else if(whichside == 'rightmapswitch') {
					  		var leftid = 0;
					  		var rightid = currtmapids;
					  		var tmaptype = rightType;
					  	} else {
					  		var leftid = currtmapids;
					  		var rightid = 0;
					  		var tmaptype = leftType;
					  	}
					  	if(this.id == "newtmap") {
					  		var tmapname = $('#ntmapname').val();
					  		$.get("/_inc/ajaXUpdate.php", { newmapids: currtmapids, addTMAP:'yes', type:tmaptype, tmapname:tmapname },
					  			function(data) {
					  				alert(data);
					  				$("#TMAPbox").overlay().close();
					  		});
					  	} else {
								var tmapname = $('.edittmap').data("tmapname").name;
								$.get("/_inc/ajaXUpdate.php", { tmapid:ETMapid, newmapids: currtmapids, updateTMAP:'yes', type:tmaptype, tmapname:tmapname },
								  function(data){
								  	if(whichside == 'rightmapswitch') {
								  		//only run if team/talent on right
										  $.get("/_inc/ajaXMAPReCalc.php", { newLeftMapID:leftid, newRightMapID:rightid, Lt: leftType, Rt: rightType },
										  function(data){
													var resultData = jQuery.trim(data).split("---");
													var startSlide = resultData[5];
													$("#rankholder").html(resultData[4]);
													$("#sliders").html(resultData[0]);
													//check what righttype is from session set in ajaxGEN so can show correct icon!
													$("#leftresultsname").html(resultData[2]);
													setIcons(leftType, rightType);
										  		//$("#rightresultsname").html(resultData[3]).effect("highlight", {color:"#f3a9c0"}, 1000);
										  		//scroll it up baby
									  			$('#rankscroll').cycle({ 
									  					next:   '#next', 
					    								prev:   '#prev',
													    fx: 'scrollHorz', 
													    delay: -1000,
													    speed:  800, 
					    								timeout: 0, 
													    pager: '#pagernav',
													    after: onAfter, 
													    startingSlide:startSlide
													});
						
										  	 //show tooltip
												 goTips();
										  });
								  	} else {
								  		//trigger generate click
								   		$("#generatemap").trigger('click');
										}
								  $("#TMAPbox").overlay().close();
								});
							}
						} else {
							alert('I\'m sorry, but you need to select at least one MAP');
						}
				  });
				  
				  $(".MAPoverlay").live("click", function() {
				  	//remove all overlay classes then add the one for just clicked!
				  	$(".MAPoverlay").removeClass('overlayOn');
				  	$(this).addClass('overlayOn');
				  	var mapid = $(this).attr("id").substr(4);
				  	$(".dynadd").remove();
				  	$.get("/_inc/ajaXoverlay.php", { mapid:mapid },
					  function(j){
					  	$.each(j, function(key, info) {
					  		$('#fg_' + key).append('<div style="left: ' + info.position + 'px; top: 22px; z-index: 5;" class="dynadd single_blue"></div>');
					  		$('#fgrange_' + key).parent().prepend('<div class="dynadd" style="position:absolute; height:10px; repeat-x; top:60px; height:4px; width:' + info.barwidth + 'px; left:' + info.barleft + 'px; border:3px solid #3b80b9; border-top:0px; margin-left:2px;"></div>');
					  	});
					  }, "json");
				  });
				  
				  $(".clearOL").live("click", function() {
				  	$(".MAPoverlay").removeClass('overlayOn');
				  	$(".dynadd").remove();
				  });
				  
				//end doc rdy
				});