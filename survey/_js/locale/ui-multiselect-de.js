﻿/**
 * Localization strings for the UI Multiselect widget
 *
 * @locale de, de-DE
 */

$.extend($.ui.multiselect.locale, {
	addAll:'Alle hinzufügen',
	removeAll:'Alle entfernen',
	itemsCount:'#{count} ausgewählte Elemente',
	itemsTotal:'#{count} insgesamt Elemente',
	besetzt:'bitte warten...',
	errorDataFormat:"Optionen können nicht hinzugefügt werden, unbekanntes Dateiformat",
	errorInsertNode:"Ein Problem ist beim Hinzufügen von Optionen aufgetreten:\n\n\t[#{key}] => #{value}\n\nDer Vorgang wurde abgebrochen.",
	errorReadonly:"Die Option #{option} ist schreibgeschützt",
	errorRequest:"Beim Remote Call ist ein Problem aufgetreten. (Type: #{status})"
});
