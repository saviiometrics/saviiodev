	var oTable;

	$(function() {
		oTable = $('.tablesorter').dataTable({
			"iDisplayStart": 0,
			"aaSorting": [],
			"oLanguage": {
				"sUrl": "dtblLocale/dataTables." + $('#currLocale').val() + ".txt"
			},
			"iDisplayLength": 25,
			"sPaginationType": "full_numbers",
			"sDom": '<"filter_container"fl><"top"ip>rt<"bottom"ip<"clear">',
			"bAutoWidth": false
		});

		var selbox = new Array();
  	selbox['map'] = new Array("map_view", false, false, false, false, false, false, false, false);
  	selbox['comp'] = new Array("comp_view", "comp_add", false, "comp_edit", false, false, false, false, "comp_share");
  	selbox['tmap'] = new Array("tmap_view", "tmap_add", "tmap_delete", "tmap_edit", "tmap_archive", false, false, false, false);
  	selbox['user'] = new Array("user_view", "user_add", false, "user_edit", false, "user_retake", "user_resend", "user_activation", false);
  	selbox['imap'] = new Array("imap_view", "imap_add", "imap_delete", "imap_edit", false, false, false, false, false);
		var modulelist = new Array("MAPs", "Accounts", "talentMAPs &amp; teamMAPs", "Users", "idealMAPs");
		var modnames = new Array("map", "comp", "tmap", "user", "imap");

		var accessbox = new Array();
  	accessbox['access'] = new Array("analytics", "admin");
  	var accesslist = new Array("Site Access");
		var accessnames = new Array("access");
		var htmlstr;

		$('.addLevel').live("click", function() {
			//activate addLevel popup
			var addLevelAPI = $("#addlevelbox").overlay({
		    top: 120,
		    mask: {
		        color: '#fff',
		        loadSpeed: 200,
		        opacity: 0.5
		    },
		    closeOnClick: false,
		    onBeforeLoad: function(event) {
					//
		    },
		    onLoad: function() {
					//
		    },
		    onClose: function() {
		    	$("#addlevelform").validationEngine('hideAll');
		    },
		    api: true
			});
			addLevelAPI.load();

			htmlstr = '';
	  	htmlstr = '<table border="0" cellpadding="2" cellspacing="0"><thead><tr><th width="120">Information</th><th width="100">View</th><th width="100">Add</th><th width="100">Delete</th><th width="100">Edit</th><th width="100">Archive</th><th width="100">Retake</th><th width="100">Resend</th><th width="100">Activation</th><th width="100">Sharing</th></tr></thead><tbody>';
  		$.each(modulelist, function(modindex, modvalue) {
	  		htmlstr += '<tr><td width="120">' + modvalue + '</td>';
	  		var selarr = selbox[modnames[modindex]];
	  		$.each(selarr, function(selindex, selboxname) {
	  			if(selboxname == false) {
	  				htmlstr += '<td width="100">&nbsp;</td>';
	  			} else {
	  				var CND = '';
	  				//if adding then it needs to be TRUE/FALSE not ALL/MA etc etc.. selindex 1 (i.e. 2nd in array) is the adding ones :)
	  				if(selindex == 1) {
	  					htmlstr += '<td width="100"><select name="' + selboxname + '" id="' + selboxname + '" class="' + selboxname + '"><option value="TRUE">Yes</option><option value="FALSE">No</option></select></td>';
	  				} else {
	  					htmlstr += '<td width="100"><select name="' + selboxname + '" id="' + selboxname + '" class="' + selboxname + '"><option value="ALL">Master</option><option value="MA">MA</option><option value="SA">SA</option><option value="CREATED">Admin</option></select></td>';
	  				}
	  			}
	  		});
	  		htmlstr += '</tr>';
  		});
  		htmlstr += '</tbody></table>';

  		//site access
  		htmlstr += '<br /><div class="addLevelboxheader"><strong>Site Access</strong></div><br/><table border="0" cellpadding="2" cellspacing="0"><tr><td width="120">Information</td><td width="70">Analytics</td><td width="70">Admin</td></tr></table>';
  		htmlstr += '<table border="0" cellpadding="2" cellspacing="0">';
  		$.each(accesslist, function(modindex, modvalue) {
	  		htmlstr += '<tr><td width="120">' + modvalue + '</td>';
	  		var selarr = accessbox[accessnames[modindex]];
	  		$.each(selarr, function(selindex, selboxname) {
	  			if(selboxname == false) {
	  				htmlstr += '<td width="70"></td>';
	  			} else {
	  				if(selboxname == 'map_view') {
	  					var CND = ' checked="checked" disabled="disabled"';
	  				} else {
	  					var CND = '';
	  				}
	  				htmlstr += '<td width="70"><input type="checkbox" name="' + selboxname + '" class="' + selboxname + '" ' + CND + ' /></td>';
	  			}
	  		});
	  		htmlstr += '</tr>';
  		});
  		htmlstr += '</table>';

		  $("#addcontent").html(htmlstr);
	  });

	  var modstring = '';
	  $('.saveLevel').live("click", function() {
	  	var valid = $("#addlevelform").validationEngine('validate');
	  	if(valid == true) {
		  	modstring = '';
				var selnames = new Array();
				$('select').each(function(){
				  selnames[$(this).attr("name")] = $(this).attr("value");
				});
				var rightsObj = [];
				//zomg... multidimensional objects ftw.. well arrays! :)
				rightsObj.push({map:{view:selnames['map_view']}, company:{add:selnames['comp_add'], view:selnames['comp_view'], edit:selnames['comp_edit'], share:selnames['comp_share']}, tmap:{add:selnames['tmap_add'], deleteMe:selnames['tmap_delete'], view:selnames['tmap_view'], edit:selnames['tmap_edit'], archive:selnames['tmap_archive']}, imap:{add:selnames['imap_add'], deleteMe:selnames['imap_delete'], view:selnames['imap_view'], edit:selnames['imap_edit']}});
	  		rightsObj.push({user:{add:selnames['user_add'], view:selnames['user_view'], edit:selnames['user_edit'], retake:selnames['user_retake'], resend:selnames['user_resend'], activation:selnames['user_activation']}});

	  		if($('.analytics').is(':checked')) {
	  			var seeAnalytics = 'TRUE';
	  		} else {
	  			var seeAnalytics = 'FALSE';
	  		}

	  		 if($('.admin').is(':checked')) {
	  			var seeAdmin = 'TRUE';
	  		} else {
	  			var seeAdmin = 'FALSE';
	  		}

	  		$('.moduleaccess').each(function(index, value) {
	  			if($(this).is(':checked')) {
	  				modstring += this.id.substr(6) + ',';
	  			}
	  		});
	  		modstring = modstring.substr(0, modstring.length-1);
	  		rightsObj.push({siteaccess:{analytics:seeAnalytics, admin:seeAdmin}});

	  		//submit that shit!
				$.post("AjaXUser.php", { levelObj: rightsObj, levelName: $('#levelName').val(), minAccess: $('#minAccess').val(), modids: modstring, addLevel: true },
			  function(data){
	  			 $.jGrowl(data, {
						header:'<strong>System Message</strong>',
						closeTemplate:'<div class="jGrowlCloseMe">close (x)</div>',
						position:'bottom-right'
					});
			  });
			}//end check valid
	  });

	  $('.addAccount').live("click", function() {
			//activate addAccount popup
			var addAccountAPI = $("#addAccountBox").overlay({
		    top: 120,
		    mask: {
		        color: '#fff',
		        loadSpeed: 200,
		        opacity: 0.5
		    },
		    closeOnClick: false,
		    onBeforeLoad: function(event) {
					//
		    },
		    onLoad: function() {
					//
		    },
		    onClose: function() {
		    	$("#addAccountForm").validationEngine('hideAll');
		    },
		    api: true
			});
			addAccountAPI.load();

	  });//end addAccount

	  //set global vars
	  var useCustomLimits;
	  var curraccid;
	  var serviceid;
	  var currRow;
	  var limitBoxes = new Array("maps", "accounts", "subaccounts", "teamMAPs", "talentMAPs", "languages", "users", "emails");

	  $('.editAccount').live("click", function() {
	  	curraccid = this.id.substr(8);
	  	currRow = $(this).closest("tr").get(0);
			//activate addAccount popup
			var editAccountAPI = $("#editAccountBox").overlay({
		    top: 120,
		    mask: {
		        color: '#fff',
		        loadSpeed: 200,
		        opacity: 0.5
		    },
		    closeOnClick: false,
		    onBeforeLoad: function(event) {

			  	$.post("AjaXUser.php", { accid:curraccid, getAccount: true },
				  function(j){
				  	$.each(j, function(index, value) {
				  		$('#total_' + index).html(value);
				  	});
				  	useCustomLimits = j.useAccLimits;
				  	(useCustomLimits == 1) ? $('#toggleCustom').addClass('toggleCustomOn').removeClass('toggleCustomOff') : $('#toggleCustom').addClass('toggleCustomOff').removeClass('toggleCustomOn');
				  	serviceid = j.serviceid;
				  	$('#edit_accname').val(j.accname);
				  	$('#edit_servicelevel').val(j.serviceid);
				  	$('#edit_accemail').val(j.accEmail);
            $("#accid_data").val(JSON.stringify(j));
				  }, "json");
		    },
		    onLoad: function() {
				  $.post("AjaXUser.php", { accid:curraccid, useLimits:useCustomLimits, serviceid:serviceid, getLimits: true },
				  function(j){
				  	$.each(j, function(index, value) {
				  		//if custom limits aren't set then disable the edit boxes!
				  		if(useCustomLimits == 0) {
				  			$('#edit_' + index).attr('disabled', true);
				  		} else {
				  			$('#edit_' + index).attr('disabled', false);
				  		}
				  		$('#edit_' + index).val(value);
				  	});
				  }, "json");

          data = JSON.parse($("#accid_data").val());
          $("#total_idealMAPs").html(data.ideal);
		    },
		    onClose: function() {
		    	$("#editAccountBox").validationEngine('hideAll');
		    },
		    api: true
			});
			editAccountAPI.load();

	  });//end addAccount

	  //sets custom limits to yes or no. Also update the tablesorter value
	  $('#toggleCustom').live("click", function() {
	  	(useCustomLimits == 0) ? clVal = 1 : clVal = 0;
	  	$.each(limitBoxes, function(index, value) {
	  		(clVal == 0) ? $('#edit_' + value).attr('disabled', true) : $('#edit_' + value).attr('disabled', false);
	  	});

	  	$.post("AjaXUser.php", { accid:curraccid, useLimits:clVal, updateCustomLimits: true },
	  	 function(j){
  	 		$.jGrowl(j.message, {
					header:'<strong>System Message</strong>',
					closeTemplate:'<div class="jGrowlCloseMe">close (x)</div>',
					position:'bottom-right'
				});
				if(clVal == 0) {
					oTable.fnUpdate( j.clMsg, oTable.fnGetPosition(currRow), 1 );
				} else {
					oTable.fnUpdate( j.clMsg, oTable.fnGetPosition(currRow), 1 );
				}
	  	 	(useCustomLimits == 1) ? $('#toggleCustom').addClass('toggleCustomOn').removeClass('toggleCustomOff') : $('#toggleCustom').addClass('toggleCustomOff').removeClass('toggleCustomOn');
	  	}, "json");

	  	useCustomLimits = clVal;
			//these need updating straight away because if they choose custom limit and then don't go ahead and update the limits then it will be working off acc limits with no limits inside the DB and break nicely :)
	  	$('.updateLimits').trigger("click");

	  });

	  $('.addNewAccount').live("click", function() {
	  	var valid = $("#addAccountForm").validationEngine('validate');
	  	if(valid == true) {
		  	$.post("AjaXUser.php", { serviceLevel:$('#serviceLevel').val(), accName: $('#newAccName').val(), adminEmail: $('#newAdminEmail').val(), salutation: $('#salutation').val(), firstname: $('#savfirstname').val(), surname: $('#savsurname').val(), countryid: $('#countryid').val(), addAccount: true },
			  function(data){
			  	 $("#addAccountBox").overlay().close();
	  			 $.jGrowl(data, {
						header:'<strong>System Message</strong>',
						closeTemplate:'<div class="jGrowlCloseMe">close (x)</div>',
						position:'bottom-right'
					});
					var ai = oTable.fnAddData([
						$('#newAccName').val(),
						'No',
						''
					]);
			  });
			}
	  });

	  $('.incTotal, .decTotal').live("click", function() {
	  	var changeType;
	  	var currValue;
	  	if($(this).hasClass('incTotal')) {
	  		changeType = 'increment';
	  		currValue = $(this).prev('.currTotal').html();
	  		$(this).prev('.currTotal').html(parseFloat(currValue)+1);
	  	  currValue = $(this).prev('.currTotal').html();
	  	} else {
	  		currValue = $(this).next('.currTotal').html();
	  	  $(this).next('.currTotal').html(parseFloat(currValue)-1);
	  		currValue = $(this).next('.currTotal').html();
	  		changeType = 'decrement';
	  	}
	  	var currKey = this.id.substr(4);
	  	$.get("AjaXUser.php", { incDec:changeType, updateKey:currKey, accid:curraccid, updateTotal:true },
			  function(j){
	  			 $.jGrowl(j.message, {
						header:'<strong>System Message</strong>',
						closeTemplate:'<div class="jGrowlCloseMe">close (x)</div>',
						position:'bottom-right'
					});
			  }, "json");
	  });

	  $('.updateAccount').live("click", function() {
	  		$.post("AjaXUser.php", { accid:curraccid, accName:$('#edit_accname').val(), adminEmail:$('#edit_accemail').val(), serviceid:$('#edit_servicelevel').val(), updateAccount:true },
			  function(j){
	  			 $.jGrowl(j.message, {
						header:'<strong>System Message</strong>',
						closeTemplate:'<div class="jGrowlCloseMe">close (x)</div>',
						position:'bottom-right'
					});
			  }, "json");
	  });

	  $('.updateLimits').live("click", function() {
				var limitsObj = ({maps : $('#edit_maps').val(), accounts : $('#edit_accounts').val(), subaccounts : $('#edit_subaccounts').val(), teamMAPs : $('#edit_teamMAPs').val(), talentMAPs : $('#edit_talentMAPs').val(),languages : $('#edit_languages').val(), users : $('#edit_users').val(), emails : $('#edit_emails').val(), idealMAPs : $('#edit_idealMAPs').val() });
	  		$.post("AjaXUser.php", { accid:curraccid, limitsObj:limitsObj, useLimits:useCustomLimits, updateLimits:true },
			  function(j){
	  			 $.jGrowl(j.message, {
						header:'<strong>System Message</strong>',
						closeTemplate:'<div class="jGrowlCloseMe">close (x)</div>',
						position:'bottom-right'
					});
			  }, "json");
	  });


	  //add validation for the add account form
	  $("#addlevelform").validationEngine('attach');
	  $("#addAccountForm").validationEngine('attach');

	});
