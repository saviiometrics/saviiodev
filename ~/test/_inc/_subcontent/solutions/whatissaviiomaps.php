
		<div id="innerContent">
			<div id="contentMenuBar">
	      <div id="contentSiFR">
	      	<h2 class="MWxBox">Saviio MAPs - Motivation and Preferences that predict success.<br /><br />Saviio MAPs improves your individual, team and organisational performance by providing continuous insight into the unique combination of human factors that predict success.</h2>
	      </div>
	      <div class="cleaner"></div>
	     </div>
	     <div id="contentSideGFX">
	     	<img src="/_images/_sidebar/sidebar_maps.jpg" width="194" height="150" />
	     </div>
	     <div class="cleaner"></div>
	     <div class="infoLeft">
				<p><span class="contentHeader">Saviio MAPs</span></p>
				<em>Saviio</em> MAPs (Motivation And Preferences) is an online &quot;Software as a Service&quot; (SaaS) to empower organisations to better understand how people are uniquely motivated in four major areas:</p>
				 <ol>
				 	<li>Relationships</li>
					 <li>Thinking &amp; Planning</li>
					 <li>Making Decisions</li>
					 <li>Getting Things Done</li>
					 </ol>
				  <p>This insight enables individuals, managers and their teams to work more productively and succeed in their roles.</p>
				  <br />
					<span class="contentHeader">How does <em>Saviio MAPs </em>work? </span>
					<p>People make unconscious and conscious judgments about the people they meet during the assessment and selection process.</p>
					<p><em>Saviio MAPs </em>helps individuals to positively influence these judgments using predictive data to support 'gut feel' <br />
					 People want to feel that they will be doing a job that matters to them and where they will have 'significance' - it matters to others too.</p>
					<p><em>Saviio MAPs </em>helps managers to understand the motivational preferences of team members and responds to meet those needs - people that feel understood feel valued and are more likely to stay. <br />
					 People generally leave managers - not organisations.</p>
					<p>People want to be part of something and feel that they &quot;fit in&quot; and belong.</p>
					<p><em>Saviio MAPs </em>helps inform the dialogue between manager, coach and individual in ways that help people feel that they belong to something more than just a means to provide income.</p>
					<p>Once people feel that they 'fit in' and feel part of something that matters to them, they gain more intrinsic motivation from their job, feel more engaged, and are less open to changing their situation.</p>
					<p>Saviio MAPs helps managers to secure and develop people within their organisation.</p>
			 </div>
  	</div>
  	  