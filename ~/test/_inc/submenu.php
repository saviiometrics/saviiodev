    	<?php
    	$menuArray = $controlObj->generateMenu($_GET['section']);
    	$sideGFXArray = $controlObj->getSideGFX($_GET['section']);
    	$sideGFX = $sideGFXArray[rand(0, (count($sideGFXArray)-1))];
    	?>
    	
    	<div id="submenuHolder">
				<ul id="menu" class="noaccordion collapsible">
					<?php
					//loops through the menu array to generate the menuzzz
					foreach($menuArray as $key => $linkArr) {
						if($linkArr['submenu'] != false) {
							$linkme = 'javascript:void(0);';
						} else {
							$linkme = $linkArr['link'];
						}
						if($linkArr['submenu'] == false) {
							if($_GET['subcontent'] == $linkme) {
								$actclass = ' class="active"';
							} else {
								$actclass = '';
							}
							echo '<li><a href="/saviio/content/' . $_GET['section'] . '/' . $linkme . '"' . $actclass . '><b>' . $linkArr['title'] . ' </b></a></li>' . "\n";
						} else {
							if($linkArr['subgroup'] == $_GET['subgroup']) {
								$expandme = ' class="expand"';
							} else {
								$expandme = '';
							}
							echo '<li' . $expandme . '><a href="' . $linkme . '"><b>' . $linkArr['title'] . ' </b></a>' . "\n";
							echo '<ul class="acitem">';
								foreach($linkArr['submenu'] as $subkey => $subArr) {
									if($subArr['link'] == $_GET['subcontent']) {
										$activeSelect = ' class="activeSelect"';
									} else {
										$activeSelect = '';
									}
									echo '<li><a href="/saviio/content/' . $_GET['section'] . '/' . $linkArr['subgroup'] . '/' . $subArr['link'] . '"' . $activeSelect . '>' .  $subArr['title'] . '</a></li>' . "\n";
								}
							echo '</ul>' . "\n";
							echo '</li>' . "\n";
						}
					}
					?>
				 </ul>
    	</div>