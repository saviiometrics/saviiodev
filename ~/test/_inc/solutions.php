
		<div id="innerContent">
			<div id="contentMenuBar">
	      <h1 class="MWxHeader">Our solutions</h1>
	      <div id="contentSiFR">
	      	<h2 class="MWxBox">
	      		<ul>
			      	<li>How do I achieve better quality with fewer resources?</li>
							<li>How can I increase revenues without increasing head-count?</li>
							<li>Where is my focus when coaching less-effective performers?</li>
							<li>How should I reorganise my teams to maximise value?</li>
							<li>How can I define our employee brand to attract the right people?</li>
						</ul>
					</h2>
	      </div>
	      <div class="cleaner"></div>
	     </div>
	     <div id="contentSideGFX">
	     	<img src="/_images/_sidebar/sidebar_analytics.jpg" width="194" height="150" />
	     </div>
	     <div class="cleaner"></div>
	     <div id="maincontentPad">
	     	  <span class="contentHeader">Saviio MAPs</span>
		      <p>Saviio MAPs is an online survey that can compliment other traditional psychometrics. Saviio MAPs provides "the missing piece" of data that are the key to employee success within a role and team.</p> 
					<p>This missing piece predicts employee success based on their motivation and preferences.</p>
					<p>Saviio MAPs describes the way in which a person prefers to work in four relevant areas:-</p>
						<ol>
						<li>Relationships</li> 
						<li>Thinking and Planning</li> 
						<li>Decision Making</li>
						<li>Getting Things Done</li>
					</ol>
					<p>Saviio MAPs predicts success in a role or a team. Professional recruiters and HR professionals are now using Saviio MAPs routinely to increase productivity, minimise attrition and increase employee engagement.</p>
		  	</div>
		  	<div class="infoLeft">
		  		<span class="contentHeader">Ease of Use</span>
		  		<p>Saviio MAPs provides insight into the unique combination of human factors that predict success - in the 'here and now' not the 'there and then' often associated with  normative psychometrics.</p>
					<p>Saviio MAPs is accessed on line, 24/7 through a secure portal. Employees are  automatically sent a secure log in and on completion of the twenty minute  survey, the detailed report of their Saviio MAP is immediately available online.</p>
					<p>There are three types of Saviio MAP Reports:</p>
					<ul>
					  <li>For an individual (personal MAP) </li>
					  <li>Useful to track changes in motivational preferences over time and used as part of the PDR (Personal Development Review)</li>
					  <li>For a Team (teamMAP)</li>
					  <li>Designed to ensure that each team member recognises the working styles and preferences of each other in the team - enabling each team member to play to their strengths more often.</li>
					  <li>For a job role (talentMAP)</li>
					  <li>A benchmarking tool for gathering data from those who excel in that role and then applying those data for new-hires - helping to predict which candidate is likely to be most successful in that role, with a specific manager and in a specific team - over and above the skills or experience that they already have.</li>
					</ul>
		  	</div>
  	</div>
  	