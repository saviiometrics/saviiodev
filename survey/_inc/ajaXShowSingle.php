 <?php

	/*******************************************************************/
	/***************** SHOW TOP TEN DEFAULT STATEMENTS *****************/
	/*******************************************************************/
	$_SESSION['rightType'] = 'single';
	echo '<div style="float:left; position:relative; min-height:100px; width:620px;">';
	
	//sort all diffs and devs hehe!
	if($_SESSION['leftType'] != 'MAP' && $_SESSION['leftType'] != 'idealMAP') {
		$matching = 'diff';
		uasort($arrDeviations[$rankid]['allinfo'], cmpdevleft);
	}

	$gcount = 0;
	$avcount = 0;
	$pcount = 0;
	$qtotal = count($arrDeviations[$rankid]['allinfo']);
	$leftStateGCOUNT = 0;
	
	echo '<div id="genStatBar"><div id="genReportBar">' . _('Generated Report Statistics') . '</div></div>' . "\n";
	echo '<div class="cleaner"></div>';
	
	echo '<div id="clusterLegend">';
	echo '<span class="clusterText">' . _("Factors") . '</span>';
	echo '<table border="0" cellpadding="4" cellspacing="0" style="font-family:Arial; font-size:15px; margin:6px 0px 6px 0px;"><tr>' . "\n";
	echo '<td valign="top"><img src="' . ACCOUNT_PATH . '_images/_map/rel_icon.png" width="19" height="19" /></td><td valign="top">' . _("Relationships") . '</td>' . "\n";
	echo '<td valign="top"><img src="' . ACCOUNT_PATH . '_images/_map/tap_icon.png" width="19" height="19" /></td><td valign="top">' . _("Thinking & Planning") . '</td>' . "\n";
	echo '<td valign="top"><img src="' . ACCOUNT_PATH . '_images/_map/mak_icon.png" width="19" height="19" /></td><td valign="top">' . _("Making Decisions") . '</td>' . "\n";
	echo '<td valign="top"><img src="' . ACCOUNT_PATH . '_images/_map/gtd_icon.png" width="19" height="19" /></td><td valign="top">' . _("Getting Things Done") . '</td>' . "\n";
	echo '</tr></table><br />';
	if($_SESSION['rightType'] == 'TalentMAP' || $_SESSION['leftType'] == 'TalentMAP' || $_SESSION['rightType'] == 'TeamMAP' || $_SESSION['leftType'] == 'TeamMAP') {
		echo '<div id="MAP_PredictorHolder">';
			echo '<span class="clusterText">' . _("MAP Predictor Strength") . '</span>';
			echo '<table border="0" cellpadding="4" cellspacing="0" style="font-family:Arial; font-size:15px; margin:6px 0px 6px 0px;"><tr>' . "\n";
			echo '<td valign="top"><img src="' . ACCOUNT_PATH . '_images/_map/excellent_bullet.png" /></td><td valign="top">' . _("Excellent") . '</td>' . "\n";
			echo '<td valign="top"><img src="' . ACCOUNT_PATH . '_images/_map/good_bullet.png" /></td><td valign="top">' . _("Good") . '</td>' . "\n";
			echo '<td valign="top"><img src="' . ACCOUNT_PATH . '_images/_map/poor_bullet.png" /></td><td valign="top">' . _("Average") . '</td>' . "\n";
			echo '<td valign="top"><img src="' . ACCOUNT_PATH . '_images/_map/bad_bullet.png" /></td><td valign="top">' . _("Poor") . '</td>' . "\n";
			echo '</tr></table>';
		echo '</div>';	
	}
	echo '<div id="BehaviourFlexLegend">' . "\n";
		echo '<span class="clusterText">' . _("Behavioural Flexibility") . '</span><br />' . "\n";
		echo '<div class="behaviourBarText">' . _("Behavioural Range"). ' &nbsp;<img src="' . ACCOUNT_PATH . '_images/_map/behaviour_bar.png" /><br /><br />' . "\n";
		echo '</div>' . "\n";
	echo '</div>' . "\n";
	echo '</div>'; 
	echo '<div class="cleaner"></div>';

	if($_SESSION['leftType'] == 'TalentMAP' || $_SESSION['leftType'] == 'TeamMAP') {
			$leftmapids = $arrFactorScores[$rankid]['l_mapids'];
			$leftmaparr = explode(',',$leftmapids);
			$lmcount = count($leftmaparr);
			//calc star score for each q!
			function calcStar($nummaps, $stdev) {
				$const = 0.01;
				$rating = ($nummaps / ($stdev + $const));
				return $rating; 
			}
			if($_SESSION['leftType'] == 'TalentMAP') {
				$heightval = 170;
			} else {
				$heightval = 56;
			}
			
			if($_SESSION['leftType'] == 'MAP') {
				$titleBarTXT = _('Top 10 Matched Statements');
			} else {
				$titleBarTXT = _('Top Predictive Statements');
			}
			echo '<div class="topTenSingleHolder"><div id="toptenbar">' . $titleBarTXT . '</div></div>' . "\n";
			
			$i = 0;
			
			//include('lb_rb_hovers.php');
			$stddevCount = 0;
			$clusterPredictorCount = array();
			$clusterPredictorCount[1] = 0;
			$clusterPredictorCount[2] = 0;
			$clusterPredictorCount[3] = 0;
			$clusterPredictorCount[4] = 0;
		
			foreach($arrDeviations[$rankid]['allinfo'] as $qid => $value) {
				if($value['stddev_l'] <= 2.01) {
					//this increments the number of predictive statements at a per cluster level (i.e less than or equal to 2.01)
					$clusterPredictorCount[$value['clusid']]++;
				}
				$strating = calcStar($lmcount, $value['stddev_l']);
				if($strating >= 8 ) {
					$leftStateGCOUNT++;
				} else if ($strating >= 6) {
					$leftStateAVCOUNT++;
				} else if($strating < 6) {
					$leftStatePCOUNT++;
				}
				//loop array of q's stored in as customs... so then do foreach of those then do $arrDeviations[$rankid]['allinfo']['KEYHERE_QID']['lscore'] etc to work values and display in order! (as keys will have been moved by sort);
				if($i < 10) {
					if($value['lscore'] <= 5.0) {
						$qresponse = $stquestions[$qid]['l'];
					} else if($value['lscore'] >= 5.01) {
						$qresponse = $stquestions[$qid]['r'];
					}
					//get class to show
					if($_SESSION['leftType'] == 'MAP') {
						$stddevclass = 'SDNeutralBullet';
					} else {
						$stddevclass = getDevClass($value['stddev_l']);
					}
					echo '<div class="showStatementSi"><li class="' . $stddevclass . '">' . _($qresponse) . '</li></div><div class="XDiF_Blue">' . getClusterABRID($value['clusid']) . '</div><br />' . "\n";
				}
				$i++;
			}
		echo '</div>';
		echo '<div class="cleaner"></div><br />' . "\n";
	 }//end check type of map
	 
   foreach($cids as $cid => $value) {
   	  $clusname = $value;
    	switch($cid) {
    		case 1:
    		$clust_class = 'RELHeaderS';
    		break;
    		case 2:
    		$clust_class = 'TAPHeaderS';
    		break;
    		case 3:
    		$clust_class = 'DECHeaderS';
    		break;
    		case 4:
    		$clust_class = 'GTDHeaderS';
    		break;
    	}
    	
			if($_SESSION['leftType'] == 'MAP' || $_SESSION['leftType'] == 'idealMAP') {
  			uasort($arrDeviations[$rankid][$cid], cmpdiff);
  		} else {
  			uasort($arrDeviations[$rankid][$cid], cmpdevleft);
  		}

    	echo '<div class="cluster_container">';
    	if($_SESSION['leftType'] == 'MAP') {
    		echo '<div class="' . $clust_class. '">' . _($value) .  '<div class="clusterClosed">&nbsp;</div></div>';
    	} else {
    		if($clusterPredictorCount[$cid] != 1) {
    			$predictText = sprintf (_("%s statements are predictive"), $clusterPredictorCount[$cid]);
    		} else {
    			$predictText = sprintf (_("%s statement is predictive"), $clusterPredictorCount[$cid]);
    		}
    		echo '<div class="' . $clust_class. '">' . _($value) .  '<div class="clusterClosed">' . $predictText . '</div></div>';
    	}
  		echo '<div class="statementHolder">';
			echo '<ul>';
					

			foreach($arrDeviations[$rankid][$cid] as $qid => $value) {
				if($value['lscore'] <= 5.0) {
					$qresponse = $stquestions[$qid]['l'];
				} else if($value['lscore'] >= 5.1) {
					$qresponse = $stquestions[$qid]['r'];
				}
				
				//get class to show
					if($_SESSION['leftType'] == 'MAP') {
						$stddevclass = 'SDNeutralBullet';
					} else {
						$stddevclass = getDevClass($value['stddev_l']);
					}
				echo '<div class="showStatementSi"><li class="' . $stddevclass . '">' . _($qresponse) . '</li></div><div class="XDiF_Standard">' . round($value['lscore'],1) . '</div><br>';
			}
			echo '</ul>';
    	echo '</div>';
    	echo '<div class="cleaner"></div>';
    	echo '<div class="inner_cluster">';
	   	$fgcount = 0;
			$clusterscore = 0;
    		foreach($fgs as $fg => $clusterid) {
  				$leftpositive = "";
    			$leftnegative = "";
    			$leftfacdesc = "";
    			$rightpositive = "";
    			$rightnegative = "";
    			$rightfacdesc = "";
    			foreach($fids as $fid => $info) {
    				if($info['factor_group'] == $fg) {
    					if($info['factor_align'] == 'l') {
    						$left = $info['factor_text'];
    						
    						//get pos and neg for this factor group (left side)
    						$thisfactors = $fdescs[$fid];
    						foreach($thisfactors as $type => $factdescs) {
    							foreach($factdescs as $fact) {
    								if($type == 'positive') {
    									$leftpositive .= '<li>' . _($fact) . '</li>';
    								} else if($type == 'negative') {
    									$leftnegative .= '<li>' . _($fact) . '</li>';
    								} else if($type == 'desc') {
    									$leftfacdesc = _($fact);
    								}
    							}
    						}
    					} else if ($info['factor_align'] == 'r') {
    						$right = $info['factor_text'];
    						
    						//get pos and neg for this factor group (right side)
    						$thisfactors = $fdescs[$fid];
    						foreach($thisfactors as $type => $factdescs) {
    							foreach($factdescs as $fact) {
    								if($type == 'positive') {
    									$rightpositive .= '<li>' . _($fact) . '</li>';
    								} else if($type == 'negative') {
    									$rightnegative .= '<li>' . _($fact) . '</li>';
    								} else if($type == 'desc') {
    									$rightfacdesc = _($fact);
    								}
    							}
    						}
    						
    					}
    				}
    			}
    			if($clusterid == $cid) {
    				
    				$fgcount++;
	    			$intNumF = $arrFactorScores[$rankid][$fg]['num'];
						$intModF = $arrFactorScores[$rankid][$fg]['diff'];
						$intScoreMin = min($arrFactorScores[$rankid][$fg]['lscoreall']);
						$intScoreMax = max($arrFactorScores[$rankid][$fg]['lscoreall']);
						
						//this value = 1 point on the score bar
						$WsConstant = 17.8;
						$clusterscore += $permatch;
						
						$intScoreWidth = (round((($intScoreMax - $intScoreMin) * $WsConstant),2) - 8);
						$intScoreWidth = number_format($intScoreWidth,2,'.','');
						
    				echo '<div class="leftfactor">' . _($left) . '</div>';
    				echo '<div class="tooltip"><div class="tooltiptop"></div><div class="tooltipmid"><span class="tooltipfactor">' . _($left) . '</span><br /><br />' . _($leftfacdesc) . '<br /><br /><span class="factorPos">' . _("Constructive Behaviours") . '</span><br /><ul>' . $leftpositive . '</ul><span class="factorNeg">' . _("Less Constructive Behaviours") . '</span><ul>' . $leftnegative . '</ul></div><div class="tooltipbtm"></div></div>' . "\n";
    				echo '<div class="slider_con">';
    					echo '<div id="fgrange_' . $fg . '" style="position:absolute; height:10px; repeat-x; top:14px; height:4px; width:' . $intScoreWidth . 'px; left:' . round(($WsConstant * $intScoreMin), 0) . 'px; border:3px solid grey; border-bottom:0px; margin-left:2px;"></div>';
	    			  echo '<div class="resultslider" id="fg_' . $fg . '">';
		    			  $lf_total = number_format($arrFactorScores[$rankid][$fg]['lscore'],2,'.','');
		    			  $fac_total = number_format($arrFactorScores[$rankid][$fg]['num'],2,'.','');
		    			  $roundnum_l = $lf_total/$fac_total;
		    			  $engnum_l = number_format($roundnum_l, 2, '.', '');
		    			  //infoDesc class activates the left brain / right brain popup
		    			 	$lbrb_tt_left = '<div class="tooltip"><div class="tooltiptop"></div><div class="tooltipmid">' . lrBrain($engnum_l, $cid) . '</div><div class="tooltipbtm"></div></div>' . "\n";
	    			  	if($_SESSION['leftType'] == 'MAP') {
									echo '<div class="single_grey infoDesc" style="left:' . number_format((($engnum_l * $WsConstant)-11),2,'.','') . 'px; top:22px; z-index:6;"></div>';
								} else if ($_SESSION['leftType'] == 'idealMAP') {
									echo '<div class="ideal_grey infoDesc" style="left:' . number_format((($engnum_l * $WsConstant)-16),2,'.','') . 'px; top:24px; z-index:6;"></div>';
								} else {
									echo '<div class="cross_grey infoDesc" style="left:' . number_format((($engnum_l * $WsConstant)-15),2,'.','') . 'px; top:26px; z-index:6;"></div>';
								}
								echo $lbrb_tt_left;
							echo '</div>';
    			  echo '</div>';
    			  echo '<div class="rightfactor">' . _($right) . '</div>' . "\n";
    			  echo '<div class="tooltip"><div class="tooltiptop"></div><div class="tooltipmid"><span class="tooltipfactor">' . _($right) . '</span><br /><br />' . _($rightfacdesc) . '<br /><br /><span class="factorPos">' . _("Constructive Behaviours") . '</span><br /><ul>' . $rightpositive . '</ul><span class="factorNeg">' . _("Less Constructive Behaviours") . '</span><ul>' . $rightnegative . '</ul></div><div class="tooltipbtm"></div></div>' . "\n";
    			  echo '<div class="factorscore">' . round(($arrFactorScores[$rankid][$fg]['lscore']/$arrFactorScores[$rankid][$fg]['num']),1) . '</div>' . "\n";
    				echo '<div class="cleaner"></div>';
    				
    			}//end if clusterid;
    		}

    	echo '<br />';
    	echo '</div>';//end inner cluster
    	echo '</div>';
    }
    ?>