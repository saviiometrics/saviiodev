(function($) {
	$.fn.validationEngineLanguage = function() {};
	$.validationEngineLanguage = {
		newLang: function() {
			$.validationEngineLanguage.allRules = 	{"required":{    			// Add your regex rules here, you can take telephone as an example
						"regex":"žiadna",
						"alertText":"* Toto pole je povinné",
						"alertTextCheckboxMultiple":"* Prosím, zvolte niektorú možnost",
						"alertTextCheckboxe":"* Toto polícko je povinné"},
					"length":{
						"regex":"žiadna",
						"alertText":"*Od ",
						"alertText2":" do ",
						"alertText3": " znakov je povolených"},
					"maxCheckbox":{
						"regex":"žiadna",
						"alertText":"* Prekrocený pocet oznacených"},
					"minCheckbox":{
						"regex":"žiadna",
						"alertText":"* Prosím, vyberte ",
						"alertText2":" možnosti"},
					"confirm":{
						"regex":"žiadna",
						"alertText":"* Vaše heslá sa nezhodujú"},
					"telephone":{
						"regex":"/^[0-9\-\(\)\ ]+$/",
						"alertText":"* Neplatné telefónne císlo"},
					"email":{
						"regex":"/^[a-zA-Z0-9_\.\-]+\@([a-zA-Z0-9\-]+\.)+[a-zA-Z0-9]{2,4}$/",
						//"regex":"/^[a-zA-Z0-9_\.-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9\.]{2,5}$/",
						"alertText":"* Neplatná e-mailová adresa"},
					"date":{
                         "regex":"/^[0-9]{4}\-\[0-9]{1,2}\-\[0-9]{1,2}$/",
                         "alertText":"* Neplatný dátum, musí byt vo formáte RRRR-MM-DD"},
					"onlyNumber":{
						"regex":"/^[0-9\ ]+$/",
						"alertText":"* Len císla"},
					"noSpecialCharacters":{
						"regex":"/^[0-9a-zA-Z]+$/",
						"alertText":"* Nie sú povolené žiadne zvláštne znaky"},
					"ajaxUser":{
						"file":"validateUser.php",
						"extraData":"",
						"alertTextOk":"* Toto meno je k dispozícii",
						"alertTextLoad":"* Nacítava sa, cakajte",
						"alertText":"* Toto meno sa už používa"},
					"ajaxName":{
						"file":"validateUser.php",
						"alertText":"* Toto meno sa už používa",
						"alertTextOk":"* Toto meno je k dispozícii",
						"alertTextLoad":"* Nacítava sa, cakajte"},
					"onlyLetter":{
						"regex":"/^[a-zA-Z\ \']+$/",
						"alertText":"* Len písmená"}
					}	
		}
	}
})(jQuery);

$(document).ready(function() {	
	$.validationEngineLanguage.newLang()
});