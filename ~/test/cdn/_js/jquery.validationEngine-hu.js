

(function($) {
	$.fn.validationEngineLanguage = function() {};
	$.validationEngineLanguage = {
		newLang: function() {
			$.validationEngineLanguage.allRules = 	{"required":{    			// Add your regex rules here, you can take telephone as an example
						"regex":"egyik sem",
						"alertText":"* Kötelezoen kitöltendo mezo ",
						"alertTextCheckboxMultiple":"* Válasszon egy opciót ",
						"alertTextCheckboxe":"* Ezt a jelölodobozt kötelezo kiválasztani "},
					"length":{
						"regex":"egyik sem",
						"alertText":"*Között ",
						"alertText2":" és ",
						"alertText3": " karakter engedélyezett"},
					"maxCheckbox":{
						"regex":"egyik sem",
						"alertText":"* Túllépte az engedélyezett bejelölések számát"},	
					"minCheckbox":{
						"regex":"egyik sem",
						"alertText":"* Kérjük, válasszon ",
						"alertText2":" opciók"},	
					"confirm":{
						"regex":"egyik sem",
						"alertText":"* Hibás jelszót adott meg"},		
					"telephone":{
						"regex":"/^[0-9\-\(\)\ ]+$/",
						"alertText":"* Telefonszám érvénytelen "},	
					"email":{
						"regex":"/^[a-zA-Z0-9_\.\-]+\@([a-zA-Z0-9\-]+\.)+[a-zA-Z0-9]{2,4}$/",
						//"regex":"/^[a-zA-Z0-9_\.-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9\.]{2,5}$/",
						"alertText":"* Érvénytelen e-mail cím"},	
					"date":{
                         "regex":"/^[0-9]{4}\-\[0-9]{1,2}\-\[0-9]{1,2}$/",
                         "alertText":"* Érvénytelen dátum, a formátum csak ÉÉÉÉ-HH-NN lehet"},
					"onlyNumber":{
						"regex":"/^[0-9\ ]+$/",
						"alertText":"* Csak számok "},	
					"noSpecialCharacters":{
						"regex":"/^[0-9a-zA-Z]+$/",
						"alertText":"* Különleges karakterek nem adhatók meg "},	
					"ajaxUser":{
						"file":"validateUser.php",
						"extraData":"",
						"alertTextOk":"* Ez a felhasználó nem elérheto ",	
						"alertTextLoad":"* Töltés elindult, kérjük, várjon",
						"alertText":"* Ez a felhasználó már foglalt "},	
					"ajaxName":{
						"file":"validateUser.php",
						"alertText":"* Ez a név már foglalt ",
						"alertTextOk":"* Ez a név nem elérheto ",	
						"alertTextLoad":"* Töltés elindult, kérjük, várjon "},		
					"onlyLetter":{
						"regex":"/^[a-zA-Z\ \']+$/",
						"alertText":"* Csak betuk"}
					}	
		}
	}
})(jQuery);

$(document).ready(function() {	
	$.validationEngineLanguage.newLang()
});
