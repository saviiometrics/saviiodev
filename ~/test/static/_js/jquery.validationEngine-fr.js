﻿

(function($) {
	$.fn.validationEngineLanguage = function() {};
	$.validationEngineLanguage = {
		newLang: function() {
			$.validationEngineLanguage.allRules = 	{"required":{    			// Ajoutez vos règles regex ici, vous pouvez prendre le téléphone comme exemple
						"regex":"aucune",
						"alertText":"* Ce champ est obligatoire",
						"alertTextCheckboxMultiple":"* Veuillez sélectionner une option",
						"alertTextCheckboxe":"* Cette case à cocher est obligatoire"},
					"length":{
						"regex":"aucune",
						"alertText":"*Entre ",
						"alertText2":" et ",
						"alertText3":" caractères permis"},
					"maxCheckbox":{
						"regex":"aucune",
						"alertText":"* Coches permises dépassées"},	
					"minCheckbox":{
						"regex":"aucune",
						"alertText":"* Veuillez sélectionner ",
						"alertText2":" options"},	
					"equals":{
						"regex":"aucune",
						"alertText":"* Vos mots de passe ne correspondent pas"},		
					"telephone":{
						"regex":/^[0-9\-\(\)\ ]+$/,
						"alertText":"* Numéro de téléphone invalide"},	
					"email":{
						"regex":/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.(?:[A-Z]{2}|COM|ORG|NET|EDU|GOV|MIL|BIZ|INFO|MOBI|NAME|AERO|ASIA|JOBS|MUSEUM)$/i,
						//regex":/^[a-zA-Z0-9_\.-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9\.]{2,5}$/,
						"alertText":"* Adresse mail invalide"},	
					"date":{
                         "regex":/^[0-9]{4}\-\[0-9]{1,2}\-\[0-9]{1,2}$/,
                         "alertText":"* Date invalide, doit être au format YYYY-MM-DD"},
					"onlyNumber":{
						"regex":/^[0-9\ ]+$/,
						"alertText":"* Numérique seulement"},	
					"noSpecialCharacters":{
						"regex":/^[0-9a-zA-Z]+$/,
						"alertText":"* Aucuns caractères spéciaux autorisés"},	
					"ajaxUser":{
						"file":"validateUser.php",
						"extraData":"",
						"alertTextOk":"* Cet utilisateur est disponible",	
						"alertTextLoad":"* Chargement en cours, veuillez patienter",
						"alertText":"* Cet utilisateur est déjà assigné"},	
					"ajaxName":{
						"file":"validateUser.php",
						"alertText":"* Ce nom est déjà assigné",
						"alertTextOk":"* Ce nom est disponible",	
						"alertTextLoad":"* Chargement en cours, veuillez patienter"},		
					"onlyLetter":{
						"regex":/^[a-zA-Z\ \']+$/,
						"alertText":"* Alphabétique seulement"}
            };
            
        }
    };
    $.validationEngineLanguage.newLang();
})(jQuery);


