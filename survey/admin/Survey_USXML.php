<?php
if(!isset($_SESSION)) { session_start(); }
$phpsessid = session_id();

//include localization and site config files
require_once("../site.config.php");

include CONTENT_PATH . '/_classes/db-class.php';
include CONTENT_PATH . '/_classes/account-class.php';
$accobj = new Account($_SESSION['accid']);

//include other classes
include FULL_PATH . '/_inc/_classes/user-class.php';
include FULL_PATH . '/_inc/_classes/admin-class.php';

require_once(FULL_PATH . "/_inc/localization.php");
require_once(FULL_PATH . "/_inc/scripts.php");

function get_months($date1, $date2) {
   $time1  = $date1;
   $time2  = $date2;
   $my     = date('mY', $time2);
	
	 $months = array();
	 $i = 0;
   $months[$i] = date('F', $time1);
	 
   while($time1 < $time2) {
   	$i++;
      $time1 = strtotime(date('Y-m-d', $time1).' +1 month');
      if(date('mY', $time1) != $my && ($time1 < $time2)) {
         $months[$i] = date('F', $time1);
      }
   }

   $months[] = date('F', $time2);
   return $months;
} 

$adminobj = new Admin($_SESSION['uid']);
// create a new XML document
$doc = new DomDocument('1.0');
// create root node

if($_GET['amtype'] == 'pie') {
	$root = $doc->createElement('pie');
	$root = $doc->appendChild($root);
	
	/***************************************************************
	****************ALL QUERIES FOR XML GEN HERE********************
	****************************************************************/
	$gtitle = $_GET['surveyname'];
	
	$labelroot = $doc->createElement('labels');
	$labelroot = $root->appendChild($labelroot);
	$value = $doc->createElement('label');
	$value = $labelroot->appendChild($value);
	$value->setAttribute('lid', 0);
	$labvals = $doc->createElement('x');
	$labvals = $value->appendChild($labvals);
	$valuenode = $doc->createTextNode(0);
	$valuenode = $labvals->appendChild($valuenode);
	$labvals = $doc->createElement('y');
	$labvals = $value->appendChild($labvals);
	$valuenode = $doc->createTextNode(10);
	$valuenode = $labvals->appendChild($valuenode);
	$labvals = $doc->createElement('align');
	$labvals = $value->appendChild($labvals);
	$valuenode = $doc->createTextNode('center');
	$valuenode = $labvals->appendChild($valuenode);
	$labvals = $doc->createElement('text_size');
	$labvals = $value->appendChild($labvals);
	$valuenode = $doc->createTextNode(12);
	$valuenode = $labvals->appendChild($valuenode);
	$labvals = $doc->createElement('text');
	$labvals = $value->appendChild($labvals);
	$valuenode = $doc->createTextNode('<b>' . $gtitle . '</b>');
	$valuenode = $labvals->appendChild($valuenode);
	
	
	//place new xml node in for completed and uncompleted to complete the pie chart. no need for a loop as only returns one record!
	
	//this is company id .. need to send this through dependings on user!
	if(isset($_GET['compid'])) {
		$companyid = $_GET['compid'];
	} else {
		$companyid = $adminobj->companyid;
	}
		
	if($_GET['gtype'] == 'tmap') {
		$results = $adminobj->getTMAPUsage($companyid);
		
		if($results['team'] == '') {
			$teamval = 0;
		} else {
			$teamval = $results['team'];
		}
		if($results['talent'] == '') {
			$talentval = 0;
		} else {
			$talentval = $results['talent'];
		}
		// add node for each row
		$occ = $doc->createElement('slice');
		$occ = $root->appendChild($occ);
		$occ->setAttribute('title', "TeamMAPs");
		$occ->setAttribute('pull_out', "true");
		$value = $doc->createTextNode($teamval);
		$value = $occ->appendChild($value);
		
		// add node for each row
		$occ = $doc->createElement('slice');
		$occ = $root->appendChild($occ);
		$occ->setAttribute('title', "TalentMAPs");
		$value = $doc->createTextNode($talentval);
		$value = $occ->appendChild($value);
		
	} else if ($_GET['gtype'] == 'surveystats') {
		//this is company id .. need to send this through dependings on user!
		$results = $adminobj->getSurveyUsage($companyid);
		
		if($results['complete'] == '') {
			$complete = 0;
		} else {
			$complete = $results['complete'];
		}
		if($results['incomplete'] == '') {
			$incomplete = 0;
		} else {
			$incomplete = $results['incomplete'];
		}
		if($results['issued'] == '') {
			$issued = 0;
		} else {
			$issued = $results['issued'];
		}
		// add node for each row
		$occ = $doc->createElement('slice');
		$occ = $root->appendChild($occ);
		$occ->setAttribute('title', "Completed Surveys");
		$value = $doc->createTextNode($complete);
		$value = $occ->appendChild($value);
		
		// add node for each row
		$occ = $doc->createElement('slice');
		$occ = $root->appendChild($occ);
		$occ->setAttribute('title', "In Progress");
		$value = $doc->createTextNode($incomplete);
		$value = $occ->appendChild($value);
		
		// add node for each row
		$occ = $doc->createElement('slice');
		$occ = $root->appendChild($occ);
		$occ->setAttribute('title', "Issued");
		$occ->setAttribute('pull_out', "true");
		$value = $doc->createTextNode($issued);
		$value = $occ->appendChild($value);
	}
	
} else if($_GET['amtype'] == 'line') {
	
	$gtitle = $_GET['surveyname'];
		
	// create root node
	$root = $doc->createElement('chart');
	$root = $doc->appendChild($root);
		
	// add node for each row
	$occ = $doc->createElement('series');
	$occ = $root->appendChild($occ);

	$gtitle = $_GET['surveyname'];
	
	$labelroot = $doc->createElement('labels');
	$labelroot = $root->appendChild($labelroot);
	$value = $doc->createElement('label');
	$value = $labelroot->appendChild($value);
	$value->setAttribute('lid', 0);
	$labvals = $doc->createElement('x');
	$labvals = $value->appendChild($labvals);
	$valuenode = $doc->createTextNode(0);
	$valuenode = $labvals->appendChild($valuenode);
	$labvals = $doc->createElement('y');
	$labvals = $value->appendChild($labvals);
	$valuenode = $doc->createTextNode(10);
	$valuenode = $labvals->appendChild($valuenode);
	$labvals = $doc->createElement('align');
	$labvals = $value->appendChild($labvals);
	$valuenode = $doc->createTextNode('center');
	$valuenode = $labvals->appendChild($valuenode);
	$labvals = $doc->createElement('text_size');
	$labvals = $value->appendChild($labvals);
	$valuenode = $doc->createTextNode(12);
	$valuenode = $labvals->appendChild($valuenode);
	$labvals = $doc->createElement('text');
	$labvals = $value->appendChild($labvals);
	$valuenode = $doc->createTextNode('<b>' . $gtitle . '</b>');
	$valuenode = $labvals->appendChild($valuenode);
	
	if($_GET['gtype'] == 'tmapline') {
		//get the current month and year
		$currmonth = date('m', time());
		$curryear = date('Y', time());
		
		//get number of months passed through, if non are then simply set it to 5(which is six months) - the 1
		if(!isset($_GET['me'])) {
			if(!$_GET['monthsago']) {
				$monthsago = 3;
			} else {
				$monthsago = $_GET['monthsago'];
			}
			$end_time = time();
			$start_time = mktime(0,0,0,$currmonth-$monthsago, 1, $curryear);
			$monthsarr = get_months($start_time, $end_time);
		} else {
			$start_time = mktime(0,0,0,$_GET['ms'], 1, $_GET['ys']);
			$end_time = mktime(0,0,0,$_GET['me'], 1, $_GET['ye']);
			$monthsarr = get_months($start_time, $end_time);
		}
		
		$selector_arr = array();
		$startmonth = date('m', $start_time);
		$startyear = date('Y', $start_time);
		$h=0;
		foreach($monthsarr as $nummonth => $currmonth) {
			if(!isset($_GET['me'])) {
				$currMS = mktime(0, 0, 0, $startmonth+$nummonth, 1, $startyear);
			} else {
				$currMS = mktime(0, 0, 0, $_GET['ms']+$nummonth, 1, $_GET['ys']);
			}
			$month = date('M', $currMS);
			$year = date('y', $currMS);
			$child = $doc->createElement('value');
			$child = $occ->appendChild($child);
			$child->setAttribute('xid', $nummonth);
			$value = $doc->createTextNode($month . ' ' . $year);
			$value = $child->appendChild($value);
			
			//make the sql data key array for access to results later
			$month = date('M', $currMS);
			$year = date('y', $currMS);
			$sqlval = $month . $year;
			$selector_arr[$h] = $sqlval; 
			$h++;
		}

		/***********************************************************************************/
		/********************************** ALL TYPES FOR LINE *****************************/
		/***********************************************************************************/
		
		// add node for each row
		$graphs = $doc->createElement('graphs');
		$graphs = $root->appendChild($graphs);
		
		$r=1;
		$colorarr = array("#73a1d2", "#e19c46", "#e1a0b6", "#98c69c", "#e8d04d", "#df575d");
		//loop for admin/company ids
		$results = $adminobj->getUsage($monthsarr, $startmonth, $startyear, $_GET['maptype'], $_GET['uidstring'], $_GET['cidstring']);
		foreach($results as $resid => $resultdata) {
		  // add a child node for each field
			$graph = $doc->createElement('graph');
			$graph = $graphs->appendChild($graph);
			$graph->setAttribute('gid', $r);
			$graph->setAttribute('fill_alpha', 14);
			$graph->setAttribute('line_width', 2);
			$graph->setAttribute('bullet', 'round');
			$graph->setAttribute('color', $colorarr[$r-1]);
			$graph->setAttribute('title', $resultdata['gname']);
			$graph->setAttribute('color_hover', "#999999");
			$h=0;
			foreach($monthsarr as $nummonth => $currmonth) {
				$value = $doc->createElement('value');
				$value = $graph->appendChild($value);
				$value->setAttribute('xid', $nummonth);
				$valuenode = $doc->createTextNode($resultdata[$selector_arr[$h]]);
				$valuenode = $value->appendChild($valuenode);
				$h++;
			}
			$r++;
		}


	}//if tmapline

}//end what amtype

// get completed xml document
$doc->encoding = 'utf-8';
$xml_string = $doc->saveXML();

echo $xml_string;
?>
