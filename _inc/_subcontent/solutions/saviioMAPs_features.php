	
		<div id="innerContent">
			<div id="caseStudyTop">
<<<<<<< HEAD
				<h1>A major International Leisure and Travel Organisation</h1>
				<p><strong>Attracting staff who can sell and still provide a safe flying experience</strong></p>
=======
				<h1>Saviio MAPs Features</h1>
				<p><strong>How the features in Saviio MAPs can help your business or team succeed.</strong></p>
>>>>>>> DEV
			</div>
			<div class="cleaner"></div>
	     <div class="caseStudyList">
         <div class="infoLeft">
<<<<<<< HEAD
         	<img src="/_images/_sidebar/sidebar_analytics.jpg" width="194" height="150" align="left" />
         	<strong>The Situation</strong>
         	<p>With rising fuel costs, increased competition in the 'value' airline sector and internet-savvy customers selecting on price, the organisation needed to attract and retain cabin crew who can maximise in-flight SALES whilst maintaining SAFE operating procedures.</p>
         </div>
         <div class="infoRight">
         	<img src="/_images/_sidebar/sidebar_quadman.jpg" width="194" height="150" align="right" />
         	<strong>The Challenges</strong>
         	<p>To recruit and retain cabin crew who are motivated to sell yet maintain rigid safety procedures - a potential recruitment paradox. To achieve the right balance of over and under-servicing passengers so that in-flight sales are maximised and passengers want to fly again with the airline.</p>
         </div>
          <div class="infoLeft">
         	<strong>The Solution</strong>
            <ul>
           		<li>Profile top talent in the role using Saviio TalentMAPs to produce an ideal motivational profile for the job role.  Select, interview, recruit and motivate  using this profile.</li>
							<li>Re-invent the cabin-experience for passengers and crew by changing the language and behaviour of all cabin crew.</li>
							<li>Investment in Saviio c&pound;50k over 6 months.</li>
							<li>Further opportunities to provide portable on demand training devices, content and analytics to improve access to training and product information for cabin crew</li>
						</ul>
         </div>
         <div class="infoRight">
         	<strong>The Outcomes</strong>
=======
         	<img src="/_images/_features/_thumb/mapsingle_small.jpg" width="204" height="153" align="left" />
         	<strong>Find out your strengths at work...</strong>
         	<p>With rising fuel costs, increased competition in the 'value' airline sector and internet-savvy customers selecting on price, the organisation needed to attract and retain cabin crew who can maximise in-flight SALES whilst maintaining SAFE operating procedures.</p>
         </div>
         <div class="infoRight">
         	<img src="/_images/_features/_thumb/mapcommon_small.jpg" width="204" height="153" align="right" />
         	<strong>and what you have in common with others..</strong>
         	<p>To recruit and retain cabin crew who are motivated to sell yet maintain rigid safety procedures - a potential recruitment paradox. To achieve the right balance of over and under-servicing passengers so that in-flight sales are maximised and passengers want to fly again with the airline.</p>
         </div>
          <div class="infoLeft">
          	<img src="/_images/_features/_thumb/mapuncommon_small.jpg" width="204" height="153" align="left" />
         		<strong>and what you don't..</strong>
						<p>To recruit and retain cabin crew who are motivated to sell yet maintain rigid safety procedures - a potential recruitment paradox. To achieve the right balance of over and under-servicing passengers so that in-flight sales are maximised and passengers want to fly again with the airline.</p>
         </div>
         <div class="infoRight">
         	<img src="/_images/_features/_thumb/mapcompare_small.jpg" width="204" height="153" align="right" />
         	<strong>so you don't need to guess what you both need to succeed at work.</strong>
>>>>>>> DEV
         	 <ul>
		       	<li>Average increase of 17% of in-flight sales across all routes (where changes have been implemented).</li>
						<li>Cabin crew say they are more motivated to sell and 'love' the new guidelines on language and behaviour.</li>
						<li>A commitment to roll-out Saviio for all job roles and to implement in-flight changes across all routes</li>
						<li>Return on Investment (at this stage): Savings on improving retention c&pound;100k - In-flight sales up 17%</li>
					</ul>
         </div>
      </div>
		</div>