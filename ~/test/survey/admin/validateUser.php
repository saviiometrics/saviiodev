<?php

if(!isset($_SESSION)) { session_start(); }
$phpsessid = session_id();

//include localization and site config files
require_once("../site.config.php");

//include DB AND ACCOUNT INFO CLASSES
include CONTENT_PATH . '/_classes/db-class.php';
include CONTENT_PATH . '/_classes/account-class.php';
$accobj = new Account($_SESSION['accid']);

include FULL_PATH . '/_inc/_classes/user-class.php';
include FULL_PATH . '/_inc/_classes/admin-class.php';

require_once(FULL_PATH . "/_inc/localization.php");
require_once(FULL_PATH . "/_inc/scripts.php");

/* RECEIVE VALUE */
$validateValue=$_REQUEST['fieldValue'];
$validateId=$_REQUEST['fieldId'];
$validateError=$_REQUEST[''];


/* RETURN VALUE */
$arrayToJs = array();
$arrayToJs[] = $validateId;


$userobj = new User();
$userValid = $userobj->checkUser($validateValue);

//return true or false based on user check
if($userValid == true){		// validate??
	$arrayToJs[] = true;			// RETURN TRUE
} else {
	$arrayToJs[] = false;			// RETURN TRUE
}

$arrayToJs[] = $validateError;
echo json_encode($arrayToJs);			// RETURN ARRAY WITH success

?>