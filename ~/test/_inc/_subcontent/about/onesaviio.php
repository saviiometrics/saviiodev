		<div id="innerContent">
			<div id="contentMenuBar">
	      <div id="contentSiFR">
	      	<h2 class="MWxBox">The oneSaviio Foundation is based on a simple idea: Donate 1% of Saviio's resources to support organisations that are working to improve relationships and make the world a better place.</h2>
	      </div>
	      <div class="cleaner"></div>
	     </div>
	     <div id="contentSideGFX">
	     	<img src="/_images/_sidebar/sidebar_quadman.jpg" width="194" height="150" />
	     </div>
	     <div class="cleaner"></div>
     	<div class="contentPadTop">
	      <p><strong>Our goal: to disseminate the financial, technological and intellectual wealth of our organisation to those in our communities who need it most.</strong></p>
	      <p><strong>That's oneSaviio.</strong></p>
    	</div>
     <div class="infoLeft">
	     	<div style="float:left; width:204px; height:206px; padding:0px 12px 0px 0px;">
		  		<img src="/_images/_sidebar/<?=$sideGFX;?>" width="194" height="150" align="left" />
		  	</div>
		  	<div style="float:left; width:404px; margin:6px 0px 0px 0px;">
		  		<span class="contentHeader">1% time | 1% product | 1% equity</span>
		     	<p><strong>1% time:</strong>We promote a culture of care and help Saviio employees and partners find meaningful ways to spend their six paid volunteer days off and respond to community needs around the globe.</p>
					<p><strong>1% product:</strong>We donate Saviio MAPs licenses to help non-profit organisations to increase their effectiveness - so they can focus their time and resources on the organisation's core mission.</p>
					<p><strong>1% equity:</strong>Founding stock from Saviio Ltd. provides funds for grants, with a specific focus on supporting youth, technology innovation and employee-inspired volunteer projects. We call our integrated philanthropic approach the oneSaviio. We take a small fraction of Saviio's time, product and equity and use it to make an impact in our world.</p>
		  	</div>
	 	  </div>
	  	<div class="infoLeft">
				<p>If you would like to know more about our current oneSaviio projects or request help, <a href="/saviio/content/contact">get in touch...</a></p>
      </div>
	</div>
 
 

 




