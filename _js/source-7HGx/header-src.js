		/*Copyright 2007-2010 by Marco van Hylckama Vlieg
		web: http://www.i-marco.nl/weblog/
		email: marco@i-marco.nl*/
		jQuery.fn.initMenu = function() {  
			    return this.each(function(){
			        var theMenu = $(this).get(0);
			        $('.acitem', this).hide();
			        $('li.expand > .acitem', this).show();
			        $('li.expand > .acitem', this).prev().addClass('active');
			        $('li a', this).click(
			            function(e) {
			                e.stopImmediatePropagation();
			                var theElement = $(this).next();
			                var parent = this.parentNode.parentNode;
			                if($(parent).hasClass('noaccordion')) {
			                    if(theElement[0] === undefined) {
			                        window.location.href = this.href;
			                    }
			                    $(theElement).slideToggle('normal', function() {
			                        if ($(this).is(':visible')) {
			                            $(this).prev().addClass('active');
			                        }
			                        else {
			                            $(this).prev().removeClass('active');
			                        }    
			                    });
			                    return false;
			                }
			                else {
			                    if(theElement.hasClass('acitem') && theElement.is(':visible')) {
			                        if($(parent).hasClass('collapsible')) {
			                            $('.acitem:visible', parent).first().slideUp('normal', 
			                            function() {
			                                $(this).prev().removeClass('active');
			                            }
			                        );
			                        return false;  
			                    }
			                    return false;
			                }
			                if(theElement.hasClass('acitem') && !theElement.is(':visible')) {         
			                    $('.acitem:visible', parent).first().slideUp('normal', function() {
			                        $(this).prev().removeClass('active');
			                    });
			                    theElement.slideDown('normal', function() {
			                        $(this).prev().addClass('active');
			                    });
			                    return false;
			                }
			            }
			        }
			    );
			});
		};
	
		$(function() {

			/*
			$('#twitter').twitterSearch({ 
			    term:   'saviio',
			    bird:    true, 
			    colorExterior: '#ddd', 
			    colorInterior: 'white', 
			    pause:   true, 
			    timeout: 5000,
			    title: '',
			    css: {  
			      img: {  
			        width: '24px', height: '24px'  
			      } 
			    } 
			});
			*/
	    
	    //top / left / link values
	    var slides = new Array();
	    slides[0] = new Array(194, 344, 'freetrial');
	    slides[1] = new Array(182, 720, 'solutions/saviiomaps/whatissaviiomaps');
	    
			$('#saviio_flash').cycle({ 
					fx: 'scrollDown, scrollRight', 
					randomizeEffects: true, 
					speedIn:  2000, 
					speedOut: 800, 
					easeIn:  'easeOutBounce', 
					easeOut: 'easeInBack', 
					before:   onBefore,
					after: onAfter,
					timeout: 8000,
					pause:  1
				});

			function onBefore(currElement, nextElement, opts, isForward) {
				$("#flash_overlay").fadeOut(300);
			}
			
			function onAfter(currElement, nextElement, opts, isForward) {
				var index = opts.currSlide; 
				$("#flash_overlay").css("top",slides[index][0] + 'px');
				$("#flash_overlay").css("left",slides[index][1] + 'px');
				$("#flash_overlay a").attr('href', 'saviio/content/' + slides[index][2]);
				$("#flash_overlay").fadeIn(400);
			}
			
		 //init left menu (if it exists!);
		 $('#menu').initMenu();
		 
		 
		 $('#loginMAPs').click(function() {
		 		$('#loginForm').submit();
		 	});

		});