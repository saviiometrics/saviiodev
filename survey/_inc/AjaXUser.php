 <?php
	
	if(!isset($_SESSION)) { session_start(); }
	$phpsessid = session_id();

	
	//include localization and site config files
	require_once("../site.config.php");
	
	//include DB AND ACCOUNT INFO CLASSES
	include CONTENT_PATH . '/_classes/db-class.php';
	include CONTENT_PATH . '/_classes/account-class.php';
	if(!isset($_SESSION['accid'])) {
		$accobj = new Account(1);
	} else {
		$accobj = new Account($_SESSION['accid']);
	}

	//include other classes
	include FULL_PATH . '/_inc/_classes/MAP-class.php';
	include FULL_PATH . '/_inc/_classes/question-class.php';
	include FULL_PATH . '/_inc/_classes/user-class.php';
	include FULL_PATH . '/_inc/_classes/auth-class.php';
	
	$userobj = new User();
	
	require_once(FULL_PATH . "/_inc/localization.php");
	require_once(FULL_PATH . "/_inc/scripts.php");
	
	if(isset($_GET['completeAuth'])) {
		
		$authobj = new Auth($_GET['authCode']);
		if(!$authobj->AuthUser($_GET['authCode'], $_GET['newPass'])) {
			$msgStr = _("I'm sorry your password was not set please try again");
			$resultArr = array("error" => true, "message" => $msgStr);
			echo json_encode($resultArr);
		} else {
			$msgStr = _("Authentication was successful! Please login using the 'login' button above");
			$resultArr = array("error" => false, "message" => $msgStr);
			echo json_encode($resultArr);
		}
		/*
		if(!$userobj->checkLogin(addslashes($_GET['username']), md5($_GET['newPass']))) {
			$msgStr = _("I'm sorry your username and password were incorrect");
			$resultArr = array("error" => true, "message" => $msgStr);
			echo json_encode($resultArr);
		} else {
			$_SESSION['username'] = addslashes($_GET['username']);
			$_SESSION['passw'] = ($_GET['newPass']);
			$userobj->getUservUN($_GET['username']);
			$_SESSION['uid'] = $userobj->uid;
			$_SESSION['level'] = $userobj->level;
			$_SESSION['accid'] = $userobj->accid;
			$msgStr = _("Authentication was successful!");
			$resultArr = array("error" => false, "message" => $msgStr);
			echo json_encode($resultArr);
		}
		*/
	}
	
	if(isset($_GET['resetPass'])) {
		if(!$userobj->resetPasswordVUN($_GET['lostuser'])) {
			echo _('Your username cannot be found, therefore the password has not been reset');
		} else {
			echo _('An email has sent to you with information on how to reset your password.');
		}
	}
	
	if(isset($_GET['readUpdate'])) {
		if(!isset($_SESSION['uid'])) {
			echo 'FALSE';
		} else {
			if(!$userobj->checkReadUpdate($_SESSION['uid'])) {
				echo 'FALSE';
			} else {
				echo 'TRUE';
			}
		}
	}
	
	//set read to values 1 when people close the update window :)
	if(isset($_GET['setRead'])) {
		//no need for notify here. Maybe it will keep popping up? LOL!
		$userobj->setReadUpdate($_SESSION['uid']);
	}
	
	
	
	?>