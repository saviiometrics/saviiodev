
<div class="contentPadding">
  <h2 class="MWxMedium"><?= _("Forbidden:");?></h2>
  <p><?= _("I'm sorry you do not have access to this page. Either because you are not logged in, or because you do not have the correct access level.");?></p><br />
</div>