<?php

//see if this versions
if(!isset($_SESSION)) { session_start(); }
$phpsessid = session_id();
$start_time=microtime(true);
//ini_set('display_errors',1);


/******************************* CHECK SESSION IS SET AND THAT ARR DEV ARRAY IS ACTUALLY AN ARRAY!!!! ******************************/

//get factor scores set from AjaXGen Script!
$arrFactorScores = $_SESSION['arrFactorScores'];
$arrDeviations = $_SESSION['arrDeviations'];

if(!isset($_SESSION['arrFactorScores']) || !is_array($_SESSION['arrDeviations']) || !isset($_SESSION['arrDeviations']) || !is_array($_SESSION['arrFactorScores'])) {
	printf (_("I'm sorry, there seems to be a problem generating your PDF because your sessions have timed out or been reset! Please close this window and re login to your %s account. Thank You"), 'Saviio.');
	exit;
} else {
	//include localization and site config files
	require_once("site.config.php");
	//include DB AND ACCOUNT INFO CLASSES
	include CONTENT_PATH . '/_classes/db-class.php';
	include CONTENT_PATH . '/_classes/account-class.php';
	$accobj = new Account($_SESSION['accid']);
	//include other classes
	include FULL_PATH . '/_inc/_classes/question-class.php';
	include FULL_PATH . '/_inc/_classes/MAP-class.php';
	include FULL_PATH . '/_inc/_classes/user-class.php';
	include FULL_PATH . '/_inc/_classes/admin-class.php';
	include FULL_PATH . '/_inc/_classes/pdf-class.php';
	//$_GET['pdflocale'] = 'da_DK';
	require_once(FULL_PATH . "/_inc/localization.php");
	require_once(FULL_PATH . "/_inc/scripts.php");
	function getRankOverlay($dispPDFObj, $topLeft, $topRight, $bottomLeft, $bottomRight, $middle) {
		$quadhtml = '<table border="0" width="508" cellpadding="0" cellspacing="0">
			<tr>
				<td align="center"><img src="' . ACCOUNT_PATH . '_images/_pdf/quad_blue.png" /></td>
			</tr>
		</table>';
	
		$xmid = 68;
		$yscore = $pdf->GetY();
		$pdf->writeHTMLCell(0, 0, $xmid, $yscore, $quadhtml, 0, 1, 0, true, 'L');
		//Write scores onto image
		$rankx = 108.2;
	
		$rankHTML = '<table border="0" width="220" cellpadding="0" cellspacing="0">
		<tr>
			<td align="center" height="46"><font size="14" color="' .$dispPDFObj->getWhite() . '">' . $topLeft . '%</font></td>
			<td align="center"><font size="14" color="' .$dispPDFObj->getWhite() . '">' . $topRight . '%</font></td>
		</tr>
		<tr>
			<td align="center" colspan="2" height="62"><font size="14" color="' .$dispPDFObj->getWhite() . '">' . $middle . '%</font></td>
		</tr>
		<tr>
			<td align="center" height="50"><font size="14" color="' .$dispPDFObj->getWhite() . '">' . $bottomLeft . '%</font></td>
			<td align="center"><font size="14" color="' .$dispPDFObj->getWhite() . '">' . $bottomRight . '%</font></td>
		</tr>
		</table>';
		$pdf->writeHTMLCell(0, 0, $rankx, $yscore+5.8, $rankHTML, 0, 1, 0, true, 'L');
	}
	$dispPDFObj = new DisplayPDF();
	$fontn = $dispPDFObj->getFont($_SESSION['locale']);
	$_SESSION['fontn'] = $fontn;
	if(!isset($_REQUEST['orientation']) || $_REQUEST['orientation'] == 'portrait') {
		$PDF_PAGE_ORIENTATION = 'P';
		$height = 16.8;
		$width = 55.5;
		$clusttoptenwidth1 = "30";
		$clusttoptenwidth2 = "540";
		$matchedStatements = "550";
		$clusttoptenwidth3 = "52.5";
		$clusttoptenwidth4 = "0";
		$clusterBar1 = "582";
		$clusterBar2 = "4";
		$clusterBar3 = "52.5";
	} else {
		$PDF_PAGE_ORIENTATION = 'L';
		$height = 16.8;
		$width = 55.5;
		$clusttoptenwidth1 = "3.3%";
		$clusttoptenwidth2 = "90.7%";
		$matchedStatements = "90.7%";
		$clusttoptenwidth3 = "5.7%";
		$clusttoptenwidth4 = "0.3%";
		$clusterBar1 = "92.7%";
		$clusterBar2 = "0.8%";
		$clusterBar3 = "5.7%";
	}
	$pdf = new SaviioTCPDF($PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false, 16.8, 55.5);
	//Benchmark VS Comparison
	//Count matched statements
	$matches = array('count', 'REL'=>array('count','statement'=>'scale'), 'GTD'=>array('count','statement'=>'scale'), 'TAP'=>array('count','statement'=>'scale'), 'MAK'=>array('count','statement'=>'scale'));
	
	//Count mis-matched statements
	$mismatches = array('count', 'REL'=>array('count','statement'=>'scale'), 'GTD'=>array('count','statement'=>'scale'), 'TAP'=>array('count','statement'=>'scale'), 'MAK'=>array('count','statement'=>'scale'));
	
	//MATCHES
	//Write image to page
	getRankOverlay($dispPDFObj, ($matches['REL']['count'] / $matches['count']) * 100, ($matches['TAP']['count'] / $matches['count']) * 100, ($matches['MAK']['count'] / $matches['count']) * 100, ($matches['GTD']['count'] / $matches['count']) * 100, ($matches['count'] / 60) * 100);
	//Write each cluster & matched statements
	
	//MISMATCHES
	//Write image to page
	getRankOverlay($dispPDFObj, ($mismatches['REL']['count'] / $mismatches['count']) * 100, ($mismatches['TAP']['count'] / $mismatches['count']) * 100, ($mismatches['MAK']['count'] / $mismatches['count']) * 100, ($mismatches['GTD']['count'] / $mismatches['count']) * 100, ($mismatches['count'] / 60) * 100);
	//Write each cluster & mismatched statements
	
	
	//Comparison VS Benchmark
	
}//end check if session exists


//============================================================+
// END OF FILE                                                 
//============================================================+
?>
