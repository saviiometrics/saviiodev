<?php
require_once("site.config.php");
/**
		* MSajax.php
		*
		* The results returned by this script matches the format found in data.txt,
		* ie: "key=value", one per line.
		*
		* Arguments :
		*
		* q the query string to search. The provided string will be
		* tested against the "value" part of the data (case insensitive)
		* Ex: q=en will match any data containing *en*
		* limit (optional) specify a limit of results to return. If not
		* specified, the default value is DEFAULT_LIMIT
		* Ex: limit=20 will not return more than 20 results even
		* if more matches
		*
		*
		*/
if(!isset($_SESSION)) { session_start(); }

//include localization and site config files
		//include DB AND ACCOUNT INFO CLASSES
		include CONTENT_PATH . '/_classes/db-class.php';
		include CONTENT_PATH . '/_classes/account-class.php';
		$accobj = new Account($_SESSION['accid']);
	
		//include other classes
		include FULL_PATH . '/_inc/_classes/question-class.php';
		include FULL_PATH . '/_inc/_classes/MAP-class.php';
		include FULL_PATH . '/_inc/_classes/user-class.php';
		include FULL_PATH . '/_inc/_classes/admin-class.php';
				
		$userobj = new Admin($_SESSION['uid']);
		$MAPobj = new MAP();
		
		require_once(FULL_PATH . "/_inc/localization.php");
		require_once(FULL_PATH . "/_inc/scripts.php");

		
		// the default limit if the parameter is not specified
		define('DEFAULT_LIMIT', 25);
		 
		if($_GET['maptype'] == 'MAP') {
			$access = $userobj->getLevelAccess('map');
			if($access['VIEW'] == 'MA') {
				$compid = $userobj->headid;
			} else {
				$compid = $userobj->companyid;
			}
			$maplist = $MAPobj->getMAPList($compid, $access);
		} else if($_GET['maptype'] == 'idealMAP') {
			$access = $userobj->getLevelAccess('imap');
			if($access['VIEW'] == 'MA') {
				$compid = $userobj->headid;
			} else {
				$compid = $userobj->companyid;
			}
			$maplist = $MAPobj->getidealMAPList($compid, $access);
		} else {
			$access = $userobj->getLevelAccess('tmap');
			if($access['VIEW'] == 'MA') {
				$compid = $userobj->headid;
			} else {
				$compid = $userobj->companyid;
			}
			if($_GET['maptype'] == 'TalentMAP') {
				$mapt = 'talent';
			} else {
				$mapt = 'team';
			}
			$maplist = $MAPobj->getTMAPList($mapt, $compid, $access, 'analytics');
		}
		//print_r($maplist);
		header('Content-type: text/plain; charset=utf-8');
		 
		$limit = isset($_GET['limit']) && !empty($_GET['limit']) ? $_GET['limit'] : DEFAULT_LIMIT;
		
		if ( isset($_GET['q']) && !empty($_GET['q']) ) {
			$count = 0;
			foreach($maplist as $mapid => $userarr) {
				if($_GET['maptype'] == 'MAP') {
					$name = $userarr['fullname'];
					$emailaddy = $userarr['email'];
					$searchStr = $name . ' - ' . $emailaddy;
				} else if($_GET['maptype'] == 'idealMAP') {
					$name = $userarr['imapname'];
					$searchStr = $name;
				} else {
					$name = $userarr['tmapname'];
					$searchStr = $name;
				}
				if ( false !== stripos($searchStr, urldecode($_GET['q'])) ) {
					if($_GET['maptype'] == 'MAP') {
						//echo trim($mapid . '=' . $name) . "\n";
				  	echo trim($mapid . '=' . $name . ' - (' . $emailaddy . ')') . "\n";
				  } else {
				  	echo trim($mapid . '=' . $name) . "\n";
				  }
				  if ( ++$count >= $limit ) break;
			  }
			}
		} else {
			if(isset($_GET['newmapids'])) {
				$altmapids = array();
				$newmapids = explode(',',$_GET['newmapids']);
				foreach($newmapids as $newmapid) {
					if(!array_key_exists($newmapid, $maplist)) {
						$altmapids = $MAPobj->getUpdatedTMAPids($newmapid);
						$fullname = $maplist[$altmapids[0]]['fullname'];
						$displayStr = $maplist[$newmapid]['fullname'] . ' - (' . $maplist[$altmapids[0]]['email'] . ')';
						if($fullname == '') {
							$usrInfo = $MAPobj->getMAPvid($altmapids[0]);
							$displayStr = $usrInfo->firstname . ' ' . $usrInfo->surname . ' - (' . $usrInfo->email . ')';
						}
						echo trim($altmapids[0] . '=' . $displayStr) . "\n";
					} else {
						$displayStr = $maplist[$newmapid]['fullname'] . ' - (' . $maplist[$altmapids[0]]['email'] . ')';
						if($fullname == '') {
							$usrInfo = $MAPobj->getMAPvid($newmapid);
							$displayStr = $usrInfo->firstname . ' ' . $usrInfo->surname . ' - (' . $usrInfo->email . ')';
						}
						echo trim($newmapid . '=' . $displayStr) . "\n";
					}
				}
			} else { 
			$count = 0;
			foreach($maplist as $mapid => $userarr) {
				if($_GET['maptype'] == 'MAP') {
					$name = $userarr['fullname'];
					$emailaddy = $userarr['email'];
					$searchStr = $name . ' - ' . $emailaddy;
				} else if($_GET['maptype'] == 'idealMAP') {
					$name = $userarr['imapname'];
					$searchStr = $name;
				} else {
					$name = $userarr['tmapname'];
					$searchStr = $name;
				}
				if($_GET['maptype'] == 'MAP') {
			  	echo trim($mapid . '=' . $name . ' - (' . $emailaddy . ')') . "\n";
			  	//echo trim($mapid . '=' . $name) . "\n";
			  } else {
			  	echo trim($mapid . '=' . $name) . "\n";
			  }
			  if ( ++$count >= $limit ) break;
			}
		}
	}