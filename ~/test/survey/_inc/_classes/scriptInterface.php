<?php
include_once FULL_PATH . '/_inc/scripts.php';
class scriptsInterface {
	static function limit_text($text, $limit) {
		return limit_text($text, $limit);
	}	
	
	static function ordinal($cdnl) {
		return ordinal($cdnl);
	}
	
	static function cmpdev($a, $b) {
		return cmpdev($a, $b);
	}
	
	static function cmpdevleft($a, $b) {
		return cmpdevleft($a, $b);
	}
	
	static function cmpdiff($a, $b) {
		return cmpdiff($a, $b);
	}
	
	static function cmprank($a, $b) {
		return cmprank($a, $b);
	}
	
	static function loopGenScore($leftscores, $rightscores, $rankid, $l_mapids, $r_mapids, $rightname, $leftname, $rightmapUID, $leftmapUID, $leftmapid, $rightmapid) {
		return loopGenScore($leftscores, $rightscores, $rankid, $l_mapids, $r_mapids, $rightname, $leftname, $rightmapUID, $leftmapUID, $leftmapid, $rightmapid, null, null, null, null);
	}
	
	static function loopScoreSingle($leftscores, $l_mapids, $leftname, $leftmapUID, $leftmapid) {
		return loopScoreSingle($leftscores, $l_mapids, $leftname, $leftmapUID, $leftmapid);
	}
	
	static function array_sum_key($arr, $index=null) {
		return array_sum_key($arr, $index);
	}
	
	static function genRankHTML($rank) {
		return genRankHTML($rank);
	}
	
	static function GenSelectHTML($mapUID, $side, &$MAPobj, $mapids, $currname) {
		return GenSelectHTML($mapUID, $side, &$MAPobj, $mapids, $currname);
	}
}