	$(function() {
			$('#email_contents').tinymce({
				// Location of TinyMCE script
				script_url : '_js/tiny_mce/tiny_mce.js',

				// General options
				theme : "advanced",
				plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

				// Theme options
				theme_advanced_buttons1 : "bold,italic,underline,|,fontselect,fontsizeselect",
				theme_advanced_buttons2 : "cut,copy,paste,link,unlink,code,|,preview,iespell,|,fullscreen",
				theme_advanced_toolbar_location : "top",
				theme_advanced_toolbar_align : "left",
				theme_advanced_statusbar_location : "bottom",
				theme_advanced_resizing : false,
				width: "732px",
				height: "300px",

				// Drop lists for link/image/media/template dialogs
				template_external_list_url : "lists/template_list.js",
				external_link_list_url : "lists/link_list.js",
				external_image_list_url : "lists/image_list.js",
				media_external_list_url : "lists/media_list.js"
			});

			var currLocale;
			if($('#currLocale').val() == '') {
				currLocale = 'en_EN';
			} else {
				currLocale = $('#currLocale').val();
			}

			var oTable = $('.tablesorter').dataTable({
				//"bProcessing": true,
				"iDisplayStart": 0,
				"aaSorting": [],
				"oLanguage": {
					"sUrl": "dtblLocale/dataTables." + currLocale + ".txt"
				},
				"iDisplayLength": 25,
				"sPaginationType": "full_numbers",
				"sDom": '<"filter_container"fl><"top"ip>rt<"bottom"ip<"clear">',
				"bAutoWidth": false
			});

			$('.deleteEmail').live("click", function(){
				var clicked = $(this);
				$(this).closest('tr').removeClass('odd even').addClass('red');
				var eid = this.id.substr(9);
	  		$.get("AjaXUser.php", { emailid:eid, emailname:clicked.closest('tr').find('.emailname').html(), deleteEmail: true },
				  function(data){
						//clicked.closest('td').html('<strong>' + $("#removedcopy").val() + '</strong>');
						oTable.fnDeleteRow($(this).parent());
				  });
			}).confirm({dialogShow:'fadeIn', wrapper:'<div class="confirmdialog"></span>', timeout:10000, msg:$("#deleteemailcopy").val() + '? &nbsp;', buttons: {ok:'Yes',cancel:'No', separator:' / '}});

			// select the overlay element - and "make it an overlay"
		  $('.editEmail, .addEmail').live("click", function(){
		  	if($(this).hasClass('addEmail')) {
					$("button.save_email").html($("#addemailcopy").val()).attr("id", "addCX");
		  		$("#EmailBox div > h2").html($("#addemailcopy").val());
		  	} else {
		  		$("#savuser").val('').attr('disabled', true);
		  		$("button.save_email").html($("#saveemailcopy").val()).attr("id", "editCX");
		  		$("#EmailBox div > h2").html($("#updateemailcopy").val());
		  	}
		  	emailobj = $(this);
				var eapi = $("#EmailBox").overlay({
				    top: 'center',
				    expose: {
				        color: '#fff',
				        loadSpeed: 200,
				        opacity: 0.5
				    },
				    onClose: function() {
				    	//closes validation prompts if open when overlay is closed
				    	$('#emailform').validationEngine('hideAll');
					  },
				    closeOnClick: false,
				    onBeforeLoad: function(event) {
				    	//reset values
				    	var seleid;
				    	if(emailobj.hasClass('addEmail')) {
				    		var editid = null;
							  $.get("AjaXUser.php", { langval: $('#currLocale').val(), getEmailList:true },
							  function(j){
									var opstring = '<option value="nolang">--Please Select--</option>';
									var selectedNow;
							    $.each(j, function(eid, infoobj) {
							    	if(infoobj.name == 'Informal Version') {
							    		seleid = eid;
							    		selectedNow = ' selected="selected"';
							    	} else {
							    		selectedNow = '';
							    	}
							    	opstring += '<option value="' + eid + '"' + selectedNow + '>' + infoobj.name + '</option>';
							    });
							    $("#tempeid").html(opstring);
								  if($(".selTemplate").is(':hidden')) {
										$(".selTemplate").show();
									}

									$.get("AjaXUser.php", { eid:seleid, getEMail: 'yes' },
								  function(data){
										var resultData = data.split('---');
										$('#email_contents').tinymce().execCommand('mceSetContent',false,resultData[1]);
								  });

							  }, "json");
							  $('#emailname').val('');
							} else {
				    		var editid = emailobj.attr("id").substr(9);
				    		$("#eid").val(editid);
				    	}
				    	//disable and reset value of the username field (not for editing!)

				    	if(editid != null) {
					    	$.get("AjaXUser.php", { eid: editid, getEMail: 'yes' },
							  function(data){
									var resultData = data.split('---');
									$('#emailname').val(jQuery.trim(resultData[0]));
									$('#email_contents').tinymce().execCommand('mceSetContent',false,resultData[1]);
									$('#elang').val(jQuery.trim(resultData[2]));
							  });
							}
				    },
				    onLoad: function(event) {
							//nothing yet
				    },
				    api: true
				});
				eapi.load();
		  });

		   $(".save_email").click(function(){
		   	if($("#elang").val() != 'nolang') {
		   		var langok = true;
		   	} else {
		   		alert($("#langcheck").val());
		   	}
		  	var valid = $("#emailform").validationEngine('validate');
		  	if(valid == true && langok == true) {
			  	if(this.id == "addCX") {
			  		var en = jQuery.trim($("#emailname").val());
			  		var ec = $("#email_contents").html();
						$.post("AjaXUser.php", { emailname:en, contents: ec, elang:$("#elang").val(), addEmail:1 },
						  function(j){
							  if(j.error == false) {
									var ai = oTable.fnAddData([
									en,
									$("#elang").val(),
									j.createdOn,
									j.adminString]);
								}
						    $.jGrowl(j.message, {
									header:'<strong>System Message</strong>',
									closeTemplate:'<div class="jGrowlCloseMe">close (x)</div>',
									position:'bottom-right'
								});
						    $("#EmailBox").overlay().close();
						  }, "json");
					} else if (this.id == "editCX") {
			  		var en = jQuery.trim($("#emailname").val());
			  		var ec = $("#email_contents").html();
			  		var eid = $("#eid").val();
			  		$.post("AjaXUser.php", { eid:eid, emailname:en, contents: ec, elang:$("#elang").val(), editEmail:1 },
						  function(data){
						    $.jGrowl(data, {
									header:'<strong>System Message</strong>',
									closeTemplate:'<div class="jGrowlCloseMe">close (x)</div>',
									position:'bottom-right'
								});
								$("#EmailBox").overlay().close();
						  });
					}
				}//end validation check
		  });

		  //this is for loading in correct emails based on language selected
			$("#elang").change(function() {
				var optionid = 'MUeid';
				var langval = $("#elang").val();
			  $.get("AjaXUser.php", { langval: langval, getEmailList:true },
			  function(j){
					var opstring = '<option value="nolang">--Please Select--</option>';
			    $.each(j, function(eid, infoobj) {
			    	opstring += '<option value="' + eid + '">' + infoobj.name + '</option>';
			    });
			    $("#tempeid").html(opstring);
				  if($(".selTemplate").is(':hidden')) {
						$(".selTemplate").show();
					}
			  }, "json");
			});

			$("#tempeid").change(function() {
				$.get("AjaXUser.php", { eid: $(this).val(), getEMail: 'yes' },
			  function(data){
					var resultData = data.split('---');
					$('#email_contents').tinymce().execCommand('mceSetContent',false,resultData[1]);
			  });
			});

	    $("#emailform").validationEngine('attach');

	});//end doc rdy
