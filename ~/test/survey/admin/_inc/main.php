
<div id="surveybar"><div class="iconfloat"><img src="<?=ADMIN_ACCOUNT_PATH;?>_images/_icons/IC_ViewUsage.png" /></div> <div class="floatleft">Welcome <?= $adminobj->firstname . ' ' . $adminobj->surname; ?></div></div>
<div class="titlebar_shadow"></div>

<div id="content_padmain">
	<div id="content_innerpad">
    <h1 class="MWxHeader"><?php printf(_("Welcome to %s"), ACC_NAME);?></h1>
    <p><?php printf(_("Welcome to the Administration section of %s! Access to this section is restricted to head-hunters and administrators only to allow you to control the way you use %s."), ACC_NAME, ACC_NAME);?><br />
    <?=_("There are two main areas of control");?>:</p>
    <ul>
	    <li><?=_("Menu choices - left of your screen.");?></li>
	    <li><?=_("Context specific choices - top right.");?></li>
    </ul>
    <p><strong><?=_("Terminology");?></strong></p>
    <p><?php printf(_("Wherever possible, we aim to keep our language jargon free and consistent.  To help you become familiar with %s below are some essential terms:"), ACC_NAME);?></p>
    <ul>
        <li><?=_("Account - A Client Organisation");?></li>
        <li><?=_("Sub Account - A Client Organisation's subsidiary or another business unit under the main account umbrella.");?></li>
        <li><?=_("User - Anybody who has access to the system, from candidate's taking a survey through to clients with view-only access through to head-hunters and administrators.");?></li>
        <li><?=_("MAP - A report showing an individual's Motivation And Preference results.");?></li>
        <li><?=_("teamMAP - A report consolidating several MAPs for a team.");?></li>
        <li><?=_("talentMAP - A report consolidating several MAPs to produce a benchmark or job profile.");?></li>
    </ul>
    <p><strong><?=_("Menu Choices - Summary");?></strong></p>
      <?=_("In this section you will be able to:");?><br />
    <ul>
      <li><?=_("View Survey Usage - Track and display MAP activity for any account or user for any selected time period.");?></li>
      <li><?=_("Manage Accounts - Create and edit a new Client Account or Associated Account.");?></li>
      <li><?php printf(_("Manage Users - Create, edit, activate or deactivate users. Control users' access to %s."), ACC_NAME);?></li>
      <li><?=_("Manage Emails - Choose templates when emailing people.");?></li>
      <li><?=_("Manage MAPS - Create, edit and archive talentMAPs and teamMAPs.");?></li>
     </ul>
   </div>
</div>
<div id="rightcolumn">
	<div id="rightColPad">
		<div id="rightColGrav">
			<?php 
			$email = $adminobj->email;
			$default = "http://static.saviio.com/_images/_misc/gravatar.jpg";
			$size = 50;
			$avaurl = $adminobj->avatarURL;
			if($avaurl == '') {
				$avaurl = "http://www.gravatar.com/avatar/" . md5( strtolower( $email ) ) . "?default=" . urlencode( $default ) . "&size=" . $size;
			}
			?>
			<img src="<?php echo $avaurl; ?>" alt="your avatar!" />
		</div>
		<div id="rightColDetails">
			<h3 class="MWxLogin"><?php echo _('User Details');?></h3>
			<div id="rightColDetailsPad"><?php echo $adminobj->firstname . ' ' . $adminobj->surname; ?></div>
		</div>
		<div id="rightColContentFloat">
			<table border="0" cellspacing="0" cellpadding="0" width="230" id="detailstable">
				<tr>
					<td width="160" class="dtitle">Last Login</td>
					<td width="70" align="right"><?=date("d/m/Y", $adminobj->getLastLogin());?></td>
				</tr>
			</table>
			<br />
			<h3 class="MWxLogin"><?php echo _('New Alerts');?></h3>
			<?php
			$req_arr = $adminobj->getShareRequests('owner');
			?>
			<table border="0" cellspacing="0" cellpadding="0" width="230" id="detailstable">
				<?php
				if(count($req_arr) == 0) {
					echo '<tr><td width="230" colspan="2">' . _("No pending requests") . '</td></tr>' . "\n";
				} else {
					echo '<tr><td width="230" colspan="2" class="dtitle">' . _("Share Requests") . '</td></tr>' . "\n";
					foreach($req_arr as $srid => $reqinfo) {
						echo '<tr>' . "\n";
						echo '<td width="200" class="reqComp" id="reqcid_' . $reqinfo['reqcid'] . '">' . $reqinfo['companyname'] . '</td>' . "\n";
						echo '<td width="30" align="right"><a href="?admin=companies">Go &gt;</a></td>' . "\n";
						echo '</tr>' . "\n";
					}
				}
				?>
			</table>
			<div style="font-size:11px; margin:10px 0px 0px 0px;">
				<h3 class="MWxLogin"><?php echo _('MAPs Remaining');?></h3>
				<div style="padding:5px 0px 0px 0px;"><?=_("Shown below is how many MAPs are remaining for this account. A warning is displayed when you reach 60 percent or more of your set limit.");?></div>
				<br />
				<?php
				$limitsArr = $adminobj->getLimits();
				$used = TOTAL_MAPS;
				$displayName = 'MAPs';
				$limitval = $limitsArr['maps'];
				$rawratio = ($used / $limitval);
				$widthval = floor(230 * $rawratio);
				$limitView = '<span class="limitUsed">' . $used . '</span> / ' . $limitval;
				//this works out the position (height wise) of the sprite based upon the ratio of used items compared to limit ratio.
				if($rawratio <= 0.6){
					$top = -10;
					$boxClass = '';
				}elseif($rawratio < 0.8){
					$top = -20;
					$boxClass = 'MWOrange';
				}elseif($rawratio <= 1){
					$top = -30;
					$boxClass = 'MWRed';
				} else {
					$top = -30;
					$boxClass = 'MWRed';
				}
				$mapUsedPercentage = round(($rawratio * 100),0);

				echo '<div class="limitBarSmall"><div id="limits_maps" style="float:left; background:url(' . ADMIN_ACCOUNT_PATH . '_images/limitsprite_small.png) no-repeat 0px ' . $top . 'px; height:10px; width:' . $widthval . 'px;"></div></div><div class="cleaner"></div>';
				echo '<div class="infoBoxMainMAP">' . $displayName . ' ' . $limitView . '</div>';
				if($rawratio > 0.6) {
				?>
					<div id="MAPWarning" class="<?=$boxClass;?>">
						You have used <?=$mapUsedPercentage;?>&#037; of your MAPs, if you need more please contact.<br /><br />
						<a href="mailto:accounts@saviio.com">accounts@saviio.com</a>
					</div>
				<?php } ?>
			</div>
		</div>
  </div>
</div>

