/**
 * Localization strings for the UI Multiselect widget
 *
 * @locale en, en-US
 */

$.extend($.ui.multiselect.locale, {
	addAll:"Aggiungi tutti",
	removeAll:"Rimuovi tutti",
	itemsCount:"#{count} elementi selezionati",
	itemsTotal:"#{count} elementi in tutto",
	busy:"attendere...",
	errorDataFormat:"Impossibile aggiungere opzioni: formato dati sconosciuto",
	errorInsertNode:"Si � verificato un problema durante l'aggiunta dell'elemento:\n\n\t[#{key}] => #{value}\n\nOperazione interrotta.",
	errorReadonly:"L'opzione #{option} � di sola lettura",
	errorRequest:"� stato riscontrato un problema relativo alla chiamata remota. (Type: #{status})"
});