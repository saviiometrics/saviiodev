/**
 * Localization strings for the UI Multiselect widget
 *
 * @locale no, nb-NO
 */

$.extend($.ui.multiselect.locale, {
	addAll:'Legg til alle',
	removeAll:'Fjern alle',
	itemsCount:'#{count} artikler valgt',
	itemsTotal:'#{count} artikler totalt',
	busy:'vennlisgt vent...',
	errorDataFormat:"Kan ikke legge til valg, ukjent dataformat",
	errorInsertNode:"Det oppsto et problem da artikkelen skulle legges til:\n\n\t[#{key}] => #{value}\n\nOperasjonen ble avbrutt.",
	errorReadonly:"Dette valget #{option} kan kun leses",
	errorRequest:"Beklager! Det er oppst�tt et problem med oppringningen. (Type: #{status})"
});
