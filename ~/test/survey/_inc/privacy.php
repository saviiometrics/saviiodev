<div class="contentPadding">
  <h2 class="MWxMedium"><?= _("Privacy Notice");?></h2>
  <p>We respect your right to privacy and any personal information which is obtained as a result of you accessing this site  will not be disclosed to any third party unless we are obliged to do so by law.</p>
	<p><strong>Information Collected</strong></p>
	<ul type="disc">
	  <li>When you request additional information or register for services, the Company will ask you to provide basic contact information.</li>
	  <li>When you purchase services online, the Company will ask you to provide billing information.</li>
	  <li>Saviiometrics uses common Internet technologies, such as cookies and Web beacons, to keep track of interactions with the Company's Web sites and emails.</li>
	</ul>
	<p><strong>Use of Information</strong></p>
	<ul type="disc">
	  <li>Saviiometrics uses information collected to provide you with the services you request.</li>
	  <li>Saviiometrics may use information collected to provide you with additional information about the Company's services, partners, promotions, and events.</li>
	  <li>Saviiometrics may use information  collected to improve the Company's Web sites and services.</li>
	  <li>Saviiometrics does not share, sell, rent, or trade personally identifiable information with third parties for their promotional purposes. Saviiometrics may share information collected with other companies that work on Saviiometrics's behalf.</li>
	</ul>
	<p><strong>CustomerData</strong></p>
	<ul type="disc">
	  <li>Customers of Saviiometrics use the Company's services to host data and information ("Customer Data").</li>
	  <li>Saviiometrics will not review, share, distribute, or reference any such Customer Data except as provided in the Saviiometrics Master Subscription Agreement, or as may be required by law. Individual records of Customer Data may be viewed or accessed only for the purpose of resolving a problem, support issues, or suspected violation of the Saviiometrics Master Subscription Agreement, or as may be required by law.</li>
	</ul>
	<p>
	  <strong>How to Contact Us</strong><br />
	  <br />
	  For more information about Saviio's information practices, please view the Company's full privacy statement. Questions regarding Saviio's Privacy Statement or information practices should be requested by email to: <a href="mailto:privacy@saviio.com" style="font-size:12px;">privacy@saviio.com</a> .<br />
	  <br />
	</p>
</div>