
		<div id="innerContent">
	      <h1 class="MWxHeader">So how can Saviio help you?</h1>
	      <div id="contentSiFR">
	      	<h2 class="MWxBox">
	      		Engaged colleagues will make a big difference to your business. They'll each take 5 fewer sick days a year and raise profitability by 12%, as well as productivity by 18%. This same workforce will also generate 43% more revenue.<br /><br /> 
						And, if you asked them, they'd tell you they were 87% less likely to leave.*</h2>
	      </div>
	     <div class="cleaner"></div>
	     <div class="contentPadTop">
		      <p>We help our clients succeed by providing online tools, training and consulting to help business people improve the way they engage with each other.</p>
					<p>Employee engagement is a route to business success. It is a very practical way of strengthening your business' productivity, innovation, attendance rate and voluntary turnover.</p>
      </div>
      <div class="infoLeft">
      	<span class="contentHeader">Using Saviio MAPs you are able to:</span>
      		<ol>
      			<li>Benchmark the motivational preferences of "excellent performance" so that you can predict who is likely to succeed in any given job within your organisation.
							<blockquote>"I'm convinced they've got the skills - but will they succeed in our organisation?"</blockquote>
						</li>
						<li>Understand how to tap into an individuals' motivational drivers so that they feel significant and appreciated by people that matter to them.
							<blockquote>"People don't leave jobs - they leave managers"</blockquote>
						</li>
						<li>Structure language and behaviour in any sales process to create compelling buying opportunities for customers.
							<blockquote>"Make it easy for Customers to buy from you"</blockquote>
						</li>
					</ol>
      </div>
	    <div class="infoLeft">
	    	<p>&nbsp;</p>
	     	<span class="contentHeader">Don't just take our word for it!</span>
	    	<p>Still not convinced why you need Saviio to help your organisation? Check out the testimonials below to see how we've made other clients happy, really happy!</p>
				<p><blockquote><span class="testMonHead">Sitel</span><br />Since implementing Saviio in five of our major outsourced clients' campaigns we have managed to reduce the annual attrition rate by an average of over 10% - equivalent to an annual saving in excess of six figures. I have also noticed that our team leaders and campaign managers have created a "buzz about the place" by better understanding the needs of their teams and being able to coach them accordingly.<br /><br /><strong>Director, Corporate Development, Sitel</strong><br /><br /></blockquote></p>
				<p><blockquote><span class="testMonHead">Office Depot</span><br />"I have worked with Saviio in a number of totally different companies. Saviio has a unique ability to fit perfectly with the culture, ethos and objectives of those businesses."<br /><br /><strong>HR Director, Office Depot</strong><br /><br /></blockquote></p>
				<p><blockquote><span class="testMonHead">SAM Headhunting</span><br />"We use Saviio MAPs to improve the quality of our service to our clients and candidates. We have now made Saviio MAPs a strategic tool in our offer and look forward to a long relationship with Saviio and future applications which they will provide for us." <br /><br /><strong>Board Member, SAM Headhunting, Denmark</strong><br /><br /></blockquote></p>
				<p><blockquote><span class="testMonHead">Staples</span><br />" Thanks so much for all your support and challenge that's been instrumental in what I believe to have been a remarkable gathering for Staples UK & Ireland. Unique to date and one of our best. I have a feeling we will work together again and, for me, a special connection and friendship has emerged with you that I have already placed great value in."<br /><br /><strong>HR Director, Staples UK & Ireland</strong><br /><br /></blockquote></p>
  			<p>&nbsp;</p>
  			<p>*Source: MacLeod and Clarke (2009) - "Engaging for Success: Enhancing performance through employee engagement"</p>
  	  </div>	
  	</div>