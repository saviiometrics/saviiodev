<?php
require_once("site.config.php");
//if($_SERVER['REMOTE_ADDR'] != "86.26.254.79") {
//    die("<h1>Saviio Ltd. is upgrading their servers this weekend which means that this system will be unavailable fromFriday 4th November 18:00 UK time until Sunday 6th November 23:59 UK time.</h1>");
//}
if(!isset($_SESSION)) { session_start(); }
$phpsessid = session_id();

$using_ie6 = (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 6.') !== FALSE);
if($using_ie6 == true) {
        include('unsupported_browser.php');
        exit();
    }

//include localization and site config files
	//include DB AND ACCOUNT INFO CLASSES
	include CONTENT_PATH . '/_classes/db-class.php';
	include CONTENT_PATH . '/_classes/account-class.php';

	$accobj = new Account($_SESSION['accid']);

	//include other classes
	include CONTENT_PATH . '/_classes/user-class.php';
	include CONTENT_PATH . '/_classes/question-class.php';
	include CONTENT_PATH . '/_classes/MAP-class.php';
	include CONTENT_PATH . '/_classes/admin-class.php';
	
	$userobj = new User();	
	$userobj->checkExpiration();	
	//first login
	if(isset($_POST['firstlogin'])) {
		if($sess_err == 0) {
			if(!$userobj->checkLogin(addslashes($_POST['savuser']), md5($_POST['savpass']))) {
				$login_error = 'Login details incorrect';
			} else {
				session_regenerate_id(true);
				$_SESSION['username'] = addslashes($_POST['savuser']);
				$_SESSION['passw'] = md5($_POST['savpass']);
				$_SESSION['dashpass'] = $_POST['savpass'];
				$userobj->getUservUN(addslashes($_POST['savuser']));
				$userobj->setLastLogin(time());
				$_SESSION['uid'] = $userobj->uid;
				$_SESSION['level'] = $userobj->level;
				$_SESSION['accid'] = $userobj->accid;
				$accobj = new Account($userobj->accid);
				$login_error = false;
				//header('Location: ' . $surveyDomain);
			}
		}
	}


 	if(isset($_GET['logout'])) {
 		$langatm = $_SESSION['locale'];
 		session_destroy();
 		$_SESSION = array();
 		if(!isset($_SESSION)) { session_start(); }
 		$phpsessid = session_id();
 		$_SESSION['locale'] = $langatm;
	}
	
	if(isset($_REQUEST['product']) && $_REQUEST['product'] == 'saviiomaps') {
		$_SESSION['sesscheck'] = true;
	}
	
	$sess_err = 0;
	if(!isset($_SESSION['sesscheck'])) {
		$_SESSION['sesscheck'] = true;
		if(isset($_POST['firstlogin'])) {
			$sess_err = 1;
		}
	} else {
		$sess_err = 0;
	}
	
	function checkLevel($pagename, &$userobj) {
		$feataccess = $userobj->getLevelAccess('siteaccess');
			if($pagename == 'analytics') {
				if($feataccess['ANALYTICS'] == 'TRUE') {
					return TRUE;
				} else {
					return FALSE;
				}
			}
			if($pagename == 'admin') {
				if($feataccess['ADMIN'] == 'TRUE') {
					return TRUE;
				} else {
					return FALSE;
				}
			}
		return TRUE;
	}
		
	if(isset($_SESSION['username'])) {
		if(!$userobj->checkLogin($_SESSION['username'], $_SESSION['passw'])) {
			session_destroy();
			$_SESSION = array();
		} else {
			$userobj->getUser($userobj->uid);
			$_SESSION['level'] = $userobj->level;
		}
	}
	require_once(CONTENT_PATH . "/localization.php");
	require_once(CONTENT_PATH . "/scripts.php");
	
	//if($_SERVER['REMOTE_ADDR'] != '86.10.37.142') {
	//	include (FULL_PATH .  "/upgrade.php");
	//} else {
		//build template file
		require (FULL_PATH . "/header_menu.php");
	
		if( !isset($_GET['saviio'])) {
			require (CONTENT_PATH .  "/main.php");
		} else {
			if(checkLevel($_GET['saviio'], $userobj)) {
				if($sess_err == 1) {
					require (FULL_PATH . '/sess_error.php');
				}
				require (CONTENT_PATH . '/' . $_GET['saviio'] . '.php');
			} else {
				require (CONTENT_PATH .  "/noaccess.php");
			}
		}
		require (FULL_PATH . "/footer.php");
	//}
	?>
