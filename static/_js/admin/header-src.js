
	function array_diff(a1, a2)
	{
	  var a=[], diff=[];
	  for(var i=0;i<a1.length;i++)
	    a[a1[i]]=true;
	  for(var i=0;i<a2.length;i++)
	    if(a[a2[i]]) delete a[a2[i]];
	    else a[a2[i]]=true;
	  for(var k in a)
	    diff.push(k);
	  return diff;
	}

	function removeByElement(arrayName,arrayElement) {
	  for(var i=0; i<arrayName.length;i++ ) {
	  	if(arrayName[i]==arrayElement) {
	    	arrayName.splice(i,1);
	    }
		}
		return arrayName;
	}

	function positionOverlay() {
		var bH = $(window).height();
		var sW = $(window).width();
		var sT = document.documentElement.scrollTop;
		var nH = (((bH / 2)-30)+sT);
		var nW = ((sW / 2)-100);

		$("#sortoverlay").css({top:nH, left:nW});
	}

	//event to check session time variable declaration (global)
	var checkSessionTimeEvent;
	var countdownTickerEvent;
	function keepMeLoggedIn() {
		if($('#keepSessionAlive').length > 0 && $('#keepSessionAlive').prop("checked")) {
			var timeNow = new Date();
			var timeDifference = 0;
			timeDifference = timeNow - pageRequestTime;
			if(timeDifference > refreshTime) {
				$.get("/admin/AjaXUser.php", { refreshSession:true },
						function(data){pageRequestTime = new Date();
				});
			}
		}
	}
	$(document).ready(function() {
		//event to check session time left (times 1000 to convert seconds to milliseconds)
    checkSessionTimeEvent = setInterval("checkSessionTime()",10*1000);
    keepMeLoggedInEvent = setInterval("keepMeLoggedIn()",10*1000);
	 	$('.reLogNow').click(function() {
	  	$.get("AjaXUser.php", { uname:$('#reloguser').val(), repass:$('#relogpass').val(), relogLocale:$('#currLocale').val(), accid:$('#accid').val(), relogNow: true },
		  function(data){
				if(jQuery.trim(data) == 'true') {
					//reset the session times as they have just logged in!
					checkSessionTimeEvent = setInterval("checkSessionTime()",10*1000);
			    pageRequestTime = new Date();
			    timeoutLength = sessionLength*1000;
			    warningTime = timeoutLength - (warning*1000);
			    countdownTime = warning;
			    warningStarted = false;
			    tickerActive = false;
					$("#loginBox").overlay().close();
					$.jGrowl('Your login was successful', {
						header:'<strong>Login Alert</strong>',
						closeTemplate:'<div class="jGrowlCloseMe">close (x)</div>',
						position:'bottom-right'
					});
				} else {
					alert('Login incorrect, Please try again');
				}
		  });
		});
	 	$('#keepSessionAliveDiv').click(function(){
	 		$('#keepSessionAliveDiv').toggleClass('checked unchecked');
	 		if($('#keepSessionAlive').prop("checked")) {
	 			$('#keepSessionAlive').prop("checked", false);
	 			$(location).attr('href',"/?saviio=main&logout=1");
	 		} else {
	 			$('#keepSessionAlive').prop("checked", true);
	 			$('#keepSessionAliveDivText').hide();
	 			$('#keepSessionAliveDivImg').show();
	 		}
	 	});
	});

	//total length of session in seconds
	var sessionLength = 900;
	//time warning shown (10 = warning box shown 10 seconds before session starts)
	var warning = 60;
	//time session started
	var pageRequestTime = new Date();
	//session timeout length
	var timeoutLength = sessionLength*1000;
	//set time for first warning, ten seconds before session expires
	var warningTime = timeoutLength - (warning*1000);
	//set number of seconds to count down from for countdown ticker
	var countdownTime = warning;
	//warning dialog open; countdown underway
	var warningStarted = false;
	var tickerActive = false;

	function checkSessionTime() {
		var timeNow = new Date();
		var timeDifference = 0;
		timeDifference = timeNow - pageRequestTime;
		  if(timeDifference > warningTime && warningStarted === false) {
				$('#sessionWarning').jGrowl("Session Expiration countdown", {
					speed: 2000,
					sticky: true,
					corners:'8px',
					header:'<strong>Session Expiration</strong>',
					closeTemplate:'<div class="jGrowlCloseMe">close (x)</div>'
				});
		    warningStarted = true;

		    if(tickerActive == false) {
			    //call now for initial dialog box text (time left until session timeout)
			    countdownTicker();
			    //set as interval event to countdown seconds to session timeout
			    countdownTickerEvent = setInterval("countdownTicker()", 1000);
			    tickerActive = true;
			  }

		  } else if (timeDifference > timeoutLength){
		    //$("#loginBox").data("overlay").close();
		    $('#sessionWarning').jGrowl('close');
	    	var loginAPI = $("#loginBox").overlay({
				  top: 'center',
				  mask: {
				      color: '#fff',
				      loadSpeed: 200,
				      opacity: 0.5
				  },
				  closeOnClick: false,
				  onLoad: function() {
						clearInterval(countdownTickerEvent);
						clearInterval(checkSessionTimeEvent);
				  },
				  api: true,
				  fixed: true
				});
		    loginAPI.load();
		 }
	}

	function countdownTicker() {
		$(".jGrowl-message").html('<p>Due to inactivity, your session is about to expire, to prevent this from happening please click the "Refresh Session" button below. When your session times out you will have to login again.</p><p>Your session will expire in <strong>' + countdownTime + '</strong></p><p><a href="javascript:void(0);" class="refreshSession">Refresh Session</a></p>');
		countdownTime--;
	}

	$(function() {

		//IMPORTANT
		$.tools.overlay.conf.fixed = false;

		jQuery.fn.fadeToggle = function(speed, easing, callback) {
 			return this.animate({opacity: 'toggle'}, speed, easing, callback);
		};

		$("#sortoverlay").ajaxStart(function() {
			positionOverlay();
			//this resets the timer every time there is an ajax request which essentially keeps the session alive.
			pageRequestTime = new Date();
			$("#sortoverlay").show(0);
		});
		$("#sortoverlay").ajaxComplete(function() {
    	$("#sortoverlay").hide(0);
		});

		$(".refreshSession").live("click", function() {
			$.get("AjaXUser.php", {refreshSession:true },
			  function(data){
					//do nothin here just close the current jGrowl? No need to reset the timer again as this is down above when an ajax call is dealt with
					$('#sessionWarning').jGrowl('close');
			  });
		});

		// select the overlay element - and "make it an overlay"
	  $('#userpanel').live("click", function(){
	  	//UPobj = $(this);
			var UPapi = $("#UserPanelBox").overlay({
			    top: 'center',
			    mask: {
			        color: '#fff',
			        loadSpeed: 200,
			        opacity: 0.5,
			        zIndex: 999999
			    },
			    closeOnClick: false,
		    	onClose: function() {
			    	$("#UPForm").validationEngine('hideAll');
			    },
			    api: true
			});
			UPapi.load();
	  });

	  // select the overlay element - and "make it an overlay"
	  $('#submitFB').live("click", function(){
	  	//UPobj = $(this);
			var UPapi = $("#FeedbackBox").overlay({
			    top: 'center',
			    expose: {
			        color: '#fff',
			        loadSpeed: 200,
			        opacity: 0.5
			    },
			    closeOnClick: false,
			    onBeforeLoad: function(event) {
						//$("#UPfn").val(
			    },
			    onLoad: function(event) {
			    	var filego = false;
			    	$('#imgattach').html($('#imgattach').html());

							var upload = new AjaxUpload('imgattach', {
				        action: 'AjaXUser.php',
				        autoSubmit: false,
				        onSubmit : function(file , ext){
					        if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){
					          // extension is not allowed
					          alert('Error: invalid file extension');
					          // cancel upload
					          return false;
					        } else {
					        	positionOverlay();
										$("#sortoverlay").show(0);
					        	this.setData({uid:$("#xhuid").val(), title:$("#fbtitle").val(), comments:$("#fbcomments").val(), priority:$("#priority").val(), submitfeedback: true});
					        }
				        },
							  name:'imgattach',
							  onComplete: function(file, response) {
							  	alert(response);
							  	$("#sortoverlay").hide(0);
							  	$("#FeedbackBox").overlay().close();
							  },
							  onChange: function(file, extension) {
							  	filego = true;
							  	$('#ImgSelect').html('Image Attachment <span style="color:green;">(Selected - ' + file + ')</span>');
							  }
						});
					  $(".FBSave").click(function(){
					  	var valid = $("#FBForm").validationEngine('validate');
					  	if(valid == true) {
					  		if(filego == true) {
					  			upload.submit();
					  		} else {
									$.post("AjaXUser.php", { uid:$("#xhuid").val(), title:$("#fbtitle").val(), comments:$("#fbcomments").val(), priority:$("#priority").val(), submitfeedback: true },
								  function(data){
										alert(data);
										$("#FeedbackBox").overlay().close();
								  });
					  		}
							}
					  });
			    },
		    	onClose: function() {
			    	//closes validation prompts if open when overlay is closed
			    	$("#FBForm").validationEngine('hideAll');
			    },
			    api: true
			});
			UPapi.load();
	  });


	  $(".UPSave").click(function(){
	  	var valid = $("#UPForm").validationEngine('validate');
	  	if(valid == true) {
	  		$.get("AjaXUser.php", { uid:$("#xhuid").val(), firstname:$("#UPfn").val(), surname:$("#UPsn").val(), email:$("#UPemail").val(), password:$("#UPpasswordC").val(), updateDetails: true },
				  function(data){
						alert(data);
						$("#UserPanelBox").overlay().close();
				  });
	  	}
	  });

	  $("#UPForm").validationEngine('attach');

	  $("#lang_select").hover(
			 function() {$("#langselbox").fadeToggle(200);},
       function() {$("#langselbox").fadeToggle(200);}
		);

	});//end doc rdy
