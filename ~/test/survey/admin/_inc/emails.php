<input type="hidden" name="eid" id="eid" />
<input type="hidden" name="addemailcopy" id="addemailcopy" value="<?=_("Add new Email Template");?>" />
<input type="hidden" name="updateemailcopy" id="updateemailcopy" value="<?=_("Update Email Template");?>" />
<input type="hidden" name="saveemailcopy" id="saveemailcopy" value="<?=_("Save Email Template");?>" />
<input type="hidden" name="deleteemailcopy" id="deleteemailcopy" value="<?=_("Are you sure you want to delete this email?");?>" />
<input type="hidden" name="removedcopy" id="removedcopy" value="<?=_("Removed");?>" />
<input type="hidden" name="langcheck" id="langcheck" value="<?=_("You must select a language for the email template!");?>" />

<div id="EmailBox" class="emailBox">
	<div class="addpad">
		<div class="cicon" style="padding:10px;"><img src="<?=ADMIN_ACCOUNT_PATH;?>_images/_icons/IC_Mail.png" /></div>
		<h2><?=_("Add Email Template");?></h2>
		<p><?=_('From here you can create personal welcome emails for your clients and survey takers!');?>
		<p style="padding:5px;">
			<?=_("Username");?>: [USERNAME] - <?=_("Inserts the user's username");?><br />
			<?=_("Password");?>: [PASSWORD] - <?=_("Inserts the user's password");?><br />
			<?=_("Site URL");?>: [SITEURL] - <?=_("Inserts the link to the survey page");?><br />
			<?=_("Firstname");?>: [FIRSTNAME] - <?=_("Inserts the user's firstname");?><br />
			<?=_("Lastname");?>: [LASTNAME] - <?=_("Inserts the user's lastname");?><br />
			<?=_("Salutation");?>: [SALUTATION] - <?=_("Inserts the user's salutation");?><br />
			<?=_("Admin Firstname");?>: [ADMIN_FN] - <?=_("Inserts the administrators firstname i.e. your firstname");?><br />
			<?=_("Admin Surname");?>: [ADMIN_SN] - <?=_("Inserts the administrators surname i.e. your surname");?><br />
			<?=_("Surveyname");?>: [SURVEYNAME] - <?=_("Inserts the Survey's name.");?><br />
		</p>
		<div id="addTMAPheader"><?=_("Email Details");?></div>
		<form id="emailform" name="emailform">
			<table border="0" cellpadding="3" cellspacing="2" width="736">
				<tr>
					<td><?=_("Email Name");?></td>
					<td align="right"><input type="text" name="emailname" id="emailname" class="validate[required,length[5,50]] textfield" size="30" /></td>
				</tr>
				<tr>
					<td><?=_("Select Email Language");?></td>
					<td align="right">
						<select name="elang" id="elang">
							<option value="nolang"><?=_("--Please Select--");?></option>
							<?php
							foreach($langs as $langid => $langInfo) {
								if($langInfo['locale'] == $_SESSION['locale']) {
									$selected = ' selected="selected"';
								} else {
									$selected = '';
								}
								echo '<option value="' . $langInfo['locale'] . '"' . $selected . '>' . _($langInfo['lang_name']) . '</option>' . "\n";
							}
							?>
						</select>	
					</td>
				</tr>
				<tr>
					<td><div class="selTemplate"><?=_("Select Welcome Template");?></div></td>
					<td align="right">
						<div class="selTemplate">
							<select name="tempeid" id="tempeid">
							</select>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="2"><textarea name="email_contents" id="email_contents"></textarea></td>
				</tr>
			</table>
		</form>
		<br />
		<!-- yes/no buttons -->
		<p>
			<button class="save_email"> <?=_("Save Details");?> </button>
			<button class="close"> <?=_("Close");?> </button>
		</p>
		<div class="cleaner"></div>
	</div>
</div>

<div id="userbar"><div class="iconfloat"><img src="<?=ADMIN_ACCOUNT_PATH;?>_images/_icons/IC_Structure.png" /></div> <div class="floatleft"><?=_("Manage Welcome Emails");?></div></div>
<div class="titlebar_shadow"></div>

<div id="content_pad">
	<h1 class="MWxHeader"><?=_("Manage Welcome Emails")?></h1>
		<p style="padding:5px;"><?=_("When creating a new welcome email template there are a number of things you must include. The email template system uses a tag placeholder system, meaning if you enter, for example, [FIRSTNAME] then when the email is sent out, this placeholder will be replaced with the user's firstname. There is a key below which you can use to see the relevant placeholders and what each one inserts in it's place. There are some placeholders which must always be included. These are [USERNAME], [PASSWORD] and [SITEURL]");?></p>
		<p style="padding:5px;"><?=_("When adding a new email, you can also choose to load in one of the pre-built templates to get you started. You must also select the language the email will be sent out in.");?></p>
		<p style="padding:5px;">
			<?=_("Username");?>: [USERNAME] - <?=_("Inserts the user's username");?><br />
			<?=_("Password");?>: [PASSWORD] - <?=_("Inserts the user's password");?><br />
			<?=_("Site URL");?>: [SITEURL] - <?=_("Inserts the link to the survey page");?><br />
			<?=_("Firstname");?>: [FIRSTNAME] - <?=_("Inserts the user's firstname");?><br />
			<?=_("Lastname");?>: [LASTNAME] - <?=_("Inserts the user's lastname");?><br />
			<?=_("Salutation");?>: [SALUTATION] - <?=_("Inserts the user's salutation");?><br />
			<?=_("Admin Firstname");?>: [ADMIN_FN] - <?=_("Inserts the administrators firstname i.e. your firstname");?><br />
			<?=_("Admin Surname");?>: [ADMIN_SN] - <?=_("Inserts the administrators surname i.e. your surname");?><br />
			<?=_("Surveyname");?>: [SURVEYNAME] - <?=_("Inserts the Survey's name.");?><br />
		</p>
	<br />
	
	<div id="tableHolder">
		<table cellspacing="1" class="tablesorter">
			<thead>
				<tr>
					<th><?=_("Email Name");?></th>
					<th><?=_("Language");?></th>
					<th><?=_("Creation Date");?></th>
					<th><?=_("Admin Panel");?></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th><?=_("Email Name");?></th>
					<th><?=_("Language");?></th>
					<th><?=_("Creation Date");?></th>
					<th><?=_("Admin Panel");?></th>
				</tr>
			</tfoot>
			<tbody>
				<?php
				$emailslist = $adminobj->getEmailList('user');
				if(!empty($emailslist)) {
					foreach($emailslist as $eid => $emailarr) {
						echo '<tr>' . "\n";
						echo '<td class="emailname">' . $emailarr['name'] . '</td>' . "\n";
						echo '<td>' . $emailarr['lang'] . '</td>' . "\n";
						echo '<td>' . date("d/m/Y", $emailarr['createdon']) . '</td>' . "\n";
						echo '<td><div class="makerelative">
						<a href="javascript:void(0);" class="editEmail" id="mailedit_' . $eid . '"><img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_EditMail.png" alt="Edit Email" title="Edit Email" /></a>
						<a href="javascript:void(0);" class="deleteEmail" id="emaildel_' . $eid . '"><img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_DeleteMail.png" alt="Delete EMail" title="Delete Email" /></a>&nbsp;
						</div></td>' . "\n";
						echo '</tr>' . "\n";
					}
				}
				?>
			</tbody>
		</table>
	</div>
	<div class="cleaner"></div>
</div>
