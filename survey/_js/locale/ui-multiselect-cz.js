﻿/**
 * Localization strings for the UI Multiselect widget
 *
 * @locale cz, cs-CZ
 */

$.extend($.ui.multiselect.locale, {
	addAll:'přidat vše',
	removeAll:'odstranit vše',
	itemsCount:'#{count} položky označeny',
	itemsTotal:'#{count} položek celkem',
	besetzt:'prosím vyčkejte...',
	errorDataFormat:"nelze přidat možnosti, neznámý formát",
	errorInsertNode:"nastal problém při pokusu zadání:\n\n\t[#{key}] => #{value}\n\nProces se nezdařil.",
	errorReadonly:"Tato volba #{option} je pouze ke čtení",
	errorRequest:"Omlouváme se, musí být nějaký problém s dálkovým voláním. (Type: #{status})"
});