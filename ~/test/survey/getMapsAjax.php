<?php
//include localization and site config files
require_once("site.config.php");
if(!isset($_SESSION)) { session_start(); }

//include DB AND ACCOUNT INFO CLASSES
		include CONTENT_PATH . '/_classes/db-class.php';
		include CONTENT_PATH . '/_classes/account-class.php';
		$accobj = new Account($_SESSION['accid']);
	
		//include other classes
		include FULL_PATH . '/_inc/_classes/question-class.php';
		include FULL_PATH . '/_inc/_classes/MAP-class.php';
		include FULL_PATH . '/_inc/_classes/user-class.php';
		include FULL_PATH . '/_inc/_classes/admin-class.php';
				
		$userobj = new Admin($_SESSION['uid']);
		$MAPobj = new MAP();
		
		require_once(FULL_PATH . "/_inc/localization.php");
		require_once(FULL_PATH . "/_inc/scripts.php");
	
		$maptype = $_REQUEST['maptype'];
		$search = $_REQUEST['search'];
		$start = $_REQUEST['start'];
		$search = is_null($search) || $search == '' ? null : $search;
		
		// the default limit if the parameter is not specified
		define('DEFAULT_LIMIT', 25);
		 
		if($maptype == 'MAP') {
			$access = $userobj->getLevelAccess('map');
			if($access['VIEW'] == 'MA') {
				$compid = $userobj->headid;
			} else {
				$compid = $userobj->companyid;
			}
			$maplist = $MAPobj->getMAPList($compid, $access);
		} else if($maptype == 'idealMAP') {
			$access = $userobj->getLevelAccess('imap');
			if($access['VIEW'] == 'MA') {
				$compid = $userobj->headid;
			} else {
				$compid = $userobj->companyid;
			}
			$maplist = $MAPobj->getidealMAPList($compid, $access);
		} else {
			$access = $userobj->getLevelAccess('tmap');
			if($access['VIEW'] == 'MA') {
				$compid = $userobj->headid;
			} else {
				$compid = $userobj->companyid;
			}
			if($maptype == 'TalentMAP') {
				$mapt = 'talent';
			} else {
				$mapt = 'team';
			}
			$maplist = $MAPobj->getTMAPList($mapt, $compid, $access, 'analytics');
		}
		//print_r($maplist);
		header('Content-type: text/plain; charset=utf-8');
		 
		$limit = DEFAULT_LIMIT;
		$maps = array();
			$count = 0;
			$current = 0;
			foreach($maplist as $mapid => $userarr) {
				if($current < $start) {

				} else {
				$map = array();
				if($maptype == 'MAP') {
					$name = $userarr['fullname'];
					$emailaddy = $userarr['email'];
					$avatar = $userarr['avatarurl'];
					$searchStr = $name . ' - ' . $emailaddy;
				} else if($maptype == 'idealMAP') {
					$name = $userarr['imapname'];
					$searchStr = $name;
				} else {
					$name = $userarr['tmapname'];
					$searchStr = $name;
				}
				if(is_null($search) || false !== stripos($searchStr, $search) ) {
					if($maptype == 'MAP') {
						$map['email'] = $emailaddy;
						$map['avatar'] = isset($avatar) && !is_null($avatar) && $avatar != '' ? $avatar : '/_accounts/_saviio/_images/_icons/mymap_icon.png';
				  	}
				  	$map['name'] = $name;
				  	$map['mapid'] = $mapid;
				  	$company = $userobj->getCompany($userarr['companyid']);
				  	$map['company'] = $company->companyname;
				  	$maps[] = $map;
				  	$count ++;
				 
				}
			  }
			   $current ++;
			   if ($count >= $limit) break;
			}
			$out = array();
			$out['start'] = $current;
			$out['results'] = $maps;
			echo json_encode($out);