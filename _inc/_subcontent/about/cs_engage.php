			<div id="innerContent">
			<div id="caseStudyTop">
				<h1>A Global Software Solutions provider</h1>
				<p><strong>Engaging with Staff and Clients during stressful times!</strong></p>
			</div>
			<div class="cleaner"></div>
	     <div class="caseStudyList">
	     	<div class="cleaner"></div>
         <div class="infoLeft">
          <img src="/_images/_sidebar/sidebar_analytics.jpg" width="194" height="150" align="left" />
         	<strong>The Situation</strong>
         	<p>The organisation provides support to it's thousands of clients - small and medium sized accounting professionals. During annual service peaks (the end of a tax year) - things can get really busy and stressful! To manage this rise in demand, an additional 42 technical advisors were required to augment their existing staff. An outsourcing contact centre (a Saviio client) was awarded the contract to undertake the work.</p>
         </div>
         <div class="infoRight">
         	<img src="/_images/_sidebar/sidebar_quadman.jpg" width="194" height="150" align="right" />
         	<strong>The Challenges</strong>
         	<p>How does the organisation ensure that the new recruits engage quickly with existing staff, the team culture and provide great service - as if they were the top 'talent' employed within the software company itself?</p>
         </div>
          <div class="infoLeft">
         	<strong>The Solution</strong><p>The contact centre profiled the top performers within the role at their client with Saviio MAPs to create a talentMAP. This was used as an essential part of the selection and recruitment process by benchmarking each individual against this talentMAP.</p>
         </div>
         <div class="infoRight">
         	<strong>The Outcomes</strong>
         	 <ul>
		       	<li>The contact centre was appointed to provide this service because they could demonstrate to their client HOW they would attract, engage, develop and retain the right staff.</li>
						<li>Only one of the 42 people left the induction programme and 41 remained through until the end of the contract (9 months later).</li>
						<li>The Client was delighted with the performance of the "out-sourced" client service advisors and on completion of the contract immediately filled 28 internal posts with 28 people, selected from the contact centre's contracted staff.</li>
					</ul>
         </div>
      </div>
		</div>
