
<div id="menuholder">
  <div id="menu">   
	<?php
	$admincats = $adminobj->getAdminCats();
	foreach($admincats as $catid => $catname) {
		if(is_array($MODArr[$catid])) {
			switch($catid) {
				case 1:
				$sectionclass = 'SVsection';
				break;
				case 2:
				$sectionclass = 'STsection';
				break;
				case 3:
				$sectionclass = 'USsection';
				break;
				case 4:
				$sectionclass = 'MPsection';
				break;
				case 5:
				$sectionclass = 'STsection';
				break;
			}
			echo '<li><a href="javascript:void(0);" class="' . $sectionclass . '">' . _($catname) . '</a></li>';
			foreach($MODArr[$catid] as $modid => $infoarr) {
				switch($infoarr['link']) {
					case "cp":
					$iconimg = 'IC_ViewUsage';
					break;
					case "feedback":
					$iconimg = 'IC_ViewUsage';
					break;
					case "export":
					$iconimg = 'IC_Export';
					break;
					case "companies":
					$iconimg = 'IC_ManageCompanies';
					break;
					case "users":
					$iconimg = 'IC_ManageUsers';
					break;
					case "accesslog":
					$iconimg = 'IC_AdminLog';
					case "emails":
					$iconimg = 'IC_Mail';
					break;
					case "archive":
					$iconimg = 'IC_ManageMaps';
					break;
					case "tmaps&tmap=teammap":
					$iconimg = 'IC_ManageMaps';
					break;
					case "tmaps&tmap=talentmap":
					$iconimg = 'IC_ManageMaps';
					break;
				}
				if($infoarr['link'] != "accesslog") {
					echo '<li><a style="background:url(' . ADMIN_ACCOUNT_PATH . '_images/_icons/' . $iconimg . '.png) no-repeat 7px center;" class="download_now" href="?admin=' . $infoarr['link'] . '">' . _($infoarr['modulename']) . '</a></li>';
				}
			}
		}
	}
	?>
	</div>
</div>
<div id="content_holder">