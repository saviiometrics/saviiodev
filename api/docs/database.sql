SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

CREATE TABLE IF NOT EXISTS `system_api_functions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(70) NOT NULL,
  `description` text NOT NULL,
  `required` text NOT NULL,
  `returns` text NOT NULL,
  `originLocked` char(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

INSERT INTO `system_api_functions` (`id`, `name`, `description`, `required`, `returns`, `originLocked`) VALUES
(null, 'getMapList', '', '', '', '1'),
(null, 'getCompanyList', '', '', '', '1'),
(null, 'getLanguageList', '', '', '', '1'),
(null, 'getUserLevelList', '', '', '', '1'),
(null, 'getCountryList', '', '', '', '1'),
(null, 'getEmailTemplateList', '', '', '', '1'),
(null, 'getAPIData', '', '', '', '1'),
(null, 'getMapData', '', '', '', '1'),
(null, 'getMapComparison', '', '', '', '1'),
(null, 'getUserMaps', '', '', '', '1'),
(null, 'getMapDescription', '', '', '', '1'),
(null, 'getClusterDescription', '', '', '', '1'),
(null, 'getFactorGroupDescription', '', '', '', '1'),
(null, 'getMatchScoreDescription', '', '', '', '1'),
(null, 'addTalentMap', '', '', '', '1'),
(null, 'addTeamMap', '', '', '', '1'),
(null, 'getEmailTemplate', '', '', '', '1'),
(null, 'addUser', '', '', '', '1'),
(null, 'getFunctionList', '', '', '', '1'),
(null, 'getFunctionDescription', '', '', '', '0'),
(null, 'addEmailTemplate', '', '', '', '1'),
(null, 'getTalentMapShareList', '', '', '', '1'),
(null, 'getTeamMapShareList', '', '', '', '1'),
(null, 'addTalentMapShare', '', '', '', '1'),
(null, 'addTeamMapShare', '', '', '', '1');

CREATE TABLE IF NOT EXISTS `system_api_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `resultCode` char(1) NOT NULL,
  `xml` text NOT NULL,
  `origin` varchar(255) NOT NULL,
  `errorMessage` varchar(255) NOT NULL,
  `time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `system_api_originfunctions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `function_id` int(11) NOT NULL,
  `origin_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

INSERT INTO `system_api_originfunctions` (`id`, `function_id`, `origin_id`) VALUES
(null, 1, 1),
(null, 2, 1),
(null, 3, 1),
(null, 4, 1),
(null, 5, 1),
(null, 6, 1),
(null, 7, 1),
(null, 8, 1),
(null, 9, 1),
(null, 10, 1),
(null, 11, 1),
(null, 12, 1),
(null, 13, 1),
(null, 14, 1),
(null, 15, 1),
(null, 16, 1),
(null, 17, 1),
(null, 18, 1),
(null, 19, 1),
(null, 20, 1),
(null, 21, 1),
(null, 22, 1),
(null, 23, 1),
(null, 24, 1),
(null, 25, 1),
(null, 1, 2),
(null, 2, 2),
(null, 3, 2),
(null, 4, 2),
(null, 5, 2),
(null, 6, 2),
(null, 7, 2),
(null, 8, 2),
(null, 9, 2),
(null, 10, 2),
(null, 11, 2),
(null, 12, 2),
(null, 13, 2),
(null, 14, 2),
(null, 15, 2),
(null, 16, 2),
(null, 17, 2),
(null, 18, 2),
(null, 19, 2),
(null, 20, 2),
(null, 21, 2),
(null, 22, 2),
(null, 23, 2),
(null, 24, 2),
(null, 25, 2);

CREATE TABLE IF NOT EXISTS `system_api_origins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `userLocked` char(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

INSERT INTO `system_api_origins` (`id`, `name`, `userLocked`) VALUES
(null, 'Testing Env', '1'),
(null, 'Dashboard', '0');

CREATE TABLE IF NOT EXISTS `system_api_originusers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `origin_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

