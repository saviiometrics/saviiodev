<script type="text/javascript">

	$(function() {
		//   
	});//end doc rdy

</script>

<div id="mapbar"><div class="iconfloat"><img src="<?=ADMIN_ACCOUNT_PATH;?>_images/_icons/IC_Structure.png" /></div> <div class="floatleft"><?=_("Manage Feedback");?></div></div>
<div class="titlebar_shadow"></div>
<div id="content_pad">
<h1 class="MWxHeader"><?=_("Export Data")?></h1>
<p>From here you can export all of the data from the system to a CSV file. Later on you will be able to drag and drop which accounts / companies you want to include / exclude in the export process so you can avoid "junk" data being included.</p>
<p><a href="_inc/exportdata.php" target="_blank">Export All Data</a></p>
<br />

	<div class="cleaner"></div>
</div>

