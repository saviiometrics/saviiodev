<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $iso;?>" />
<title>Saviio Admin</title>
<style type="text/css">
<!--

-->
</style>

<link rel="stylesheet" href="../<?=ADMIN_ACCOUNT_PATH;?>_css/sifr.css" type="text/css" media="all">
<link rel="stylesheet" href="<?=ADMIN_ACCOUNT_PATH;?>_css/login.css" type="text/css" media="all">

</head>

<body>

<div id="container">
	<div id="logotop"></div>
	<div id="boxtop"></div>
    <div id="boxcontent">
    	<form name="loginadmin" id="loginadmin" method="post" action="index.php" enctype="multipart/form-data">
            <h2 class="MWxBox"><?=_("Username");?></h2><br />
            <input type="text" name="savuser" id="savuser" class="ATLogin" /><br />
            <h2 class="MWxBox"><?=_("Password");?></h2><br />
            <input type="password" name="savpass" id="savpass" class="ATLogin" /><br />
            <div style="margin:16px 0px 0px 118px; width:171px;"><input type="submit" value="<?=_("Login");?>" name="firstlogin" id="firtlogin" class="loginbutton" /></div><br />
        </form>
        <p><?=_('This area is for administrators only, your IP has already been logged for security reasons. If you have forgotten your password please use the "forgotten password" link.');?></p>
        <p><?=_("If you don't have a login but require one. Please contact your administrator.");?></p>
        <br />
        <?php
        if($errorstr != '') {
        	echo '<div style="color:red; font-weight:bold; font-size:13px; padding:0px 0px 10px 0px;">' . $errorstr . '</div>';
        }
        ?>
    </div>
    <div id="boxbtm"></div>

</div>

</body>
</html>
