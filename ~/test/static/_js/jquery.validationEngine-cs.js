﻿

(function($) {
	$.fn.validationEngineLanguage = function() {};
	$.validationEngineLanguage = {
		newLang: function() {
			$.validationEngineLanguage.allRules = 	{"required":{    			// Add your regex rules here, you can take telephone as an example
						"regex":"žádný",
						"alertText":"* Toto pole je požadováno",
						"alertTextCheckboxMultiple":"* Prosím oznacte volbu",
						"alertTextCheckboxe":"* Toto pole je vyžadováno."},
					"length":{
						"regex":"žádný",
						"alertText":"*mezi ",
						"alertText2":" a ",
						"alertText3": " znaky povoleny"},
					"maxCheckbox":{
						"regex":"žádný",
						"alertText":"* Povolené množství overení prekroceno"},	
					"minCheckbox":{
						"regex":"žádný",
						"alertText":"* Prosím vyberte ",
						"alertText2":" volby"},	
					"equals":{
						"regex":"žádný",
						"alertText":"* vaše heslo nesouhlasí"},		
					"telephone":{
						"regex":/^[0-9\-\(\)\ ]+$/,
						"alertText":"* neplatné telefonní císlo"},	
					"email":{
						"regex":/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.(?:[A-Z]{2}|COM|ORG|NET|EDU|GOV|MIL|BIZ|INFO|MOBI|NAME|AERO|ASIA|JOBS|MUSEUM)$/i,
						//regex":/^[a-zA-Z0-9_\.-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9\.]{2,5}$/,
						"alertText":"* neplatná emailová adresa"},	
					"date":{
                         "regex":/^[0-9]{4}\-\[0-9]{1,2}\-\[0-9]{1,2}$/,
                         "alertText":"* neplatný datum, musí být ve formátu YYYY-MM-DD"},
					"onlyNumber":{
						"regex":/^[0-9\ ]+$/,
						"alertText":"* pouze císla"},	
					"noSpecialCharacters":{
						"regex":/^[0-9a-zA-Z]+$/,
						"alertText":"* Žádné specialní znaky povoleny"},	
					"ajaxUser":{
						"file":"validateUser.php",
						"extraData":"",
						"alertTextOk":"* Tento uživatel je dostupný",	
						"alertTextLoad":"* Nahrávání, prosím vyckejte",
						"alertText":"* Tento uživatel již existuje"},	
					"ajaxName":{
						"file":"validateUser.php",
						"alertText":"* Toto jméno již existuje",
						"alertTextOk":"* Toto jméno je k dispozici",	
						"alertTextLoad":"* Nahrávání, prosím vyckejte"},		
					"onlyLetter":{
						"regex":/^[a-zA-Z\ \']+$/,
						"alertText":"* Pouze písmena"}
					}	
		}
	}
})(jQuery);

$(document).ready(function() {	
	$.validationEngineLanguage.newLang()
});