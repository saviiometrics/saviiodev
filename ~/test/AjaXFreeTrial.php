 <?php

	//this doesn't use the paths as normal. Just uses relative paths to find the classes as being loaded for the main site. So easier to just use relative paths?
	if(!isset($_SESSION)) { session_start(); }
	$phpsessid = session_id();
	//include localization and site config files
	require_once("survey/site.config.php");

	//include DB AND ACCOUNT INFO CLASSES
	include 'survey/_inc/_classes/db-class.php';
	include 'survey/_inc/_classes/account-class.php';

	$accobj = new Account();
 
	//include other classes
	include 'survey/_inc/_classes/MAP-class.php';
	include 'survey/_inc/_classes/question-class.php';
	include 'survey/_inc/_classes/user-class.php';
	include 'survey/_inc/_classes/admin-class.php';
		
	require_once("survey/_inc/localization.php");
	require_once("survey/_inc/scripts.php");
	

	/****************************************************************************
	*																																						*
	*																	ADD ACCOUNT																*
	*																																						*
	****************************************************************************/
	
	/*
	if(isset($_POST['addAccount'])) {
		//print_r($_POST['levelObj']);
		//true at the end is to say it is a free trial account being added. Basically uses special account to add the new account!
		if(!$accobj->addAccount($_POST['accName'], $_POST['adminEmail'], $_POST['serviceLevel'], $_POST['salutation'], $_POST['firstname'], $_POST['surname'], $_POST['countryid'], true)) {
			echo 'There was an error adding the account. Please try again.';
		} else {
			echo 'The account has been added successfully!';
		}
	}
	*/
	
	class Push_Highrise{
	
		var $highrise_url = 'https://saviio.highrisehq.com'; // your highrise url, e.g. http://yourcompany.highrisehq.com
		var $api_token = 'eab7b4e00274617f2525451f3bf82b23'; // your highrise api token; can be found under My Info
		//Jessicas ID
		var $task_assignee_user_id = '401761'; // user id of the highrise user who gets the task assigned 
		var $category = '2522836'; // the category where deals will be assigned to
		
		var $errorMsg = "";
		
		function pushDeal($request) {
			$curl = curl_init($this->highrise_url.'/deals.xml');
			curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
			curl_setopt($curl,CURLOPT_USERPWD,$this->api_token.':x');
			
			//Setup XML to POST
			curl_setopt($curl,CURLOPT_HTTPHEADER,Array("Content-Type: application/xml"));
			curl_setopt($curl,CURLOPT_POST,true);
			curl_setopt($curl,CURLOPT_POSTFIELDS,'<deal>
				<name>'.htmlspecialchars($request['sSubject']).'</name>
				<price-type>fixed</price-type>
				<category-id type="integer">'.$category.'</category-id>
				<responsible-party-id type="integer">'.$this->task_assignee_user_id.'</responsible-party-id>
				<background>'.htmlspecialchars($request['sNotes']).'</background>
				<visible-to>Everyone</visible-to>
				<party-id type="integer">'.$this->_person_in_highrise($request).'</party-id>
			</deal>');
			
			curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
			curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
			$xml = curl_exec($curl);
			curl_close($curl);
			return '';
		}
		
		function pushNote($request) {
			$curl = curl_init($this->highrise_url.'/notes.xml');
			curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
			curl_setopt($curl,CURLOPT_USERPWD,$this->api_token.':x');
			
			$bodyPrefix = "This person has requested a free Trial for the website. Please contact them using the details saved within the contact information above";
					
			//Setup XML to POST
			curl_setopt($curl,CURLOPT_HTTPHEADER,Array("Content-Type: application/xml"));
			curl_setopt($curl,CURLOPT_POST,true);
			curl_setopt($curl,CURLOPT_POSTFIELDS,'<note>
				<subject-id type="integer">'.$this->_person_in_highrise($request).'</subject-id>
				<subject-type>Free Trial</subject-type>
				<body>'.$bodyPrefix.'</body>
			</note>');
			
			curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
			curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
			$xml = curl_exec($curl);
			curl_close($curl);
			return '';
		}
		
		function pushTask($request) {
			$curl = curl_init($this->highrise_url.'/tasks.xml');
			curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
			curl_setopt($curl,CURLOPT_USERPWD,$this->api_token.':x');
			
			$bodyPrefix = "New Free Trial Request"; // set the subject
					
			//Setup XML to POST
			curl_setopt($curl,CURLOPT_HTTPHEADER,Array("Content-Type: application/xml"));
			curl_setopt($curl,CURLOPT_POST,true);
			curl_setopt($curl,CURLOPT_POSTFIELDS,'<task>
				<subject-id type="integer">'.$this->_person_in_highrise($request).'</subject-id>
				<subject-type>Party</subject-type>
				<body>'.$bodyPrefix.': ' . $request['sFirstName'] . ' ' . $request['sLastName'] . ' (' . $request['sEmail'] . ')' . '</body>
				<frame>today</frame>
				<public type="boolean">true</public>
				<owner-id type="integer">'.$this->task_assignee_user_id.'</owner-id>
			</task>');
			
			curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
			curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
			$xml = curl_exec($curl);
			curl_close($curl);
			return '';
		}
		
		function pushContact($request) {
			//Check that person doesn't already exist
			
			$id = $this->_person_in_highrise($request);
			if($id < 0){
					
				$curl = curl_init($this->highrise_url.'/people.xml');
				curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
				curl_setopt($curl,CURLOPT_USERPWD,$this->api_token.':x'); 
				
				curl_setopt($curl,CURLOPT_HTTPHEADER,Array("Content-Type: application/xml"));
				curl_setopt($curl,CURLOPT_POST,true);
				curl_setopt($curl,CURLOPT_POSTFIELDS,'<person>
					<first-name>'.htmlspecialchars($request['sFirstName']).'</first-name>
					<last-name>'.htmlspecialchars($request['sLastName']).'</last-name>
					<background>'.htmlspecialchars($request['staff_comment']).'</background>
					<company-name>'.htmlspecialchars($request['sCompany']).'</company-name>
					<contact-data>
						<email-addresses>
							<email-address>
								<address>'.htmlspecialchars($request['sEmail']).'</address>
								<location>Work</location>
							</email-address>
						</email-addresses>
					<phone-numbers>
						<phone-number>
							<number>'.htmlspecialchars($request['sPhone']).'</number>
							<location>Work</location>
						</phone-number>
					</phone-numbers>
					<addresses>
					    <address>
					      <city>'.htmlspecialchars($request['sCity']).'</city>
					      <country>'.htmlspecialchars($request['sCountry']).'</country>
					      <state>'.htmlspecialchars($request['sState']).'</state>
					      <street>'.htmlspecialchars($request['sStreet']).'</street>
					      <zip>'.htmlspecialchars($request['sZip']).'</zip>
					      <location>Work</location>
					    </address>
					  </addresses>
					</contact-data>
				</person>');
		
				curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
				curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
		
				$xml = curl_exec($curl);
				curl_close($curl);		
			
			}else{
				$this->errorMsg = "Person already in Highrise";
			}
			return '';
		}
		
		//Search for a person in Highrise 
		function _person_in_highrise($person){
			$curl = curl_init($this->highrise_url.'/people/search.xml?term='.urlencode($person['sFirstName'].' '.$person['sLastName']));
			curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
			curl_setopt($curl,CURLOPT_USERPWD,$this->api_token.':x'); //Username (api token, fake password as per Highrise api)
	
			curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
			curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
	
			$xml = curl_exec($curl);
			curl_close($curl);	
			
			//Parse XML
			$people = simplexml_load_string($xml);
			echo($people);
			$id = '-1';
			foreach ($people->person as $person ) {
				if($person != null) {
					$id = $person->id;
				}	
			}
			return $id;
			
		}	
		
	}
	
	
		$error = false;
		if(!preg_match("(^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.(?:[A-Z]{2}|COM|ORG|NET|EDU|GOV|MIL|BIZ|INFO|MOBI|NAME|AERO|ASIA|JOBS|MUSEUM)$)", strtoupper($_REQUEST['sEmail']))) {
			$error = true;
		} else if(trim($_REQUEST['sFirstName']) == '' || trim($_REQUEST['sLastName']) == '' || trim($_REQUEST['sPhone']) == '' || trim($_POST['contactWhen']) == '') {
			$error = true;
		}
	
		//CREATE A NEW HIGHRISE INSTANCE AND ADD CONTACT (IF THEY DONT EXIST) ALSO PUSH TASK AND NOTE TO DAVID
		if($error == false) {
			
			$p = new Push_Highrise();
			if(isset($_POST['freeTrial'])) {
			
				$p->pushContact($_REQUEST);
				$p->pushTask($_REQUEST);
				$p->pushNote($_REQUEST);
				
			  /****************************************************************************
			  //															SEND OUT MAIL																*
			  //**************************************************************************/
				
				$mail = new PHPMailer();
				$mail->IsSMTP(); // telling the class to use SMTP
				$mail->Host       = MAIL_HOSTNAME; // SMTP server
				//$mail->SMTPDebug  = 1;                     // enables SMTP debug information (for testing)
				$mail->SMTPAuth   = true;                  // enable SMTP authentication
				$mail->Username   = MAIL_USERNAME; // SMTP account username
				$mail->Password   = MAIL_PASSWORD;        // SMTP account password
				
				$body .= '<p>Thank you for your interest in the free trial for Saviio MAPs! We will contact you shortly according to your request. Your details can be seen below for information purposes.</p><br />';
				$body .= '<strong>FullName:</strong> ' . $_POST['sFirstName'] . ' ' . $_POST['sLastName'] . '<br />';
				$body .= '<strong>Company Name:</strong> ' . $_POST['accName'] . '<br />';
				$body .= '<strong>Email:</strong> ' . $_POST['sEmail'] . '<br />';
				$body .= '<strong>Phone:</strong> ' . $_POST['sPhone'] . '<br />';
				$body .= '<strong>We will contact you:</strong> ' . $_POST['contactWhen'] . '<br />';
			
				//now get the template file!
				$email_template = file_get_contents('/var/www/saviio.com/survey/_inc/email_template.php');
				$tags_original   = array("[EMAIL_HEADER]", "[EMAIL_COPY]");
				$tags_replaced = array(_('Thank you for your interest in our Free Trial'), $body);
			
				$emailHTML = str_replace($tags_original, $tags_replaced, $email_template);
			
				$mail->From = NR_EMAIL;
				$mail->FromName = ACC_NAME;
				$mail->CharSet = "UTF-8";
				$mail->Subject = _("Saviio MAPs Free Trial");
				$mail->AltBody = _("To view the message, please use an HTML compatible email viewer."); // optional, comment out and test
				$mail->MsgHTML($emailHTML);
				$mail->AddAddress($_POST['sEmail'], $_POST['sFirstName'] . ' ' . $_POST['sLastName']);
				$mail->AddBCC('david.foggin@saviio.com', 'David Foggin');
				$mail->AddBCC('jessica.foggin@saviio.com', 'Jessica Foggin');
				$mail->AddBCC('richard.thompson@saviio.com', 'Richard Thompson');
			
				if(!$mail->Send()) {
				 	printf('Your free trial request was not sent. Please check your email address and try again.');
				} else {
				  printf('Thank you, your free trial request was successfully received.');
				}
			} else if(isset($_POST['contactMe'])) {
				
				$p->pushContact($_REQUEST);
				$p->pushTask($_REQUEST);
				
				/****************************************************************************
			  //															SEND OUT MAIL																*
			  //**************************************************************************/
				
				$mail = new PHPMailer();
				$mail->IsSMTP(); // telling the class to use SMTP
				$mail->Host       = MAIL_HOSTNAME; // SMTP server
				//$mail->SMTPDebug  = 1;                     // enables SMTP debug information (for testing)
				$mail->SMTPAuth   = true;                  // enable SMTP authentication
				$mail->Username   = MAIL_USERNAME; // SMTP account username
				$mail->Password   = MAIL_PASSWORD;        // SMTP account password
				
				$body .= '<p>The following person has requested we contact them:</p><br />';
				$body .= '<strong>FullName:</strong> ' . $_POST['sFirstName'] . ' ' . $_POST['sLastName'] . '<br />';
				$body .= '<strong>Company Name:</strong> ' . $_POST['accName'] . '<br />';
				$body .= '<strong>Email:</strong> ' . $_POST['sEmail'] . '<br />';
				$body .= '<strong>Phone:</strong> ' . $_POST['sPhone'] . '<br />';
				$body .= '<strong>We will contact you:</strong> ' . $_POST['contactWhen'] . '<br />';
			
				//now get the template file!
				$email_template = file_get_contents('/var/www/saviio.com/survey/_inc/email_template.php');
				$tags_original   = array("[EMAIL_HEADER]", "[EMAIL_COPY]");
				$tags_replaced = array(_('New contact Request'), $body);
			
				$emailHTML = str_replace($tags_original, $tags_replaced, $email_template);
			
				$mail->From = NR_EMAIL;
				$mail->FromName = ACC_NAME;
				$mail->CharSet = "UTF-8";
				$mail->Subject = _("New contact request");
				$mail->AltBody = _("To view the message, please use an HTML compatible email viewer."); // optional, comment out and test
				$mail->MsgHTML($emailHTML);
				$mail->AddAddress('david.foggin@saviio.com', 'David Foggin');
				$mail->AddAddress('jessica.foggin@saviio.com', 'Jessica Foggin');
				$mail->AddAddress('richard.thompson@saviio.com', 'Richard Thompson');
				
				$mail->Send();
				
			}
		} else {
			printf('Sorry but you cannot send a false email or blank fields to this form!');
		}


?>