/**
 * Localization strings for the UI Multiselect widget
 *
 * @locale zh, zh_CN
 */

$.extend($.ui.multiselect.locale, {
	addAll:'添加所有',
	removeAll:'删除所有',
	itemsCount:'#{count} 已选项目',
	itemsTotal:'#{count} 项目总数',
	busy:'请等待...',
	errorDataFormat:"无法添加选项, 未知的数据格式",
	errorInsertNode:"添加此项目时出现错误:\n\n\t[#{key}] => #{value}\n\n操作终止.",
	errorReadonly:"此选项 #{option} 为只读",
	errorRequest:"对不起! 长途通话好像出现问题. (类型: #{status})"
});
