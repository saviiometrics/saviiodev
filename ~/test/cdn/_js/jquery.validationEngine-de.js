

(function($) {
	$.fn.validationEngineLanguage = function() {};
	$.validationEngineLanguage = {
		newLang: function() {
			$.validationEngineLanguage.allRules = 	{"required":{    			// Fügen Sie hier Ihre Regex-Regeln hinzu – Sie können Telefon als Beispiel nehmen
						"regex":"keine",
						"alertText":"* Eingabe erforderlich",
						"alertTextCheckboxMultiple":"* Bitte wählen Sie eine Option",
						"alertTextCheckboxe":"* Diese Checkbox wird benötigt"},
					"length":{
						"regex":"keine",
						"alertText":"*Zwischen ",
						"alertText2":" und ",
						"alertText3": " Zeichen erlaubt"},
					"maxCheckbox":{
						"regex":"keine",
						"alertText":"* Anzahl erlaubter Optionen überschritten"},	
					"minCheckbox":{
						"regex":"keine",
						"alertText":"* Bitte auswählen ",
						"alertText2":" Optionen"},	
					"confirm":{
						"regex":"keine",
						"alertText":"* Ihre Passwörter stimmen nicht überein"},		
					"telephone":{
						"regex":"/^[0-9\-\(\)\ ]+$/",
						"alertText":"* Ungültige Telefonnummer"},	
					"email":{
						"regex":"/^[a-zA-Z0-9_\.\-]+\@([a-zA-Z0-9\-]+\.)+[a-zA-Z0-9]{2,4}$/",
						//"regex":"/^[a-zA-Z0-9_\.-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9\.]{2,5}$/",
						"alertText":"* Ungültige E-Mail-Adresse"},	
					"date":{
                         "regex":"/^[0-9]{4}\-\[0-9]{1,2}\-\[0-9]{1,2}$/",
                         "alertText":"* Ungültiges Datum: JJJJ.MM.TT verwenden"},
					"onlyNumber":{
						"regex":"/^[0-9\ ]+$/",
						"alertText":"* Nur Zahlen von 0-9 erlaubt"},	
					"noSpecialCharacters":{
						"regex":"/^[0-9a-zA-Z]+$/",
						"alertText":"* Keine speziellen Buchstaben erlaubt"},	
					"ajaxUser":{
						"file":"validateUser.php",
						"extraData":"",
						"alertTextOk":"* Dieser Benutzer ist verfügbar",	
						"alertTextLoad":"* Lädt, bitte warten",
						"alertText":"* Dieser Benutzer ist bereits vergeben"},	
					"ajaxName":{
						"file":"validateUser.php",
						"alertText":"* Dieser Name ist bereits vergeben",
						"alertTextOk":"* Dieser Name ist verfügbar",	
						"alertTextLoad":"* Lädt, bitte warten"},		
					"onlyLetter":{
						"regex":"/^[a-zA-Z\ \']+$/",
						"alertText":"* Nur Buchstaben verwenden"}
					}	
		}
	}
})(jQuery);

$(document).ready(function() {	
	$.validationEngineLanguage.newLang()
});