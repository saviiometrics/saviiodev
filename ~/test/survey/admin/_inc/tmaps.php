<?php
	$MAPobj = new MAP();
	if($_GET['tmap'] == 'talentmap') {
		$tmapheader = 'Talent MAP';
		$tmaptype = 'talent';
	} else {
		$tmapheader = 'Team MAP';
		$tmaptype = 'team';
	}
?>
<input type="hidden" name="tmapsnomapcopy" id="tmapsnomapcopy" value="<?php printf(_("I'm sorry you need to enter at least one person to a %s"), $tmapheader);?>" />
<input type="hidden" name="tmapdeletecopy" id="tmapdeletecopy" value="<?php printf(_("Are you sure you want to delete this %s"), $tmapheader);?>" />
<input type="hidden" name="tmapcopyadd" id="tmapcopyadd" value="<?php printf(_("Add new %s"), $tmapheader);?>" />
<input type="hidden" name="tmapcopyedit" id="tmapcopyedit" value="<?php printf(_("Edit %s"), $tmapheader);?>" />
<input type="hidden" name="tmapcopysave" id="tmapcopysave" value="<?php printf(_("Save %s"), $tmapheader);?>" />

<input type="hidden" name="tmapheader" id="tmapheader" value="<?=$tmapheader;?>" />
<input type="hidden" name="tmaptype" id="tmaptype" value="<?=$tmaptype;?>" />
<input type="hidden" name="compid" id="compid" value="<?=$adminobj->companyid;?>" />
<input type="hidden" name="editTMAPID" id="editTMAPID" />
<input type="hidden" name="addeditTMAP" id="addeditTMAP" />

<!-- Overlay Box for Adding / Editing Companies -->
<div id="MAPbox" class="MAPsBox">
	<div class="addpad">
		<div class="cicon" style="padding:10px;"><img src="<?=ADMIN_ACCOUNT_PATH;?>_images/_icons/IC_AddCompany.png" /></div>
		<h2><? printf(_('Add a new %s'), $tmapheader);?></h2>
		<p>
			<?php if ($_GET['tmap'] == 'talentmap') {
				echo _("Drag and drop the names of the people you want in your Talent MAP. This always uses the latest MAP from the individuals you select.");
			} elseif ($_GET['tmap'] == 'teammap') {
				echo _("Drag and drop the names of the people you want in your Team MAP. This always uses the latest MAP from the individuals you select.");
			}
			?>
		</p>
		<div id="addTMAPheader"><? printf(_('%s details'), $tmapheader);?></div>
		<form id="tmapform" name="tmapform">
			<table border="0" cellpadding="3" cellspacing="4" width="736">
				<tr>
					<td><?php printf (_("%s Name"), $tmapheader);?></td>
					<td align="right"><input type="text" name="tmapName" id="tmapName" class="validate[required,length[5,50]] textfield" size="30" /></td>
				</tr>
				<tr>
					<td><?php printf (_("%s Description"), $tmapheader);?></td>
					<td align="right"><input type="text" name="tmapDesc" id="tmapDesc" class="validate[required,length[5,100]] textfield" size="30" /></td>
				</tr>
			</table>
		</form>
		<br />
		<strong><span style="padding-left:5px;"><? printf(_('Create your %s'), $tmapheader);?></span></strong>
		<div style="width:728px; float:left; margin:0px 0px 0px 2px;">
			<p style="padding:2px;"><?php printf(_("To the left is list of MAP's available for you to choose from in order to create your %s. Simply drag and drop from the available list (on the left) to the selected list (on the right). Alternatively you can simply click the + icon on the name tab to add the desired item to the selected list."), $tmapheader);?></p>
		</div>
		<div class="cleaner"></div>
		<div id="addMAPbox">
		  <select id="addMAP" class="multiselect" multiple="multiple" name="addMAP[]"></select>
		</div>

		<div class="cleaner"></div>
		<!--HeadHunter Share here-->
		<div id="addTMAPheader"><?php echo _("Access Controls");?></div><br />
		<div style="width:344px; min-height:100px; float:left; margin:0px 0px 0px 12px; border-right:1px solid #ccc;">
			<span style="color:#96c09c; font-weight:bold;"><?=_("Share Across Administrators");?></span>
			<br /><br />
			<select class="SelectAdmins" multiple size="5">
				<?php
				$userlist = $adminobj->getUsers(4,1,true);
				foreach($userlist as $uid => $userARR) {
					echo '<option value="' . $uid . '">' . $userARR['firstname'] . ' ' . $userARR['surname'] . '</option>'. "\n";
				}
				?>
			</select>
		</div>
		<div style="width:356px; min-height:100px; float:left; margin:0px 0px 0px 12px; border-right:1px solid #ccc;">
			<span style="color:#96c09c; font-weight:bold;"><?=_("Share Across Specific Accounts");?></span>
			<br /><br />
			<select class="SelAccessCompanies" multiple size="5">
				<?php
				$companies = $adminobj->getCompanies();
				foreach($companies as $companyid => $comparr) {
					echo '<option value="' . $companyid . '">' . $comparr['companyname'] . '</option>'. "\n";
				}
				?>
			</select>
		</div>
		<div class="cleaner"></div>
	  <p>* <?php echo str_replace('%s',$tmapheader,_("Please Note that the %s's will ALWAYS be viewable by any Account Administrators. If you want to allow others access to this %s please select the Accounts or Administrators you wish to share with from the appropriate list. When sharing with entire Accounts you can also select the level the users in that Account must have in order view your created %s"));?> 
		<p>
			<?=_('To save the MAP click the "Save" Button, else click "close" or press ESC to close the dialog box.');?>
		</p>
		<!-- yes/no buttons -->
		<p>
			<button class="savemap"> Save </button>
			<button class="close"> Close </button>
		</p>

	</div>
</div>


<div id="mapbar"><div class="iconfloat""><img src="<?=ADMIN_ACCOUNT_PATH;?>_images/_icons/IC_Structure.png" /></div> <div class="floatleft"><?=_($tmapheader);?></div></div>
<div class="titlebar_shadow"></div>
<div id="content_pad">
<h1 class="MWxHeader"><?php printf (_("Manage %s's"), $tmapheader); ?></h1>
<p><?php printf(_("This page enables you to manage all of your %s's"), $tmapheader);?></p>
<br />

<?php
if($_GET['tmap'] == 'teammap') {
	$strMAP = 'team';
} else if($_GET['tmap'] == 'talentmap') {
	$strMAP = 'talent';
}
$access = $adminobj->getLevelAccess('tmap');
if($access['VIEW'] == 'MA') {
	$compid = $adminobj->headid;
} else {
	$compid = $adminobj->companyid;
}
$tmaplist = $MAPobj->getTMAPList($strMAP, $compid, $access);
?>

<div id="tableHolder">
	<table cellspacing="1" class="tablesorter">
		<thead>
			<tr>
				<th><?php printf(_("%s Name"),$tmapheader);?></th>
				<th><?=_("Description");?></th>
				<th><?=_("Contains");?></th>
				<th><?=_("Created By");?></th>
				<th><?=_("Created On");?></th>
				<th><?=_("Admin Panel");?></th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<th><?php printf(_("%s Name"),$tmapheader);?></th>
				<th><?=_("Description");?></th>
				<th><?=_("Contains");?></th>
				<th><?=_("Created By");?></th>
				<th><?=_("Created On");?></th>
				<th><?=_("Admin Panel");?></th>
			</tr>
		</tfoot>
		<tbody>
			<?php 
			
			$shareaccess = $adminobj->getAccountShareAccess('tmap');
	
			function checkAccess($acctype, $html, $tmaparr, $adminid, $headid, $companyid, $checktype, $sharerights) {
				switch($acctype) {
					case 'ALL':
					$adminstring = $html;
					break;
					case 'CREATED':
					if($tmaparr['uid'] == $adminid || $tmaparr['owneruid'] == $adminid) {
							$adminstring = $html;
					} else {
						if($sharerights[$tmaparr['companyid']][$checktype] == 'TRUE') {
							$adminstring = $html;
						}
					}
					break;
					case 'MA':
					if($tmaparr['headid'] == $headid) {
						$adminstring = $html;
					} else {
						if($sharerights[$tmaparr['companyid']][$checktype] == 'TRUE') {
							$adminstring = $html;
						}
					}
					break;
					case 'SA':
					if($tmaparr['companyid'] == $companyid) {
						$adminstring = $html;
					} else {
						if($sharerights[$tmaparr['companyid']][$checktype] == 'TRUE') {
							$adminstring = $html;
						}
					}
					break;
				}
				return $adminstring;
			}
			
			foreach($tmaplist as $tmapid => $tmaparr) {
				$adminstring = '';
				$editmapHTML = '<a href="javascript:void(0);" class="editMAP" id="tmap_' . $tmapid . '"><img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_EditApplication.png" alt="Edit ' . $tmapheader . '" title="Edit ' . $tmapheader . '" /></a>&nbsp;&nbsp;';
				$deletemapHTML = '<a href="javascript:void(0);" class="deleteMAP" id="tmapdel_' . $tmapid . '"><img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_EraserMinus.png" alt="Delete ' . $tmapheader . '" title="Delete ' . $tmapheader . '" /></a>&nbsp;&nbsp;';
				$archivemapHTML = '<a href="javascript:void(0);" class="archiveMAP" id="tmaparc_' . $tmapid . '"><img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_Archive.png" alt="Archive ' . $tmapheader . '" title="Archive ' . $tmapheader . '" /></a>';

				$adminstring .= checkAccess($access['DELETE'], $deletemapHTML, $tmaparr, $adminobj->uid, $adminobj->headid, $adminobj->companyid, 'DELETE', $shareaccess);
				$adminstring .= checkAccess($access['EDIT'], $editmapHTML, $tmaparr, $adminobj->uid, $adminobj->headid, $adminobj->companyid, 'EDIT', $shareaccess);
				$adminstring .= checkAccess($access['ARCHIVE'], $archivemapHTML, $tmaparr, $adminobj->uid, $adminobj->headid, $adminobj->companyid, 'ARCHIVE', $shareaccess);

				if($adminstring == '') {
					$adminstring = _('Read Only');
				}
				
				$userstring = '<strong>' . sprintf(_("User List for this %s"),$tmapheader) . '</strong><br /><ul class="mapuserlist">';
				$usernames = $MAPobj->getNamesVMAPids($tmaparr['mapids']);
				$usercount = count($usernames);
				foreach($usernames as $uid => $username) {
						$userstring .= '<li>'.$username['fullname'] . '</li>';
				}
				echo '</ul>' . "\n";
				echo '<tr>' . "\n";
				echo '<td>' . $tmaparr['tmapname'] . '</td>' . "\n";
				echo '<td>' . $tmaparr['tmapdesc'] . '</td>' . "\n";
				echo '<td><a class="userlist">User List</a><div class="tooltip"><div class="tooltiptop"></div><div class="tooltipmid">' . $userstring . '</div><div class="tooltipbtm"></div></div></td>' . "\n";
				echo '<td>' . $tmaparr['creator'] . '</td>' . "\n";
				echo '<td>' . date("d/m/Y", $tmaparr['created']) . '</td>' . "\n";
				echo '<td><div class="makerelative">' . $adminstring . '</div></td>' . "\n";
				echo '</tr>' . "\n";
			}
			?>
		</tbody>
	</table>
	</div>
	<div class="cleaner"></div>
</div>
