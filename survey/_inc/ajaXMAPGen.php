﻿<?php
		//header('Content-Type: text/html; charset=ISO-8859-1');
	  if(!isset($_SESSION)) { session_start(); }
		$phpsessid = session_id();
		$start_time=microtime(true);

		//include localization and site config files
		require_once("../site.config.php");
		//include DB AND ACCOUNT INFO CLASSES
		include CONTENT_PATH . '/_classes/db-class.php';
		include CONTENT_PATH . '/_classes/account-class.php';
		$accobj = new Account($_SESSION['accid']);
	
	//include other classes
		include FULL_PATH . '/_inc/_classes/question-class.php';
		include FULL_PATH . '/_inc/_classes/MAP-class.php';
		include FULL_PATH . '/_inc/_classes/user-class.php';
		include FULL_PATH . '/_inc/_classes/admin-class.php';
		
		$userobj = new Admin($_SESSION['uid']);
		$MAPobj = new MAP();
		
		require_once(FULL_PATH . "/_inc/localization.php");
		require_once(FULL_PATH . "/_inc/scripts.php");

		$fids = $MAPobj->getfids();
    $fgs = $MAPobj->getFactorGroups();
    $cids = $MAPobj->getClusters();
    $qobj = new Question();
    //get list of responses sorted in array via qid than alignment;
    $stquestions = $qobj->getSTResponses();
    $_SESSION['stquestions'] = $stquestions;
		
		$leftside = $_GET['leftType'];
		$rightside = $_GET['rightType'];
		$_SESSION['leftType'] = $leftside;
		$_SESSION['rightType'] = $rightside;

		$access = $userobj->getLevelAccess('tmap');
		if($access['EDIT'] == 'MA') {
			$compid = $userobj->headid;
		} else {
			$compid = $userobj->companyid;
		}
		//rankLink
		$leftIssueData = array();
		switch($leftside) {
			case 'idealMAP':
				$l_mapids = $_GET['leftMAP'];
				$imapObj = $MAPobj->getiMAPvid($_GET['leftMAP']);
				$leftname = $imapObj->name;
				$leftEmail = null;
				$leftmapUID = $imapObj->uid;
				$issuer = $userobj->getUserObject($imapObj->uid);
				$leftIssueData['issuedbyname'] = "$issuer->firstname $issuer->surname";
				$leftIssueData['issuedbyemail'] = $issuer->email;
				$leftIssueData['completedon'] = $imapObj->time_created;
				//get multigen left map select0r
				//$leftswitchHTML = GenSelectHTML($leftmapUID, 'left', $MAPobj, $l_mapids);
				$leftswitchHTML = '';
			break;
			case 'MAP':
				$l_mapids = $_GET['leftMAP'];
				$tmapobj = $MAPobj->getMAPvid($_GET['leftMAP']);
				$leftname = $tmapobj->firstname . ' ' . $tmapobj->surname;
				$leftEmail = $tmapobj->email;
				$leftmapUID = $tmapobj->uid;
				$issuer = $userobj->getUserObject($tmapobj->uid);
				$issuer = $userobj->getUserObject($issuer->adminid);
				$leftIssueData['issuedbyname'] = "$issuer->firstname $issuer->surname";
				$leftIssueData['issuedbyemail'] = $issuer->email;
				$leftIssueData['issuedon'] = $tmapobj->timeissue;
				$leftIssueData['completedon'] = $tmapobj->timefinish;
				//get multigen left map select0r
				$leftswitchHTML = GenSelectHTML($leftmapUID, 'left', $MAPobj, $l_mapids, $leftname);
			break;
			case 'TeamMAP':
				$l_mapids = $MAPobj->getmapidsVTMAP($_GET['leftMAP']);
				$tmapobj = $MAPobj->getTMAP($_GET['leftMAP']);
				$leftname = $tmapobj->tmapname;
				$leftEmail = null;
				$issuer = $userobj->getUserObject($tmapobj->uid);
				$leftIssueData['issuedbyname'] = "$issuer->firstname $issuer->surname";
				$leftIssueData['issuedbyemail'] = $issuer->email;
				$leftIssueData['completedon'] = $tmapobj->created;
				
				if(!$MAPobj->checkTMAPAccess($_GET['leftMAP'], $compid, $userobj->uid, $access)) {
					$leftswitchHTML = sprintf(_('This %s is read only'), $leftside);
				} else {
					$leftswitchHTML = '<strong style="font-size: 11px;">' . _("Edit this") . ' ' .  $leftside . '</strong><br /><div class="fl marginpad"><img src="' . ACCOUNT_PATH . '_images/_icons/IC_EditTMAP.png" /></div><div class="fl marginpad"><a href="javascript:void(0)" id="tmap_' . $_GET['leftMAP'] . '" class="edittmap">' . $leftname . '</a></div><div class="cleaner"></div>';
				}
			break;
			case 'TalentMAP':
				$l_mapids = $MAPobj->getmapidsVTMAP($_GET['leftMAP']);
				$tmapobj = $MAPobj->getTMAP($_GET['leftMAP']);
				$leftname = $tmapobj->tmapname;
				$leftEmail = null;
				$issuer = $userobj->getUserObject($tmapobj->uid);
				$leftIssueData['issuedbyname'] = "$issuer->firstname $issuer->surname";
				$leftIssueData['issuedbyemail'] = $issuer->email;
				$leftIssueData['completedon'] = $tmapobj->created;
				if(!$MAPobj->checkTMAPAccess($_GET['leftMAP'], $compid, $userobj->uid, $access)) {
					$leftswitchHTML = sprintf(_('This %s is read only'), $leftside);
				} else {
					$leftswitchHTML = '<strong style="font-size: 11px;">' . _("Edit this") . ' ' . $leftside . '</strong><br /><div class="fl marginpad"><img src="' . ACCOUNT_PATH . '_images/_icons/IC_EditTMAP.png" /></div><div class="fl marginpad"><a href="javascript:void(0)" id="tmap_' . $_GET['leftMAP'] . '" class="edittmap">' . $leftname . '</a></div><div class="cleaner"></div>';
				}
			break;
		}	
		//set session var for lmapids
		$_SESSION['l_mapids'] = $l_mapids;
		$r_maparr = explode(',',$_GET['rightMAPs']);
		
		//change this for imaps as uses different survey db!
		if($leftside == 'idealMAP') {
			$leftscores = $MAPobj->getiScores($l_mapids);
		} else {
			$leftscores = $MAPobj->getScores($l_mapids);
		}
		
		
		//don't rank if only 1v1! as that's comparing innit :)
		$arrFactorScores = array();
		$arrDeviations = array();
		$rightIssueData = array();
		if(count($r_maparr) > 1) {
			$k = 0;//rankid
			foreach($r_maparr as $mapid) {
				if($rightside == 'TeamMAP' || $rightside == 'TalentMAP') {
					//mapid here relates to the tmapid not the MAP mapid.. if you see what i mean :)
					$tmapobj = $MAPobj->getTMAP($mapid);
					$rightname = $tmapobj->tmapname;
					$rightEmail = null;
					$r_mapids = $MAPobj->getmapidsVTMAP($mapid);
					$rightscores = $MAPobj->getScores($r_mapids);
					$issuer = $userobj->getUserObject($tmapobj->uid);
					$rightIssueData['issuedbyname'] = "$issuer->firstname $issuer->surname";
					$rightIssueData['issuedbyemail'] = $issuer->email;
					$rightIssueData['completedon'] = $tmapobj->created;
				} else if($rightside == 'MAP') {
					$r_mapids = $mapid;
					$tmapobj = $MAPobj->getMAPvid($mapid);
					$rightname = $tmapobj->firstname . ' ' . $tmapobj->surname;
					$rightEmail = $tmapobj->email;
					$rightmapUID = $tmapobj->uid;
					$rightscores = $MAPobj->getScores($r_mapids);
					$issuer = $userobj->getUserObject($tmapobj->uid);
					$issuer = $userobj->getUserObject($issuer->adminid);
					$rightIssueData['issuedbyname'] = "$issuer->firstname $issuer->surname";
					$rightIssueData['issuedbyemail'] = $issuer->email;
					$rightIssueData['issuedon'] = $tmapobj->timeissue;
					$rightIssueData['completedon'] = $tmapobj->timefinish;		
				} else if($rightside == 'idealMAP') {
					$r_mapids = $mapid;
					$imapObj = $MAPobj->getiMAPvid($mapid);
					$rightname = $imapObj->name;
					$rightEmail = null;
					$rightmapUID = $imapObj->uid;
					$rightscores = $MAPobj->getiScores($r_mapids);
					$issuer = $userobj->getUserObject($imapObj->uid);
					$rightIssueData['issuedbyname'] = "$issuer->firstname $issuer->surname";
					$rightIssueData['issuedbyemail'] = $issuer->email;
					$rightIssueData['completedon'] = $imapObj->time_created;	
				}
				loopGenScore($leftscores, $rightscores, $k, $l_mapids, $r_mapids, $rightname, $leftname, $rightmapUID, $leftmapUID, $_GET['leftMAP'], $mapid, $leftEmail, $rightEmail, $leftIssueData, $rightIssueData);
				$k++;
			}//end foreach RIGHTSIDE
		} else {
			$k = 0;
			$rightIssueData = array();
			if($rightside == 'TeamMAP' || $rightside == 'TalentMAP') {
				$tmapobj = $MAPobj->getTMAP($r_maparr[0]);
				$rightname = $tmapobj->tmapname;
				$rightEmail = null;
				$r_mapids = $MAPobj->getmapidsVTMAP($r_maparr[0]);
				$rightscores = $MAPobj->getScores($r_mapids);
				$issuer = $userobj->getUserObject($tmapobj->uid);
				$rightIssueData['issuedbyname'] = "$issuer->firstname $issuer->surname";
				$rightIssueData['issuedbyemail'] = $issuer->email;
				$rightIssueData['completedon'] = $tmapobj->created;
				if(!$MAPobj->checkTMAPAccess($r_maparr[0], $compid, $userobj->uid, $access)) {
					$rightswitchHTML = sprintf(_('This %s is read only'), $rightside);
				} else {
					$rightswitchHTML = '<strong style="font-size: 11px;">' . _("Edit this") . ' ' . $rightside . '</strong><br /><div class="fl marginpad"><img src="' . ACCOUNT_PATH . '_images/_icons/IC_EditTMAP.png" /></div><div class="fl marginpad"><a href="javascript:void(0)" id="tmap_' . $r_maparr[0] . '" class="edittmap">' . $rightname . '</a></div><div class="cleaner"></div>';
				}
			} else if($rightside == 'MAP') {
				$tmapobj = $MAPobj->getMAPvid($r_maparr[0]);
				$rightname = $tmapobj->firstname . ' ' . $tmapobj->surname;
				$rightEmail = $tmapobj->email;
				$rightmapUID = $tmapobj->uid;
				$r_mapids = $r_maparr[0];
				$rightscores = $MAPobj->getScores($r_mapids);
				
				//PDF Array
				$issuer = $userobj->getUserObject($tmapobj->uid);
				$issuer = $userobj->getUserObject($issuer->adminid);
				$rightIssueData['issuedbyname'] = "$issuer->firstname $issuer->surname";
				$rightIssueData['issuedbyemail'] = $issuer->email;
				$rightIssueData['issuedon'] = $tmapobj->timeissue;
				$rightIssueData['completedon'] = $tmapobj->timefinish;
				$rightswitchHTML = GenSelectHTML($rightmapUID , 'right', $MAPobj, $r_mapids, $rightname);		
			} else if($rightside == 'idealMAP') {
				$tmapobj = $MAPobj->getiMAPvid($r_maparr[0]);
				$rightname = $tmapobj->name;
				$rightEmail = null;
				$rightmapUID = $tmapobj->uid;
				$r_mapids = $r_maparr[0];
				$rightscores = $MAPobj->getiScores($r_mapids);
				$rightswitchHTML = '';
				
				//PDF Array
				$issuer = $userobj->getUserObject($imapObj->uid);
				$rightIssueData['issuedbyname'] = "$issuer->firstname $issuer->surname";
				$rightIssueData['issuedbyemail'] = $issuer->email;
				$rightIssueData['completedon'] = $imapObj->time_created;
			}
			loopGenScore($leftscores, $rightscores, $k, $l_mapids, $r_mapids, $rightname, $leftname, $rightmapUID, $leftmapUID, $_GET['leftMAP'], $r_maparr[0], $leftEmail, $rightEmail, $leftIssueData, $rightIssueData);
		}
	
		//if rightside has multiple values then do the RANK shizzle!
		if(count($r_maparr) > 1) {
				$l = sizeof($arrFactorScores);
				$rank = array();
				for($j=0;$j < $l;$j++){
					$totalscore = 0;
					foreach($cids as $cid => $value) {
						$fgcount = 0;
						$permatch = 0;
						foreach($fgs as $fg => $clusterid) {
							if($clusterid == $cid) {
								$fgcount++;
								$intNumF = $arrFactorScores[$j][$fg]['num'];
								$intModF = $arrFactorScores[$j][$fg]['diff'];
								$scorediff = ($intModF/$intNumF);
								$permatch += round((10 - $scorediff),2);
							}
						}
						$clustertotal = round(($permatch / $fgcount),1); 
						$rank[$j][$cid] = $clustertotal;
						switch($cid) {
							case 1:
							$rank[$j]['rel_total'] = $clustertotal;
							break;
							case 2:
							$rank[$j]['tap_total'] = $clustertotal;
							break;
							case 3:
							$rank[$j]['mak_total'] = $clustertotal;
							break;
							case 4:
							$rank[$j]['gtd_total'] = $clustertotal;
							break;
						}
						$totalscore += $clustertotal;
					}
					$rank[$j]['total'] = round(($totalscore/count($cids)),1);
					$rank[$j]['leftname'] = $arrFactorScores[$j]['leftname'];
					$rank[$j]['rightname'] = $arrFactorScores[$j]['rightname'];
				}
				if(!isset($_GET['rankSort'])) {
					uasort($rank, cmprank);
				} else {
					switch($_GET['rankSort']) {
						case 'REL':
						uasort($rank, cmprankREL);
						break;
						case 'TAP':
						uasort($rank, cmprankTAP);
						break;
						case 'MAK':
						uasort($rank, cmprankMAK);
						break;
						case 'GTD':
						uasort($rank, cmprankGTD);
						break;
						default:
						uasort($rank, cmprank);
					}
				}
				$_SESSION['rank'] = $rank;

			echo genRankHTML($rank);
    	echo '---' . $leftswitchHTML;
    	echo '---' . $rightswitchHTML;
    	
		} else {
			$_SESSION['rank'] = array();
			//get factor descriptions
			if(!isset($_SESSION['fdescs'])) {
				$fdescs = $MAPobj->getFactDescs();
				$_SESSION['fdescs'] = $fdescs;
			} else {
				$fdescs = $_SESSION['fdescs'];
			}
			
			//this must be set to 0 when no rankings are being 
			$rankid = 0;
			$_SESSION['rankID'] = $rankid;
			require_once('ajaXDisplayResults.php');
			
	    echo '---' . $arrFactorScores[$k]['leftname'] . '---' . $arrFactorScores[$k]['rightname'];
    	echo '---' . $leftswitchHTML;
    	echo '---' . $rightswitchHTML;
    	
    	$_SESSION['leftType'] = $leftside;
			$_SESSION['rightType'] = $rightside;
	    $leftUser = new Admin($arrFactorScores[$rankid]['leftmapUID']);
	    $rightUser = new Admin($arrFactorScores[$rankid]['rightmapUID']);
	    
	    //this is so idealMAPs do not display the creators avatar as leftmapUID etc is set for idealMAPs when it's slightly different to MAPs still
	    if($_SESSION['leftType'] == 'MAP') {
	    	$luAva = $leftUser->avatarURL;
	    } else {
	    	$luAva = '';
	    }
	     if($_SESSION['rightType'] == 'MAP') {
	    	$ruAva = $rightUser->avatarURL;
	    } else {
	    	$ruAva = '';
	    }
		  echo '---' . $luAva;
		  echo '---' . $ruAva;
	  }
	  
	  //these sessions are set so that the ajaXMAP rank file has access to them without DB access!
	  $_SESSION['arrFactorScores'] = $arrFactorScores;
	  $_SESSION['arrDeviations'] = $arrDeviations;
	  $end_time = microtime(true);

		$time_taken = $end_time-$start_time;
		//echo $time_taken; 
    ?>