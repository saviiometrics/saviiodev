	$(function() {

		/*$(document).mousemove(function(e){
	      $('#status').html(e.pageX +', '+ e.pageY);
	   });*/

		$('.goIconBox').click(function(e) {
			var boxID = this.id.substr(0,2);
			if($('#' + boxID + '_Box').is(':hidden')) {
				$('#' + boxID + '_Box').css({top:e.pageY+10, left:e.pageX+10}).fadeIn('fast');
			} else {
				$('#' + boxID + '_Box').fadeOut('fast');
			}
			
			if(boxID == 'UU') {
					//ADD UPLOADER FOR AVATAR UPLOAD
		   	 	var AvatarUpload = new AjaxUpload('avaUpload', {
		        action: '/admin/AjaXUser.php',
		        name:'upload',
		        autoSubmit: false,
		        onSubmit : function(file, ext){
			        if (! (ext && /^(jpg|jpeg)$/.test(ext))){
			          // extension is not allowed
			          alert('Error: invalid file extension (Only .jpg, .jpeg is accepted)');
			          // cancel upload
			          return false;
			        } else {
			        	$('#UU_Box').block({ message: 'Processing' }); 
			        	this.setData({userid: 1, uploadAvatar: true});
			        }
		        },
					  onComplete: function(file, response) {
					  	var resultData = jQuery.trim(response).split("---");
					  	$('#UU_Box').unblock();
					    $.jGrowl(resultData[1], {
								header:'<strong>System Message</strong>',
								closeTemplate:'<div class="jGrowlCloseMe">close (x)</div>',
								position:'bottom-right'
							});
							$("#userAvatar").html('<img src="' + resultData[0] + '" />');
					  	//$("#CSVUpload").overlay().close();
					  },
					  onChange: function(file, extension) {
					  	$('#picShowUL').html('Image File <span style="color:green;">(Selected - ' + file + ')</span>');
					  }
				});
			  $(".uploadAvaToCDN").click(function(){
					AvatarUpload.submit();
			  });
			}//END IF UU
		});
		
		$('.changeType').click(function() {
			var currId = this.id;
			var avaVar = '';
			if(currId == "goGR") {
				avaVar = '';
			} else if(currId == "goFB") {
				avaVar = $('#FBuser').val();
			} else if(currId == "goTW") {
				avaVar = $('#TWuser').val();
			}
			$.get("/_inc/ajaXUpdate.php", { gravatarType: currId, uidstring:avaVar, updateAvatar:true },
		  function(j){
		  	$('#userAvatar img').attr('src', j.avaURL);
		  	$('#' + currId).closest('.avatarPBox').fadeOut('fast');
				if(currId == "goGR") {
					$("#avaDisplayType").html('gravatar');
				} else if(currId == "goFB") {
					$("#avaDisplayType").html('facebook');
				} else if(currId == "goTW") {
					$("#avaDisplayType").html('twitter');
				}
				$.jGrowl(j.message, {
					header:'<strong>System Message</strong>',
					closeTemplate:'<div class="jGrowlCloseMe">close (x)</div>',
					position:'bottom-right'
				});
		  }, "json");
		});
		
		$('.closeAvatar').click(function() {
			$(this).closest('.avatarPBox').fadeOut('fast');
		});
		
		$('.showContent').click(function() {
			var currBox = this.id;
			if($('#settings' + currBox).is(':hidden')) {
				//remove all current class
				$('.showContent').each(function() {
					$(this).removeClass('current');
				});
				//add current class to current menu button
				$(this).addClass('current');
				$('.settingsContent').each(function(index, value) {
					$(this).hide();
				});
				$('#settings' + currBox).fadeIn('fast');
			}
		});
		
		$('#updateUserDetails').click(function() {
			var valid = $("#settingUserForm").validationEngine('validate');
	  	if(valid == true) {
	  		$.get("/_inc/ajaXUpdate.php", {firstname:$("#SAfirst_name").val(), surname:$("#SAlast_name").val(), newemail:$("#SAemail").val(), newpassword:$("#SApassword").val(), updateDetails: true },
				  function(j){
						$.jGrowl(j.message, {
							header:'<strong>System Message</strong>',
							closeTemplate:'<div class="jGrowlCloseMe">close (x)</div>',
							position:'bottom-right'
						});
				  }, "json");
	  	}
		});

		$('#regenAPIToken').click(function() {
			$.get("/_inc/ajaXUpdate.php", { regenToken:true },
		  function(j){
				$.jGrowl(j.message, {
					header:'<strong>System Message</strong>',
					closeTemplate:'<div class="jGrowlCloseMe">close (x)</div>',
					position:'bottom-right'
				});
				if(j.apitoken != '') {
					$('#APIToken').html(j.apitoken);
				}
		  }, "json");
		});

	});