
		<div id="innerContent">
			<div id="contentMenuBar">
	      <div id="contentSiFR">
	      	<h2 class="MWxBox">Saviio consultants bring a wealth of knowledge in best practices to assist organisations on leveraging their talent, culture and strategic plan in order to meet business goals and objectives.</h2>
	      </div>
	      <div class="cleaner"></div>
	     </div>
	     <div id="contentSideGFX">
	     	<img src="/_images/_sidebar/<?=$sideGFX;?>" width="194" height="150" />
	     </div>
	     <div class="cleaner"></div>
	     <div class="infoLeft">
				<p>Our experienced team can work with you to uncover and exploit the strengths in your current talent management processes as well as develop new methods for achieving superior results in recruiting, developing and retaining top talent.</p>
				<p>People costs account for 70% of the total cost of doing business - and with good reason. Because the right people are the most valuable resource in any organisation, it's a competitive necessity to have the best processes in place to attract, hire, train, and retain the best employees.</p>
				<p>Our Talent Management Consultants are experts at helping you align your HR organisational structure with streamlined, processes that enhance employee productivity and performance and as a result, support your strategic objectives.</p>
			</div>
  	</div>
  	  