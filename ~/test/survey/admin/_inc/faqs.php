
<?php
$allmodules = $adminobj->getALLModules();
?>
<div id="surveybar"><div class="iconfloat"><img src="<?=ADMIN_ACCOUNT_PATH;?>_images/_icons/IC_ViewUsage.png" /></div> <div class="floatleft"><?=_("FAQ's");?></div></div>
<div class="titlebar_shadow"></div>

<div id="content_pad">
<a name="top"></a>
<h1 class="MWxHeader"><?=_("FAQ's");?></h1>

<p><?php printf(_("Please click on the links below and you will be taken to the corresponding FAQ of your choice. This FAQ section gives a basic overview of topics on %s. If you require more indepth help please visit the learning resources section of the site."), ACC_NAME);?></p>

<?php
	$faqs = $adminobj->getFAQs();
	foreach($faqs as $group => $faqinfo) {
		echo '<p><strong>' . ucwords($group) . '</strong></p>'; 
		foreach($faqinfo as $faqid => $infoarr) {
			echo '- <a href="#faq_' . $faqid . '">' . $infoarr['title'] . '</a><br />';
		}
	}
	?>
	<br />
	<h1 class="MWxHeader"><?=_("Answers");?></h1><br />
	<?php
	foreach($faqs as $group => $faqinfo) {
		foreach($faqinfo as $faqid => $infoarr) {
			echo '<a name="faq_' . $faqid . '"></a>';
			echo '<strong>' . $infoarr['title'] . '</strong> - <a href="#top">Back to top</a><br />';
			echo $infoarr['content'];
		}
	}
	
?>
<p>&nbsp;</p>
</div>
<div class="cleaner"></div>