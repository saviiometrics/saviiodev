/**
 * Localization strings for the UI Multiselect widget
 *
 * @locale en, en-US
 */

$.extend($.ui.multiselect.locale, {
	addAll:'Lisää kaikki',
	removeAll:'Poista kaikki',
	itemsCount:'#{count} kohdetta valittu',
	itemsTotal:'#{count} kohdetta yhteensä',
	busy:'odota...',
	errorDataFormat:"Ei voi lisätä vaihtoehtoja, tuntematon tietomuoto",
	errorInsertNode:"Ongelma yritettäessä lisätä item:\n\n\t[#{key}] => #{value}\n\nToiminto keskeytettiin.",
	errorReadonly:"Vaihtoehto #{option} on vain luku-tyyppiä",
	errorRequest:"Valitan! Ongelma etäkutsussa. (Type: #{status})"
});
