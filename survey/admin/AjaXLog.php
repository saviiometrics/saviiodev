<?php

	if(!isset($_SESSION)) { session_start(); }
	$phpsessid = session_id();
	
	//include localization and site config files
	require_once("../site.config.php");
	
	//include DB AND ACCOUNT INFO CLASSES
	include CONTENT_PATH . '/_classes/db-class.php';
	include CONTENT_PATH . '/_classes/account-class.php';
	$accobj = new Account($_SESSION['accid']);

	//include other classes
	include FULL_PATH . '/_inc/_classes/user-class.php';
	include FULL_PATH . '/_inc/_classes/admin-class.php';
	
	require_once(FULL_PATH . "/_inc/localization.php");
	require_once(FULL_PATH . "/_inc/scripts.php");
	
	$adminobj = new Admin($_SESSION['uid']);
	
	//echo $_GET['gogo'];
	$sLimit = "";
	if (isset($_GET['iDisplayStart'])) {
		$sLimit = "LIMIT ".$_GET['iDisplayStart'].", ".$_GET['iDisplayLength'];
	}

	if ( $_GET['sSearch'] != "" ) {
		$searchstr = $_GET['sSearch'];
	} else {
		$searchstr = '';
	}
	
	if(isset( $_GET['iSortCol_0'])) {
		$sOrder = "ORDER BY ";
		for ( $i=0 ; $i < $_GET['iSortingCols']  ; $i++ ) {
			$sOrder .= fnColumnToField( $_GET['iSortCol_'.$i] ). " " . $_GET['sSortDir_'.$i] .", ";
		}
		$sOrder = substr_replace( $sOrder, "", -2 );
	}
	
	function fnColumnToField( $i )
	{
		if ( $i == 0 )
			return "ut.firstname";
		else if ( $i == 1 )
			return "lt.timestamp";
	}

	$accesslog = $adminobj->getAccessLog($searchstr, $sLimit, $sOrder, $_GET['timeStart'], $_GET['timeEnd']);
	
	if($searchstr == '' && $_GET['timeStart'] == '' && $_GET['timeEnd'] == '') {
		$iTotal = $adminobj->TotalAccessLog();
		$iFilteredTotal = $iTotal;
	} else {
		$iTotal = $adminobj->TotalAccessLog();
		$iFilteredTotal = count($accesslog);
	}

	$sOutput = '{';
	$sOutput .= '"sEcho": '.intval($_GET['sEcho']).', ';
	$sOutput .= '"iTotalRecords": '.$iTotal.', ';
	$sOutput .= '"iTotalDisplayRecords": '.$iFilteredTotal.', ';
	$sOutput .= '"aaData": [ ';
	
	/****************** START DATA LOOP ********************/

	if(!empty($accesslog)) {
		foreach($accesslog as $aL_ARR) {
			$sOutput .= "[";
			$sOutput .= json_encode( $aL_ARR['name'] . ' ' . $aL_ARR['action'] . ' - ' . $aL_ARR['result']).',';
			$sOutput .= json_encode($aL_ARR['ip']).',';
			$sOutput .= json_encode($aL_ARR['country']).',';
			$sOutput .= json_encode($aL_ARR['user_agent']).',';
            $sOutput .= json_encode(date("F j, Y, g:i a", $aL_ARR['timestamp']));
			$sOutput .= "],";
		}
	}
	$sOutput = substr_replace( $sOutput, "", -1 );
	$sOutput .= '] }';
	
	echo $sOutput;
	
?>