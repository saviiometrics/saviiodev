<?php
	$userlist = $adminobj->getUsers(5);
	$companies = $adminobj->getCompanies();
?>
	
<!-- Overlay Box for changing the graph options (all 3 graphs) -->
<div id="Graphbox" class="panelBox">
	<div class="addpad">
		<div class="cicon" style="padding:10px;"><img src="<?=ADMIN_ACCOUNT_PATH;?>_images/_icons/IC_AddCompany.png" /></div>
		<h2><?=_("Graph Options");?></h2>
		<p>
		</p>
		<div id="addTMAPheader"><?php echo _("Set Graph Options");?></div>
		<p><?=_("Please set the options for the graph.");?></p>
		<table border="0" cellpadding="3" cellspacing="2" width="664">
			<tr>
				<td width="280"><strong><?=_("Select Date Range");?></strong></td>
				<td align="right">
					<select id="TMLD_MonthS" name="TMLD_MonthS">
						<?php 
						$currmonth = date('m', time());
			      $months = array(
			        1 => 'january',
			        2 => 'february',
			        3 => 'march',
			        4 => 'april',
			        5 => 'may',
			        6 => 'june',
			        7 => 'july',
			        8 => 'august',
			        9 => 'september',
			        10 => 'october',
			        11 => 'november',
			        12 => 'december');
			        
							for($i=1; $i<=12; $i++) {
								echo '<option value="' . $i . '">' . ucfirst($months[$i]) . '</option>' . "\n";
							}
						?>
					</select>
					<select id="TMLD_YearS" name="TMLD_YearS">
						<?php 
						$startdate = 2009;
						$enddate = date('Y', time());
						for($i=$startdate; $i<=$enddate; $i++) {
							echo '<option value="' . $i . '">' . $i . '</option>' . "\n";
						}
						?>
					</select> &nbsp;to
					<select id="TMLD_MonthE" name="TMLD_MonthE">
						<?php  
						for($i=1; $i<=12; $i++) {
							if($currmonth == $i) {
								$selected = ' selected="selected"';
							} else {
								$selected = '';
							}
							echo '<option value="' . $i . '"' . $selected . '>' . ucfirst($months[$i]) . '</option>' . "\n";
						}   
						?>
					</select>
					<select id="TMLD_YearE" name="TMLD_YearE">
						<?php 
						$startdate = 2009;
						$enddate = date('Y', time());
						for($i=$startdate; $i<=$enddate; $i++) {
							if($i == $enddate) {
								$selected = ' selected="selected"';
							} else {
								$selected = '';
							}
							echo '<option value="' . $i . '"' . $selected . '>' . $i . '</option>' . "\n";
						}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td><strong><?=_("Select MAP Type");?></strong></td>
				<td align="right">
					<select name="maptype" id="maptype">
						<option value="talent">Talent MAP's</option>
						<option value="team">Team MAP's</option>
						<option value="MAP">MAP's</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					<strong><?=_("Select Accounts and Administrators");?></strong><br />
					<p><strong>Note:</strong> <?=_("To select multiple companies accounts/administrators simply hold down your \"ctrl\" key and click the ones you wish to view. You can also select multiple values by clicking on an option, then shift clicking on an option below or above it, this will select both clicked options and all the options in-between.");?></p>
				</td>
				<td align="right">
					<?php if($_SESSION['level'] <=4) { ?>
					<div class="pieGraphSelector">
						<span style="color:#96c09c; font-weight:bold;"><?=_("Select Accounts");?></span>
						<br /><br />
						
						<select class="g_companies" multiple size="5">
							<?php
							foreach($companies as $companyid => $comparr) {
								echo '<option value="' . $companyid . '">' . $comparr['companyname'] . '</option>'. "\n";
							}
							?>
						</select><br />
					</div>
			
					<?php } ?>
					
					<!--HeadHunter Share here-->
					<div class="pieGraphSelector">
						<span style="color:#96c09c; font-weight:bold;"><?=_("Select Administrators");?></span>
						<br /><br />
						
						<select class="g_admins" multiple size="5">
							<?php
							foreach($userlist as $uid => $userARR) {
								echo '<option value="' . $uid . '">' . $userARR['firstname'] . ' ' . $userARR['surname'] . '</option>'. "\n";
							}
							?>
						</select><br />
					</div>

				</td>
			</tr>
		</table>
		<div id="addTMAPheader"><?php echo _("Set Pie Chart Options");?></div>
			<div style="float:left; width:334px; margin:8px 0px 10px 0px;">
				<strong><?=_("talentMAP / teamMAP Totals");?></strong><br /><br />
				<strong><?=_('Select Account:');?></strong><select name="surveycids" id="surveycids">
	      <?php
					foreach($companies as $companyid => $comparr) {
					if($companyid == $adminobj->companyid) {
						$selected = ' selected="selected"';
					} else {
						$selected = '';
					}
					echo '<option value="' . $companyid . '"' . $selected . '>' . $comparr['companyname'] . '</option>'. "\n";
					}
				?>
	      </select>
	     </div>
	     
		   <div style="float:left; width:334px; margin:8px 0px 4px 0px;">
		   	<strong><?=_("Survey usage results");?></strong><br /><br />
	  		<strong><?=_('Select Account:');?></strong> <select name="creationcids" id="creationcids">
	      <?php
					foreach($companies as $companyid => $comparr) {
						if($companyid == $adminobj->companyid) {
							$selected = ' selected="selected"';
						} else {
							$selected = '';
						}
						echo '<option value="' . $companyid . '"' . $selected . '>' . $comparr['companyname'] . '</option>'. "\n";
					}
				?>
	      </select>
	    </div>
		<br />
    <div class="cleaner"></div>
		<p>
			<?=_('After you have selected all the options you wish to view, simply click the "See Results" button and this dialog box will hide and the graph will be generated for you.');?>
		</p>
		<!-- yes/no buttons -->
		<p>
			<input type="hidden" class="formgo" id="daterange" value="go" />
			<button class="go_gopts"> See Results </button>
			<button class="close"> <?=_("Close");?> </button>
		</p>

	</div>
</div>

<?php
$allmodules = $adminobj->getALLModules();
$limitsArr = $adminobj->getLimits();
?>

<input type="hidden" name="userCompID" id="userCompID" value="<?=$adminobj->companyid;?>" />
<input type="hidden" name="langLimitVal" id="langLimitVal" value="<?=$limitsArr['languages'];?>" />
	

<div id="surveybar"><div class="iconfloat"><img src="<?=ADMIN_ACCOUNT_PATH;?>_images/_icons/IC_ViewUsage.png" /></div> <div class="floatleft"><?=_("Control Panel");?></div></div>
<div class="titlebar_shadow"></div>

<div id="content_pad">
	<h1 class="MWxHeader"><?=_("Control Panel");?></h1><br />
	<div class="infoBoxFull">
		<strong><?=_("Limits and Languages");?></strong><br />
		<p><?=_("Below you can see your account limits for every aspect of your account along with the ability to select which languages you would like to display on your account. There are 16 Languages available but you can only choose as many as your account limits allow. If you wish to increase your account limits for any section then please contact <a href=\"mailto:accounts@saviio.com\">accounts@saviio.com</a> to discuss your requirements.");?></p>
	</div>
	<div style="float:left; width:620px; margin:0px 0px 20px 0px;">
		<?php
		foreach($limitsArr as $limitkey => $limitval) {
			switch($limitkey) {
				case 'accounts':
				$used = TOTAL_ACCOUNTS;
				$displayName = 'Accounts';
				break;
				case 'maps':
				$used = TOTAL_MAPS;
				$displayName = 'MAPs';
				break;
				case 'subaccounts':
				$used = TOTAL_SUBACCOUNTS;
				$displayName = 'Sub Accounts';
				break;
				case 'talentMAPs':
				$used = TOTAL_TALENTMAPS;
				$displayName = 'talentMAPs';
				break;
				case 'teamMAPs':
				$used = TOTAL_TEAMMAPS;
				$displayName = 'teamMAPs';
				break;
				case 'languages':
				$used = TOTAL_LANGUAGES;
				$displayName = 'Languages';
				break;
				case 'users':
				$used = TOTAL_USERS;
				$displayName = 'Administrators';
				break;
				case 'idealMAPs':
				$used = TOTAL_IDEAL;
				$displayName = 'idealMAPs';
				break;
				case 'emails':
				$used = TOTAL_EMAILS;
				$displayName = 'E-Mail Templates';
				break;
			}
			if($limitval != 'NOLIMIT') {
				$rawratio = ($used / $limitval);
				$widthval = floor(476 * $rawratio);
				$limitView = '<span class="limitUsed">' . $used . '</span> / ' . $limitval;
			} else {
				$widthval = 1;
				$limitView = '<span class="limitUsed">' . $used . '</span>';
			}
			
			//this works out the position (height wise) of the sprite based upon the ratio of used items compared to limit ratio.
			if($rawratio <= 0.6){
				$top = -10;
			}elseif($rawratio < 0.8){
				$top = -20;
			}elseif($rawratio <= 1){
				$top = -30;
			}
			
			echo '<div style="float:left; background:url(' . ADMIN_ACCOUNT_PATH . '_images/limitsprite.png) no-repeat 0px 0px; width:476px; height:10px;"><div id="limits_' . $limitkey . '" style="float:left; background:url(' . ADMIN_ACCOUNT_PATH . '_images/limitsprite.png) no-repeat 0px ' . $top . 'px; height:10px; width:' . $widthval . 'px;"></div></div><div style="float:left;" class="limitInfoBox"> - ' . $displayName . ' ' . $limitView . '</div><div class="cleaner"></div><br />';
		}
		?>
	</div>

	<div id="adminSelectLangHolder">
		<h1 class="MWxHeader"><?=_("Select Languages");?></h1><br />
		<table border="0" cellspacing="0" cellpadding="2">
		<?php
			$i = 1;
			foreach($allLangs as $langid => $langInfo) {
				if(array_key_exists($langid, $langs)) {
					$checked = ' checked="checked" ';
				} else {
					$checked = '';
				}
				if($i == 1) {
					echo '<tr>';
				}
				if ($i % 9 == 0) {
					echo '<tr>';
				}
				echo '<td><input name="lang_' . $langInfo['locale'] . '" id="lang_' . $langInfo['locale'] . '"'.$checked.'class="langBoxes" type="checkbox" value="' . $langid . '" /> <strong>' . _($langInfo['lang_name']) . '</strong></td>' . "\n";
				if($i % 8 == 0) {
					echo '</tr>';
				}
				$i++;
			}
		?>
		</table>
		<br />
		<a href="javascript:void(0);" class="saveLangs blueButton"><?=_("Update Language Selection");?></a>
	</div>
	<div class="cleaner"></div>
	<h1 class="MWxHeader"><?=_("Survey Usage & Statistics");?></h1><br />
	<div class="infoBoxFull">
	<div id="g_options"><?=_("graph options");?></div>
	<strong><?=_("MAP Survey Statistics");?></strong><br />
	<?=_("This graph will enable you to view a snapshot summary of MAPs issued, in progress or completed in pie chart format for a selected account.");?><br />
	<br />
	<strong><?=_("teamMAP and talentMAP Statistics");?></strong><br />
	<?=_("This graph will enable you to view a snapshot summary of teamMAPs and talentMAPs created for a selected account.");?><br />
	<br />
	<strong><?=_("MAP Statistics - Advanced");?></strong><br />
	<?=_("Using graph options will enable you to select several accounts and/or head-hunter activity in any date range for any MAP type.");?><br />
	<br />
	<br />
	</div>
	<div class="cleaner"></div>
	
	<div id="container" style="width: 640px; height: 400px; margin: 0px; float:left;"></div>
	<div id="piecontainer" style="width: 312px; height: 260px; margin: 0px 0px 0px 12px; float:left;"></div>
	<div id="piecontainer_survey" style="width: 312px; height: 260px; margin: 0px 0px 0px 12px; float:left;"></div>

	<div class="cleaner"></div>
</div>