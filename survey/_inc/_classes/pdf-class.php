<?php
require_once(FULL_PATH . 'tcpdf/config/lang/eng.php');
require_once(FULL_PATH . 'tcpdf/tcpdf.php');
class DisplayPDF {
	
	private static $grey = '8A8A8A';
	private static $white = 'FFFFFF'; 
	private static $greenMatch = '0E8737';
	private static $blueMatch = '3F8FF5';
	private static $orangeMatch = 'E28C1B';
	private static $redMatch = 'E51500';
	private static $excellent = '0e8737';
	private static $good = '3f8ff5';
	private static $average = 'e28c1b';
	private static $poor = 'e61500';
	private static $cluster1 = '82bcfb';
	private static $cluster2 = 'e7a95e';
	private static $cluster3 = 'a9d9af'; 
	private static $cluster4 = 'd796aa';
	private static $top10background = '6fa7d1';
	private static $relationshipsCluster = '6FA2D6';
	private static $thinkingandplanningCluster = 'E9AD65';
	private static $makingdecisionsCluster = 'A8D6AE';
	private static $gettingthingsdoneCluster = 'ECA7BB';
	private static $clusterTableDescription = 'CECECE';
	
	public $pdfname;

	public function __construct($pdfid=0) {
		$this->pdfname = 'waio';
		$_SESSION['leftTypeDisp'] = $_SESSION['leftType'];
		$_SESSION['rightTypeDisp'] = $_SESSION['rightType'];
		if($_SESSION['leftTypeDisp'] != 'MAP') {
			$_SESSION['leftTypeDisp'][0] = strtolower($_SESSION['leftTypeDisp'][0]);
		}
		if($_SESSION['rightTypeDisp'] != 'MAP' && $_SESSION['rightTypeDisp'] != 'single') {
			$_SESSION['rightTypeDisp'][0] = strtolower($_SESSION['rightTypeDisp'][0]);
		}
	}
	
	public function getFont($locale) {
		if($locale == 'ar_SA') {
			$font = 'almohanad';
		} else if($locale == 'zh_CN') {
			$font = 'stsongstdlight';
		} else if($locale == 'pl_PL' || $locale == 'cs_CZ' || $locale == 'sk_SK') {
			$font = 'dejavusans';
		} else {
			$font = 'helvetica';
		}
		return $font;
	}
	
	public function getPDFFlexBarColour($score) {
		if($score >= 90){
			return $this->getMatchGreen(true);
		}elseif($score >= 70){
			return $this->getMatchBlue(true);
		}elseif($score >= 50){
			return $this->getMatchOrange(true);
		}else{
			return $this->getMatchRed(true);
		}
	}
	
	public function showLegend($leftType, $rightType, $leftname, $rightname, $orientation, $leftemail, $rightemail) {
		if($rightType == 'MAP') {
			if($orientation == 'P') {
				$rightname .= "<br />($rightemail)";
			} else {
				$rightname .= " ($rightemail)";
			}
		}
		if($leftType=='MAP') {
			if($orientation == 'P') {
				$leftname .= "<br />($leftemail)";
			} else {
				$leftname .= " ($leftemail)";
			}
			if($rightType == 'MAP') {
				$imghtml_left = '<img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_pdf/single_grey_alt.png" width="20" height="27" />';
			} else {
				$imghtml_left = '<img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_pdf/single_grey.png" width="20" height="27" />';
			}
		} else if ($leftType=='idealMAP') {
			if($rightType == 'MAP') {
				$imghtml_left = '<img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_pdf/ideal_grey_alt.png" width="24" height="23" />';
			} else {
				$imghtml_left = '<img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_pdf/ideal_grey.png" width="24" height="23" />';
			}
		} else if($leftType == 'talentMAP') {
			if($rightType == 'talentMAP' || $rightType == 'teamMAP') {
				$imghtml_left = '<img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_pdf/cross_grey_alt.png" width="24" height="24" />';
			} else {
				$imghtml_left = '<img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_pdf/cross_grey.png" width="24" height="24" />';
			}
		} else if($leftType == 'teamMAP') {
			if($rightType == 'teamMAP') {
				$imghtml_left = '<img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_pdf/cross_grey_alt.png" width="24" height="24" />';
			} else {
				$imghtml_left = '<img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_pdf/cross_grey.png" width="24" height="24" />';
			}
		}
						
		if($rightType=='MAP') {
			if($leftType == 'MAP') {
				$imghtml_right = '<img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_pdf/single_grey.png" width="20" height="27" />';
			} else if($leftType == 'talentMAP' || $leftType == 'teamMAP') {
				$imghtml_right = '<img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_pdf/single_grey.png" width="20" height="27" />';
			} else {
				$imghtml_right = '<img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_pdf/single_grey.png" width="20" height="27" />';
			}
		} else if($rightType=='idealMAP') {
			if($leftType == 'idealMAP') {
				$imghtml_right = '<img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_pdf/ideal_grey.png" width="24" height="23" />';
			} else {
				$imghtml_right = '<img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_pdf/ideal_grey_alt.png" width="24" height="23" />';
			}
		} else if($rightType == 'talentMAP') {
			if($leftType == 'teamMAP') {
				$imghtml_right = '<img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_pdf/cross_grey_alt.png" width="24" height="24" />';
			} else {
				$imghtml_right = '<img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_pdf/cross_grey.png" width="24" height="24" />';
			}
		} else if($rightType == 'teamMAP') {
			if($leftType == 'talentMAP') {
				$imghtml_right = '<img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_pdf/cross_grey.png" width="24" height="24" />';
			} else {
				$imghtml_right = '<img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_pdf/cross_grey.png" width="24" height="24" />';
			}
		}
		
		if($orientation == 'P') {
			$twidth = 580;
			$tdwidthmid = 265;
			$iconCellwid = 67;
		} else {
			$twidth = 900;
			$tdwidthmid = 425;
			$iconCellwid = 67;
		}
		$nameCellwid = $tdwidthmid - $iconCellwid;
	
		$legendhtml = '<table width="' . $twidth . '" border="0" cellpadding="3" cellspacing="0">
		  <tr>
		  	<td align="center" width="' . $tdwidthmid . '">
			  	<table border="0" cellpadding="3" align="center" width=100%>
				    	<tr>
				    		<td width="' . $iconCellwid . '" align="right">' . $imghtml_left . ' </td>
				    		<td width="' . $nameCellwid . '" align="left">' . $leftname . '</td>
				    	</tr>
			   	 </table>
		   	 </td>
		   	<td width="50" align="center">Vs</td>
		   	<td align="center" width="' . $tdwidthmid . '">
		   		<table border="0" cellpadding="3" align="center" width=100%>
				    	<tr>
				    		<td width="' . $nameCellwid . '" align="right">' . $rightname . ' </td>
				    		<td width="' . $iconCellwid . '" align="left">' . $imghtml_right . '</td>
				    	</tr>
			   	 </table>
		   	</td>
		  </tr>
		</table>';
		return $legendhtml;
	}
	
	//generates PDF legend for single MAP views
	public function showLegendSingle($MAPtype, $leftname, $oriantation, $leftemail) {
		if($MAPtype == 'MAP') {
			$imghtml_left = '<img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_pdf/single_grey.png" width="20" height="27" />';
		} else if($MAPtype == 'idealMAP') {
			$imghtml_left = '<img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_pdf/ideal_grey.png" width="24" height="23" />';
		} else {
			$imghtml_left = '<img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_pdf/cross_grey.png" width="24" height="24" />';
		}
	
		$legendhtml = '<table border="0" cellpadding="3">
		  <tr>
		    <td width="26">' . $imghtml_left . ' </td>
		    <td width="400"><table border="0" cellpadding="8"><tr><td>' . $leftname . ' (' . $leftemail . ')</td>
		    </tr></table></td>
		  </tr>
		</table>';
		return $legendhtml;
	}
	
	private function getColor($color, $rgb) {
		if($rgb) {
			$out = array();
			$out[] = hexdec(substr($color, 0, 2));
			$out[] = hexdec(substr($color, 2, 2));
			$out[] = hexdec(substr($color, 4, 2));
		} else {
			$out = '#' . $color;
		}
		return $out;
	}
	
	public function getGrey($rgb=false) {
		return $this->getColor(self::$grey, $rgb);
	}
	
	public function getWhite($rgb=false) {
		return $this->getColor(self::$white, $rgb);
	}
	
	public function getMatchGreen($rgb=false) {
		return $this->getColor(self::$greenMatch, $rgb);
	}
	
	public function getMatchBlue($rgb=false) {
		return $this->getColor(self::$blueMatch, $rgb);
	}
	
	public function getMatchOrange($rgb=false) {
		return $this->getColor(self::$orangeMatch, $rgb);
	}
	
	public function getMatchRed($rgb=false) {
		return $this->getColor(self::$redMatch, $rgb);
	}

	public function getExcellentColour($rgb=false) {
		return $this->getColor(self::$excellent, $rgb);
	}
	
	public function getGoodColour($rgb=false) {
		return $this->getColor(self::$good, $rgb);
	}
	
	public function getAverageColour($rgb=false) {
		return $this->getColor(self::$average, $rgb);
	}
	
	public function getPoorColour($rgb=false) {
		return $this->getColor(self::$poor, $rgb);
	}

	public function getCluster1Colour($rgb=false) {
		return $this->getColor(self::$cluster1, $rgb);
	}

	public function getCluster2Colour($rgb=false) {
		return $this->getColor(self::$cluster2, $rgb);
	}
	
	public function getCluster3Colour($rgb=false) {
		return $this->getColor(self::$cluster3, $rgb);
	}
	
	public function getCluster4Colour($rgb=false) {
		return $this->getColor(self::$cluster4, $rgb);
	}
	
	public function getTop10BackgroundColour($rgb=false) {
		return $this->getColor(self::$top10background, $rgb);
	}
	
	public function getRelationshipsClusterColour($rgb=false) {
		return $this->getColor(self::$relationshipsCluster, $rgb);
	}
	
	public function getThinkingAndPlanningClusterColour($rgb=false) {
		return $this->getColor(self::$thinkingandplanningCluster, $rgb);
	}

	public function getMakingDecisionsClusterColour($rgb=false) {
		return $this->getColor(self::$makingdecisionsCluster, $rgb);
	}
	
	public function getGettingThingsDoneClusterColour($rgb=false) {
		return $this->getColor(self::$gettingthingsdoneCluster, $rgb);
	}
	
	public function getClusterTableDescriptionColour($rgb=false) {
		return $this->getColor(self::$clusterTableDescription, $rgb);
	}
	
	public function getRelationshipsTable() {
		$MAPobj = new MAP();
		$fids = $MAPobj->getfids();
		$fdescs = $MAPobj->getFactDescs();
		$relationships = array(array(array(), array()), array(array(), array()), array(array(), array()));
		$relationships[1][1]["title"] = $fids[1]['factor_text'];
		$relationships[1][1]["description"] = $fdescs[1]['desc'][0];
		$relationships[1][2]["title"] = $fids[2]['factor_text'];
		$relationships[1][2]["description"] = $fdescs[2]['desc'][0];
		$relationships[2][1]["title"] = $fids[20]['factor_text'];
		$relationships[2][1]["description"] = $fdescs[20]['desc'][0];
		$relationships[2][2]["title"] = $fids[19]['factor_text'];
		$relationships[2][2]["description"] = $fdescs[19]['desc'][0];
		$relationships[3][1]["title"] = $fids[21]['factor_text'];
		$relationships[3][1]["description"] = $fdescs[21]['desc'][0];
		$relationships[3][2]["title"] = $fids[22]['factor_text'];
		$relationships[3][2]["description"] = $fdescs[22]['desc'][0];
		
		return $this->getClusterTable('Relationships', $relationships, $this->getRelationshipsClusterColour(false));
	}
	
	public function getThinkingAndPlanningTable() {
		$MAPobj = new MAP();
		$fids = $MAPobj->getfids();
		$fdescs = $MAPobj->getFactDescs();
		$ThinkingAndPlanning = array(array(array(), array()), array(array(), array()), array(array(), array()));
		$ThinkingAndPlanning[1][1]["title"] = $fids[14]['factor_text'];
		$ThinkingAndPlanning[1][1]["description"] = $fdescs[14]['desc'][0];
		$ThinkingAndPlanning[1][2]["title"] = $fids[13]['factor_text'];
		$ThinkingAndPlanning[1][2]["description"] = $fdescs[13]['desc'][0];
		$ThinkingAndPlanning[2][1]["title"] = $fids[15]['factor_text'];
		$ThinkingAndPlanning[2][1]["description"] = $fdescs[15]['desc'][0];
		$ThinkingAndPlanning[2][2]["title"] = $fids[16]['factor_text'];
		$ThinkingAndPlanning[2][2]["description"] = $fdescs[16]['desc'][0];
		$ThinkingAndPlanning[3][1]["title"] = $fids[18]['factor_text'];
		$ThinkingAndPlanning[3][1]["description"] = $fdescs[18]['desc'][0];
		$ThinkingAndPlanning[3][2]["title"] = $fids[17]['factor_text'];
		$ThinkingAndPlanning[3][2]["description"] = $fdescs[17]['desc'][0];
		
		return $this->getClusterTable('Thinking & Planning', $ThinkingAndPlanning, $this->getThinkingAndPlanningClusterColour(false));
	}
	
	public function getmakingDecisionsTable() {
		$MAPobj = new MAP();
		$fids = $MAPobj->getfids();
		$fdescs = $MAPobj->getFactDescs();
		$makingDecisions = array(array(array(), array()), array(array(), array()), array(array(), array()));
		$makingDecisions[1][1]["title"] = $fids[12]['factor_text'];
		$makingDecisions[1][1]["description"] = $fdescs[12]['desc'][0];
		$makingDecisions[1][2]["title"] = $fids[11]['factor_text'];
		$makingDecisions[1][2]["description"] = $fdescs[11]['desc'][0];
		$makingDecisions[2][1]["title"] = $fids[6]['factor_text'];
		$makingDecisions[2][1]["description"] = $fdescs[6]['desc'][0];
		$makingDecisions[2][2]["title"] = $fids[5]['factor_text'];
		$makingDecisions[2][2]["description"] = $fdescs[5]['desc'][0];
		$makingDecisions[3][1]["title"] = $fids[23]['factor_text'];
		$makingDecisions[3][1]["description"] = $fdescs[23]['desc'][0];
		$makingDecisions[3][2]["title"] = $fids[24]['factor_text'];
		$makingDecisions[3][2]["description"] = $fdescs[24]['desc'][0];
		
		return $this->getClusterTable('Making Decisions', $makingDecisions, $this->getMakingDecisionsClusterColour(false));
	}

	public function getgettingThingsDoneTable() {
		$MAPobj = new MAP();
		$fids = $MAPobj->getfids();
		$fdescs = $MAPobj->getFactDescs();
		$gettingThingsDone = array(array(array(), array()), array(array(), array()), array(array(), array()));
		$gettingThingsDone[1][1]["title"] = $fids[8]['factor_text'];
		$gettingThingsDone[1][1]["description"] = $fdescs[8]['desc'][0];
		$gettingThingsDone[1][2]["title"] = $fids[7]['factor_text'];
		$gettingThingsDone[1][2]["description"] = $fdescs[7]['desc'][0];
		$gettingThingsDone[2][1]["title"] = $fids[3]['factor_text'];
		$gettingThingsDone[2][1]["description"] = $fdescs[3]['desc'][0];
		$gettingThingsDone[2][2]["title"] = $fids[4]['factor_text'];
		$gettingThingsDone[2][2]["description"] = $fdescs[4]['desc'][0];
		$gettingThingsDone[3][1]["title"] = $fids[10]['factor_text'];;
		$gettingThingsDone[3][1]["description"] = $fdescs[10]['desc'][0];
		$gettingThingsDone[3][2]["title"] = $fids[9]['factor_text'];
		$gettingThingsDone[3][2]["description"] = $fdescs[9]['desc'][0];
		
		return $this->getClusterTable('Getting Things Done', $gettingThingsDone, $this->getGettingThingsDoneClusterColour(false));
	}
	
	
	private function getClusterTable($clusterTitle, $factorGroups, $clusterColour) {
		return '<table border="0" cellspacing="15" cellpadding="2" width="100%">
			<tr>
				<td colspan="2" color="' . $this->getWhite() . '" bgcolor="' . $clusterColour . '" align="center" style="vertical-align:middle"><strong><font size="11">' . _($clusterTitle) . '</font></strong></td>
			</tr>
			<tr>
				<td color="' . $this->getWhite() . '" bgcolor="' . $clusterColour . '" width="50%" align="center"><strong><font size="10">' . _($factorGroups[1][1]['title']) . '</font></strong></td>
				<td color="' . $this->getWhite() . '" bgcolor="' . $clusterColour . '" width="50%" align="center"><strong><font size="10">' . _($factorGroups[1][2]['title']) . '</font></strong></td>
			</tr>
			<tr>
				<td bgcolor="' . $this->getClusterTableDescriptionColour(false) . '" align="center">' . _($factorGroups[1][1]['description']) . '</td>
				<td bgcolor="' . $this->getClusterTableDescriptionColour(false) . '" align="center">' . _($factorGroups[1][2]['description']) . '</td>
			</tr>
			<tr>
				<td color="' . $this->getWhite() . '" bgcolor="' . $clusterColour . '" width="50%" align="center"><strong><font size="10">' . _($factorGroups[2][1]['title']) . '</font></strong></td>
				<td color="' . $this->getWhite() . '" bgcolor="' . $clusterColour . '" width="50%" align="center"><strong><font size="10">' . _($factorGroups[2][2]['title']) . '</font></strong></td>
			</tr>
			<tr>
				<td bgcolor="' . $this->getClusterTableDescriptionColour(false) . '" align="center">' . _($factorGroups[2][1]['description']) . '</td>
				<td bgcolor="' . $this->getClusterTableDescriptionColour(false) . '" align="center">' . _($factorGroups[2][2]['description']) . '</td>
			</tr>
			<tr>
				<td color="' . $this->getWhite() . '" bgcolor="' . $clusterColour . '" width="50%" align="center"><strong><font size="10">' . _($factorGroups[3][1]['title']) . '</font></strong></td>
				<td color="' . $this->getWhite() . '" bgcolor="' . $clusterColour . '" width="50%" align="center"><strong><font size="10">' . _($factorGroups[3][2]['title']) . '</font></strong></td>
			</tr>
			<tr>
				<td bgcolor="' . $this->getClusterTableDescriptionColour(false) . '" align="center">' . _($factorGroups[3][1]['description']) . '</td>
				<td bgcolor="' . $this->getClusterTableDescriptionColour(false) . '" align="center">' . _($factorGroups[3][2]['description']) . '</td>
			</tr>
		</table>';
		
	}
	
	private function leadingZeros($number, $digits) {
		$length = strlen($number);
		while(strlen($number) < $digits) {
			$number = '0' . $number;
		}
		return $number;
	}
	
	public function getMapDataTable($orientation, $creatorName, $creatorEmail, $leftType, $leftName, $leftEmail, $rightType, $rightName, $rightEmail, $leftIssueData, $rightIssueData, $dateFormat) {
		if(isset($leftIssueData['completedon'])) {
			$leftday = date('d', $leftIssueData['completedon']);
			if(date('m', $leftIssueData['completedon']) + 6 > 12) {
				$leftmonth = $this->leadingZeros(date('m', $leftIssueData['completedon']) - 6, 2);
				$leftyear = $this->leadingZeros(date('Y', $leftIssueData['completedon']) + 1, 4);
			} else {
				$leftmonth = $this->leadingZeros(date('m', $leftIssueData['completedon']) + 6, 2);
				$leftyear = $this->leadingZeros(date('Y', $leftIssueData['completedon']), 4);
			}
			$leftExpires = $leftday .'/' . $leftmonth . '/' . $leftyear;
		}
		if(isset($rightIssueData['completedon'])) {
			$rightday = date('d', $rightIssueData['completedon']);
			if((date('m', $rightIssueData['completedon']) + 6) > 12) {
				$rightmonth = $this->leadingZeros(date('m', $rightIssueData['completedon']) - 6, 2);
				$rightyear = $this->leadingZeros(date('Y', $rightIssueData['completedon']) + 1, 4);
			} else {
				$rightmonth = $this->leadingZeros(date('m', $rightIssueData['completedon']) + 6, 2);
				$rightyear = $this->leadingZeros(date('Y', $rightIssueData['completedon']), 4);
			}
			$rightExpires = $rightday .'/' . $rightmonth . '/' . $rightyear;
		}
		$out = '<div align="center"><font size="12"><strong>' . _("MAP Data") . '</strong></font><br /><br />
			<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">';
		if($orientation == 'L') {
			if($leftType == 'MAP' && $rightType == 'MAP') {
				$out .= '<tr>
					<td align="right" width="25%"><strong>'. _("Survey taker") . ':&nbsp;</strong></td>
					<td align="left" width="25%">' . $leftName .'</td>
					<td align="right" width="25%"><strong>'. _("Survey taker") . ':&nbsp;</strong></td>
					<td align="left" width="25%">' . $rightName .'</td>
				</tr>
				<tr>
					<td align="right"></td>
					<td align="left">' . $leftEmail .'</td>
					<td align="right"></td>
					<td align="left">' . $rightEmail .'</td>
				</tr>
				
				<tr>
					<td align="right"><strong>'. _("Issued by") . ':&nbsp;</strong></td>
					<td align="left">' . $leftIssueData['issuedbyname'] .'</td>
					<td align="right"><strong>'. _("Issued by") . ':&nbsp;</strong></td>
					<td align="left">' . $rightIssueData['issuedbyname'] .'</td>
				</tr>
				<tr>
					<td align="right"></td>
					<td align="left">' . $leftIssueData['issuedbyemail'] .'</td>
					<td align="right"></td>
					<td align="left">' . $rightIssueData['issuedbyemail'] .'</td>
				</tr>
				<tr>
					<td align="right"><strong>'. _("Issued on") . ':&nbsp;</strong></td>
					<td align="left">' . date($dateFormat, $leftIssueData['issuedon']) .'</td>
					<td align="right"><strong>'. _("Issued on") . ':&nbsp;</strong></td>
					<td align="left">' . date($dateFormat, $rightIssueData['issuedon']) .'</td>
				</tr>
				<tr>
					<td align="right"><strong>'. _("Completed on") . ':&nbsp;</strong></td>
					<td align="left">' . date($dateFormat, $leftIssueData['completedon']) .'</td>
					<td align="right"><strong>'. _("Completed on") . ':&nbsp;</strong></td>
					<td align="left">' . date($dateFormat, $rightIssueData['completedon']) .'</td>
				</tr>
				<tr>
					<td align="right"><strong>'. _("Expires on") . ':&nbsp;</strong></td>
					<td align="left">' . $leftExpires .'</td>
					<td align="right"><strong>'. _("Expires on") . ':&nbsp;</strong></td>
					<td align="left">' . $rightExpires .'</td>
				</tr>';
			} elseif($leftType== 'MAP' && $rightType == 'single') {
				$out .= '<tr>
					<td align="right" width="25%"><strong>'. _("Survey taker") . ':&nbsp;</strong></td>
					<td align="left" width="25%">' . $leftName .'</td>
					<td align="right" width="25%"></td>
					<td align="left" width="25%"></td>
				</tr>
				<tr>
					<td align="right"></td>
					<td align="left">' . $leftEmail .'</td>
					<td colspan="2" align="center"></td>
				</tr>
				
				<tr>
					<td align="right"><strong>'. _("Issued by") . ':&nbsp;</strong></td>
					<td align="left">' . $leftIssueData['issuedbyname'] .'</td>
					<td align="right"></td>
					<td align="left"></td>
				</tr>
				<tr>
					<td align="right"></td>
					<td align="left">' . $leftIssueData['issuedbyemail'] .'</td>
					<td colspan="2" align="center"></td>
				</tr>
				<tr>
					<td align="right"><strong>'. _("Issued on") . ':&nbsp;</strong></td>
					<td align="left">' . date($dateFormat, $leftIssueData['issuedon']) .'</td>
					<td align="right"></td>
					<td align="left"></td>
				</tr>
				<tr>
					<td align="right"><strong>'. _("Completed on") . ':&nbsp;</strong></td>
					<td align="left">' . date($dateFormat, $leftIssueData['completedon']) .'</td>
					<td align="right"></td>
					<td align="left"></td>
				</tr>
				<tr>
					<td align="right"><strong>'. _("Expires on") . ':&nbsp;</strong></td>
					<td align="left">' . $leftExpires .'</td>
					<td align="right"></td>
					<td align="left"></td>
				</tr>';
			} elseif($leftType == 'MAP') {
				$out .= '<tr>
					<td align="right" width="25%"><strong>'. _("Survey taker") . ':&nbsp;</strong></td>
					<td align="left" width="25%">' . $leftName .'</td>
					<td align="right" width="25%"><strong>'. _("MAP name") . ':&nbsp;</strong></td>
					<td align="left" width="25%">' . $rightName .'</td>
				</tr>
				<tr>
					<td align="right"></td>
					<td align="left">' . $leftEmail .'</td>
					<td colspan="2" align="center"></td>
				</tr>
				
				<tr>
					<td align="right"><strong>'. _("Issued by") . ':&nbsp;</strong></td>
					<td align="left">' . $leftIssueData['issuedbyname'] .'</td>
					<td align="right"><strong>'. _("Created by") . ':&nbsp;</strong></td>
					<td align="left">' . $rightIssueData['issuedbyname'] .'</td>
				</tr>
				<tr>
					<td align="right"></td>
					<td align="left">' . $leftIssueData['issuedbyemail'] .'</td>
					<td align="right"></td>
					<td align="left">' . $rightIssueData['issuedbyemail'] .'</td>
				</tr>
				<tr>
					<td align="right"><strong>'. _("Issued on") . ':&nbsp;</strong></td>
					<td align="left">' . date($dateFormat, $leftIssueData['issuedon']) .'</td>
					<td align="right"><strong>'. _("Created on") . ':&nbsp;</strong></td>
					<td align="left">' . date($dateFormat, $rightIssueData['completedon']) .'</td>
				</tr>
				<tr>
					<td align="right"><strong>'. _("Completed on") . ':&nbsp;</strong></td>
					<td align="left">' . date($dateFormat, $leftIssueData['completedon']) .'</td>
					<td align="right"></td>
					<td align="left"></td>
				</tr>
				<tr>
					<td align="right"><strong>'. _("Expires on") . ':&nbsp;</strong></td>
					<td align="left">' . $leftExpires .'</td>
					<td align="right"></td>
					<td align="left"></td>
				</tr>';
			} elseif($rightType == 'MAP') {
				$out .= '<tr>
					<td align="right" width="25%"><strong>'. _("MAP name") . ':&nbsp;</strong></td>
					<td align="left" width="25%">' . $leftName .'</td>
					<td align="right" width="25%"><strong>'. _("Survey taker") . ':&nbsp;</strong></td>
					<td align="left" width="25%">' . $rightName .'</td>
				</tr>
				<tr>
					<td colspan="2" align="center"></td>
					<td align="left"></td>
					<td align="left">' . $rightEmail .'</td>
				</tr>
				
				<tr>
					<td align="right"><strong>'. _("Created by") . ':&nbsp;</strong></td>
					<td align="left">' . $leftIssueData['issuedbyname'] .'</td>
					<td align="right"><strong>'. _("Issued by") . ':&nbsp;</strong></td>
					<td align="left">' . $rightIssueData['issuedbyname'] .'</td>
				</tr>
				<tr>
					<td align="right"></td>
					<td align="left">' . $leftIssueData['issuedbyemail'] .'</td>
					<td align="right"></td>
					<td align="left">' . $rightIssueData['issuedbyemail'] .'</td>
				</tr>
				<tr>
					<td align="right"><strong>'. _("Created on") . ':&nbsp;</strong></td>
					<td align="left">' . date($dateFormat, $leftIssueData['completedon']) .'</td>
					<td align="right"><strong>'. _("Issued on") . ':&nbsp;</strong></td>
					<td align="left">' . date($dateFormat, $rightIssueData['issuedon']) .'</td>
				</tr>
				<tr>
					<td align="right"></td>
					<td align="left"></td>
					<td align="right"><strong>'. _("Completed on") . ':&nbsp;</strong></td>
					<td align="left">' . date($dateFormat, $rightIssueData['completedon']) .'</td>
				</tr>
				<tr>
					<td align="right"></td>
					<td align="left"></td>
					<td align="right"><strong>'. _("Expires on") . ':&nbsp;</strong></td>
					<td align="left">' . $rightExpires .'</td>
				</tr>';
			} elseif($rightType == 'single') {
				$out .= '<tr>
					<td align="right" width="25%"><strong>'. _("MAP name") . ':&nbsp;</strong></td>
					<td align="left" width="25%">' . $leftName .'</td>
					<td align="right" width="25%"></td>
					<td align="left" width="25%"></td>
				</tr>	
				<tr>
					<td align="right"><strong>'. _("Created by") . ':&nbsp;</strong></td>
					<td align="left">' . $leftIssueData['issuedbyname'] .'</td>
					<td align="right"></td>
					<td align="left"></td>
				</tr>
				<tr>
					<td align="right"></td>
					<td align="left">' . $leftIssueData['issuedbyemail'] .'</td>
					<td colspan="2" align="center"></td>
				</tr>
				<tr>
					<td align="right"><strong>'. _("Created on") . ':&nbsp;</strong></td>
					<td align="left">' . date($dateFormat, $leftIssueData['completedon']) .'</td>
					<td align="right"></td>
					<td align="left"></td>
				</tr>';
			} else {
				$out .= '<tr>
					<td align="right" width="25%"><strong>'. _("MAP name") . ':&nbsp;</strong></td>
					<td align="left" width="25%">' . $leftName .'</td>
					<td align="right" width="25%"><strong>'. _("MAP name") . ':&nbsp;</strong></td>
					<td align="left" width="25%">' . $rightName .'</td>
				</tr>
				<tr>
					<td align="right"><strong>'. _("Created by") . ':&nbsp;</strong></td>
					<td align="left">' . $leftIssueData['issuedbyname'] .'</td>
					<td align="right"><strong>'. _("Created by") . ':&nbsp;</strong></td>
					<td align="left">' . $rightIssueData['issuedbyname'] .'</td>
				</tr>
				<tr>
					<td align="right"></td>
					<td align="left">' . $leftIssueData['issuedbyemail'] .'</td>
					<td align="right"></td>
					<td align="left">' . $rightIssueData['issuedbyemail'] .'</td>
				</tr>
				<tr>
					<td align="right"><strong>'. _("Created on") . ':&nbsp;</strong></td>
					<td align="left">' . date($dateFormat, $leftIssueData['completedon']) .'</td>
					<td align="right"><strong>'. _("Created on") . ':&nbsp;</strong></td>
					<td align="left">' . date($dateFormat, $rightIssueData['completedon']) .'</td>
				</tr>';
			}
		} else {
			if($leftType == 'MAP') {
			$out .= '<tr>
					<td align="right" width="50%"><strong>'. _("Survey taker") . ':&nbsp;</strong></td>
					<td align="left" width="50%">' . $leftName .'</td>
				</tr>
				<tr>
					<td align="right"></td>
					<td align="left">' . $leftEmail .'</td>
				</tr>
				
				<tr>
					<td align="right"><strong>'. _("Issued by") . ':&nbsp;</strong></td>
					<td align="left">' . $leftIssueData['issuedbyname'] .'</td>
				</tr>
				<tr>
					<td align="right"></td>
					<td align="left">' . $leftIssueData['issuedbyemail'] .'</td>
				</tr>
				<tr>
					<td align="right"><strong>'. _("Issued on") . ':&nbsp;</strong></td>
					<td align="left">' . date($dateFormat, $leftIssueData['issuedon']) .'</td>
				</tr>
				<tr>
					<td align="right"><strong>'. _("Completed on") . ':&nbsp;</strong></td>
					<td align="left">' . date($dateFormat, $leftIssueData['completedon']) .'</td>
				</tr>
				<tr>
					<td align="right"><strong>'. _("Expires on") . ':&nbsp;</strong></td>
					<td align="left">' . $leftExpires .'</td>
				</tr>';
			
			} else {
				$out .= '<tr>
					<td align="right" width="50%"><strong>'. _("MAP name") . ':&nbsp;</strong></td>
					<td align="left" width="50%">' . $leftName .'</td>
				</tr>
				<tr>
					<td align="right"><strong>'. _("Created by") . ':&nbsp;</strong></td>
					<td align="left">' . $leftIssueData['issuedbyname'] .'</td>
				</tr>
				<tr>
					<td align="right"></td>
					<td align="left">' . $leftIssueData['issuedbyemail'] .'</td>
				</tr>
				<tr>
					<td align="right"><strong>'. _("Created on") . ':&nbsp;</strong></td>
					<td align="left">' . date($dateFormat, $leftIssueData['completedon']) .'</td>
				</tr>';
			}
			if($rightType == 'MAP') {
				$out .= '<tr><td colspan="2">&nbsp;</td></tr>
				<tr>
					<td align="right" width="50%"><strong>'. _("Survey taker") . ':&nbsp;</strong></td>
					<td align="left" width="50%">' . $rightName .'</td>
				</tr>
				<tr>
					<td align="right"></td>
					<td align="left">' . $rightEmail .'</td>
				</tr>
				
				<tr>
					<td align="right"><strong>'. _("Issued by") . ':&nbsp;</strong></td>
					<td align="left">' . $rightIssueData['issuedbyname'] .'</td>
				</tr>
				<tr>
					<td align="right"></td>
					<td align="left">' . $rightIssueData['issuedbyemail'] .'</td>
				</tr>
				<tr>
					<td align="right"><strong>'. _("Issued on") . ':&nbsp;</strong></td>
					<td align="left">' . date($dateFormat, $rightIssueData['issuedon']) .'</td>
				</tr>
				<tr>
					<td align="right"><strong>'. _("Completed on") . ':&nbsp;</strong></td>
					<td align="left">' . date($dateFormat, $rightIssueData['completedon']) .'</td>
				</tr>
				<tr>
					<td align="right"><strong>'. _("Expires on") . ':&nbsp;</strong></td>
					<td align="left">' . $rightExpires .'</td>
				</tr>';
			} elseif($rightType != 'single') {
				$out .= '<tr><td colspan="2">&nbsp;</td></tr>
				<tr>
					<td align="right" width="50%"><strong>'. _("MAP name") . ':&nbsp;</strong></td>
					<td align="left" width="50%">' . $rightName .'</td>
				</tr>
				<tr>
					<td align="right"><strong>'. _("Created by") . ':&nbsp;</strong></td>
					<td align="left">' . $rightIssueData['issuedbyname'] .'</td>
				</tr>
				<tr>
					<td align="right"></td>
					<td align="left">' . $rightIssueData['issuedbyemail'] .'</td>
				</tr>
				<tr>
					<td align="right"><strong>'. _("Created on") . ':&nbsp;</strong></td>
					<td align="left">' . date($dateFormat, $rightIssueData['completedon']) .'</td>
				</tr>';
			}
		}
		$out .= '</table></div>';
		return $out;
	}
	
	public function getFirstPage($orientation, $creatorName, $creatorEmail, $leftType, $leftName, $leftEmail, $rightType, $rightName, $rightEmail, $leftIssueData, $rightIssueData) {
		$dateFormat = 'd/m/Y';
		$return = array();
		$out = '<table border="0" cellpadding="0" cellspacing="0">
		
		<tr><td align="center">' . $leftName . ' (' . $leftType . ')</td></tr>';
		if($leftType == 'MAP' && !is_null($leftEmail)) {
			$out .= '<tr><td align="center">' . $leftEmail . '</td></tr>';
		}
		if($rightType != 'single') {
			$out .= '<tr><td align="center">Vs</td></tr>
			<tr><td align="center">' . $rightName . ' (' . $rightType . ')</td></tr>';
			if($rightType == 'MAP' && !is_null($rightEmail)) {
				$out .= '<tr><td align="center">' . $rightEmail . '</td></tr>';
			}
		}
		$out .= '<tr><td align="center">&nbsp;</td></tr>
		<tr><td align="center">' . _("Created By") . ':</td></tr>
		<tr><td align="center">' . $creatorName . '</td></tr>
		<tr><td align="center">' . $creatorEmail . '</td></tr>
		<tr><td align="center">&nbsp;</td></tr>
		<tr><td align="center">' . _("Date Created") . ':</td></tr>
		<tr><td align="center">' . date($dateFormat, time()) . '</td></tr>
		</table>';
		$return['main'] = $out;
		
		$return['mapdata'] = $this->getMapDataTable($orientation, $creatorName, $creatorEmail, $leftType, $leftName, $leftEmail, $rightType, $rightName, $rightEmail, $leftIssueData, $rightIssueData, $dateFormat);
		return $return;
	}
	
}//end class

class SaviioTCPDF extends TCPDF {
	private $height;
	private $width;
	
	public function __construct($orientation, $unit, $format, $unicode, $encoding, $diskcache, $height, $width) {
		parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);
		$this->height = $height;
		$this->width = $width;
	}
	
	public function Header() {
		// Logo

		$hh = $this->height;
		$hw = $this->width;
		$himg = '_images/_pdf/saviio-horizontal-logo.jpg';

		//set the coordinates x1,y1,x2,y2 of the gradient (see linear_gradient_coords.jpg)
		$coords = array(0, 1, 0, 0);
		//$this->LinearGradient(0, 0, 210, 22, $grey, $white, $coords);
		$this->Image(FULL_PATH . ACCOUNT_PATH . $himg, 14, 6, $hw,$hh, '', 'http://' .$_SERVER['SERVER_NAME'], '', false, 300);
		// Set font
		$this->SetFont($_SESSION['fontn'], '', 18);

		// Title
		$this->Ln(3);
		//$this->Image(ACCOUNT_PATH . '_images/_pdf/header_bg.jpg', 80, 0,140,22.5, '', 'http://' .$_SERVER['SERVER_NAME'], '', false, 300);
		$this->SetTextColor(0, 0, 0);
		$headertext = sprintf(_('%s MAPs'), ACC_NAME);
		$this->Cell(0, 16,$headertext, 0, 0, 'R');
		$this->SetTextColor(0, 0, 0);
		// Line break
		$this->Ln(20);
	}
	
	// Page footer
	public function Footer() {
		// Position at 1.5 cm from bottom
		$this->SetY(-15);
		// Set font
		$this->SetFont($_SESSION['fontn'], '', 10);
		// Page number
		$width = ($this->w / 3) - 7;
		$this->Cell($width, 10, 'Copyright© Saviio 2011', 0, 0, 'L');
		$this->Cell($width, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, 0, 'C');
		$this->Cell($width - 10, 10, 'Date ' . date('d/m/Y', time()) , 0, 0, 'R');
	}

}

?>
