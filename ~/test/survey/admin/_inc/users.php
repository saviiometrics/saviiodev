<?php
$countries = $adminobj->getCountries();
$companies = $adminobj->getCompanies();
$companyAccess = $adminobj->getLevelAccess('company');
?>

<input type="hidden" name="editUserID" id="editUserID" />
<input type="hidden" name="xRTG" id="xRTG" value="<?php if($companyAccess['VIEW'] != 'SA') { echo 'Y'; } else { echo 'N'; }?>" />
<input type="hidden" name="headID" id="headID" value="<?=$adminobj->headid; ?>" />

<?php
$allmodules = $adminobj->getALLModules();
?>

<div id="userbar"><div class="iconfloat"><img src="<?=ADMIN_ACCOUNT_PATH;?>_images/_icons/IC_ManageUsers.png" /></div> <div class="floatleft"><?=_("Users");?></div></div>
<div class="titlebar_shadow"></div>

<div id="CSVUpload" class="userBox">
	<div class="addpad">
		<div class="cicon" style="padding:10px;"><img src="<?=ADMIN_ACCOUNT_PATH;?>_images/_icons/IC_AddCompany.png" /></div>
		<h2><?=_("Upload Multiple Users from XLS File");?></h2>
		<p><?=_('From here you can add multiple users to an existing account. A MAP invitation is automatically created for each of the users in the XLS file.');?></p>
		<p><?=_('When uploading via a XLS file you need to use this template file in order to enter your user details into.');?><br />
		<div class="floatleft"><img src="<?=ADMIN_ACCOUNT_PATH;?>_images/_icons/_files/xls.jpg" /></div><div class="flpad"><a href="<?=$adminobj->staticURL;?>_files/userupload.xls"><?=_("Download Upload Template");?></a></div><br /><br />
		<div class="cleaner"></div>
		<div id="userboxheader"><?php echo _("User Details");?></div>
		<table border="0" cellpadding="3" cellspacing="2" width="664">
			<tr>
				<td id="CSVfSel"><?=_("CSV File");?></td>
				<td align="right"><button id="usercsv"><?=_("Click to Browse for File");?></button></td>
			</tr>
			<tr>
				<td><?=_("User Country");?></td>
				<td align="right">
					<select name="UUcountryid" id="UUcountryid">
						 <?php
						 foreach($countries as $countryid => $countryname) {
						 		//$cid is set in header.php
						 		if($cid == $countryid) {
						 			$cidSelected = ' selected="selected"';
						 		} else {
						 			$cidSelected = '';
						 		}
						    echo '<option value="' . $countryid . '"' . $cidSelected . '>' . $countryname . '</option>' . "\n";
						 }
						 ?>
					</select>	
				</td>
			</tr>
			<tr>
				<td><?=_("Select Account");?></td>
				<td align="right">
					<?php
					echo '<select name="UUcompanyid" id="UUcompanyid">' . "\n";
					echo '<option value="0">--Please Select--</option>' . "\n";
					foreach($companies as $compid => $comparr) {
					    echo '<option value="' . $compid . '">' . $comparr['companyname'] . '</option>' . "\n";
					}
				  echo '</select>' . "\n";
					?>
				</td>
			</tr>
			<tr>
				<td><?=_("User Level");?></td>
				<td align="right">
					<select name="UUlevel" id="UUlevel">
						 <?php
						 //the levelAccess controls what levels are seen by which user. The LevelAccess of your current level has to be the same as or above it in order to be able to see it. If a levelAccess is below yours you dont see it!
						 foreach($adminlevels as $levelid => $levelInfo) {
								if(intval($levelInfo['levelAccess']) >= $levelAccess) {
									$selected = $levelid == '4' ? "SELECTED " : "";
									echo '<option value="' . $levelid . '" ' . $selected . '>' . $levelInfo['levelname'] . '</option>';
								}
						 }
						 ?>
					</select>	
				</td>
			</tr>
			<tr>
				<td><?=_("Select Email Language");?></td>
				<td align="right">
					<select name="MULang" id="MULang">
						<option value="nolang">--Please Select--</option>
						<?php
						foreach($langs as $langid => $langInfo) {
							echo '<option value="' . $langInfo['locale'] . '">' . _($langInfo['lang_name']) . '</option>' . "\n";
						}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td><div class="selWelcomeMULang"><?=_("Select Welcome Email");?></div></td>
				<td align="right">
					<div class="selWelcomeMULang">
						<select name="MUeid" id="MUeid">
						</select>
					</div>
				</td>
			</tr>
		</table>
		
		</form>
		<br />
		<strong>&nbsp;<?=_('What Happens when I add users via XLS Upload?');?></strong>
		<p style="padding:5px;"><?=_('Mass uploading users via XLS will add all of those users in the .xls file to the database and assign the country and level you select in this dialog box to each one. A welcome email will be sent to each of the users with instructions on how to activate their account and access the system.');?></p>
		<!-- yes/no buttons -->
		<p>
			<button class="UUsaveuser"> <?=_("Save");?> </button>
			<button class="close"> <?=_("Close");?> </button>
		</p>

	</div>
</div>

<div id="UserBox" class="userBox">
	<div class="addpad">
		<div class="cicon" style="padding:10px;"><img src="<?=ADMIN_ACCOUNT_PATH;?>_images/_icons/IC_AddCompany.png" /></div>
		<h2><?=_("Add new User");?></h2>
		<p class="usercopy"></p>
		<div id="userboxheader"><?php echo _("User Details");?></div><br />
		<form name="userform" id="userform">
			<table border="0" cellpadding="3" cellspacing="2" width="664">
				<tr class="editonly">
					<td valign="top"><?=_("Upload Photo");?></td>
					<td align="right">
						<strong><?=_('Current Photo');?></strong>
						<div id="userAvatarHolder"></div>
						<a href="javascript:void(0);" id="avaUpload"><?=_("Select an Image");?></a>&nbsp;<div id="showmeul"></div><br />
						<a href="javascript:void(0);" class="uploadAvaToCDN"><?=_("Upload Photo");?></a>
					</td>
				</tr>
				<tr>
					<td><?=_("Username");?>: <?=_("(This MUST be a valid Email Address)");?></td>
					<td align="right"><input type="text" name="savuser" id="savuser" class="validate[required,custom[email],length[5,25],ajax[ajaxUser]] textfield" size="30" /></td>
				</tr>
				<tr>
					<td><?=_("Firstname");?></td>
					<td align="right"><input type="text" name="savfirstname" id="savfirstname" class="validate[required,length[1,20]] textfield" size="30" /></td>
				</tr>
				<tr>
					<td><?=_("Surname");?></td>
					<td align="right"><input type="text" name="savsurname" id="savsurname" class="validate[required,length[1,20]] textfield" size="30" /></td>
				</tr>
				<tr class="editonly">
					<td><?=_("Email Address");?></td>
					<td align="right"><input type="text" name="savemail" id="savemail" class="validate[optional,custom[email]] textfield" size="30" /></td>
				</tr>
				<tr>
					<td><?=_("User Country");?></td>
					<td align="right">
						<select name="countryid" id="countryid">
							 <?php
							 foreach($countries as $countryid => $countryname) {
							 		//$cid is set in header.php
							 		if($cid == $countryid) {
							 			$cidSelected = ' selected="selected"';
							 		} else {
							 			$cidSelected = '';
							 		}
							    echo '<option value="' . $countryid . '"' . $cidSelected . '>' . $countryname . '</option>' . "\n";
							 }
								 ?>
						</select>	
					</td>
				</tr>
				<tr class="selectAccount">
					<td><?=_("Select Account");?>:<?php if($companyAccess['ADD'] == 'TRUE') { ?> <span class="addonly">(<a href="javascript:void(0);" class="showAddAcc"><?=_("Add a new Account?");?></a>)</span> <?php } ?></td>
					<td align="right">
						<?php
						echo '<select name="companyid" id="companyid">' . "\n";
						echo '<option value="0">--Please Select--</option>' . "\n";
						foreach($companies as $compid => $comparr) {
						    echo '<option value="' . $compid . '">' . $comparr['companyname'] . '</option>' . "\n";
						 }
					  echo '</select>' . "\n";
						?>
					</td>
				</tr>
				<tr>
					<td><?=_("User Level");?></td>
					<td align="right">
						<select name="level" id="level">
						 <?php
						 foreach($adminlevels as $levelid => $levelInfo) {
								if(intval($levelInfo['levelAccess']) >= $levelAccess) {
									$selected = $levelid == '4' ? "SELECTED " : "";
									echo '<option value="' . $levelid . '" ' . $selected . '>' . $levelInfo['levelname'] . '</option>';
								}
						 }
						 ?>
						</select>	
					</td>
				</tr>
				<tr class="addonly">
					<td><?=_("Select Email Language");?></td>
					<td align="right">
						<select name="ULang" id="ULang">
							<option value="nolang">--Please Select--</option>
							<?php
							foreach($langs as $langid => $langInfo) {
								echo '<option value="' . $langInfo['locale'] . '">' . _($langInfo['lang_name']) . '</option>' . "\n";
							}
							?>
						</select>
					</td>
				</tr>
				<tr class="addonly">
					<td><div class="selWelcomeULang"><?=_("Select Welcome Email");?></div></td>
					<td align="right">
						<div class="selWelcomeULang">
							<select name="eid" id="eid">
							</select>
						</div>
					</td>
				</tr>
				<tr class="editonly">
					<td><?=_("Reset User Password");?></td>
					<td align="right"><div class="makerelative"><a href="javascript:void(0);" class="resetpass"><?=_("Click to Reset User Password");?></a></div></td>
				</tr>
			</table>
		</form>
		<div id="newCompanyUserAdd">
			<div id="userboxheader"><?php echo _("Add a new Account");?></div>
			<p><?=_('From here you can add a new account just as you would do from the "Manage Accounts" page. Once your account has been added it will appear at the bottom of the list of accounts above in the "Select Account" section, enabling you to add a user to it as normal');?></p>
			<form id="companyform" name="companyform">
				<table border="0" cellpadding="3" cellspacing="2" width="664">
					<tr class="ctyperow">
						<td><?=_('Account Type');?></td>
						<td align="right">
							<select name="ctype" id="ctype">
								<?php 
								//everyone apart from sub account admins can add a main account.
								if($companyAccess['VIEW'] != 'SA') {
									echo '<option value="head" selected="selected">' . _("Main Account") . '</option>' . "\n";
								}
								echo '<option value="sub">' . _("Associated Account") . '</option>' . "\n";
								?>
							</select>
						</td>
					</tr>
						<tr class="compheadlist">
							<td><?=("Select Main Account");?></td>
							<td align="right">
								<select name="compheadid" id="compheadid">
									<option value="0">--Please Select--</option>
									<?php
									$headcomps = $adminobj->getCompanies("head");
									foreach($headcomps as $compid => $infoarr) {
										echo '<option value="' . $compid . '">' . $infoarr['companyname'] . '</option>' . "\n";
									}
									?>
								</select>	
							</td>
						</tr>
					<tr>
						<td><?=_('Account Name');?></td>
						<td align="right"><input type="text" name="companyN" id="companyN" class="validate[required,length[3,32]] textfield" size="30" /></td>
					</tr>
					<tr>
						<td><?=_('Account Description');?></td>
						<td align="right"><input type="text" name="companyD" id="companyD" class="validate[required,length[3,32]] textfield" size="30" /></td>
					</tr>
					<tr>
						<td><?=_('Account Country');?></td>
						<td align="right">
							<select name="CCountry" id="CCountry">
								<?php
								$countries = $adminobj->getCountries();
								foreach($countries as $cid => $country) {
									echo '<option value="' . $cid . '">' . $country . '</option>' . "\n";
								}
								?>
							</select>	
								<input type="hidden" name="editCompID" id="editCompID" />
								<input type="hidden" name="xRTG" id="xRTG" value="<?php if($companyAccess['VIEW'] != 'SA') { echo 'Y'; } else { echo 'N'; }?>" />
								<input type="hidden" name="headID" id="headID" value="<?=$adminobj->headid; ?>" />
						</td>
					</tr>
					<tr><td>
						<a href="javascript:void(0);" class="savecompany blueButton">Add new Account</a>
					</td></tr>
				</table>
			</form>
			</div>
		<br />
		<p style="padding:5px; display:none;"><strong>&nbsp;<?=_('What Happens when I add a user?');?></strong><?=_('When creating a new user a MAP is automatically assigned to them with 1 (one) "retake" allowed i.e. they can only take it once. Their password is automatically generated and sent to them in an email. You can choose whether to send the email right away when adding the user or go back and do it later!');?></p>
		<!-- yes/no buttons -->
		<p>
			<button class="saveuser"> Save </button>
			<button class="close"> Close </button>
		</p>

	</div>
</div>

<div id="reSendEmailBox" class="userBox">
	<div class="addpad">
		<div class="cicon" style="padding:10px;"><img src="<?=ADMIN_ACCOUNT_PATH;?>_images/_icons/IC_AddCompany.png" /></div>
		<h2><?=_("Resend Email Invitation");?></h2>
		<p><?=_('From here you can resend a user their welcome invitation email. Simply select the email language and then the email template you wish to send. After this click the "Resend Invitation" button and the email invitation will be resent.');?></p>
		<br />
		
		<table border="0" cellpadding="3" cellspacing="2" width="664">
			<tr>
				<td><?=_("Select Email Language");?></td>
				<td align="right">
					<select name="MULangRS" id="MULangRS">
						<option value="nolang">--Please Select--</option>
						<?php
						foreach($langs as $langid => $langInfo) {
							echo '<option value="' . $langInfo['locale'] . '">' . _($langInfo['lang_name']) . '</option>' . "\n";
						}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td><div class="selWelcomeMULangRS"><?=_("Select Welcome Email");?></div></td>
				<td align="right">
					<div class="selWelcomeMULangRS">
						<select name="MUeidRS" id="MUeidRS">
						</select>
					</div>
				</td>
			</tr>
		</table>

		<!-- yes/no buttons -->
		<p>
			<button class="UUResendEmail"> <?=_("Resend Invitation");?> </button>
			<button class="close"> <?=_("Close");?> </button>
		</p>

	</div>
</div>

<input type="hidden" name="addusercopy" id="addusercopy" value="<?=_('From here you can add a new user to an existing account. Once a user is added a MAP invitation is automatically created for them.');?>" />
<input type="hidden" name="editusercopy" id="editusercopy" value="<?=_("Please edit the user's details below");?>" />
<input type="hidden" name="edituserstr" id="edituserstr" value="<?=_("Edit User");?>" />
<input type="hidden" name="adduserstr" id="adduserstr" value="<?=_("Add New User");?>" />
<input type="hidden" name="saveuserstr" id="saveuserstr" value="<?=_("Save User");?>" />

<div id="content_pad">
	<h1 class="MWxHeader"><?=_("Manage Users");?></h1>
	<p><?=_("This page enables you to create & edit users as well as control their admin access.");?></p> 
	<ul>
	    <li><?=_("Manage Access - Choose or restrict administrator options");?></li>
	    <li><?=_("Edit User - Reset and issue a user password and change their access level");?></li>
	    <li><?=_("Activate User - Restores a deactivated user, so they can access the system again.");?></li>
	    <li><?=_("Deactivate user - Removes access rights for this user. They will not be able to login, however you can still see TalentMAPs and other content associated with the user.");?></li>
	</ul>
	<p><?=_("You can quickly check who has completed a survey (or not).  The shading indicates that the survey taken has not been taken (blue); is still valid (green); about to expire (amber) or needs to be retaken (red).  To remain valid, the 'shelf-life' of a MAP survey is approximately six months before it needs to be retaken.");?></p>
	<p><?=_("You may also deactivate and reactivate existing users.");?></p>
	<p><strong><?=_("Using the Search Filter");?></strong></p>
	<p><?=_("To sort via a particular column simply click the column header and the data will be dynamically sorted for you. You can also sort via multiple columns by repeating the process whilst holding down the 'shift' key.");?></p>
	<p><?=_("You can also search for a particular user or combinations of users and other criteria. For example by typing \"Richard\" into the search box all records matching \"Richard\" will be shown. You can also type, for example, \"Richard Saviio\" and all of the records with a firstname of \"Richard\" and a company name of \"Saviio\" will be shown. You can also add a '-' before words to return records without that word in.");?></p>
	<p><strong><?=_("Here is a key for the icons in the \"Access Controls\" column, simply click these icons to access the features.");?></strong></p>
	<p>
	<?php
	echo '<img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_Access.png" alt="' . _("Manage Access") . '" title="' . _("Manage Access") . '" /> - ' . _("Manage Access") . '<br />' . "\n";
	echo '<img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_ManageUsers.png" alt="' . _("Edit User") . '" title="' . _("Edit User") . '" /> - ' . _("Edit User") . '<br />' . "\n";
	echo '<img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_ActivateUser.png" alt="' . _("Activate User") . '" title="' . _("Activate User") . '" /> - ' . _("Activate User") . '<br />' . "\n";
	echo '<img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_DeactivateUser.png" alt="' . _("Deactivate User") . '" title="' . _("Deactivate User") . '" /> - ' . _("Deactivate User") . '<br />' . "\n";
	echo '<img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_Retake.png" alt="' . _("Allow user to retake Survey") . '" title="' . _("Allow user to retake Survey") . '" /> - ' . _("Allow user to retake Survey") . '<br />' . "\n";
	echo '<img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_ResendEmail.png" alt="' . _("Resend Email Invitation") . '" title="' . _("Resend Email Invitation") . '" /> - ' . _("Resend Email Invitation (Resets User Password)") . '<br />' . "\n";
	?>
	</p>
	<div id="tableHolder">
		<table cellspacing="1" cellpadding="0" class="tablesorter">
			<thead>
				<tr>
					<th><?=_("Firstname");?></th>
					<th><?=_("Surname");?></th>
					<th><?=_("Country");?></th>
					<th><?=_("Completed Last Survey");?></th>
					<th><?=_("Account");?></th>
					<th><?=_("Active");?></th>
					<th><?=_("Access Controls");?></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td colspan="7" class="dataTables_empty"><?=_("Loading data from server");?></td>
				</tr>
			</tbody>
			<tfoot>
				<tr>
					<th><?=_("Firstname");?></th>
					<th><?=_("Surname");?></th>
					<th><?=_("Country");?></th>
					<th><?=_("Completed Last Survey");?></th>
					<th><?=_("Account");?></th>
					<th><?=_("Active");?></th>
					<th><?=_("Access Controls");?></th>
				</tr>
			</tfoot>
		</table>
	</div>
	<div class="cleaner"></div>
</div>

