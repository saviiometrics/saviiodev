<?php

	if(!isset($_SESSION)) { session_start(); }
	$phpsessid = session_id();
	
	//include localization and site config files
	require_once("../site.config.php");
	//include DB AND ACCOUNT INFO CLASSES
	include CONTENT_PATH . '/_classes/db-class.php';
	include CONTENT_PATH . '/_classes/account-class.php';
	
	$accobj = new Account($_SESSION['accid']);

	//include other classes
	include FULL_PATH . '/_inc/_classes/user-class.php';
	include FULL_PATH . '/_inc/_classes/admin-class.php';
	include FULL_PATH . '/_inc/_classes/MAP-class.php';

	$adminobj = new Admin($_SESSION['uid']);
	$limitsArr = $adminobj->getLimits();
	$superAccess = $adminobj->getLevelAccess('admin');
		
	require_once(FULL_PATH . "/_inc/localization.php");
	require_once(FULL_PATH . "/_inc/scripts.php");

	if(isset($_GET['addTMAP'])) {
		$MAPobj = new MAP();
		$addme = false;
		if($_GET['type'] == 'team') {
			if($limitsArr['teamMAPs'] == 'NOLIMIT') {
				$limit_team = 'NOLIMIT';
			} else {
				$limit_team = ($limitsArr['teamMAPs'] - TOTAL_TEAMMAPS);
			}
			$tmapdisp = 'teamMAP';
			if($limit_team >= 1 || $limitsArr['teamMAPs'] == 'NOLIMIT') {
				$addme = true;
			}
		} else if($_GET['type'] == 'talent') {
			if($limitsArr['talentMAPs'] == 'NOLIMIT') {
				$limit_talent = 'NOLIMIT';
			} else {
				$limit_talent = ($limitsArr['talentMAPs'] - TOTAL_TALENTMAPS);
			}
			$tmapdisp = 'talentMAP';
			if($limit_talent >= 1 || $limitsArr['talentMAPs'] == 'NOLIMIT') {
				$addme = true;
			}
		}
		if($addme == true || $superAccess['GODADMIN'] == 'TRUE') {
			if(!$MAPobj->addTMAP($_GET['type'], $_GET['companyid'], $_SESSION['uid'], $_GET['tmapname'], $_GET['tmapdesc'], $_GET['mapids'], $_GET['accessids'], $_GET['accesslevel'], $_GET['adminAIDs'])) {
				$result_str = sprintf (_('An error has occurred adding your %s'), $tmapdisp);
				$tmapArr = array("message" => $result_str, "error" => true);
				echo json_encode($tmapArr);
			} else {
				$result_str = sprintf (_('Your %s was added successfully!'), $tmapdisp);
				//get the userlist here from the mapids!
				$usernames = $MAPobj->getNamesVMAPids($_GET['mapids']);
				$userstring = '<strong>' . sprintf(_("User List for this %s"),$tmapheader) . '</strong><br /><ul class="mapuserlist">';
				foreach($usernames as $uid => $username) {
					$userstring .= '<li>'.$username['fullname'] . '</li>';
				}
				$userstring .= '</ul>';
	
				$adminstring = '';
				$adminstring .= '<a href="javascript:void(0);" class="deleteMAP" id="tmapdel_' . $_SESSION['tmapadd'] . '"><img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_EraserMinus.png" alt="Delete ' . $tmapdisp . '" title="Delete ' . $tmapheader . '" /></a>&nbsp;&nbsp;';
				$adminstring .= '<a href="javascript:void(0);" class="editMAP" id="tmap_' . $_SESSION['tmapadd'] . '"><img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_EditApplication.png" alt="Edit ' . $tmapdisp . '" title="Edit ' . $tmapheader . '" /></a>&nbsp;&nbsp;';
				$adminstring .= '<a href="javascript:void(0);" class="archiveMAP" id="tmaparc_' . $_SESSION['tmapadd'] . '"><img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_Archive.png" alt="Archive ' . $tmapdisp . '" title="Archive ' . $tmapheader . '" /></a>';
	
				$tmapArr = array("message" => $result_str, "userlist" => $userstring, "error" => false, "createdBy" => $adminobj->firstname . ' ' . $adminobj->surname, "createdOn" =>  date('d/m/Y', time()), "tmapid" => $_SESSION['tmapadd'], "adminString" => $adminstring);
				$adminobj->runTracker('Added ' . $_GET['type'], $_GET['tmapname'], $_SESSION['tmapadd'], 'TMAP');
				echo json_encode($tmapArr);
			}
		} else {
			$result_str = sprintf (_('You have reached your limit for adding %ss'), $tmapdisp);
			$tmapArr = array("message" => $result_str, "error" => true);
			echo json_encode($tmapArr);
		}
	}

	if(isset($_GET['getTMAP'])) {
		$MAPobj = new MAP();
		$TMAPobj = $MAPobj->getTMAP($_GET['tmapid']);
		$infoArr = array("tmapname" => $TMAPobj->tmapname, "tmapdesc" => $TMAPobj->tmapdesc, "mapids" => $TMAPobj->mapids);
		$identid = $adminobj->companyid;
		$atype = 'company';
		$TMAPAString = '';
		$TMAPAccess = $MAPobj->getTMAPAccess($_GET['tmapid'], $atype, $adminobj->companyid);
		if(is_array($TMAPAccess)) {
			$i = 1;
			$acount = count($TMAPAccess);
			foreach($TMAPAccess as $identid => $accesslevel) {
				if($i < $acount) {
					$TMAPAString .= $identid . ',';
				} else {
					$TMAPAString .= $identid;
				}
				$i++;
			}
		}
		$infoArr['compids'] = $TMAPAString;
		
		//now for compadmins and for headhunters (no need for ident id as not selecting for a particular company)
		$TMAPAString = '';
		$atype = 'user';
		$TMAPAccess = $MAPobj->getTMAPAccess($_GET['tmapid'], $atype);
		if(is_array($TMAPAccess)) {
			$i = 1;
			$acount = count($TMAPAccess);
			foreach($TMAPAccess as $identid => $accesslevel) {
				if($i < $acount) {
					$TMAPAString .= $identid . ',';
				} else {
					$TMAPAString .= $identid;
				}
				$i++;
			}
		}
		$infoArr['adminids'] = $TMAPAString;
		echo json_encode($infoArr);
	}
	
	//edit TMAP and Access Levels!
	if(isset($_GET['editTMAP'])) {
		$MAPobj = new MAP();
		if(!$MAPobj->updateTMAP($_GET['tmapid'], $_GET['newmapids'], $_GET['tmapname'], $_GET['tmapdesc'])) {
			printf (_("Error updating %s"),'TMAP');
		} else {
			if(!$MAPobj->updateTMAPAccess($_GET['tmapid'], $_GET['companyids'], $_GET['adminAIDs'], $_GET['accesslevel'], $_GET['type'])) {
				printf (_("Error Updating Access Levels, however, %s has updated successfully"),'TMAP');
			} else {
				$adminobj->runTracker('Updated ' . $_GET['type'], $_GET['tmapname'], $_GET['tmapid'], 'TMAP');
				printf (_("%s and Access Levels updated successfully!"),'TMAP');
			}
		}
	}
	
	//delete TMAP!
	if(isset($_GET['deleteTMAP'])) {
		$MAPobj = new MAP();
		if(!$MAPobj->deleteTMAP($_GET['tmapid'], $_GET['tmapType'])) {
			printf (_("Error deleting %s"),'TMAP');
		} else {
			printf (_("%s successfully deleted!"),'TMAP');
		}
	}
	
	//archive TMAP!
	if($_GET['archiveTMAP'] == 'yes') {
		$MAPobj = new MAP();
		if(!$MAPobj->archiveTMAP($_GET['tmapid'])) {
			printf (_("Error archiving %s"),'TMAP');
		} else {
			printf (_("%s successfully archived!"),'TMAP');
		}
	}
	
	if($_GET['archiveTMAP'] == 'reverse') {
		$MAPobj = new MAP();
		if(!$MAPobj->unarchiveTMAP($_GET['tmapid'])) {
			printf (_("Error restoring %s"),'TMAP');
		} else {
			printf (_("%s successfully restored!"),'TMAP');
		}
	}
	
	if(isset($_GET['getnewTMAPids'])) {
		$MAPobj = new MAP();
		$newmapids = $MAPobj->getUpdatedTMAPids($_GET['oldmapids']);
		$newmapids = implode(',', $newmapids);
		echo $newmapids;
	}
	
	/****************************  idealMAPS *************************/
	
	if(isset($_POST['addiMAP'])) {
		$MAPobj = new MAP();
		$addme = false;
		$limit_ideal = ($limitsArr['idealMAPs'] - TOTAL_IDEAL);
		if($limit_ideal >= 1) {
			$addme = true;
		}
		if($addme == true || $superAccess['GODADMIN'] == 'TRUE') {
			if(!$MAPobj->addiMAP($_POST['resultsObj'], $adminobj->companyid, $adminobj->uid, $_POST['imapname'], $_POST['parts'])) {
				$result_str = sprintf (_('An error has occurred adding your %s'), 'idealMAP');
				$imapArr = array("message" => $result_str, "error" => true);
				echo json_encode($imapArr);
			} else {
        if( !isset( $db->mySQLConnection ) ) {
          $db = new DbaseMySQL();
          $db->mySQLConnect();
        }        
        mysql_query("UPDATE system_accounts set ToT_ideal = ToT_ideal+1 where accid = ".$adminobj->accid, $db->mySQLConnection);
        
				$adminstring = '';
				$adminstring .= '<a href="javascript:void(0);" class="editiMAP" id="imap_' . $_SESSION['curriMAP'] . '"><img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_EditApplication.png" alt="Edit iMAP" title="Edit iMAP" /></a>&nbsp;&nbsp;';
				$adminstring .= '<a href="javascript:void(0);" class="deleteiMAP" id="imapdel_' . $_SESSION['curriMAP'] . '"><img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_EraserMinus.png" alt="Delete iMAP" title="Delete iMAP" /></a>&nbsp;&nbsp;';
		
				$adminobj->runTracker('Added idealMAP', $_POST['imapname'], $_SESSION['curriMAP'], 'IMAP');
				$result_str = sprintf (_('Your %s was added successfully!'), 'iMAP');
				$imapArr = array("message" => $result_str, "error" => false, "createdBy" => $adminobj->firstname . ' ' . $adminobj->surname, "createdOn" =>  date('d/m/Y', time()), "participants" => $_POST['parts'], "adminString" => $adminstring, "imapid" => $_SESSION['curriMAP']);
				echo json_encode($imapArr);
			}
		} else {
			$result_str = sprintf (_('You have reached your limit for adding %s'), 'idealMAPs');
			$imapArr = array("message" => $result_str, "error" => true);
			echo json_encode($imapArr);
		}
	}
	
	if(isset($_GET['deleteiMAP'])) {
		$MAPobj = new MAP();
		if(!$MAPobj->deleteiMAP($_GET['imapid'])) {
			echo _('Your idealMAP was not deleted. Please try again!');
		} else {
			$adminobj->runTracker('Deleted idealMAP', $_GET['imapname'], $_GET['imapid'], 'IMAP');
			echo _('Your idealMAP was successfully deleted');
		}
	}
	
	if(isset($_GET['editiMAP'])) {
		$MAPobj = new MAP();
		if(!$MAPobj->editiMAP($_GET['imapid'], $_GET['imapname'], $_GET['participants'])) {
			echo _('Your idealMAP was not updated. Please try again!');
		} else {
			$adminobj->runTracker('Edited idealMAP', $_GET['imapname'], $_GET['imapid'], 'IMAP');
			echo _('Your idealMAP was successfully updated');
		}
	}
	
	if(isset($_GET['getiMAP'])) {
		$MAPobj = new MAP();
		$imaparr = $MAPobj->getiMAP($_GET['imapid']);
		echo json_encode($imaparr);
	}

?>