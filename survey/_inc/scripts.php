<?php

	function limit_text( $text, $limit ) {
	  if( strlen($text)>$limit )
	  {
	    $text = substr( $text,0,$limit );
	    $text = substr( $text,0,-(strlen(strrchr($text,' '))) );
	  }
	  return $text;
	}
	
	$actionurl = '?';
	$i = 0;
	/*
	$localekey = array_search('locale', $_GET);
	if ($localekey != NULL || $localekey !== FALSE) {
    unset($_GET[$localekey]); // remove key from array
	}
	*/
	foreach( $_GET as $name => $value ) {
		if($name == 'locale') {
			continue;
		}
		if( $name != "logout" )
			if( $i == 0 )
				$actionurl = $actionurl. $name .'='. $value;
			else
				$actionurl = $actionurl. '&' .$name.'='.$value;
		$i = $i + 1;
	}//end foreach
	
	function ordinal($cdnl){
	  $test_c = abs($cdnl) % 10;
	  $ext = ((abs($cdnl) %100 < 21 && abs($cdnl) %100 > 4) ? 'th'
	          : (($test_c < 4) ? ($test_c < 3) ? ($test_c < 2) ? ($test_c < 1)
	          ? 'th' : 'st' : 'nd' : 'rd' : 'th'));
	  return $cdnl.$ext;
	}  
	
	function cmpdev($a, $b) {
    if ($a['stddev'] == $b['stddev']) {
        return 0;
    }
    return ($a['stddev'] < $b['stddev']) ? -1 : 1;
	}
	
	function cmpdevleft($a, $b) {
    if ($a['stddev_l'] == $b['stddev_l']) {
        return 0;
    }
    return ($a['stddev_l'] < $b['stddev_l']) ? -1 : 1;
	}
	
	function cmpdiff($a, $b) {
    if ($a['diff'] == $b['diff']) {
        return 0;
    }
    return ($a['diff'] < $b['diff']) ? -1 : 1;
	}
	
	function cmprank($a, $b) {
    if ($a['total'] == $b['total']) {
        return 0;
    }
    return ($a['total'] < $b['total']) ? 1 : -1;
	}
	
	function cmprankREL($a, $b) {
    if ($a['rel_total'] == $b['rel_total']) {
        return 0;
    }
    return ($a['rel_total'] < $b['rel_total']) ? 1 : -1;
	}
	
	function cmprankTAP($a, $b) {
    if ($a['tap_total'] == $b['tap_total']) {
        return 0;
    }
    return ($a['tap_total'] < $b['tap_total']) ? 1 : -1;
	}
	
	function cmprankMAK($a, $b) {
    if ($a['mak_total'] == $b['mak_total']) {
        return 0;
    }
    return ($a['mak_total'] < $b['mak_total']) ? 1 : -1;
	}
	
	function cmprankGTD($a, $b) {
    if ($a['gtd_total'] == $b['gtd_total']) {
        return 0;
    }
    return ($a['gtd_total'] < $b['gtd_total']) ? 1 : -1;
	}
	
	function loopGenScore($leftscores, $rightscores, $rankid, $l_mapids, $r_mapids, $rightname, $leftname, $rightmapUID, $leftmapUID, $leftmapid, $rightmapid, $leftEmail, $rightEmail, $leftIssueData, $rightIssueData) {
		//set arrays to global!
		global $arrFactorScores;
		global $arrDeviations;
		foreach($leftscores as $key => $lscores) {
			$fg = $lscores['factorgroup'];
			//getscores
			$lscore = $lscores['score'];
			$rscore = $rightscores[$key]['score'];
			if($rscore == "" || $lscore == "") {
				continue;
			}
			//get timers
			$lefttimer = $lscores['timer'];
			$LSTDev = $lscores['deviation'];
			$righttimer = $rightscores[$key]['timer'];
			//get deviations
			$RSTDev = $rightscores[$key]['deviation'];
			$clusid = $rightscores[$key]['clusterid'];
			
			//a note about deviations set here. The left deviation is only ever needed when talent MAP is on the left hand side. And as it purely for working out the grade of the 
			//talentMAP in this case, it doesn't need to be included in the general working out of the values. This is because the stddev is ONLY used to rank which questions
			//are best for that talentMAP.. it is not used as such for any "comparisons" as obviously the values related to these statements in the diff(point scoring) which already
			//uses both scores for a comparison!!
			
			$intModulus = abs($lscore - $rscore);
			$arrFactorScores[$rankid][$fg]['lscore']+=$lscore;
			$arrFactorScores[$rankid][$fg]['rscore']+=$rscore;
			$arrFactorScores[$rankid][$fg]['diff']+=$intModulus;
			$arrFactorScores[$rankid][$fg]['num']++;
			$arrFactorScores[$rankid][$fg]['lscoreall'][$key]=$lscore;
			$arrFactorScores[$rankid][$fg]['rscoreall'][$key]=$rscore;
			
			//NO NEED TO RESET THESE
			$arrDeviations[$rankid][$clusid][$key]['stddev'] = $RSTDev;
			$arrDeviations[$rankid][$clusid][$key]['diff'] = $intModulus;
			$arrDeviations[$rankid][$clusid][$key]['stddev_l'] = $LSTDev;
			$arrDeviations[$rankid][$clusid][$key]['lscore'] = $lscore;
			$arrDeviations[$rankid][$clusid][$key]['rscore'] = $rscore;

			$arrDeviations[$rankid]['allinfo'][$key]['stddev'] = $RSTDev;
			$arrDeviations[$rankid]['allinfo'][$key]['stddev_l'] = $LSTDev;
			$arrDeviations[$rankid]['allinfo'][$key]['diff'] = $intModulus;
			$arrDeviations[$rankid]['allinfo'][$key]['lscore'] = $lscore;
			$arrDeviations[$rankid]['allinfo'][$key]['rscore'] = $rscore;
			$arrDeviations[$rankid]['allinfo'][$key]['clusid'] = $clusid;
			
			$totalscore += round((10 - $intModulus),0);
		}

		$arrFactorScores[$rankid]['rightname'] = $rightname;
		$arrFactorScores[$rankid]['leftname'] = $leftname;
		
		
		
		$arrFactorScores[$rankid]['l_mapids'] = $l_mapids;
		$arrFactorScores[$rankid]['r_mapids'] = $r_mapids;
		$arrFactorScores[$rankid]['leftmapUID'] = $leftmapUID;
		$arrFactorScores[$rankid]['rightmapUID'] = $rightmapUID;
		$arrFactorScores[$rankid]['leftmapid'] = $leftmapid;
		$arrFactorScores[$rankid]['rightmapid'] = $rightmapid;
		
		if(!is_null($leftEmail)) {
			$arrFactorScores[$rankid]['leftemail'] = $leftEmail;
		}
		if(!is_null($rightEmail)) {
			$arrFactorScores[$rankid]['rightemail'] = $rightEmail;
		}
		if(!is_null($rightIssueData)) {
			$arrFactorScores[$rankid]['rightissuedata'] = $rightIssueData;
		}
		if(!is_null($leftIssueData)) {
			$arrFactorScores[$rankid]['leftissuedata'] = $leftIssueData;
		}
		
		foreach($arrDeviations[$rankid] as $clusid => $keyarr) {
			$difftotal = 0;
			foreach($keyarr as $key) {
				$difftotal += $key['diff'];
			}
			$clustertotal = array_sum_key($keyarr, 'diff');
			$cluscores[$clusid] = round((10 - ($clustertotal / count($keyarr))),1);
		}
		$totalscore = ($totalscore / count($leftscores));
		$cluscores['total'] =	$totalscore ;
		$cluscores['rel_total'] = $cluscores[1];
		$cluscores['tap_total'] = $cluscores[2];
		$cluscores['mak_total'] = $cluscores[3];
		$cluscores['gtd_total'] = $cluscores[4];
		return $cluscores;
	}	
	
	function loopScoreSingle($leftscores, $l_mapids, $leftname, $leftmapUID, $leftmapid, $leftIssueData=null, $leftEmail=null) {
		$rankid = 0;
		//set arrays to global!
		global $arrFactorScores;
		global $arrDeviations;
		foreach($leftscores as $key => $lscores) {
			$fg = $lscores['factorgroup'];
			//getscores
			$lscore = $lscores['score'];
			$lefttimer = $lscores['timer'];
			$LSTDev = $lscores['deviation'];
			$clusid = $lscores['clusterid'];

			$arrFactorScores[$rankid][$fg]['lscore']+=$lscore;
			$arrFactorScores[$rankid][$fg]['num']++;
			$arrFactorScores[$rankid][$fg]['lscoreall'][$key]=$lscore;
			
			//NO NEED TO RESET THESE
			$arrDeviations[$rankid][$clusid][$key]['lscore'] = $lscore;
			$arrDeviations[$rankid][$clusid][$key]['stddev_l'] = $LSTDev;
			$arrDeviations[$rankid]['allinfo'][$key]['stddev_l'] = $LSTDev;
			$arrDeviations[$rankid]['allinfo'][$key]['lscore'] = $lscore;
			$arrDeviations[$rankid]['allinfo'][$key]['clusid'] = $clusid;
		}
		$arrFactorScores[$rankid]['leftname'] = $leftname;
		$arrFactorScores[$rankid]['l_mapids'] = $l_mapids;
		$arrFactorScores[$rankid]['leftmapUID'] = $leftmapUID;
		$arrFactorScores[$rankid]['leftmapid'] = $leftmapid;
		if(!is_null($leftIssueData)) {
			$arrFactorScores[$rankid]['leftissuedata'] = $leftIssueData;
		}
		if(!is_null($leftEmail)) {
			$arrFactorScores[$rankid]['leftemail'] = $leftEmail;
		}
		
	}
	
	function array_sum_key( $arr, $index = null ){
    if(!is_array( $arr ) || sizeof( $arr ) < 1){
        return 0;
    }
    $ret = 0;
    foreach( $arr as $id => $data ){
        if( isset( $index )  ){
            $ret += (isset( $data[$index] )) ? $data[$index] : 0;
        }else{
            $ret += $data;
        }
    }
    return $ret;
	}
	
	function genRankHTML($rank) {
		$rankHTML = '';
		$rankHTML .= '<div id="pagernav"></div>';
	 	$rankHTML .= '<div id="prevSS"><a id="prev" href="javascript:void(0);"><img src="' . ACCOUNT_PATH . '_images/_map/prev.jpg" width="27" height="27" alt="previous" /></a></div>' . "\n";
   	$rankHTML .= '<div id="nextSS"><a id="next" href="javascript:void(0);"><img src="' . ACCOUNT_PATH . '_images/_map/next.jpg" width="27" height="27" alt="next" /></a></div>' . "\n";
		$n=0;
		$j=0;
		$rankHTML .= '<div id="rankscroll">' . "\n";
		$rankHTML .= '<div class="sc">' . "\n";
	
		foreach($rank as $key => $clus_arr) {
			$totalscore = round($rank[$key]['total'],1);
			//$totalscore = 3;
			switch($totalscore) {
				case ($totalscore >= 9):
				$RKmClass = 'rankgreen';
				break;
				case ($totalscore >= 7):
				$RKmClass = 'rankblue';
				break;
				case ($totalscore >= 5):
				$RKmClass = 'rankorange';
				break;
				case ($totalscore >= 0):
				$RKmClass = 'rankred';
				break;
			}
		  $n++;
		  $j++;
		  if(($n % 5) == 0) {
		  	$n=1;
		  	$rankHTML .= '<div class=sc_' . $n . '">' . "\n";
		  }
			//could add a foreach clusterid in here if necessary
			if($key == $_SESSION['rankID']) {
				$rankHTML .= '<div class="rankmapholder rankselected">';
			} else {
				$rankHTML .= '<div class="rankmapholder">';
			}
			 	$rankHTML .= '<div class="ranklink"><a href="javascript:void(0);" id="faARR_' . $key . '" class="RankMLink" title="' . _("Click to Select Results") . '"></a></div>'; 
			  $rankHTML .= '<div class="quadoverlay"></div>'; 
				$rankHTML .= '<div class="tlscore">' . round((($rank[$key][1] / 10)*100),0) . '%</div>';
				$rankHTML .= '<div class="trscore">' . round((($rank[$key][2] / 10)*100),0) . '%</div>';
				$rankHTML .= '<div class="blscore">' . round((($rank[$key][3] / 10)*100),0) . '%</div>';
				$rankHTML .= '<div class="brscore">' . round((($rank[$key][4] / 10)*100),0) . '%</div>';
				$rankHTML .= '<div class="totalscore">' . round((($totalscore / 10)*100),0) . '%</div>';
				$rankHTML .= '<div class="' . $RKmClass . '"></div>';
				$rankHTML .= '<div class="rankpos"><strong>'. ordinal($j) . '</strong> - ' . $rank[$key]['rightname']. '</div>';
				$rankHTML .= '</div>';

			if(($n % 4) == 0) {
				$rankHTML .= '</div>';
			} else if ($n == count($rank)) {
				$rankHTML .= '</div>';
			}
			
		}
		$rankHTML .= '</div>';

		return $rankHTML;
	}
	
	//generates HTML for SWITCH MAP on left / right side i.e user & benchmark!
	function GenSelectHTML($mapUID, $side, &$MAPobj, $mapids, $currname) {
		//get multigen left map select0r
		if($side == 'left') {
			$selectid = 'altleftmap';
			$selectcolor = '#398dca';
			$p_text = sprintf(_("Change %s's MAP"), $currname);
		} else if ($side == 'right') {
			$selectid = 'altrightmap';
			$selectcolor = '#d47097';
			$p_text = sprintf(_("Change %s's MAP"), $currname);
		}
		$switchHTML = '<div style="font-size:11px; margin:0px 0px 5px 0px;"><strong>' . $p_text . '</strong></div>';
	  $maplist = $MAPobj->getMAPListvUID($mapUID);

	  if(count($maplist > 1)) {
	  	$switchHTML .= '<select name="' . $selectid . '" id="' . $selectid . '" style="font-size:12px; font-family:arial;">';
	  	$switchHTML .= '<option value="0" disabled="disabled">--Please Select--</option>' . "\n";
			foreach($maplist as $mapid => $userarr) {
				if($mapid != $mapids) {
					$switchHTML .= '<option value="' . $mapid . '">' . date('F j, Y, g:i a',$userarr['finish']) . '</option>' . "\n";
				} else {
					$switchHTML .= '<option value="' . $mapid . '" style="color:' . $selectcolor . ';" selected="selected">' . date('F j, Y, g:i a',$userarr['finish']) . '</option>' . "\n";
				}
			}
	   	$switchHTML .= '</select>' . "\n";
	  } else {
	  	$switchHTML = 'Only one MAP exists for this user';
	  }
	  return $switchHTML;
	}
	
	
	function getDiffClass($intDiff) {
  		if($intDiff <= 1.01){
				return "XDiF_Green";
			}
			elseif($intDiff <= 3.01){
				return "XDiF_Blue";
			}
			elseif($intDiff <= 5.01){
				return "XDiF_Orange";
			}
			else{
				return "XDiF_Red";
			}
  }
  
  function getDevClass($stddev) {
		if($stddev <= 1.01){
			return "SDGreenBullet";
		}
		elseif($stddev <= 2.01){
			return "SDBlueBullet";
		}
		elseif($stddev <= 3.01){
			return "SDOrangeBullet";
		}
		else{
			return "SDRedBullet";
		}
  }

  function getClusterABR($value) {
		switch($value) {
			case 'Relationships':
			return _('REL');
			break;
			case 'Thinking & Planning':
			return _('TAP');
			break;
			case 'Making Decisions':
			return _('DEC');
			break;
			case 'Getting Things Done':
			return _('GTD');
			break;
		}
  }
  
  function getClusterABRID($value) {
		switch($value) {
			case 1:
			return _('REL');
			break;
			case 2:
			return _('TAP');
			break;
			case 3:
			return _('DEC');
			break;
			case 4:
			return _('GTD');
			break;
		}
  }
  
	function getScoreColour($score) {
		$score = number_format($score, 0);
		if($score >= 90){
			return "green";
		} elseif ($score >= 70){
			return "blue";
		} elseif ($score >= 50){
			return "orange";
		} else{
			return "red";
		}
	}
	
	function getFlexBarColour($score) {
		$score = number_format($score, 0);
		if($score >= 90){
			return "#0a9e2c";
		} elseif ($score >= 70){
			return "#3b80b9";
		} elseif ($score >= 50){
			return "#e7970e";
		} else {
			return "#eb3c3c";
		}
	}
	
	function oppositeSide($intScore,$intIdeal) {
		if(($intIdeal < 5 && $intScore < 5) || ($intIdeal > 5 && $intScore > 5) || ($intIdeal == 5 || $intScore == 5 )){
			return false;
		}
		return true;
	}
	
	function lrBrain($score, $clusid) {
		switch($clusid) {
			case 1:
				if($score <= 5) {
					return '<span class="tooltipfactor">' . _("Relationships") . '</span><br />
						<strong>' . _("Left Brain Inventory") . '</strong>
						<ul>
						<li>' . _("Verbal, focusing on words, symbols, numbers") . '</li>
					  <li>' . _("Likely to follow rules without questioning them") . ' </li>
					  <li>' . _("Spelling and mathematical formula easily memorized") . ' </li>
					  <li>' . _("Enjoy observing") . ' </li>
					  <li>' . _("Listen to what is being said") . ' </li>
					  <li>' . _("Rarely use gestures when talking") . ' </li></ul>';
				} else if ($score > 5) {
					return '<span class="tooltipfactor">' . _("Relationships") . '</span><br />
						<strong>' .  _("Right Brain Inventory") . '</strong>
						<ul>
						<li>' . _("Visual, focusing on images, patterns") . '</li>
						<li>' . _("Like to know why you're doing something or why rules exist (reasons)") . ' </li>
						<li>' . _("May have trouble with spelling and finding words to express yourself") . ' </li>
						<li>' . _("Enjoy touching and feeling actual objects (sensory input)") . ' </li>
						<li>' . _("Listen to how something is being said") . ' </li>
						<li>' . _("Talk with your hands") . ' </li></ul>';
						//<li>' . _("Likely to think you're naturally creative, but need to apply yourself to develop your potential") . ' </li></ul>';
				}
			break;
			case 2:
				if($score <= 5) {
					return '<span class="tooltipfactor">' . _("Thinking & Planning") . '</span><br />
						<strong>' . _("Left Brain Inventory") . '</strong>
						<ul>
					  <li>' . _("Words used to remember things, remember names rather than faces") . ' </li>
					  <li>' . _("Make logical deductions from information") . ' </li>
					  <li>' . _("Like making lists and planning") . ' </li>
					  <li>' . _("Likely to follow rules without questioning them") . ' </li>
					  <li>' . _("Good at keeping track of time") . ' </li>
					  <li>' . _("Enjoy observing") . ' </li>
					  <li>' . _("Plan ahead") . ' </li></ul>';
				} else if ($score > 5) {
					return '<span class="tooltipfactor">' . _("Thinking & Planning") . '</span><br />
						<strong>' .  _("Right Brain Inventory") . '</strong>
						<ul>
						<li>' . _("\"Mind photos\" used to remember things, writing things down or illustrating them helps you remember") . ' </li>
						<li>' . _("Make lateral connections from information") . '</li>
						<li>' . _("Free association") . ' </li>
						<li>' . _("Like to know why you're doing something or why rules exist (reasons)") . ' </li>
						<li>' . _("No sense of time") . ' </li>
						<li>' . _("Enjoy touching and feeling actual objects (sensory input)") . ' </li>
						<li>' . _("Trouble prioritizing, so often late, impulsive") . ' </li></ul>';
				}
			break;
			case 3:
				if($score <= 5) {
					return '<span class="tooltipfactor">' . _("Making Decisions") . '</span><br />
						<strong>' . _("Left Brain Inventory") . '</strong>
						<ul>
					  <li>' . _("Analytical, led by logic") . '</li>
					  <li>' . _("Process ideas sequentially, step by step") . '</li>
					  <li>' . _("Words used to remember things, remember names rather than faces") . ' </li>
					  <li>' . _("Highly organized") . ' </li>
					  <li>' . _("Good at keeping track of time") . ' </li>
					  <li>' . _("Enjoy observing") . ' </li></ul>';
				} else if ($score > 5) {
					return '<span class="tooltipfactor">' . _("Making Decisions") . '</span><br />
						<strong>' .  _("Right Brain Inventory") . '</strong>
						<ul>
						<li>' . _("Intuitive, led by feelings") . ' </li>
						<li>' . _("Process ideas simultaneously") . ' </li>
						<li>' . _("\"Mind photos\" used to remember things, writing things down or illustrating them helps you remember") . ' </li>
						<li>' . _("Organization tends to be lacking") . ' </li>
						<li>' . _("No sense of time") . ' </li>
						<li>' . _("Enjoy touching and feeling actual objects (sensory input)") . ' </li></ul>';
				}
			break;
			case 4:
				if($score <= 5) {
					return '<span class="tooltipfactor">' . _("Gettings Things Done") . '</span><br />
						<strong>' . _("Left Brain Inventory") . '</strong>
						<ul>
					  <li>' . _("Process ideas sequentially, step by step") . '</li>
					  <li>' . _("Work up to the whole step by step, focusing on details, information organized") . ' </li>
					  <li>' . _("Highly organized") . ' </li>
					  <li>' . _("Good at keeping track of time") . ' </li>
					  <li>' . _("Enjoy observing") . ' </li>
					  <li>' . _("Likely read an instruction manual before trying") . ' </li></ul>';
				} else if ($score > 5) {
					return '<span class="tooltipfactor">' . _("Gettings Things Done") . '</span><br />
						<strong>' .  _("Right Brain Inventory") . '</strong>
						<ul>
						<li>' . _("Process ideas simultaneously") . ' </li>
						<li>' . _("\"Mind photos\" used to remember things, writing things down or illustrating them helps you remember") . ' </li>
						<li>' . _("See the whole first, then the details") . ' </li>
						<li>' . _("Organization ends to be lacking") . ' </li>
						<li>' . _("No sense of time") . ' </li>
						<li>' . _("Enjoy touching and feeling actual objects (sensory input)") . ' </li>
						<li>' . _("Unlikely to read instruction manual before trying") . ' </li></ul>';
				}
			break;
		}
	}
	
?>
