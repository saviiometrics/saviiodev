<?php

  
	/***********************************************************
	 *																												 *
	 *											TOOLTIP HTML											 *
	 *																											   *
	 ***********************************************************/
	
	echo '<div id="resultsContent">';
	
	//sort all diffs and devs hehe!
	if(($_SESSION['leftType'] != 'TalentMAP' && $_SESSION['leftType'] != 'TeamMAP') && ($_SESSION['rightType'] != 'TalentMAP' && $_SESSION['rightType'] != 'TeamMAP')) {
		$matching = 'diff';
		$scoreside = 'lscore';
		uasort($arrDeviations[$rankid]['allinfo'], cmpdiff);
		echo '<div id="matchtip"><strong>Please note</strong>
		<p>' . _("These statements show the percentage of best matched statements. So a small score difference will show a good match. A large score difference will show as a poor match.") . '</p>
		<p>' . _("The Star Rating system is not used for a normal survey MAP") . '</div>';
	} else {
		//$matching = 'stddev';
		$matching = 'diff';
		if($_SESSION['leftType'] == 'TalentMAP' || $_SESSION['leftType'] == 'TeamMAP') {
			uasort($arrDeviations[$rankid]['allinfo'], cmpdevleft);
			$stdevside = 'stddev_l';
			$scoreside = 'lscore';
		} else {
			uasort($arrDeviations[$rankid]['allinfo'], cmpdev);
			$stdevside = 'stddev';
			$scoreside = 'rscore';
		}
		echo '<div id="matchtip">' . _('The TalentMAP "star rating" is worked out based on the percentage of statements that return a rating score that is considered as having a low standard deviation and therefore a high rating score.') . '
		<p>' . _('The rating score is worked out based on the number of "MAPs" that make up that TalentMAP and the standard deviation of all the individual scores within the TalentMAP for that particular statement.') . '</p></div>';
	}
	
	/***********************************************************
	 *																												 *
	 *										INITIAL VARIABLES										 *
	 *																											   *
	 ***********************************************************/

	$gcount = 0;
	$avcount = 0;
	$pcount = 0;
	$bcount = 0;
	$leftStateGCOUNT = 0;
	$rightStateGCOUNT = 0;
	$qtotal = count($arrDeviations[$rankid]['allinfo']);

	$leftmapids = $arrFactorScores[$rankid]['l_mapids'];
	$leftmaparr = explode(',',$leftmapids);
	$lmcount = count($leftmaparr);
	$rightmapids = $arrFactorScores[$rankid]['r_mapids'];
	$rightmaparr = explode(',',$rightmapids);
	$rmcount = count($rightmaparr);
	//calc star score for each q!
	function calcStar($nummaps, $stdev) {
		$const = 0.01;
		$rating = ($nummaps / ($stdev + $const));
		return $rating; 
	}
	
	/***********************************************************
	 *																												 *
	 *										HEADER REPORT GFX										 *
	 *																											   *
	 ***********************************************************/

	echo '<div id="genStatBar"><div id="toptenbar">' . _('Generated Report Statistics') . '</div></div>' . "\n";
	$i = 0;
	
	/***********************************************************
	 *																												 *
	 *											SHOW RANK MAN											 *
	 *																											   *
	 ***********************************************************/
	
	if(sizeof($_SESSION['rank']) == 0) {
		foreach($cids as $cid => $value) {
			$j = 0;
			$fgcount = 0;
			$permatch = 0;
			foreach($fgs as $fg => $clusterid) {
				if($clusterid == $cid) {
					$fgcount++;
					$intNumF = $arrFactorScores[$j][$fg]['num'];
					$intModF = $arrFactorScores[$j][$fg]['diff'];
					$scorediff = ($intModF/$intNumF);
					$permatch += round((10 - $scorediff),2);
				}
			}
			$clustertotal = round(($permatch / $fgcount),1); 
			$rank[$j][$cid] = $clustertotal;
			$totalscore += $clustertotal;
		}
		$rank[$j]['total'] = round(($totalscore/count($cids)),1);
		$rank[$j]['leftname'] = $arrFactorScores[$j]['leftname'];
		$rank[$j]['rightname'] = $arrFactorScores[$j]['rightname'];
		$key = 0;
		$position = 1;
	} else {
		$rank = $_SESSION['rank'];
		$key = $rankid;
		$j = 1;
		foreach($rank as $rankkey => $clus_arr) {
			if($rankkey == $rankid) {
				$position = $j;
			}
			$j++;
		}
	}

	$totalscore = 0;
	$totalscore = round($rank[$key]['total'],1);
	//$totalscore = 3;
	switch($totalscore) {
		case ($totalscore >= 9):
		$RKmClass = 'rankgreen';
		break;
		case ($totalscore >= 7):
		$RKmClass = 'rankblue';
		break;
		case ($totalscore >= 5):
		$RKmClass = 'rankorange';
		break;
		case ($totalscore >= 0):
		$RKmClass = 'rankred';
		break;
	}
	$rankHTML .= '<div class="rankcontainer">';
	$rankHTML .= '<div class="rankmapholder">';
  $rankHTML .= '<div class="quadoverlay"></div>'; 
	$rankHTML .= '<div class="tlscore">' . round((($rank[$key][1] / 10)*100),0) . '%</div>';
	$rankHTML .= '<div class="trscore">' . round((($rank[$key][2] / 10)*100),0) . '%</div>';
	$rankHTML .= '<div class="blscore">' . round((($rank[$key][3] / 10)*100),0) . '%</div>';
	$rankHTML .= '<div class="brscore">' . round((($rank[$key][4] / 10)*100),0) . '%</div>';
	$rankHTML .= '<div class="totalscore">' . round((($totalscore / 10)*100),0) . '%</div>';
	$rankHTML .= '<div class="' . $RKmClass . '"></div>';
	$rankHTML .= '<div class="rankpos"><strong>'. ordinal($position) . '</strong> - ' . $rank[$key]['rightname']. '</div>';
	$rankHTML .= '</div>';
	$rankHTML .= '</div>';
	
	echo $rankHTML;
	
	echo '<div id="scorePercBox">';
	echo '<span id="scorePercText">' . _("Match Percentage Range") . '</span>';
	echo '</div>';
	
	/***********************************************************
	 *																												 *
	 *										CLUSTER LEGENDS											 *
	 *																											   *
	 ***********************************************************/
		
	echo '<div class="cleaner"></div>';
	
	echo '<div id="clusterLegend">';
	echo '<span class="clusterText">' . _("Factors") . '</span>';
	echo '<table border="0" cellpadding="4" cellspacing="0" style="font-family:Arial; font-size:15px; margin:6px 0px 6px 0px;"><tr>' . "\n";
	echo '<td valign="top"><img src="' . ACCOUNT_PATH . '_images/_map/rel_icon.png" width="19" height="19" /></td><td valign="top">' . _("Relationships") . '</td>' . "\n";
	echo '<td valign="top"><img src="' . ACCOUNT_PATH . '_images/_map/tap_icon.png" width="19" height="19" /></td><td valign="top">' . _("Thinking & Planning") . '</td>' . "\n";
	echo '<td valign="top"><img src="' . ACCOUNT_PATH . '_images/_map/mak_icon.png" width="19" height="19" /></td><td valign="top">' . _("Making Decisions") . '</td>' . "\n";
	echo '<td valign="top"><img src="' . ACCOUNT_PATH . '_images/_map/gtd_icon.png" width="19" height="19" /></td><td valign="top">' . _("Getting Things Done") . '</td>' . "\n";
	echo '</tr></table><br />';
	if($_SESSION['rightType'] == 'TalentMAP' || $_SESSION['leftType'] == 'TalentMAP' || $_SESSION['rightType'] == 'TeamMAP' || $_SESSION['leftType'] == 'TeamMAP') {
		echo '<div id="MAP_PredictorHolder">';
			echo '<span class="clusterText">' . _("MAP Predictor Strength") . '</span>';
			echo '<table border="0" cellpadding="4" cellspacing="0" style="font-family:Arial; font-size:15px; margin:6px 0px 6px 0px;"><tr>' . "\n";
			echo '<td valign="top"><img src="' . ACCOUNT_PATH . '_images/_map/excellent_bullet.png" /></td><td valign="top">' . _("Excellent") . '</td>' . "\n";
			echo '<td valign="top"><img src="' . ACCOUNT_PATH . '_images/_map/good_bullet.png" /></td><td valign="top">' . _("Good") . '</td>' . "\n";
			echo '<td valign="top"><img src="' . ACCOUNT_PATH . '_images/_map/poor_bullet.png" /></td><td valign="top">' . _("Average") . '</td>' . "\n";
			echo '<td valign="top"><img src="' . ACCOUNT_PATH . '_images/_map/bad_bullet.png" /></td><td valign="top">' . _("Poor") . '</td>' . "\n";
			echo '</tr></table>';
		echo '</div>';		
	}
	echo '<div id="BehaviourFlexLegend">' . "\n";
		echo '<span class="clusterText">' . _("Behavioural Flexibility") . '</span><br />' . "\n";
		echo '<div class="behaviourBarText">' . _("Behavioural Range"). ' &nbsp;<img src="' . ACCOUNT_PATH . '_images/_map/behaviour_bar.png" /><br /><br />' . "\n";
		echo '</div>' . "\n";
	echo '</div>' . "\n";
	echo '</div>'; 
	echo '<div class="cleaner"></div>';
	echo '<br />';

	/************************************************************
	 *																													*
	 *											STATEMENT LOOPS											*
	 *																													*
	 ***********************************************************/
	 
		echo '<div class="floatleft">';
		echo '<div id="toptenMatchText">' . _("Here are the top statements which best describe the benchmark, ranked in order of importance") . '</div>';
		echo '<div class="cleaner"></div>';
		//this counts the number of statements which have a std dev of < 2 (i.e. excellent predictors) If there are non in the first 10 (ranked by stddev) then it shows a message saying there are no useful predictors.
		$stddevCount = 0;
		$clusterPredictorCount = array();
		$clusterPredictorCount[1] = 0;
		$clusterPredictorCount[2] = 0;
		$clusterPredictorCount[3] = 0;
		$clusterPredictorCount[4] = 0;
		
		foreach($arrDeviations[$rankid]['allinfo'] as $qid => $value) {
			if($value[$stdevside] <= 2.01) {
				//this increments the number of predictive statements at a per cluster level (i.e less than or equal to 2.01)
				$clusterPredictorCount[$value['clusid']]++;
			}
			//THIS WORKS OUT THE % NUMBERS ON NUM STATEMENTS MIDDLE BOX
			if($value[$matching] <= 1.01) {
				$gcount++;
			} else if ($value[$matching] <= 3.01) {
				$avcount++;
			} else if($value[$matching] <= 5.01) {
				$pcount++;
			} else {
				$bcount++;
			}
			//THIS WORKS OUT TMAP MAP RATING
			if($_SESSION['leftType'] == 'TalentMAP') {
				$stratingL = calcStar($lmcount, $value['stddev_l']);
				if($stratingL >= 8 ) {
					$leftStateGCOUNT++;
				} else if ($strating >= 6) {
					$leftStateAVCOUNT++;
				} else if($strating < 6) {
					$leftStatePCOUNT++;
				}
			}
			//this works out the left TMAP grade if TMAP is on right!
			if($_SESSION['rightType'] == 'TalentMAP') {		
				$stratingR = calcStar($rmcount, $value['stddev']);
				if($stratingR >= 8 ) {
					$rightStateGCOUNT++;
				} else if ($strating >= 6) {
					$rightStateAVCOUNT++;
				} else if($strating < 6) {
					$rightStatePCOUNT++;
				}
			}
			
			//loop array of q's stored in as customs... so then do foreach of those then do $arrDeviations[$rankid]['allinfo']['KEYHERE_QID']['lscore'] etc to work values and display in order! (as keys will have been moved by sort);
			
			/************************************************************
			 *																													*
			 *										TOP 10 STATEMENTS											*
			 *																													*
			 ***********************************************************/
			if($i < 10) {
				if($i == 0) {
					if($_SESSION['rightType'] == 'MAP' && $_SESSION['leftType'] == 'MAP') {
						$titleBarTXT = _('Top 10 Matched Statements');
					} else {
						$titleBarTXT = _('Top Predictive Statements');
					}
					echo '<div class="top10bar"><div id="toptenbar">' . $titleBarTXT . '</div></div>' . "\n";
					echo '<div class="statementHolder">';
				}
				if($value[$scoreside] <= 5.0) {
					$qresponse = $stquestions[$qid]['l'];
					$qresponseOPP = $stquestions[$qid]['r'];
				} else if($value[$scoreside] >= 5.01) {
					$qresponse = $stquestions[$qid]['r'];
					$qresponseOPP = $stquestions[$qid]['l'];
				}
				//check if scores are on opposite sides
				if(oppositeSide($value['lscore'],$value['rscore'])) {
					$starSTR = ' *';
					$tooltipclass = ' ST_TT';
				} else {
					$starSTR = '';
					$tooltipclass = '';
				}
				$qscore = round((10 - $value['diff']),1);
				//$qscore = number_format($qscore,1);
				//get class to show
				$diffclass = getDiffClass($value['diff']);
				if($_SESSION['leftType'] == 'MAP' && $_SESSION['rightType'] == 'MAP') {
					$stddevclass = 'SDNeutralBullet';
				} else {
					$stddevclass = getDevClass($value[$stdevside]);
				}
				if($_SESSION['leftType'] != 'MAP' || $_SESSION['rightType'] != 'MAP') {
					if($value[$stdevside] <= 2.01) {
						$stddevCount++;
						echo '<div class="showStatement"><li class="' . $stddevclass . '">' . _($qresponse) . '</li></div><div class="' . $diffclass . $tooltipclass . '">' . getClusterABRID($value['clusid']) . $starSTR .  '</div><div class="ST_TT_Box">' . _($qresponseOPP). '</div><br />' . "\n";
					}
				} else {
					echo '<div class="showStatement"><li class="' . $stddevclass . '">' . _($qresponse) . '</li></div><div class="' . $diffclass . $tooltipclass . '">' . getClusterABRID($value['clusid']) . $starSTR .  '</div><div class="ST_TT_Box">' . _($qresponseOPP). '</div><br />' . "\n";
				}
			}
			if($i == 9) {
				//if the comparison has a tmap for example then the devcount checks if there has been any good ones in the top 10. If not it displays a message saying there are non.
				if($_SESSION['leftType'] != 'MAP' || $_SESSION['rightType'] != 'MAP') {
					if($stddevCount == 0) {
						echo _('There are no statements which match the criteria to be considering as predicitive statements.');
					}
				}
				echo '</div>' . "\n";
			}
			
			/************************************************************
			 *																													*
			 *										BOTTOM 10 STATEMENTS									*
			 *																													*
			 ***********************************************************/
			 
			if(($_SESSION['leftType'] != 'TalentMAP' && $_SESSION['leftType'] != 'TeamMAP') && ($_SESSION['rightType'] != 'TalentMAP' && $_SESSION['rightType'] != 'TeamMAP')) {
				//this shows the 10 best "mismatches" i.e bottom 10 statements
				if($i == 50) {
					echo '<div class="cleaner"></div>';
					echo '<div class="bottom10bar"><div id="toptenbar">' . _('Top 10 Mismatched Statements') . '</div></div>' . "\n";
					echo '<div class="statementHolder">';
				}
				if($i >= 50) {
					if($value[$scoreside] <= 5.0) {
						$qresponse = $stquestions[$qid]['l'];
						$qresponseOPP = $stquestions[$qid]['r'];
					} else if($value[$scoreside] >= 5.01) {
						$qresponse = $stquestions[$qid]['r'];
						$qresponseOPP = $stquestions[$qid]['l'];
					}
					//check if scores are on opposite sides
					if(oppositeSide($value['lscore'],$value['rscore'])) {
						$starSTR = ' *';
						$tooltipclass = ' ST_TT';
					} else {
						$starSTR = '';
						$tooltipclass = '';
					}
					$qscore = round((10 - $value['diff']),1);
					//$qscore = number_format($qscore,1);
					//get class to show
					$diffclass = getDiffClass($value['diff']);
					if($_SESSION['leftType'] == 'MAP' && $_SESSION['rightType'] == 'MAP') {
						$stddevclas = 'SDNeutralBullet';
					} else {
						$stddevclass = getDevClass($value[$stdevside]);
					}
					echo '<div class="showStatement"><li class="' . $stddevclass . '">' . _($qresponse) . '</li></div><div class="' . $diffclass . $tooltipclass . '">' . getClusterABRID($value['clusid']) . $starSTR .  '</div><div class="ST_TT_Box">' . _($qresponseOPP). '</div><br />' . "\n";
				}
				if($i == 59) {
					echo '</div>' . "\n";
				}
			}
			$i++;
		}
		echo '</div">';
	
		/************************************************************
		 *																													*
		 *									SCORE PERCENTAGE BOX										*
		 *																													*
		 ***********************************************************/
	
		echo '<div id="goodmatchbox" style="right:60px;">' . "\n";
        
		$excel_val = $gcount/$qtotal;
		$good_val = $avcount/$qtotal;
		$average_val = $pcount/$qtotal;
		$poor_val = ($qtotal - ($gcount + $avcount + $pcount)) / $qtotal;
        
        //Display values - format these as you output them!
		$excel_display_val = $excel_val*100;
		$good_display_val = $good_val*100;
		$average_display_val = $average_val*100;
		$poor_display_val = $poor_val*100;
		
		$matchScores = array("excel" => $excel_val, "good" => $good_val, "average" => $average_val, "poor" => $poor_val);
		$totalVal = array_sum($matchScores);
				             
        echo "<div style='display: none;'>";
        echo "QTotal: ".$qtotal;
        echo "Excel: ".$excel_val." -> ".$excel_display_val;  
        echo "Good: ".$good_val." -> ".$good_display_val;  
        echo "Aver: ".$average_val." -> ".$average_display_val;  
        echo "Poor: ".$poor_val." -> ".$poor_display_val;  
        echo "</div>";                             
                                
		echo '<div id="STBoxEx" class="ST_TT">' . number_format($excel_display_val, 0). '%</div>' . "\n";
		printf ('<div class="ST_TT_Box"><span class="matches_excellent">' . _("%s%% of statements were an excellent match") . '</span></div>', number_format($excel_display_val, 1));
		echo '<div id="STBoxGo" class="ST_TT">' . number_format($good_display_val, 0). '%</div>' . "\n";
		printf ('<div class="ST_TT_Box"><span class="matches_good">' . _("%s%% of statements were a good match") . '</span></div>', number_format($good_display_val, 1));
		echo '<div id="STBoxAv" class="ST_TT">' . number_format($average_display_val, 0). '%</div>' . "\n";
		printf ('<div class="ST_TT_Box"><span class="matches_average">' . _("%s%% of statements were an average match") . '</span></div>', number_format($average_display_val, 1));
		echo '<div id="STBoxPo" class="ST_TT">' . number_format($poor_display_val, 0). '%</div>' . "\n";
		printf ('<div class="ST_TT_Box"><span class="matches_poor">' . _("%s%% of statements were a poor match") . '</span></div>', number_format($poor_display_val, 1));
		echo '</div>';

	echo '</div>';
	echo '<div class="cleaner"></div><br />' . "\n";
	 
	 /***********************************************************
	 *																													*
	 *										CLUSTER GEN HERE											*
	 *																													*
	 ***********************************************************/
	 
   foreach($cids as $cid => $value) {
   	  $clusname = $value;
    	switch($cid) {
    		case 1:
    		$clust_class = 'RELHeader';
    		break;
    		case 2:
    		$clust_class = 'TAPHeader';
    		break;
    		case 3:
    		$clust_class = 'DECHeader';
    		break;
    		case 4:
    		$clust_class = 'GTDHeader';
    		break;
    	}
    	
    	//cmpdev is deviation - cmpdiff is difference (for instance MAP vs MAP)
  		if(($_SESSION['leftType'] != 'TalentMAP' && $_SESSION['leftType'] != 'TeamMAP') && ($_SESSION['rightType'] != 'TalentMAP' && $_SESSION['rightType'] != 'TeamMAP')) {
  			uasort($arrDeviations[$rankid][$cid], cmpdiff);
  			$scoreside = 'lscore';
  		} else {
				if($_SESSION['leftType'] == 'TalentMAP' || $_SESSION['leftType'] == 'TeamMAP') {
	  			uasort($arrDeviations[$rankid][$cid], cmpdevleft);
					$stdevside = 'stddev_l';
					$scoreside = 'lscore';
				} else {
					uasort($arrDeviations[$rankid][$cid], cmpdev);
					$stdevside = 'stddev';
					$scoreside = 'rscore';
				}
  		}
  		
  		/**************************************************************************
  		*																CLUSTER HEADER														*
  		**************************************************************************/

    	echo '<div class="cluster_container">';

			//only shows predicitive statements if both sides do not contain a talentMAP or a teamMAP
    	if(($_SESSION['leftType'] != 'TalentMAP' && $_SESSION['leftType'] != 'TeamMAP') && ($_SESSION['rightType'] != 'TalentMAP' && $_SESSION['rightType'] != 'TeamMAP')) {
    		echo '<div class="' . $clust_class. '">' . _($value) .  '<div class="clusterClosed">&nbsp;</div></div>';
    	} else {
    		if($clusterPredictorCount[$cid] != 1) {
    			$predictText = sprintf(_("%s statements are predictive"), $clusterPredictorCount[$cid]);
    		} else {
    			$predictText = sprintf(_("%s statement is predictive"), $clusterPredictorCount[$cid]);
    		}
    		echo '<div class="' . $clust_class. '"><div class="clusNameHolder">' . _($value) .  '</div><div class="clusterClosed">' . $predictText . '</div></div>';
    	}
  		echo '<div class="statementHolder">';
			echo '<ul>';
					
			foreach($arrDeviations[$rankid][$cid] as $qid => $value) {
				if($value[$scoreside] <= 5.0) {
					$qresponse = $stquestions[$qid]['l'];
					$qresponseOPP = $stquestions[$qid]['r'];
				} else if($value[$scoreside] >= 5.1) {
					$qresponse = $stquestions[$qid]['r'];
					$qresponseOPP = $stquestions[$qid]['l'];
				}
				//check if scores are on opposite sides
				if(oppositeSide($value['lscore'],$value['rscore'])) {
					$starSTR = ' *';
					$tooltipclass = ' ST_TT';
				} else {
					$starSTR = '';
					$tooltipclass = '';
				}
				$qscore = round(((10 - $value['diff'])/10)*100,0);
				//$qscore = number_format($qscore,1);
				//get class to show
				$diffclass = getDiffClass($value['diff']);
				//show neutral bullets if neither side contains a talentMAP or teamMAP
				if(($_SESSION['leftType'] != 'TalentMAP' && $_SESSION['leftType'] != 'TeamMAP') && ($_SESSION['rightType'] != 'TalentMAP' && $_SESSION['rightType'] != 'TeamMAP')) {
					$stddevclas = 'SDNeutralBullet';
				} else {
					$stddevclass = getDevClass($value[$stdevside]);
				};
				echo '<div class="showStatement"><li class="' . $stddevclass . '">' . _($qresponse) . '</li></div><div class="' . $diffclass . $tooltipclass . '">' . $qscore . '% ' . $starSTR .  '</div><div class="ST_TT_Box">' . _($qresponseOPP). '</div><br />';
				
			}
			echo '</ul>';
    	echo '</div>';
    	echo '<div class="cleaner"></div>';
    	echo '<div class="inner_cluster">';
    	
	    /************************************************************
			 *																													*
			 *									SCORE BAR / FACTOR GEN									*
			 *																													*
			 ***********************************************************/
		 
	   	$fgcount = 0;
			$clusterscore = 0;
    		foreach($fgs as $fg => $clusterid) {
  				$leftpositive = "";
    			$leftnegative = "";
    			$leftfacdesc = "";
    			$rightpositive = "";
    			$rightnegative = "";
    			$rightfacdesc = "";
    			foreach($fids as $fid => $info) {
    				if($info['factor_group'] == $fg) {
    					if($info['factor_align'] == 'l') {
    						$left = $info['factor_text'];
    						
    						//get pos and neg for this factor group (left side)
    						$thisfactors = $fdescs[$fid];
    						foreach($thisfactors as $type => $factdescs) {
    							foreach($factdescs as $fact) {
    								if($type == 'positive') {
    									$leftpositive .= '<li>' . _($fact) . '</li>';
    								} else if($type == 'negative') {
    									$leftnegative .= '<li>' . _($fact) . '</li>';
    								} else if($type == 'desc') {
    									$leftfacdesc = _($fact);
    								}
    							}
    						}
    					} else if ($info['factor_align'] == 'r') {
    						$right = $info['factor_text'];
    						
    						//get pos and neg for this factor group (right side)
    						$thisfactors = $fdescs[$fid];
    						foreach($thisfactors as $type => $factdescs) {
    							foreach($factdescs as $fact) {
    								if($type == 'positive') {
    									$rightpositive .= '<li>' . _($fact) . '</li>';
    								} else if($type == 'negative') {
    									$rightnegative .= '<li>' . _($fact) . '</li>';
    								} else if($type == 'desc') {
    									$rightfacdesc = _($fact);
    								}
    							}
    						}
    						
    					}
    				}
    			}
    			if($clusterid == $cid) {
    				
    				$fgcount++;
	    			$intNumF = $arrFactorScores[$rankid][$fg]['num'];
						$intModF = $arrFactorScores[$rankid][$fg]['diff'];
						$intScoreMin = min($arrFactorScores[$rankid][$fg]['lscoreall']);
						$intScoreMax = max($arrFactorScores[$rankid][$fg]['lscoreall']);
						$intIdealMin = min($arrFactorScores[$rankid][$fg]['rscoreall']);
						$intIdealMax = max($arrFactorScores[$rankid][$fg]['rscoreall']);

						
						$scorediff = ($intModF/$intNumF);
						$permatch = round((10 - $scorediff),2);
						$WsConstant = 17.8;
						$clusterscore += $permatch;
						
						$intScoreWidth = (round((($intScoreMax - $intScoreMin) * $WsConstant),2) - 8);
						$intIdealWidth = (round((($intIdealMax - $intIdealMin) * $WsConstant),2) - 8);
						
						$intScoreWidth = number_format($intScoreWidth,2,'.','');
						$intIdealWidth = number_format($intIdealWidth,2,'.','');
						
						$factscore = round((($permatch / 10) * 100),0);
						
						$flexBg = getFlexBarColour($factscore);
						$iconclass = getScoreColour($factscore);
												
    				echo '<div class="leftfactor">' . _($left) . '</div>';
    				echo '<div class="tooltip"><div class="tooltiptop"></div><div class="tooltipmid"><span class="tooltipfactor">' . _($left) . '</span><br /><br />' . _($leftfacdesc) . '<br /><br /><span class="factorPos">' . _("Constructive Behaviours") . '</span><br /><ul>' . $leftpositive . '</ul><span class="factorNeg">' . _("Less Constructive Behaviours") . '</span><ul>' . $leftnegative . '</ul></div><div class="tooltipbtm"></div></div>' . "\n";
    				echo '<div class="slider_con">';
    				
    					//ideal is right and score is left. What is benchmarked vs what is decided further up this page!
    					
							$lscorehtml_grey = '<div style="position:absolute; height:10px; repeat-x; top:14px; height:4px; width:' . $intScoreWidth . 'px; left:' . round(($WsConstant * $intScoreMin), 0) . 'px; border:3px solid grey; border-bottom:0px; margin-left:2px;"></div>';
							$lscorehtml = '<div style="position:absolute; height:10px; repeat-x; top:14px; height:4px; width:' . $intScoreWidth . 'px; left:' . round(($WsConstant * $intScoreMin), 0) . 'px; border:3px solid ' . $flexBg . '; border-bottom:0px; margin-left:2px;"></div>';
							$rscorehtml_grey = '<div style="position:absolute; height:10px; repeat-x; top:60px; height:4px; width:' . $intIdealWidth . 'px; left:' . round(($WsConstant * $intIdealMin), 0) . 'px; border:3px solid grey; border-top:0px; margin-left:2px;"></div>';
							$rscorehtml = '<div style="position:absolute; height:10px; repeat-x; top:60px; height:4px; width:' . $intIdealWidth . 'px; left:' . round(($WsConstant * $intIdealMin), 0) . 'px; border:3px solid ' . $flexBg . '; border-top:0px; margin-left:2px;"></div>';
							
    			  	switch($_SESSION['leftType']) {
    			  		case 'MAP':
    			  		if($_SESSION['rightType'] == 'TalentMAP' || $_SESSION['rightType'] == 'TeamMAP') {
    			  			echo $lscorehtml;
    			  			echo $rscorehtml_grey;
    			  		} else if($_SESSION['rightType'] == 'MAP' || $_SESSION['rightType'] == 'idealMAP') {
									echo $lscorehtml_grey;
									echo $rscorehtml;
    			  		}
    			  		break;
    			  		case 'idealMAP':
    			  		if($_SESSION['rightType'] == 'TalentMAP' || $_SESSION['rightType'] == 'TeamMAP') {
    			  			echo $lscorehtml;
    			  			echo $rscorehtml_grey;
    			  		} else {
    			  			echo $lscorehtml_grey;
    			  			echo $rscorehtml;
    			  		}
    			  		break;
    			  		case 'TalentMAP':
									echo $lscorehtml_grey;
									echo $rscorehtml;
    			  		break;
    			  		case 'TeamMAP':
    			  		if($_SESSION['rightType'] == 'TalentMAP') {
    			  			echo $lscorehtml;
    			  			echo $rscorehtml_grey;
    			  		} else {
    			  			echo $lscorehtml_grey;
    			  			echo $rscorehtml;
    			  		}
    			  	}
    							
	    			  echo '<div class="resultslider">';
	    			  //needed for stupid locale comma shit hahah <3 french!
	    			  $lf_total = number_format($arrFactorScores[$rankid][$fg]['lscore'],2,'.','');
	    			  $rf_total = number_format($arrFactorScores[$rankid][$fg]['rscore'],2,'.','');
	    			  $fac_total = number_format($arrFactorScores[$rankid][$fg]['num'],2,'.','');
	    			  $roundnum_l = $lf_total/$fac_total;
	    			  $roundnum_r = $rf_total/$fac_total;
	    			  $engnum_l = number_format($roundnum_l, 2, '.', '');
	    			  $engnum_r = number_format($roundnum_r, 2, '.', '');

    			  	/*************************************************
    			  	*																								 *
    			  	*											LEFT SIDE 								 *
    			  	* 																							 *
    			  	*************************************************/
    			  	
    			  	$lbrb_tt_left = '<div class="tooltip"><div class="tooltiptop"></div><div class="tooltipmid">' . lrBrain($engnum_l, $cid) . '</div><div class="tooltipbtm"></div></div>' . "\n";
    			  	$lbrb_tt_right = '<div class="tooltip"><div class="tooltiptop"></div><div class="tooltipmid">' . lrBrain($engnum_r, $cid) . '</div><div class="tooltipbtm"></div></div>' . "\n";
    			  	
    			  	switch($_SESSION['leftType']) {
    			  		case 'MAP':
	    			  		if($_SESSION['rightType'] == 'TalentMAP' || $_SESSION['rightType'] == 'TeamMAP') {
	    			  			echo '<div class="single_' . $iconclass . ' infoDesc" style="left:' . number_format((($engnum_l * $WsConstant)-11),2,'.','') . 'px; top:22px; z-index:6;"></div>';
	    			  		} else if($_SESSION['rightType'] == 'MAP') {
	    			  			echo '<div class="single_grey_alt infoDesc" style="left:' . number_format((($engnum_l * $WsConstant)-11),2,'.','') . 'px; top:22px; z-index:5;"></div>';
	    			  		} else {
	    			  			echo '<div class="single_grey infoDesc" style="left:' . number_format((($engnum_l * $WsConstant)-11),2,'.','') . 'px; top:22px; z-index:5;"></div>';
	    			  		}
	    			  		echo $lbrb_tt_left;
    			  		break;
    			  		case 'idealMAP':
	    			  		if($_SESSION['rightType'] == 'TalentMAP' || $_SESSION['rightType'] == 'TeamMAP') {
	    			  			echo '<div class="ideal_' . $iconclass . ' infoDesc" style="left:' . number_format((($engnum_l * $WsConstant)-16),2,'.','') . 'px; top:24px; z-index:6;"></div>';
	    			  		} else if($_SESSION['rightType'] == 'idealMAP' || $_SESSION['rightType'] == 'MAP') {
	    			  			echo '<div class="ideal_grey_alt infoDesc" style="left:' . number_format((($engnum_l * $WsConstant)-16),2,'.','') . 'px; top:24px; z-index:5;"></div>';
	    			  		}
	    			  		echo $lbrb_tt_left;
    			  		break;
    			  		case 'TeamMAP':
	    			  		if($_SESSION['rightType'] == 'TalentMAP') {
	    			  			echo '<div class="cross_' . $iconclass . ' infoDesc" style="left:' . number_format((($engnum_l * $WsConstant)-15),2,'.','') . 'px; top:26px; z-index:6;"></div>';
	    			  		} else {
	    			  			if($_SESSION['rightType'] == 'TeamMAP') {
	    			  				echo '<div class="cross_grey_alt infoDesc" style="left:' . number_format((($engnum_l * $WsConstant)-15),2,'.','') . 'px; top:26px; z-index:5;"></div>';
	    			  			} else {
	    			  				echo '<div class="cross_grey infoDesc" style="left:' . number_format((($engnum_l * $WsConstant)-15),2,'.','') . 'px; top:26px; z-index:5;"></div>';
	    			  			}
	    			  		}
	    			  		echo $lbrb_tt_left;
    			  		break;
    			  		case 'TalentMAP':
    			  			if($_SESSION['rightType'] == 'TalentMAP' || $_SESSION['rightType'] == 'TeamMAP') {
    			  				echo '<div class="cross_grey_alt infoDesc" style="left:' . number_format((($engnum_l * $WsConstant)-15),2,'.','') . 'px; top:26px; z-index:5;"></div>';
    			  			} else {
    			  				echo '<div class="cross_grey infoDesc" style="left:' . number_format((($engnum_l * $WsConstant)-15),2,'.','') . 'px; top:26px; z-index:5;"></div>';
    			  			}
    			  			echo $lbrb_tt_left;
    			  		break;
    			  	}
    			  	
    			  	/*************************************************
    			  	*																								 *
    			  	*											RIGHT SIDE 								 *
    			  	* 																							 *
    			  	*************************************************/
    			  	
    			  	switch($_SESSION['rightType']) {
    			  		case 'MAP':
    			  		echo '<div class="single_' . $iconclass . ' infoDesc" style="left:' . number_format((($engnum_r * $WsConstant)-11),2,'.','') . 'px; top:22px; z-index:6;"></div>';
    			  		echo $lbrb_tt_right;
    			  		break;
    			  		case 'idealMAP':
    			  		echo '<div class="ideal_' . $iconclass . ' infoDesc" style="left:' . number_format((($engnum_r * $WsConstant)-16),2,'.','') . 'px; top:24px; z-index:6;"></div>';
    			  		echo $lbrb_tt_right;
    			  		break;
    			  		case 'TeamMAP':
    			  		if($_SESSION['leftType'] == 'MAP' || $_SESSION['leftType'] == 'idealMAP') {
    			  			echo '<div class="cross_grey infoDesc" style="left:' . number_format((($engnum_r * $WsConstant)-15),2,'.','') . 'px; top:26px; z-index:5;"></div>';
    			  		} else {
    			  			echo '<div class="cross_' . $iconclass . ' infoDesc" style="left:' . number_format((($engnum_r * $WsConstant)-15),2,'.','') . 'px; top:26px; z-index:6;"></div>';
    			  		}
    			  		echo $lbrb_tt_right;
    			  		break;
    			  		case 'TalentMAP':
    			  			if($_SESSION['leftType'] == 'TalentMAP') {
    			  				echo '<div class="cross_' . $iconclass . ' infoDesc" style="left:' . number_format((($engnum_r * $WsConstant)-15),2,'.','') . 'px; top:26px; z-index:6;"></div>';
    			  			} else {
    			  				if($_SESSION['leftType'] == 'TeamMAP') {
    			  					echo '<div class="cross_grey_alt infoDesc" style="left:' . number_format((($engnum_r * $WsConstant)-15),2,'.','') . 'px; top:26px; z-index:5;"></div>';
    			  				} else {
    			  					echo '<div class="cross_grey infoDesc" style="left:' . number_format((($engnum_r * $WsConstant)-15),2,'.','') . 'px; top:26px; z-index:5;"></div>';
    			  				}
    			  			}	
    			  			echo $lbrb_tt_right;
    			  		break;
    			  	}

						echo '</div>';
    			  echo '</div>';
    			  echo '<div class="rightfactor">' . _($right) . '</div>' . "\n";
    			  echo '<div class="tooltip"><div class="tooltiptop"></div><div class="tooltipmid"><span class="tooltipfactor">' . _($right) . '</span><br /><br />' . _($rightfacdesc) . '<br /><br /><span class="factorPos">' . _("Constructive Behaviours") . '</span><br /><ul>' . $rightpositive . '</ul><span class="factorNeg">' . _("Less Constructive Behaviours") . '</span><ul>' . $rightnegative . '</ul></div><div class="tooltipbtm"></div></div>' . "\n";
    			  echo '<div class="factorscore fs_' . $iconclass . '">' . $factscore . '%</div>' . "\n";
    				echo '<div class="cleaner"></div>';
    				
    			}//end if clusterid;
    		}
    	$clustertotal	= ($clusterscore / $fgcount); 
			$clustertotal	= number_format(round(($clustertotal/10)*100, 0),0);
			//which class to use based on clustertotal!
  		switch($clustertotal) {
				case ($clustertotal >= 90):
				$ClusmClass = 'clustergreen';
				break;
				case ($clustertotal >= 70):
				$ClusmClass = 'clusterblue';
				break;
				case ($clustertotal >= 50):
				$ClusmClass = 'clusterorange';
				break;
				case ($clustertotal >= 0):
				$ClusmClass = 'clusterred';
				break;
			}
			$_SESSION['clustotal_' . $cid] = $clustertotal;
			echo '<div class="' . $ClusmClass . '">' . $clustertotal . '%</div>';
			//THIS RELATIVE DIV IS DUE TO IE7 BEING SHIT. WHEN cluster_container style is set to pos relative it does it's zindex shit and when the tooltip renders down it goes underneath. To remedy this cluster_container was set to no position attribute
			//and then this div below everything which is essentially empty so that the cluster scores are still in the correct place as they require to be inside a relative div for position.
			//echo '<div style="float:left; position:relative; width:564px; height:1px;"><div class="' . $ClusmClass . '">' . number_format(round($clustertotal, 1),1) . '</div></div>';
    	echo '<br />';
    	echo '</div>';//end inner cluster
    	echo '</div>';
    }

    ?>