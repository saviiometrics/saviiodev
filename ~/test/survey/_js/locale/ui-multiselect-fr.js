/**
 * Localization strings for the UI Multiselect widget
 *
 * @locale fr, fr-FR, fr-CA
 */

/*
$.extend($.ui.multiselect.locale, {
	addAll:'Ajouter tout',
	removeAll:'Supprimer tout',
	itemsCount:'#{count} items sélectionnés',
	itemsTotal:'#{count} items total',
	busy:'veuillez patienter...',
	errorDataFormat:"Les données n'ont pas pu être ajoutés, format inconnu.",
	errorInsertNode:"Un problème est survenu en tentant d'ajouter l'item:\n\n\t[#{key}] => #{value}\n\nL'opération a été annulée.",
	errorReadonly:"L'option #{option} est en lecture seule.",
	errorRequest:"Désolé! Il semble que la requête ne se soit pas terminé correctement. (Type: #{status})"
});

*/
/**
 * Localization strings for the UI Multiselect widget
 *
 * @locale fr, fr_FR
 */

$.extend($.ui.multiselect.locale, {
	addAll:'Ajouter tout',
	removeAll:'Effacer tout',
	itemsCount:'#{count} éléments sélectionnés',
	itemsTotal:'#{count} éléments au total',
	busy:'veuillez patienter...',
	errorDataFormat:"Impossible d'ajouter les options, format de données inconnu",
	errorInsertNode:"Un problème est survenu lors de la tentative d'ajout de l'élément:\n\n\t[#{key}] => #{value}\n\nL'opération a été abandonnée.",
	errorReadonly:"L'option #{option} est en lecture seule",
	errorRequest:"Désolé! Il semble y avoir un problème avec l'appel distant. (Type: #{status})"
});

