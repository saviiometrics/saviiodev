		<div id="innerContent">
			<div id="contentMenuBar">
	      <div id="contentSiFR">
	      	<h2 class="MWxBox">Saviio works with clients to develop a strategic approach to recruitment which will give them a measurable competitive advantage over their competitors. We do this through a wide range  of tools and practices including:</h2>
	      </div>
	      <div class="cleaner"></div>
	     </div>
	     <div id="contentSideGFX">
	     	<img src="/_images/_sidebar/<?=$sideGFX;?>" width="194" height="150" />
	     </div>
	     <div class="cleaner"></div>
	     <div class="featureHighlights">
         <ul>
             <li><strong>Workforce planning</strong><p>Too much  recruitment still takes place against unrealistic deadlines. Saviio works with  HR and line management to ensure maximum buy-in to strategic workforce  planning. The result - longer lead times, allowing for better targeting of the  right individuals and more effective hiring.</p></li>
             <li><strong>Segmentation and &lsquo;narrow casting&rsquo;</strong><p>Understanding exactly who the right candidate is and what motivates them in  their careers allows for pinpoint targeting through advertising, networking, PR  and search, demonstrably lowering the overall cost per hire.</p></li>
             <li class="cleaner"><strong>Talent pooling</strong><p>The best talent, particularly at senior levels or in highly specialist areas, can be  especially difficult to find and attract. Saviio can help to solve this problem by developing constant contact programmes which make direct approaches easier  and more effective when a specific role arises.</p></li>
             <li><strong>Assessment and selection</strong><p>Despite the increasing use of competency based interviewing, many  hiring decisions are still based on &lsquo;gut reaction&rsquo;. We work with organisations to ensure that recruitment is based, as much as possible, on a direct match between the business&rsquo; needs and the abilities, experience, motivations and goals of the individual candidate.</p></li>
        </ul>
      </div>
	</div>