<?php
$compobj = $userobj->getCompany($userobj->companyid);
?>

<input type="hidden" id="avatarType" value="<?=$userobj->avatar;?>" />
<input type="hidden" id="avatarURL" value="<?=$userobj->avatarURL;?>" />

<div class="contentPadding">
  <div class="floatleft"><div class="largeHeader"><?=_("My Settings");?></div></div>
 	<div id="settingsMenuHolder">
 		<ul class="settingsMenu">
 			<li><a href="javascript:void(0);" class="showContent current" id="Details"><b><?=_("Your Details");?></b></a></li>
 			<li><a href="javascript:void(0);" class="showContent" id="API"><b><?=_("API Token");?></b></a></li>
 		</ul>
 	</div>
 	<div class="cleaner"></div>
	<div id="settingsLeftBox">
	 	<div id="userAvatar"><img src="<?=$userobj->avatarURL;?>" /></div>
	 	<div class="medHeader floatleft"><?=_("Account Details");?></div>
	 	<div id="staticDetailsSettings">
	 		<?=$userobj->firstname . ' ' . $userobj->surname;?><br />
	 		<?=$compobj->companyname;?><br />
	 		<?=$userobj->email;?><br /><br />
	 	</div>
	 	<div class="cleaner"></div>
	 	<div class="medHeader floatleft"><?=_("Avatar");?></div>
	 	<div class="cleaner"></div>
		<div id="avatarFloat">
			<?=_("You can change your avatar at anytime and use one of the three methods below");?>:<br /><br />
			<div id="fbicon"><img src="<?=ACCOUNT_PATH;?>_images/_icons/facebook_icon.png" class="goIconBox" id="FBAva" /></div>
			<div id="twicon"><img src="<?=ACCOUNT_PATH;?>_images/_icons/twitter_icon.png" class="goIconBox" id="TWAva" /></div>
			<div id="gaicon"><img src="<?=ACCOUNT_PATH;?>_images/_icons/gravatar_icon.png" class="goIconBox" id="GRAva" /></div>
			<div class="cleaner"></div>
			<br />
			<strong><?=_("You are currently using");?>:</strong> <span id="avaDisplayType"><?=$userobj->avatar;?></span><br />
			(<?=_("click on icon to change");?>)
		</div> 
	</div>
	
	<div id="settingsRightBox">
	 	<div id="settingsDetails" class="settingsContent">
		 	<div class="medHeader floatleft"><?=_("Change Details");?></div>
		 	<div class="cleaner"></div>
	 			<form id="settingUserForm">
		 			<table border="0" cellpadding="6" cellspacing="0" class="contactTable">
		 				<tr>
		 					<td class="settingsFormTitle">
		 						<span class="headerDark-Grey-pro"><?=_("Firstname");?></span><br />
		 					</td>
		 					<td class="settingsFormInput"><input id="SAfirst_name" maxlength="40" name="SAfirst_name" size="20" type="text" class="validate[required,length[3,32]] settingsTextField" value="<?=$userobj->firstname;?>" /></td>
		 			  </tr>
		 			 	<tr>
		 					<td class="settingsFormTitle">
		 						<span class="headerDark-Grey-pro"><?=_("Surname");?></span><br />
		 					</td>
		 					<td class="settingsFormInput"><input id="SAlast_name" maxlength="80" name="SAlast_name" size="20" type="text" class="validate[required,length[3,32]] settingsTextField" value="<?=$userobj->surname;?>" /></td>
		 			  </tr>
		 			  <tr>
		 					<td class="settingsFormTitle">
		 						<span class="headerDark-Grey-pro"><?=_("Email Address");?></span><br />
		 					</td>
		 					<td class="settingsFormInput"><input id="SAemail" maxlength="80" name="SAemail" size="20" type="text" class="validate[required,length[3,32],custom[email]] settingsTextField" value="<?=$userobj->email;?>" /></td>
		 			  </tr>
		 			  <tr>
		 					<td class="settingsFormTitle">
		 						<span class="headerDark-Grey-pro"><?=_("Password");?></span><br />
		 					</td>
		 					<td class="settingsFormInput"><input id="SApassword" maxlength="80" name="SApassword" size="20" type="password" class="validate[optional,length[6,32]] settingsTextField" /></td>
		 			  </tr>
		 			  <tr>
		 					<td class="settingsFormTitle">
		 						<span class="headerDark-Grey-pro"><?=_("Repeat Password");?></span><br />
		 					</td>
		 					<td class="settingsFormInput"><input id="SAPasswordR" maxlength="80" name="SAPasswordR" size="20" type="password" class="validate[optional,length[6,32],confirm[SApassword]] settingsTextField" /></td>
		 			  </tr>
		 			</table>
	 			</form>
	 			<br />
	 			<div class="floatright" style="margin:0px 4px 0px 0px;"><a class="blueButton" id="updateUserDetails" href="javascript:void(0);"><?=_("Update Details");?></a></div>
		</div>
		<div id="settingsAPI" class="settingsContent" style="display:none;">
		 	<div class="medHeader floatleft"><?=_("Your API Token");?></div>
		 	<div class="cleaner"></div>
		 	<?php
		 	if($userobj->API_Access == 1) {
			?>
			<p><?=_("Your API Token is a special code which allows external software and services access to your account with your permission. Keep it safe though as it works just like a username and password and grants access to your account.");?></p>
			<p><?=_("If you think your API Token may have been compromised you can simply generate a new one below.");?></p>
			<p><?=_("Your Current API Token:");?></p>
			<strong><span id="APIToken"><?=$userobj->API_Token;?></span></strong>
			<br /><br />
			<a class="blueButton" id="regenAPIToken" href="javascript:void(0);"><?=_("Regenerate");?></a>
			<?php
			} else {
				echo '<p>' . _("I'm sorry you do not have sufficient priviledges to access the API functions. Please contact your administrator to find out more.") . '</p>' . "\n";
			}
			?>
		</div>
	</div>
</div>