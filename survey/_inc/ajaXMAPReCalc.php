	  <?php
	  if(!isset($_SESSION)) { session_start(); }
		$phpsessid = session_id();
		
		//include localization and site config files
		require_once("../site.config.php");

		//include DB AND ACCOUNT INFO CLASSES
		include CONTENT_PATH . '/_classes/db-class.php';
		include CONTENT_PATH . '/_classes/account-class.php';
		$accobj = new Account($_SESSION['accid']);
		
		//include other classes
		include FULL_PATH . '/_inc/_classes/question-class.php';
		include FULL_PATH . '/_inc/_classes/MAP-class.php';
		include FULL_PATH . '/_inc/_classes/user-class.php';
		
		$userobj = new User();
		$MAPobj = new MAP();
		
		require_once(FULL_PATH . "/_inc/localization.php");
		require_once(FULL_PATH . "/_inc/scripts.php");


		$fids = $MAPobj->getfids();
    $fgs = $MAPobj->getFactorGroups();
    $cids = $MAPobj->getClusters();
    $stquestions = $_SESSION['stquestions'];
		
		//get factor scores set from AjaXGen Script!
		$arrFactorScores = $_SESSION['arrFactorScores'];
		$arrDeviations = $_SESSION['arrDeviations'];
		
		//rank ID set in RANK and GEN files... obv with Gen it's set to 0 (as only one array is used)
		$rankid = $_SESSION['rankID'];

		//set if change side so you know which to reget from DB...
		if($_GET['newLeftMapID'] != 0) {
			$newLmapid = $_GET['newLeftMapID'];
		} else {
			$newLmapid = $arrFactorScores[$rankid]['l_mapids'];
		}
		
		if($_GET['newRightMapID'] != 0) {
			$newRmapid = $_GET['newRightMapID'];
		} else {
			$newRmapid = $arrFactorScores[$rankid]['r_mapids'];
		}
		
		//for some reason is didn't take the literal boolean value and worked with only the str value.. shrug cba working out why right now!
		if($_GET['showSingle'] == 'false') {
			
			/***************************************************************
			*																															 *
			*											COMPARE AND RANK CODE										 *
			*																															 *
			***************************************************************/
			
			$leftscores = $MAPobj->getScores($newLmapid);
			$rightscores = $MAPobj->getScores($newRmapid);
	
			//these cannot be lost or it fucks stuff up, so save them before resetting array!
			$l_mapids = $newLmapid;
			$r_mapids = $newRmapid;
			//$l_mapids = $arrFactorScores[$rankid]['l_mapids'];
			$rightname = $arrFactorScores[$rankid]['rightname'];
			$leftname = $arrFactorScores[$rankid]['leftname'];
			$leftmapUID = $arrFactorScores[$rankid]['leftmapUID'];
			$rightmapUID = $arrFactorScores[$rankid]['rightmapUID'];
	
			//reset all the values for that rankid!
			$arrFactorScores[$rankid] = array();
			//this array also changed the session array for scores and deviations inside the array! It returns the new total!
			$cluscores = loopGenScore($leftscores, $rightscores, $rankid, $l_mapids, $r_mapids, $rightname, $leftname, $rightmapUID, $leftmapUID, $arrFactorScores[$rankid]['leftmapid'], $arrFactorScores[$rankid]['rightmapid'], null, null, null, null);
			$rank = $_SESSION['rank'];
			$rank[$rankid]['total'] = $cluscores['total'];
			$rank[$rankid][1] = $cluscores[1];
			$rank[$rankid][2] = $cluscores[2];
			$rank[$rankid][3] = $cluscores[3];
			$rank[$rankid][4] = $cluscores[4];
			uasort($rank, cmprank);
			
			//no need to track as Remember KEY is preserved in uasort!! so even if it moves, will still update the correct one!!
			$i = 0;
			foreach($rank as $key => $clus_arr) {
				$i++;
				if($_SESSION['rankID'] == $key) {
					break;
				}
			}
			//works out which page should be selected after re-sort (for cycle plugin)
			$cyclepage = (ceil($i / 4) - 1);
	
			//set the session with the new sorted array!
			$_SESSION['rank'] = $rank;
			$newrankHTML = genRankHTML($rank);
			
			//set the sessions again! -_^
			$_SESSION['arrFactorScores'] = $arrFactorScores;
			$_SESSION['arrDeviations'] = $arrDeviations;
			
			if(!isset($_SESSION['fdescs'])) {
				$fdescs = $MAPobj->getFactDescs();
				$_SESSION['fdescs'] = $fdescs;
			} else {
				$fdescs = $_SESSION['fdescs'];
			}
			
	 		require_once('ajaXDisplayResults.php');
	    
	    echo '---' . $_SESSION['rightType'] . '---' . $arrFactorScores[$rankid]['leftname'] . '---' . $arrFactorScores[$rankid]['rightname'] . '---' . $newrankHTML . '---' . $cyclepage;
	    
	  } else {

			/***************************************************************
			*																															 *
			*											SINGLE MAP VIEW CODE										 *
			*																															 *
			***************************************************************/
			
			$l_mapids = $newLmapid;
			$leftscores = $MAPobj->getScores($l_mapids);
			$newMAPobj = $MAPobj->getMAPvid($newLmapid);
			$leftname = $newMAPobj->firstname . ' ' . $newMAPobj->surname;
			$leftmapUID = $newMAPobj->uid;

			//don't rank if only 1v1! as that's comparing innit :)
			//$arrFactorScores = array();
			//$arrDeviations = array();
	
			loopScoreSingle($leftscores, $l_mapids, $leftname, $leftmapUID, $newLmapid);

			if(!isset($_SESSION['fdescs'])) {
				$fdescs = $MAPobj->getFactDescs();
				$_SESSION['fdescs'] = $fdescs;
			} else {
				$fdescs = $_SESSION['fdescs'];
			}
			
			//this must be set to 0 when no rankings are being 
			$rankid = 0;
			require_once('ajaXShowSingle.php');

	  }
    ?>