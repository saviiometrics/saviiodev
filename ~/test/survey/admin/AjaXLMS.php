<?php

	if(!isset($_SESSION)) { session_start(); }
	$phpsessid = session_id();
	
	//include localization and site config files
	require_once("../site.config.php");
	
	//include DB AND ACCOUNT INFO CLASSES
	include CONTENT_PATH . '/_classes/db-class.php';
	include CONTENT_PATH . '/_classes/account-class.php';

	$accobj = new Account($_SESSION['accid']);

	//include other classes
	include FULL_PATH . '/_inc/_classes/user-class.php';
	include FULL_PATH . '/_inc/_classes/admin-class.php';
	
	$adminobj = new Admin($_SESSION['uid']);
	
	require_once(FULL_PATH . "/_inc/localization.php");
	require_once(FULL_PATH . "/_inc/scripts.php");
	
	if(isset($_GET['getLMSModule'])) {
		$lmarr = $adminobj->getLearnMod($_GET['lmid']);
		$adminobj->runTracker('Viewed Learning Module', $lmarr['moduleinfo']['name'], $_GET['lmid'], 'LMS');
		echo json_encode($lmarr);
	}
	
	?>