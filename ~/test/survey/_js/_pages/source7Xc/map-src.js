			$(function() {
					var movedit = 0;
					$("#slider").slider({ 
						//values: [1, 7, 10],
						value:5,
						min:0,
						max:10,
						step:0.1,
						animate:true,
						slide: function(event, ui) {
							movedit = 1;
						}
					});
					//timer gogo
					var qtimer = 0;
					function goTimer() {
						qtimer = 0;
						$("#timerdiv").everyTime(1000,function(i) {
							qtimer = i;
						});
					}
					goTimer();
					
					$("#slider .ui-slider-handle:nth-child(1)").addClass("handleb");
					
					$("#slider > a").removeClass("ui-corner-all ui-state-default");
					$("#slider").removeClass("ui-corner-all");
					
					var mapid = $("#mapid").val();
					var increment = $("#barincrement").val();
					var barwidth =  parseFloat($("#progressbar").attr("width"));
					$("#nextbutton a").click(function() {
						if(movedit == 0) {
							alert($('#moveslider').val());
						} else {
							barwidth = parseFloat(barwidth) + parseFloat(increment);
							$("#progressbar").animate({
								width: parseFloat(barwidth)
							}, 0);
							
							//stop the time
							$("#timerdiv").stopTime();
							
							var polarize = $("#polarize").val();
							var score = $("#slider").slider('value');
							$.get("/_inc/ajaXMAP.php", { cQ: 1, timeval:qtimer, npolar:polarize, userscore:score, currmapid:mapid },
							  function(data){
							  	var checkarr = data.split("---");
							  	if(jQuery.trim(checkarr[0]) == 'ended') {
							  		$("#QoutOf").html('100%');
							  		$("#questionnum").html($('#surcompletehead').val());
							  		$("#survey_container").html('<div id="completed_survey">' + $('#completesurveycopy').val() + '</div>');
							  		$("#nextqholder").html('');
							  	} else if(jQuery.trim(checkarr[0]) == 'TIMEOUT') {
							  		$("#survey_container").html('<div id="completed_survey">' + $('#sessiontimeout').val() + '</div>');
							  		$("#nextqholder").html('');
							  	} else {
								  	var responses = data.split("---");
								  	$("#left_response").html(responses[0]);
								  	$("#right_response").html(responses[1]);
								  	var currq = $("#currq").html();
								  	currq++;
								  	$("#currq").html(currq);
								  	$("#currqbig").html(currq);
								  	$("#polarize").val(responses[2]);
										goTimer();
										//reset slider value
										$("#slider").slider( 'value', 5 );
									}
							});
							movedit = 0;
						};
					});
				//end doc rdy
				});