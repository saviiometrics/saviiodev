<?php
define ("FULL_PATH",$_SERVER['DOCUMENT_ROOT'] . '/survey');
include FULL_PATH . '/_inc/_classes/db-class.php';
include FULL_PATH . '/_inc/_classes/account-class.php';
include FULL_PATH . '/_inc/_classes/question-class.php';
include FULL_PATH . '/_inc/_classes/MAP-class.php';
include FULL_PATH . '/_inc/_classes/user-class.php';
include FULL_PATH . '/_inc/_classes/admin-class.php';
include FULL_PATH . '/_inc/_classes/scriptInterface.php';
include FULL_PATH . '/site.config.php';
require_once(FULL_PATH . "/_inc/localization.php");
require_once(FULL_PATH . "/_inc/scripts.php");

class api {
	/**
	 * @var SimpleXMLElement
	 */
	protected $xml;
	/**
	 * @var Admin
	 */
	protected $userObject;
	/**
	 * @var Account
	 */
	protected $accountObject;
	/**
	 * @var dbInterface
	 */
	protected $dbi;
	protected $uid;
	protected $origin;
	
	//Public functions
	public function __construct($uid, $xml, $origin) {
		//Set Vars for user IE access levels
		$this->xml = $xml;
		$this->origin = $origin;
		$this->dbi = new dbInterface();
		if(!is_null($uid)) {
			$accountID = $this->dbi->getAccidForUID($uid);
			$this->accountObject = new Account($accountID);
			$this->userObject = new Admin($uid);
			$this->uid = $uid;
			$_SESSION['uid'] = $uid;
		}
	}
	
	/**
	 * Returns the result of the function requested in the xml
	 */
	public function doRequest() {
		$requests = $this->xml->request;
		$response = array();
		$code = 1;
		$i = 0;
		foreach($requests as $request) {
			$function = $request->function;
			if($this->dbi->OriginCanUseFunction($this->origin, $function)) {
				switch ($function) {
					case 'getMapList':
						$resp = $this->listMaps($request);
						break;
						
					case 'getCompanyList' :
						$resp = $this->listCompanies($request);
						break;
						
					case 'getLanguageList' :
						$resp = $this->listLanguages($request);
						break;
						
					case 'getUserLevelList' :
						$resp = $this->listUserLevels($request);
						break;
						
					case 'getCountryList' :
						$resp = $this->listCountries($request);
						break;
					
					case 'getEmailTemplateList' :
						$resp = $this->listEmailTemplates($request);
						break;
						
					case 'getAPIData' :
						$resp = $this->getAPIData($request);
						break;
						
					case 'getMapData':
						$resp = $this->getMapData($request);
						break;
						
					case 'getMapComparison':
						$resp = $this->getMapComparison($request);
						break;
						
					case 'getUserMaps' :
						$resp = $this->getUserMaps($request);
						break;
						
					case 'getMapDescription':
						$resp = $this->getMapDescription($request);
						break;
						
					case 'getClusterDescription' :
						$resp = $this->describeCluster($request);
						break;
						
					case 'getFactorGroupDescription' :
						$resp = $this->describeFactorGroup($request);
						break;
						
					case 'getMatchScoreDescription' :
						$resp = $this->describeMatchScore($request);
						break;
					
					case 'addTalentMap' :
						$resp = $this->createTmap($request, 'talent');
						break;
					
					case 'addTeamMap' :
						$resp = $this->createTmap($request, 'team');
						break;	
					
					case 'getEmailTemplate' :
						$resp = $this->getEmailTemplate($request);
						break;
						
					case 'addUser' :
						$resp = $this->addUser($request);
						break;
						
					case 'getFunctionList' :
						$resp = $this->getFunctionList($request);
						break;
						
					case 'getFunctionDescription' :
						$resp = $this->getFunctionDescription($request);
						break;
					
					case 'addEmailTemplate' :
						$resp = $this->addEmailTemplate($request);
						break;
						
					case 'getTalentMapShareList' :
						$resp = $this->getTmapShareList($request, 'talent');
						break;
						
					case 'getTeamMapShareList' :
						$resp = $this->getTmapShareList($request, 'team');
						break;
						
					case 'addTalentMapShare' :
						$resp = $this->addTmapShare($request, 'talent');
						break;
						
					case 'addTeamMapShare' :
						$resp = $this->addTmapShare($request, 'team');
						break;
						
					case 'getUserData' :
						$resp = $this->getUserData($request);
						break;
						
					case 'getUID' :
						$resp = $this->getUserUID($request);
						break;
						
					case 'getFullDescriptions' :
						$resp = $this->getFullDescriptions($request);
						break;
						
					default:
						$resp = array('code'=>0, 'message'=>'Call to unknown function');
						break;
				}
			} else {
				$resp = array('code'=>0, 'message'=>"$this->origin API cannot use $function");
			}
			$resp['code'] = isset($resp['code']) ? $resp['code'] : 1; 
			if($resp['code'] == 0) {
				$code = 0;
			}
			
			$resp['request']= array('function'=>$request->function);
			if($request->params->count() > 0) {
				$resp['request']['params'] = array();
				$resp['request']['params'] = $this->getRequestParams($request->params);
			}
			$response["response$i"] = $resp;
			$i ++;
		}
		//die(print_r($response, true));
		$response['code'] = $code;
		return $response;
	}
	
	protected function addTmapShare($request, $type) {
		$tmapid = $request->params->mapid;
		$shareType = $request->params->type;
		$shareid = $reques->params->id;
		$shareType = $shareType != 'company' && $shareType != 'user' ? null : $shareType;
		$required = $this->checkRequiredParams(array('mapid'=>$tmapid, 'type'=>$shareType, 'id'=>$shareid));
		if(!empty($required)) {
			return array('code'=>0, 'message'=>$this->getRequiredMissingFieldsMessage($required));
		}
		$map = new MAP();
		$compaccids = '';
		$adminaccids = '';
		foreach($map->getTMAPAccess($tmapid, 'company') as $company=>$level) {
			$compaccids .= $company . ',';
		}
		foreach($map->getTMAPAccess($tmapid, 'user') as $user=>$level) {
			$adminaccids .= $user . ',';
		}
		if($shareType == 'company') {
			$adminaccids = rtrim($adminaccids, ',');
			$compaccids .= $shareid;
		} else {
			$compaccids = rtrim($compaccids, ',');
			$adminaccids .= $shareid;
		}
		
		$map->updateTMAPAccess($tmapid, $compaccids, $adminaccids, 5, $type);
		return array('code'=>1, 'message'=>"Shares for $tmapid updated");
	}

	protected function delTmapShare($request, $type) {
		$tmapid = $request->params->mapid;
		$shareType = $request->params->type;
		$shareid = $reques->params->id;
		$shareType = $shareType != 'company' && $shareType != 'user' ? null : $shareType;
		$required = $this->checkRequiredParams(array('mapid'=>$tmapid, 'type'=>$shareType, 'id'=>$shareid));
		if(!empty($required)) {
			return array('code'=>0, 'message'=>$this->getRequiredMissingFieldsMessage($required));
		}
		$map = new MAP();
		$compaccids = '';
		$adminaccids = '';
		foreach($map->getTMAPAccess($tmapid, 'company') as $company=>$level) {
			if($shareType == 'company' && $company == $shareid) {
			} else {
				$compaccids .= $company . ',';
			}
		}
		$compaccids = rtrim($compaccids, ',');
		foreach($map->getTMAPAccess($tmapid, 'user') as $user=>$level) {
			if($shareType == 'user' && $user == $shareid) {
			} else {
				$adminaccids .= $user . ',';
			}
		}
		$adminaccids = rtrim($adminaccids, ',');
		$map->updateTMAPAccess($tmapid, $compaccids, $adminaccids, 5, $type);
		return array('code'=>1, 'message'=>"Shares for $tmapid updated");
	}
	
	//Functions Called by doRequest
	protected function getTmapShareList($request, $type) {
		$tmapid = $request->params->id;
		$required = $this->checkRequiredParams(array('id'=>$tmapid));
		if(!empty($required)) {
			return array('code'=>0, 'message'=>$this->getRequiredMissingFieldsMessage($required));
		}
		$map = new MAP();
		$out = array();
		$i = 0;
		foreach($map->getTMAPAccess($tmapid, 'company') as $company=>$level) {
			$out["company$i"] = $this->getCompany($company);
			$i ++;
		}
		foreach($map->getTMAPAccess($tmapid, 'user') as $userid=>$level) {
			$out["user$i"] = array('id'=>$userid, 'email'=>$this->dbi->getEmailForUID($userid), 'name'=>$this->getUserFullname($userid));
			$i++;
		}
		$out['code'] = 1;
		return $out;
	}
	
	protected function addEmailTemplate($request) {
		$name = $request->params->name;
		$contents = $request->params->body;
		$language = $request->params->language;
		$required = $this->checkRequiredParams(array('name'=>$name, 'body'=>$contents, 'language'=>$language));
		if(!empty($required)) {
			return array('code'=>0, 'message'=>$this->getRequiredMissingFieldsMessage($required));
		}
		$validLang = false;
		$langArr = $this->listLanguages($request);
		foreach($langArr['languages'] as $lang) {
			if($lang['code'] == $language) {
				$language = $lang['code'];
				$validLang = TRUE;
				break;
			}
		}
		$theirName = array('[FIRSTNAME]', '[SURNAME]');
		$yourName = array('[ADMIN_FN]', '[ADMIN_SN]');
		$urls = array('[SITEURL]');
		$surveyName = array('[SURVEYNAME]');
		if(!utilities::strposArray($contents, $theirName)) {
			return array('code'=>0, 'message'=>'The survey takers name as either [FIRSTNAME] or [SURNAME] must be included in the template');
		} elseif(!utilities::strposArray($contents, $yourName)) {
			return array('code'=>0, 'message'=>'The senders name as either [ADMIN_FN] or [ADMIN_SN] must be included in the template');
		} elseif(!utilities::strposArray($contents, $urls)) {
			return array('code'=>0, 'message'=>'The survey url as [SITEURL] must be included in the template');
		} elseif(!utilities::strposArray($contents, $surveyName)) {
			return array('code'=>0, 'message'=>'The survey name as [SURVEYNAME] must be included in the template');
		} elseif(!$validLang) {
			return array('code'=>0, 'message'=>"$language is not a valid language");
		} else {
			if($this->userObject->addEmail($name, $contents, $language)) {
				 return array('code'=>1, 'message'=>'Email template created');
			} else {
				return array('code'=>0, 'message'=>'An unknown error occured when trying to add email template');
			}
		}
	}
	
	protected function getEmailTemplate($request) {
		$id = $request->params->id;
		$required = $this->checkRequiredParams(array('id'=>$id));
		if(!empty($required)) {
			return array('code'=>0, 'message'=>$this->getRequiredMissingFieldsMessage($required));
		}
		$email = $this->userObject->getEmail($id);
		if(isset($email->name)) {
			//die(utilities::encodeForeignCharacters($email->email_content));
			$out = array('code'=>1, 'name'=>$email->name, 'body'=>$email->email_content, 'language'=>$email->lang); 
			return $out;
		} else {
			return array('code'=>0, 'message'=>"Unknown email template $id");
		}
	}
	
	protected function createTmap($request, $type) {
		$out = array();
		$ids = $request->params->id;
		$tmapname = $request->params->name;
		$tmapdesc = $request->params->description;
		$required = $this->checkRequiredParams(array('name'=>$tmapname, 'id'=>$ids, 'description'=>$tmapdesc));
		if(!empty($required)) {
			return array('code'=>0, 'message'=>$this->getRequiredMissingFieldsMessage($required));
		}
		$map = new MAP();
		$mapids = '';
		foreach($ids as $id) {
			$mapids .= "$id,";
		}
		$mapids = rtrim($mapids, ',');
		$this->userObject->getUser($this->uid);
		$companyid = $this->userObject->companyid;
		try {
			$map->addTMAP($type, $companyid, $this->uid, $tmapname, $tmapdesc, $mapids, '', '', '');
			$out['code'] = 1;
		}catch (Exception $e) {
			$out['code'] = 0;
			$out['message'] = ucfirst($type) . " MAP could not error: " . $e->getMessage();
		}
		return $out;
	}
	
	protected function getUserUID($request) {
		$username = $request->params->username;
		$required = $this->checkRequiredParams(array('username'=>$username));
		if(!empty($required)) {
			return array('code'=>0, 'message'=>$this->getRequiredMissingFieldsMessage($required));
		}
		$uid = $this->dbi->getUIDforEmail($username);
		if($uid == 0) {
			$response = array('code'=>0, 'uid'=>null, 'message'=>"Username $username not found");
		} else {
			$response = array('code'=>1, 'uid'=>$uid);
		}
		return $response;
	}
	
	protected function getUserData($request) {
		$uid = $request->params->uid;
		$required = $this->checkRequiredParams(array('uid'=>$uid));
		if(!empty($required)) {
			return array('code'=>0, 'message'=>$this->getRequiredMissingFieldsMessage($required));
		}
		if($this->userObject->getUser($uid)) {
			$response = array();
			$response['firstname'] = $this->userObject->firstname;
			$response['lastname'] = $this->userObject->surname;
			$response['level'] = $this->userObject->level;
			$response['email'] = $this->userObject->email;
			$response['username'] = $this->userObject->username;
			$response['company'] = $this->userObject->companyid;
			$response['country'] = $this->userObject->countryid;
			$response['status'] = $this->userObject->status;
			$response['language'] = $this->userObject->langid;
		}	else {
			 $response = array('code'=>0, 'message'=>"User with uid $uid could not be found");
		}
		return $response;
	}
	
	protected function getAPIData($request) {
		$username = $request->params->username;
		$password = $request->params->password;
		$required = $this->checkRequiredParams(array('username'=>$username, 'password'=>$password));
		if(!empty($required)) {
			return array('code'=>0, 'message'=>$this->getRequiredMissingFieldsMessage($required));
		}
		$password = md5($password);
		$uid = $this->dbi->getUIDforEmailPassword($username, $password);
		
		if(is_null($uid)) {
			return array('code'=>0, 'message'=>'Incorrect username and password combination supplied');
		} elseif(!$this->dbi->UIDHasApiAccess($uid)) {
			return array('code'=>0, 'message'=>'User ' . $username . ' does not have API access');
		} else {
			$response = array();
			$response['code'] = 1;
			$response['uid'] = $uid;
			$response['apikey'] = $this->dbi->getApiKeyForUID($uid);
		}
		return $response;
	}
	
	protected function getUserMaps($request) {
		$email = $request->params->email;
		$required = $this->checkRequiredParams(array('email'=>$email));
		if(!empty($required)) {
			return array('code'=>0, 'message'=>$this->getRequiredMissingFieldsMessage($required));
		}
		$uid = $this->dbi->getUIDforEmail($email);
		if(is_null($uid)) {
			return array('code'=>0, 'message'=>"User $email not found");
		}
		$MAP = new MAP();
		$out = array();
		$hasIncomplete = false;
		$hasComplete = false;
		$i = 0;
		$maps = $MAP->getFullMAPListvUID($uid);
		if(count($maps) == 0) {
			return array('code'=>0, 'message'=>"User $email has no maps");
		}
		foreach($maps as $id=>$map) {
			$i ++;
			$dat = array();
			if($map['finish'] == 0) {
				$hasIncomplete = true;
				$key = "incomplete$i";
			} else {
				$hasComplete = true;
				$key = "complete$i";
				$dat['finished'] = $map['finish'];
			}
			$dat['id']=$id;
			$dat['created']=$map['create'];
			$dat['started']=$map['begin'];
			$out[$key] = $dat;
		}
		$out['hasComplete'] = $hasComplete ? 'true' : 'false';
		$out['hasIncomplete'] = $hasIncomplete ? 'true' : 'false';
		$out['code'] = 1;
		return $out;
	}
	
	protected function listUserLevels($request) {
		$levels = $this->userObject->getLevels();
		$out = array();
		$i = 0;
		foreach($levels as $level) {
			$i ++;
			$out["level$i"] = array('name'=>$level['levelname'], 'code'=>$level['levelAccess']);
		}
		return array('code'=>1, 'levels'=>$out);
	}
	
	protected function listCountries($request) {
		$countries = $this->userObject->getCountries();
		$out = array();
		$i = 0;
		foreach($countries as $id=>$country) {
			$i ++;
			$out["country$i"] = array('name'=>$country, 'code'=>$id);
		}
		return array('code'=>1, 'countries'=>$out);
	}
	
	protected function listEmailTemplates($request) {
		$language =$request->params->language;
		$required = $this->checkRequiredParams(array('language'=>$language));
		if(!empty($required)) {
			return array('code'=>0, 'message'=>$this->getRequiredMissingFieldsMessage($required));
		}
		$emailTemplates = $this->userObject->getEmailList('all', $language);
		$count = 0;
		$out = array();
		if(count($emailTemplates) == 0) {
			return array('code'=>0, 'message'=>"Language $language is not recognised");
		}
		foreach($emailTemplates as $eid=>$data) {
			$count++;
			$out["template$count"] = array('name'=>$data['name'], 'code'=>$eid);
		}
		return array('code'=>1, 'emailTemplates'=>$out);
	}
	
	protected function listLanguages($request) {
		$accid = $this->dbi->getAccidForUID($this->uid);
		$out = array();
		$i = 0;
		foreach($this->userObject->getLanguages($accid) as $key => $lang) {
			$language = array();
			$language['name'] = $lang['lang_name'];
			$language['code'] = $lang['locale'];
			$i++;
			$out["language$i"] = $language;
		}
		return array('code'=>1, 'languages'=>$out);
	}
	
	protected function addUser($request) {
		$email = $request->params->email;
		$firstname = $request->params->firstname;
		$lastname = $request->params->lastname;
		$company = $request->params->company;
		$grantAPI = $request->params->grantAPI;
		$level = $request->params->level;
		$language = $request->params->language;
		$country = $request->params->country;
		$emailTemplate = $request->params->emailTemplate; 
		$required = $this->checkRequiredParams(array('email'=>$email, 'firstname'=>$firstname, 'lastname'=>$lastname, 'company'=>$company, 'level'=>$level, 'language'=>$language, 'country'=>$country, 'emailTemplate'=>$emailTemplate));
		if(!empty($required)) {
			return array('code'=>0, 'message'=>$this->getRequiredMissingFieldsMessage($required));
		}
		if(empty($grantAPI)) {
			$grantAPI = false;
		} else {
			$grantAPI = $grantAPI == 'true';
		}
		$password = $this->generatePassword();
		try {
			$this->userObject->addUser($email, $firstname, $lastname, $company, $email, $country, $emailTemplate, $level, $language);
		} catch(Exception $e) {
			return array('code'=>0, 'message'=>$e->getMessage());
		}
		$uid = $this->dbi->getUIDforEmail($email);
		if($grantAPI) {
			$this->dbi->enableAPIAccessFor($uid);
		}
		return array('code'=>1, 'message'=>"User with email $email added successfully", 'uid'=>$uid);
	}

	protected function listCompanies($request) {
		$companies = $this->userObject->getCompanies();
		$out = array();
		$i = 0;
		foreach ($companies as $companyid => $data) {
			$i ++;
			$out['company' . $i] = $this->getCompany($companyid);
		}
		return array('code'=>1,'companies'=>$out);
	}
	
	protected function getMapDescription($request) {
		$mapid = $request->params->map->id;
		$mapType = $request->params->map->mapType;
		$required = $this->checkRequiredParams(array('map->id'=>$mapid, 'mapType'=>$mapType));
		if(!empty($required)) {
			return array('code'=>0, 'message'=>$this->getRequiredMissingFieldsMessage($required));
		}
		$out = array();
		$MAPobj = new MAP();
		$i = 0;
		foreach($request->params->map as $map) {
			$mapOut = array();
			$mapType = $map->mapType;
			$id = $map->id;
			$code = 1;
			$mapNotFound = array('code'=>0, 'message'=>"Supplied map id $id not found");
			switch ($mapType) {
				case 'MAP':
					$data = $MAPobj->getMAPvid($id);
					if(!$data) {
						$mapOut = $mapNotFound;
						$code = 0;
					} else {
						$mapOut = array('id'=>$data->mapid, 'source'=>$data->mapid, 'name'=>$data->firstname . ' ' . $data->surname, 'company'=>$this->getCompany($data->companyid), 'creator'=>$data->firstname . ' ' . $data->surname);
					}
					break;
					
				case 'idealMAP':
					$data = $MAPobj->getiMAPvid($id);
					if(!$data) {
						$mapOut = $mapNotFound;
						$code = 0;
					} else {
						$mapOut = array('id'=>$data->imapid, 'name'=>$data->name, 'source'=>$data->imapid, 'company'=>$this->getCompany($data->companyid), 'creator'=>$this->getUserFullname($data->uid));
					}
					break;
					
				case 'TalentMAP' :
					$data = $MAPobj->getTMAP($id);
					if(!$data) {
						$mapOut = $mapNotFound;
						$code = 0;
					} else {
						$mapOut = array('id'=>$data->tmapid, 'name'=>$data->tmapname, 'company'=>$this->getCompany($data->companyid), 'creator'=>$this->getUserFullname($data->uid));
						$tmi = 0;
						foreach(explode(',', $data->mapids) as $sourcemap) {
							$mapOut["source$tmi"] = $sourcemap;
							$tmi ++;
						}
					}
					break;
					
				case 'TeamMAP':
					$data = $MAPobj->getTMAP($id);
					if(!$data) {
						$mapOut = $mapNotFound;
						$code = 0;
					} else {
						$mapOut = array('id'=>$data->tmapid, 'name'=>$data->tmapname, 'company'=>$this->getCompany($data->companyid), 'creator'=>$this->getUserFullname($data->uid));
						$tmi = 0;
						foreach(explode(',', $data->mapids) as $sourcemap) {
							$mapOut["source$tmi"] = $sourcemap;
							$tmi ++;
						}
					}
					break;
					
				default:
					$mapOut = array('code'=>0, 'message'=>"Supplied maptype $mapType not recognised");
					$code = 0;
					break;
			}
			$out["map$i"] = $mapOut;
			$i ++;
		}
		$out['code'] = $code;
		return $out;
	}
	
	protected function listMaps($request) {
		$MAPobj = new MAP();
		$mapType = $request->params->mapType;
		$required = $this->checkRequiredParams(array('mapType'=>$mapType));
		if(!empty($required)) {
			return array('code'=>0, 'message'=>$this->getRequiredMissingFieldsMessage($required));
		}
		//Get Map List
		switch ($mapType) {
			case 'MAP':
				$access = $this->userObject->getLevelAccess('map');
				if($access['VIEW'] == 'MA') {
					$compid = $this->userObject->headid;
				} else {
					$compid = $this->userObject->companyid;
				}
				$maplist = $MAPobj->getMAPList($compid, $access);
				break;
				
			case 'idealMAP':
				$access = $this->userObject->getLevelAccess('imap');
				if($access['VIEW'] == 'MA') {
					$compid = $this->userObject->headid;
				} else {
					$compid = $this->userObject->companyid;
				}
				$maplist = $MAPobj->getidealMAPList($compid, $access);
				break;
				
			case 'TalentMAP' :
				$access = $this->userObject->getLevelAccess('tmap');
				if($access['VIEW'] == 'MA') {
					$compid = $this->userObject->headid;
				} else {
					$compid = $this->userObject->companyid;
				}
				$maplist = $MAPobj->getTMAPList('talent', $compid, $access, 'analytics');
				break;
				
			case 'TeamMAP':
				$access = $this->userObject->getLevelAccess('tmap');
				if($access['VIEW'] == 'MA') {
					$compid = $this->userObject->headid;
				} else {
					$compid = $this->userObject->companyid;
				}
				$maplist = $MAPobj->getTMAPList('team', $compid, $access, 'analytics');
				break;
				
			default:
				return array('code'=>0, 'message'=>"Supplied maptype $mapType not recognised");
				break;
		}
		$Maps = array();
		$i = 0;
		foreach($maplist as $mapid => $userarr) {
			$map = array();
			if($mapType == 'MAP') {
				$name = $userarr['fullname'];
				$email = $userarr['email'];
			} else if($mapType == 'idealMAP') {
				$name = $userarr['imapname'];
				$email = null;
			} else {
				$name = $userarr['tmapname'];
				$email = null;
			}
			$company = $this->userObject->getCompany($userarr['companyid']);
			$map['mapid'] = $mapid;
			$map['name'] = $name;
		  	$map['company'] = $company->companyname;
		  	$map['email'] = $email;
		  	$map['mapType'] = $mapType;
		  	$i ++;
		  	$Maps["map$i"] = $map;
		}
		return array('code'=>1,'maps'=>$Maps);
	}

	protected function getMapComparison($request) {
		global $arrFactorScores, $arrDeviations;
		$leftmapid = $request->params->masterMap->id;
		$leftMapType = $request->params->masterMap->mapType;
		$rightMaps = $request->params->comparisonMap;
		$rightMapsid = $request->params->comparisonMap->id;
		$rightMapType = $request->params->comparisonMap->mapType;
		$required = $this->checkRequiredParams(array('masterMap->id'=>$leftmapid, 'masterMap->mapType'=>$leftMapType, 'comparisonMap'=>$rightMaps, 'comparisonMap->id'=>$rightMapsid, 'comparisonMap->mapType'=>$rightMapType));
		if(!empty($required)) {
			return array('code'=>0, 'message'=>$this->getRequiredMissingFieldsMessage($required));
		}
		$MAPobj = new MAP();
		
		try{
			$leftMapData = $this->getMapUserData($leftMapType, $leftmapid, $MAPobj);
		} catch(Exception $e) {
			return array('code'=>0, 'message'=>$e->getMessage());
		}
		$leftname = $leftMapData['name'];
		$leftmapUID = $leftMapData['creator'];
		$l_mapids = $leftMapData['originids'];
		$leftscores = $MAPobj->getScores($l_mapids);
		$rankid = 0;
		$comparisons = array();
		foreach ($request->params->comparisonMap as $rightMap) {
			$clusters = array();
			$rightmapid = $rightMap->id;
			$rightMapType = $rightMap->mapType;
			try{
				$rightMapData = $this->getMapUserData($rightMapType, $rightmapid, $MAPobj);
			} catch(Exception $e) {
				return array('code'=>0, 'message'=>$e->getMessage());
			}
			$rightname = $rightMapData['name'];
			$rightmapUID = $rightMapData['creator'];
			$r_mapids = $rightMapData['originids'];
			$rightscores = $MAPobj->getScores($r_mapids);
			scriptsInterface::loopGenScore($leftscores, $rightscores, $rankid, $l_mapids, $r_mapids, $rightname, $leftname, $rightmapUID, $leftmapUID, $leftmapid, $rightmapid);
			$comparisons["comparison$rankid"] = array('masterid'=>$leftmapid, 'comparisonid'=>$rightmapid);
			$if = 0;
			foreach($arrFactorScores[$rankid] as $factorgroupid=>$data) {
				if(is_numeric($factorgroupid)) {
					$factorgroup = array('id'=>$factorgroupid, 'masterScore'=>$data['lscore'] / $data['num'], 'comparisonScore'=>$data['rscore'] / $data['num'], 'difference'=>$data['diff'] / $data['num']);
					$factorgroup['matchScore'] = (10 - $factorgroup['difference']) * 10;
					$iq = 0;
					foreach($data['lscoreall'] as $questionID=>$score) {
						$question = array();
						$question['id'] = $questionID;
						$question['masterScore'] = $score;
						$question['comparisonScore'] = $data['rscoreall'][$questionID];
						$deviations = $arrDeviations[$rankid]['allinfo'][$questionID];
						$question['standardDeviation'] = $deviations['stddev'];
						$question['standardDeviationLeft'] = $deviations['stddev_l'];
						$question['difference'] = $deviations['diff'];
						$question['matchScore'] = 10 - $deviations['diff'];
						$factorgroup["question$iq"] = $question;
						$clusterid = $deviations['clusid'];
						$iq ++; 
					}
					if(!isset($comparisons["comparison$rankid"]["cluster$clusterid"])) {
						$comparisons["comparison$rankid"]["cluster$clusterid"] = array('id'=>$clusterid);
						$clusters[$clusterid] = array('total'=>0, 'count'=>0);
					}
					$comparisons["comparison$rankid"]["cluster$clusterid"]["factorgroup$if"] = $factorgroup;
					$clusters[$clusterid]['total'] += $factorgroup['matchScore'];
					$clusters[$clusterid]['count'] ++;
					$if++;
				}
				
			}
			$arrFactorScores = array();
			$arrDeviations = array();
			$clCount = 0;
			$clTotal = 0;
			foreach ($clusters as $clusterid=>$data) {
				$matchscore = $data['total'] / $data['count'];
				$comparisons["comparison$rankid"]["cluster$clusterid"]['matchScore'] = $matchscore;
				$clCount ++;
				$clTotal += $matchscore;
			}
			$comparisons["comparison$rankid"]['matchScore'] = $clTotal / $clCount;
			$rankid++;
		}
		
		$comparisons['code'] = 1;
		return $comparisons;
	}
	
	protected function describeCluster($request) {
		$id = $request->params->id;
		$required = $this->checkRequiredParams(array('id'=>$id));
		if(!empty($required)) {
			return array('code'=>0, 'message'=>$this->getRequiredMissingFieldsMessage($required));
		}
		$ret = array();
		$reti = 0;
		foreach($request->params->id as $id) {
			$id = (string)$id;
			$out = array();
			$map = new MAP();
			$clusters = $map->getClusters();
			if(isset($clusters[$id])) {
				$out['code'] = 1;
				$out['id'] = $id;
				$out['description'] = $clusters[$id];
			} else {
				$out['code'] = 0;
				$out['message'] = "Unknown cluster ID ($id) supplied";
			}
			$ret["cluster$reti"] = $out;
			$reti ++;
		}
		return $ret;
	}
	
	protected function describeFactorGroup($request) {
		$id = $request->params->id;
		$required = $this->checkRequiredParams(array('id'=>$id));
		if(!empty($required)) {
			return array('code'=>0, 'message'=>$this->getRequiredMissingFieldsMessage($required));
		}
		$ret = array();
		$reti = 0;
		foreach($request->params->id as $id) {
			$out = $this->dbi->getFactorGroupDescriptions($id);
			if(is_null($out)) {
					return array('code'=>0, 'message'=>'Unknown factor group ID supplied');
			}
			$out['code'] = 1;
			$ret["factorGroup$reti"] = $out;
			$reti ++;
		}
		return $ret;
	}
	
	protected function getMapData($request) {
		global $arrFactorScores, $arrDeviations;
		$leftmapid = $request->params->map->id;
		$leftMapType = $request->params->map->mapType;
		$required = $this->checkRequiredParams(array('map->id'=>$leftmapid, 'map->mapType'=>$leftMapType));
		if(!empty($required)) {
			return array('code'=>0, 'message'=>$this->getRequiredMissingFieldsMessage($required));
		}
		$return = array();
		$returni = 0;
		$code = 1;
		$MAPobj = new MAP();
		foreach($request->params->map as $map) {
			$leftMapType = $map->mapType;
			$leftmapid = $map->id;
			try{
				$leftMapData = $this->getMapUserData($leftMapType, $leftmapid, $MAPobj);
				$leftname = $leftMapData['name'];
				$leftmapUID = $leftMapData['creator'];
				$l_mapids = $leftMapData['originids'];
				$leftscores = $MAPobj->getScores($l_mapids);
				scriptsInterface::loopScoreSingle($leftscores, $l_mapids, $leftname, $leftmapUID, $leftmapid);
				$clusters = array();
				$out = array('id'=>$leftmapid);
				$if = 0;
				foreach($arrFactorScores[0] as $factorid=>$data) {
					if(is_numeric($factorid)) {
						$factor = array('id'=>$factorid, 'score'=>$data['lscore']);
						$iq = 0;
						foreach($data['lscoreall'] as $questionID=>$score) {
							$question = array();
							$question['id'] = $questionID;
							$question['score'] = $score;
							$deviations = $arrDeviations[0]['allinfo'][$questionID];
							$question['standardDeviation'] = $deviations['stddev_l'];
							$factor["question$iq"] = $question;
							$clusterid = $deviations['clusid'];
							$iq ++; 
						}
						if(!isset($out["cluster$clusterid"])) {
							$out["cluster$clusterid"] = array('id'=>$clusterid);
							$clusters[$clusterid] = array('total'=>0, 'count'=>0);
						}
						$out["cluster$clusterid"]["factorgroup$if"] = $factor;
						$clusters[$clusterid]['count'] ++;
						$if++;
					}
					
				}
			} catch(Exception $e) {
				$out =  array('code'=>0, 'message'=>$e->getMessage());
				$code = 0;
			}
			$return["map$returni"] = $out;
			$returni++;
		}
		$arrFactorScores =array();
		$arrDeviations =array();
		$out['code'] = $code;
		return $return;
	}

	protected function describeMatchScore($request) {
		$matchScore = $request->params->score;
		$required = $this->checkRequiredParams(array('score'=>$matchScore));
		if(!empty($required)) {
			return array('code'=>0, 'message'=>$this->getRequiredMissingFieldsMessage($required));
		}
		$ret = array();
		$code = 1;
		$reti = 0;
		foreach($matchScore = $request->params->score as $matchScore) {
			$out = array();
			if($matchScore > 10) {
				$out = array('code'=>0, 'message'=>'Invalid match score (supplied must be below 10)');
				$code = 0;
			} elseif($matchScore < 0) {
				$out = array('code'=>0, 'message'=>'Invalid match score (supplied must be above 0)');
				$code = 0;
			} elseif($matchScore >= 9.0) {
				$out['description'] = 'Excellent'; 
				$out['rank'] = 4;
			} elseif($matchScore >= 7.0) {
				$out['description'] = 'Good';
				$out['rank'] = 3;
			} elseif($matchScore >= 5.0) {
				$out['description'] = 'Average';
				$out['rank'] = 2; 
			} else {
				$out['description'] = 'Poor';
				$out['rank'] = 1;
			}
			$out['code'] = isset($out['code']) ? $out['code'] : 1;
			$ret["score$reti"] = $out;
			$reti ++;
		}
		$ret['code'] = $code;
		return $ret;
	}
	
	protected function getFunctionList($request) {
		$funcs = $this->dbi->getFunctionListForOrigin($this->origin);
		$functions = array();
		foreach($funcs as $key=>$func) {
			$functions["function$key"] = $func;
		}
		return $functions;
	}
	
	protected function getFunctionDescription($request) {
		$function = $request->params->functionName;
		$required = $this->checkRequiredParams(array('functionName'=>$function));
		if(!empty($required)) {
			return array('code'=>0, 'message'=>$this->getRequiredMissingFieldsMessage($required));
		}
		$i =0;
		$code = 1;
		foreach ($function = $request->params->functionName as $function) {
			$func = $this->dbi->getFunctionData($function, $this->origin);
			$validXML = true;
			try {
				$requiredXML = new SimpleXMLElement('<data>' . $func['requires'] . '</data>');
				$requiredArray = $this->getRequestParams($requiredXML);
				$retunsXML = new SimpleXMLElement('<data>' . $func['returns'] . '</data>');
				$returnsArray = $this->getRequestParams($retunsXML);
				$func['requires'] = $requiredArray;
				$func['returns'] = $returnsArray;
			} catch (Exception $e) {
				$func = array('code'=>0, 'message'=>'A database error has occured');
				$code = 0;
			}
			$functions["function$i"] = $func;
			$i ++;
		}
		return $functions;
	}
	
	protected function getFullDescriptions($request) {
		$map = new MAP();
		$clusters = $map->getClusters();
		$out = array();
		foreach($clusters as $id=>$description) {
			$cluster = array();
			$cluster['id'] = $id;
			$cluster['description'] = $description;
			$out["cluster$id"] = $cluster;
			
		}
		for ($i = 1; $i <= 12; $i++) {
			$factorgroup = $this->dbi->getFactorGroupDescriptions($i);
			$cluster = $factorgroup['clusterid'];
			unset($factorgroup['clusterid']);
			$out["cluster$cluster"]["factorgroup$i"] = $factorgroup;
			$out["cluster$cluster"]["factorgroup$i"]["questions"] = $this->dbi->getQuestionsForFactorGroup($i);
		}
		
		return $out;
	}
	
	//Internal Functions 
	protected function getMapUserData($mapType, $mapid, $MAPobj = null) {
		if(is_null($MAPobj)) {
			$MAPobj = new MAP();
		}
		switch ($mapType) {
			case 'MAP':
				$data = $MAPobj->getMAPvid($mapid);
				$origins = $mapid;
				return array('id'=>$data->mapid, 'name'=>$data->firstname . ' ' . $data->surname, 'company'=>$data->companyid, 'creator'=>$data->uid, 'originids'=>$origins);
				break;
				
			case 'idealMAP':
				$data = $MAPobj->getiMAPvid($mapid);
				$origins = $mapid;
				return array('id'=>$data->imapid, 'name'=>$data->name, 'company'=>$data->companyid, 'creator'=>$data->uid, 'originids'=>$origins);
				break;
				
			case 'TalentMAP' :
				$data = $MAPobj->getTMAP($mapid);
				$origins = $MAPobj->getmapidsVTMAP($mapid);
				return array('id'=>$data->tmapid, 'name'=>$data->tmapname, 'company'=>$data->companyid, 'creator'=>$data->uid, 'originids'=>$origins);
				break;
				
			case 'TeamMAP':
				$data = $MAPobj->getTMAP($mapid);
				$origins = $MAPobj->getmapidsVTMAP($mapid);
				return array('id'=>$data->tmapid, 'name'=>$data->tmapname, 'company'=>$data->companyid, 'creator'=>$data->uid, 'originids'=>$origins);
				break;
				
			default:
				throw new Exception("Supplied maptype $mapType not recognised", 0);
				break;
		}
	}
	
	protected function checkRequiredParams($requiredParams) {
		$out = array();
		foreach($requiredParams as $key=>$value) {
			//die(var_export(, TRUE));
			if($value != TRUE) {
				$out[] = $key;
			}
		}
		return $out;
	}
	
	protected function getRequiredMissingFieldsMessage($required) {
		$message = '';
		if(count($required) == 1) {
			$message = 'Required parameter';
		} else {
			$message = 'Required parameters';
		}
		$count = count($required);
		foreach($required as $field) {
			if(count($required) == 1) {
				$message .= ' "' . $field . '"';
			} elseif($count > 1) {
				$message .= ' "' . $field . '",';
			} else {
				$message = rtrim($message, ',');
				$message .= ' and "' . $field . '"';
			}
			$count --;
		}
		if(count($required) == 1) {
			$message .= ' is not set';
		} else {
			$message .= ' are not set';
		}
		return $message;
	}
	
	protected function generatePassword($length = 8) {
		$characters = array("A","B","C","D","E","F","G","H","J","K","L","M","N","P","Q","R","S","T","U","V","W","X","Y","Z","1","2","3","4","5","6","7","8","9");
		$out = '';
		while(strlen($out) <= $length) {
			$x = mt_rand(0, count($characters) - 1);
			if(isset($characters[$x])) {
				$out .= $characters[$x];
			}
		}
		return strtolower($out);
	}
	
	protected function getCompany($companyid) {
		$out = array();
		$company = $this->userObject->getCompany($companyid);
		$out['companyid'] = $companyid;
		$out['name'] = $company->companyname;
		$out['description'] = $company->description;
		$out['country'] = $this->dbi->getCountryName($company->countryid);
		$out['type'] = $company->type;
		$out['owner'] = $this->dbi->getFullNameForUID($company->uid);
		if($out['type'] == 'sub') {
			$out['head'] = $this->getCompany($company->headid);
		}
		return $out;
	}
	
	protected function getUserFullname($uid) {
		return $this->dbi->getFullNameForUID($uid);
	}

	protected function getRequestParams($request) {
		$i = 0;
		$params = array();
		foreach($request->children() as $key=>$value) {
			if(count($value->children()) > 0) {
				$params["$key$i"] = $this->getRequestParams($value);
			} else {
				$params["$key$i"] = (string)$value;
			}
			$i ++;
		}
		return $params;
	}
}
