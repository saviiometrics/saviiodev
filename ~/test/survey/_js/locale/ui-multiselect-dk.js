﻿/**
 * Localization strings for the UI Multiselect widget
 *
 * @locale dk, da-DK
 */

$.extend($.ui.multiselect.locale, {
	addAll:'Tilføj alt',
	removeAll:'Slet alt',
	itemsCount:'#{count} Valgte enheder',
	itemsTotal:'#{count} Enheder i alt ',
	optaget:'vent venligst...',
	errorDataFormat:"Kan ikke tilføje valgmuligheder, ukendt dataformat",
	errorInsertNode:"Der opstod et problem under forsøg på at tilføje enheden :\n\n\t[#{key}] => #{value}\n\n Operationen blev afbrudt",
	errorReadonly:"Denne option #{option} er read-only",
	errorRequest:"Beklager! Der opstod tilsyneladende et problem med fjernopkaldet. (Type: #{status})"
});