		<div id="innerContent">
			<div id="contentMenuBar">
	      <div id="contentSiFR">
	      	<h2 class="MWxBox">We all know that simple financial reward is rarely enough to attract and retain the very best talent.</h2>
	      </div>
	      <div class="cleaner"></div>
	     </div>
	     <div id="contentSideGFX">
					<img src="/_images/_sidebar/<?=$sideGFX;?>" width="194" height="150" />
	     </div>
	     <div class="cleaner"></div>
	     <div class="featureHighlights">
	     	<div class="contentPadTop">
	     	<p>That's why development and coaching programmes which allow individuals to fulfil their true potential are so important to both employee and organisation. We work with clients to create a strategic approach to this challenge through a wide range of tools and practices including:</p>
      	</div>
         <ul>
             <li><strong>Employee coaching</strong><p>Saviio helps businesses to build mentoring and coaching programmes which deliver demonstrable results.</p></li>
             <li><strong>Leadership development</strong><p>Leadership is more than just management under a different name. We help clients to identify and enhance leadership ability at all levels and apply it in a pragmatic way to the day-to-day workplace.</p></li>
             <li class="cleaner"><strong>Succession planning and career frameworks</strong><p>Saviio assists clients in developing structured career frameworks which give clear direction to ambitious employees and reduce the need for "last minute" recruitment.</p></li>
        </ul>
      </div>
		</div>