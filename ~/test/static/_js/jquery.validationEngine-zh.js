

(function($) {
	$.fn.validationEngineLanguage = function() {};
	$.validationEngineLanguage = {
		newLang: function() {
			$.validationEngineLanguage.allRules = 	{"required":{    			//在此添加您的正则表达式规则, 比方说电话吧
						"regex":"无",
						"alertText":"* 此区域为必选项",
						"alertTextCheckboxMultiple":"* 请选择一项",
						"alertTextCheckboxe":"* 此小空格为必选项"},
					"length":{
						"regex":"无",
						"alertText":"*之间 ",
						"alertText2":" 与 ",
						"alertText3": " 允许出现的字符"},
					"maxCheckbox":{	
						"regex":"无",
						"alertText":"* 超出可选范围"},	
					"minCheckbox":{
						"regex":"无",
						"alertText":"* 请选择 ",
						"alertText2":" 选项"},	
					"equals":{
						"regex":"无",
						"alertText":"* 您的密码不正确"},		
					"telephone":{
						"regex":/^[0-9\-\(\)\ ]+$/,
						"alertText":"* 无效电话号码"},	
					"email":{
						"regex":/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.(?:[A-Z]{2}|COM|ORG|NET|EDU|GOV|MIL|BIZ|INFO|MOBI|NAME|AERO|ASIA|JOBS|MUSEUM)$/i,
						//regex":/^[a-zA-Z0-9_\.-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9\.]{2,5}$/,
						"alertText":"* 无效电子邮件"},	
					"date":{
                         "regex":/^[0-9]{4}\-\[0-9]{1,2}\-\[0-9]{1,2}$/,
                         "alertText":"* 无效日期, 必须是年-月-日格式"},
					"onlyNumber":{
						"regex":/^[0-9\ ]+$/,
						"alertText":"* 只限数字"},	
					"noSpecialCharacters":{
						"regex":/^[0-9a-zA-Z]+$/,
						"alertText":"* 不允许特殊字符出现"},	
					"ajaxUser":{
						"file":"validateUser.php",
						"extraData":"",
						"alertTextOk":"* 此用户可以操作",	
						"alertTextLoad":"* 装载中，请稍候",
						"alertText":"* 此用户已被使用"},	
					"ajaxName":{
						"file":"validateUser.php",
						"alertText":"* 此用户名已被使用",
						"alertTextOk":"* 此用户名可以使用",	
						"alertTextLoad":"* 装载中，请稍候"},		
					"onlyLetter":{
						"regex":/^[a-zA-Z\ \']+$/,
						"alertText":"* 只限字母"}
            };
            
        }
    };
    $.validationEngineLanguage.newLang();
})(jQuery);


