<?php
//if($_SERVER['REMOTE_ADDR'] != "86.26.254.79") {
//    die("<h1>Saviio Ltd. is upgrading their servers this weekend which means that this system will be unavailable fromFriday 4th November 18:00 UK time until Sunday 6th November 23:59 UK time.</h1>");
//}
//ini_set('session.cookie_domain', substr($_SERVER['SERVER_NAME'],strpos($_SERVER['SERVER_NAME'],"."),100));
	//session_set_cookie_params(0, '/', '.saviio.com');
	if(!isset($_SESSION)) { session_start(); }
	$phpsessid = session_id();

	//include localization and site config files
	require_once("site.config.php");
	//include DB AND ACCOUNT INFO CLASSES
	include CONTENT_PATH . '/_classes/pageController-class.php';
	include SURVEY_CONTENT_PATH . '/_classes/account-class.php';
	$accobj = new Account();
	//include other classes
	include SURVEY_CONTENT_PATH . '/_classes/user-class.php';
	//include CONTENT_PATH . '/_classes/question-class.php';
	//include CONTENT_PATH . '/_classes/MAP-class.php';
	//include CONTENT_PATH . '/_classes/admin-class.php';

	$controlObj = new PageController();
	
	/***************** LIVE ******************/
	
	$surveyDomain = 'http://survey.saviio.com/';
	$staticDomain = 'http://static.saviio.com/';
	$mainDomain = 'http://www.saviio.com/';
	
	
	/***************** DEV ******************/
/*
	$surveyDomain = 'http://survey.saviio.eu/';
	$staticDomain = 'http://static.saviio.eu/';
	$mainDomain = 'http://www.saviio.eu/';
	*/

	//build template file
	require (FULL_PATH . "/header.php");
	require (FULL_PATH . "/menu.php");

	if(!isset($_GET['saviio'])) {
		require (CONTENT_PATH .  "/main.php");
	} else {
		require (CONTENT_PATH . '/' . $_GET['saviio'] . '.php');
	}
	require (FULL_PATH . "/footer.php");
	?>
