function exportImage() {
  flashMovie.exportImage('<?=$path;?>export.php');  
}
function print(){
  flashMovie.print();
}
function printAsBitmap(){
  flashMovie.printAsBitmap();
}
function amChartInited(chart_id){
  // get the flash object into "flashMovie" variable   
  flashMovie = document.getElementById(chart_id);
  // tell the field with id "chartfinished" that this chart was initialized
  document.getElementById("chartfinished").value = "chart " + chart_id + " is finished";           
}      
function amReturnData(chart_id, data){
  document.getElementById("data").value = unescape(data);
}
function amReturnParam(chart_id, param){
  document.getElementById("returnedparam").value = unescape(param);
}
function amReturnSettings(chart_id, settings){
  document.getElementById("settings").value = unescape(settings);
}      
function amReturnImageData(chart_id, data){
  // your own functions here
}   
function amError(chart_id, message){
  alert(message);
}      
function amGetZoom(chart_id, from, to){
  document.getElementById("from").value = from;
  document.getElementById("to").value = to;
}
function amGraphSelect(chart_id, index, title){
  document.getElementById("graphselect").value = index;
}
function amGraphDeselect(chart_id, index, title){
  document.getElementById("graphdeselect").value = index;
}
function amGraphHide(chart_id, index, title){
  document.getElementById("graphhide").value = index;
}
function amGraphShow(chart_id, index, title){
  document.getElementById("graphshow").value = index;
} 
function amRolledOverSeries(series){
  document.getElementById("series_hover").value = series;
}
function amClickedOnSeries(series){
  document.getElementById("series_clicked").value = series;
}
function amClickedOnBullet(graph_index, value, series, url, description){
  document.getElementById("bullet_clicked").value = value;
}          
function amRolledOverBullet(graph_index, value, series, url, description){
  document.getElementById("bullet_hover").value = value;
}  
function selectInterval(interval){
  flashMovie.setZoom(2006 - interval, 2006);
}
