// "borrowed" from John Resig: Javascript Array Remove
// http://ejohn.org/blog/javascript-array-remove/
Array.prototype.remove = function(from, to) {
    var rest = this.slice((to || from) + 1 || this.length);
    this.length = from < 0 ? this.length + from : from;
    return this.push.apply(this, rest);
};

//repopulate table with data from rowCache
function repopulateTableBody(tbl) {
    //aka cleanTableBody from TableSorter code
    if($.browser.msie) {
        function empty() {
            while ( this.firstChild ) this.removeChild( this.firstChild );
        }
        empty.apply(tbl.tBodies[0]);
    } else {
        tbl.tBodies[0].innerHTML = "";
    }
    jQuery.each(tbl.config.rowsCopy, function() {
        tbl.tBodies[0].appendChild(this.get(0));
    });
}
//removes the passed in row and updates the tablesorter+pager
function remove(tr, table) {
    //pager modifies actual DOM table to have only #pagesize TR's
    //thus we need to repopulate from the cache first
    repopulateTableBody(table.get(0));
    var index = $("tr", table).index(tr)-2;
    var c = table.get(0).config;
    tr.remove();
    //remove row from cache too
    c.rowsCopy.remove(index);
    c.totalRows = c.rowsCopy.length;
    c.totalPages = Math.ceil(c.totalRows / config.size);
    //now update
    table.trigger("update");
    //simulate user switches page to get pager to update too
    index = c.page < c.totalPages-1;
    $(".next").trigger("click");
    if(index)
        $(".prev").trigger("click");
}

$(function() {
    var table;
    //make all students with Major Languages removable
    $('table td:contains("Languages")').css("background-color", "red").live("click", function() {
        remove($(this).parents('tr').eq(0), table);
    });

    //create tablesorter+pager
    // CHANGED HERE OOPS
    // var table = $("table#tablesorter");
    table = $("table#tablesorter");
    table.tablesorter( { sortList: [ [0,0], [2,1] ] } )
        .tablesorterPager( { container: $("#pager")}  );
});
