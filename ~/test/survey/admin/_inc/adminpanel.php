
<?php
//accobj is set in the index file on every page!
$serviceLevels = $accobj->getServiceLevels();
$countries = $adminobj->getCountries();
?>

<input type="hidden" value="<?=ADMIN_ACCOUNT_PATH;?>" name="accpath" id="accpath" />
<!-- Overlay Box for Adding / Editing Companies -->

<div id="addlevelbox" class="levelsBox">
	<div class="addpad">
		<div class="cicon" style="padding:10px;"><img src="<?=ADMIN_ACCOUNT_PATH;?>_images/_icons/IC_AddCompany.png" /></div>
		<h2>Add New Level</h2>
		<p>
			<?=_("From here you can share accounts with other administrators. You can select the users you wish to share a particular account with and grant them certain access rights. You can select which access rights to give them by simply checking the checkboxes provided.");?>
		</p>
		<div class="addLevelboxheader"><strong><?=_('Main Level Rights');?></strong></div>
			<form id="addlevelform" name="addlevelform">
			<table border="0" cellpadding="3" cellspacing="2" width="864">
				<tr>
					<td><?=_('Level Name');?></td>
					<td align="right"><input type="text" name="levelName" id="levelName" class="validate[required,length[3,32]] textfield" size="30" /></td>
				</tr>
				<tr>
					<td><?=_('Who can access this level? (Selection and Above)');?></td>
					<td align="right">
						<select name="minAccess" id="minAccess">
						 <?php
						 foreach($adminlevels as $levelid => $levelInfo) {
								if(intval($levelInfo['levelAccess']) >= $levelAccess) {
									echo '<option value="' . $levelid . '">' . $levelInfo['levelname'] . '</option>';
								}
						 }
						 ?>
						</select>		
					</td>
				</tr>
			</table>
		</form>
		<br />
		<div id="addcontent">
		</div>
		<div id="moduleAccess">
			<br /><br /><div class="addLevelboxheader"><strong>Module Access</strong></div><br />
			  <table cellspacing="1" cellpadding="4" border="0">
				<tbody>
					<tr>
					<?php 
					$modules = $adminobj->getALLModules();
					$i = 0;
					foreach($modules as $key => $modArr) {
						if($i !=0 ) {
							if(($i % 6) == 0) {
								echo '</tr><tr>';
							}
						}
						echo '<td><input type="checkbox" class="moduleaccess" id="modid_' . $key . '" />&nbsp;' . $modArr['module_name'] . '</td>';
						$i++;
					}
					?>
					</tr>
				</tbody>
			</table>
		</div>
		<p>
			<button class="saveLevel"> <?=_('Add new Level');?> </button>
			<button class="close"> <?=_('Close');?> </button>
		</p>

	</div>
</div>

<?php
$allmodules = $adminobj->getALLModules();
$limitsArr = $adminobj->getLimits();
?>
<div id="editAccountBox" class="levelsBox">
	<div class="addpad">
		<div class="cicon" style="padding:10px;"><img src="<?=ADMIN_ACCOUNT_PATH;?>_images/_icons/IC_AddCompany.png" /></div>
		<h2>Edit Account</h2>
		<p>
			<?=_("From here you can turn on custom account limits for accounts as well as edit their existing limits if they already have this activated. You can also edit the name of the account and change the account owner. Note however that users added with the old admin will not be reassocaited with the new admin. Only subsequent users will be.");?>
		</p>
		<div class="addLevelboxheader"><strong><?=_('Change Account Information');?></strong></div>
		<div id="limitsHolder">
			<strong><?=_("Activate or Disable Custom Limits");?></strong><br /><br />
			<a href="javascript:void(0);" id="toggleCustom"></a><br />
			<table border="0" cellpadding="4" cellspacing="0" width="370">
				<?php
				foreach($limitsArr as $limitkey => $limitval) {
					switch($limitkey) {
						case 'accounts':
						$used = TOTAL_ACCOUNTS;
						$displayName = 'Accounts';
						break;
						case 'maps':
						$used = TOTAL_MAPS;
						$displayName = 'MAPs';
						break;
						case 'subaccounts':
						$used = TOTAL_SUBACCOUNTS;
						$displayName = 'Sub Accounts';
						break;
						case 'talentMAPs':
						$used = TOTAL_TALENTMAPS;
						$displayName = 'talentMAPs';
						break;
						case 'teamMAPs':
						$used = TOTAL_TEAMMAPS;
						$displayName = 'teamMAPs';
						break;
						case 'languages':
						$used = TOTAL_LANGUAGES;
						$displayName = 'Languages';
						break;
						case 'users':
						$used = TOTAL_USERS;
						$displayName = 'Administrators';
						break;
						case 'idealMAPs':
						$used = TOTAL_IDEAL;
						$displayName = 'idealMAPs';
						break;
						case 'emails':
						$used = TOTAL_EMAILS;
						$displayName = 'E-Mail Templates';
						break;
					}
					
					if($limitval != 'NOLIMIT') {
						$rawratio = ($used / $limitval);
						$widthval = floor(476 * $rawratio);
						$limitView = '<span class="limitUsed">' . $used . '</span> / ' . $limitval;
					} else {
						$widthval = 1;
						$limitView = '<span class="limitUsed">' . $used . '</span>';
					}
					
					//this works out the position (height wise) of the sprite based upon the ratio of used items compared to limit ratio.
					if($rawratio <= 0.6){
						$top = -10;
					}elseif($rawratio < 0.8){
						$top = -20;
					}elseif($rawratio <= 1){
						$top = -30;
					}
					echo '<tr><td width="128"><strong>' . $displayName . '</strong> </td><td width="58"><a href="javascript:void(0);" id="dec_' . $limitkey . '" class="decTotal">-</a> <span class="currTotal" id="total_' . $limitkey . '">' . $used . '</span> <a href="javascript:void(0);" id="inc_' . $limitkey . '" class="incTotal">+</a></td><td> / <input type="text" value="' . $limitval. '" name="edit_' . $limitkey . '" id="edit_' . $limitkey . '" class="textfield"></td></tr>';
				}
				?>
			</table>
		</div>
		<div id="accinfoHolder">
			<strong>Account Information</strong><br /><br />
			<table border="0" cellspacing="0" cellpadding="4" width="422">
				<tr>
					<td width="130">Account Name: </td><td><input type="text" name="edit_accname" id="edit_accname" value="" /></td>
				</tr>
				<tr>
					<td width="130"><?=_('Select Service Level');?></td>
					<td>
						<select name="edit_servicelevel" id="edit_servicelevel">
							<?php
							foreach($serviceLevels as $serviceid => $serviceName) {
								echo '<option value="' . $serviceid . '">' . $serviceName . '</option>' . "\n";
							}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td width="130">Account Email: </td><td><input type="text" name="edit_accemail" id="edit_accemail" value="" /></td>
				</tr>
			</table>
			<p><button class="updateAccount"><?=_('Update Account');?></button></p>
		</div>
		<div class="cleaner"></div>
		<p>
			<button class="updateLimits"> <?=_('Update Limits');?> </button>
			<button class="close"> <?=_('Close');?> </button>
		</p>

	</div>
</div>

<div id="addAccountBox" class="levelsBox">
	<div class="addpad">
		<div class="cicon" style="padding:10px;"><img src="<?=ADMIN_ACCOUNT_PATH;?>_images/_icons/IC_AddCompany.png" /></div>
		<h2>Add New Account</h2>
		<p>
			<?=_("Add new account from here");?>
		</p>
		<div class="addLevelboxheader"><strong><?=_('Add a new Account');?></strong></div>
			<p><?=_('Adding a new account is very simple. All you need are the account name, the admin email (this will be used for the first admin user added to the system as a master admin) and the service level of the account which determines limits. If you wish to set custom limits for this account please add it as normal, then go to edit account and click the "custom limits" link to change them from the service level defaults.');?></p>
			<form id="addAccountForm" name="addAccountForm">
				<table border="0" cellpadding="3" cellspacing="2" width="864">
					<tr>
						<td><?=_('Account Name');?></td>
						<td align="right"><input type="text" name="newAccName" id="newAccName" class="validate[required,length[3,32]] textfield" size="30" /></td>
					</tr>
					<tr>
						<td><?=_('Admin Email');?></td>
						<td align="right"><input type="text" name="newAdminEmail" id="newAdminEmail" class="validate[required,length[3,32],custom[email],ajax[ajaxUser]] textfield" size="30" /></td>
					</tr>
					<tr>
						<td><?=_('Select Service Level');?></td>
						<td align="right">
							<select name="serviceLevel" id="serviceLevel">
								<?php
								foreach($serviceLevels as $serviceid => $serviceName) {
									echo '<option value="' . $serviceid . '">' . $serviceName . '</option>' . "\n";
								}
								?>
							</select>
							</td>
					</tr>
					<tr>
						<td><?=_("Firstname");?></td>
						<td align="right"><input type="text" name="savfirstname" id="savfirstname" class="validate[required,length[1,20]] textfield" size="30" /></td>
					</tr>
					<tr>
						<td><?=_("Surname");?></td>
						<td align="right"><input type="text" name="savsurname" id="savsurname" class="validate[required,length[1,20]] textfield" size="30" /></td>
					</tr>
					<tr>
						<td><?=_("Account Country");?></td>
						<td align="right">
							<select name="countryid" id="countryid">
								 <?php
								 foreach($countries as $countryid => $countryname) {
								    echo '<option value="' . $countryid . '">' . $countryname . '</option>' . "\n";
								 }
								 ?>
							</select>	
						</td>
					</tr>
				</table>
			</form>
		<br />
		<p>
			<button class="addNewAccount"> <?=_('Add new Account');?> </button>
			<button class="close"> <?=_('Close');?> </button>
		</p>
	</div>
</div>

<div id="structurebar"><div class="iconfloat"><img src="<?=ADMIN_ACCOUNT_PATH;?>_images/_icons/IC_Structure.png" /></div> <div class="floatleft"><?=_('Admin Panel lolz');?></div></div>
<div class="titlebar_shadow"></div>
<div id="content_pad">

	<h1 class="MWxHeader"><?=_("Administration Panel")?></h1>
	<p><?=_('In this section you can to create and edit main accounts and associated accounts. Additionally you can also share your own accounts by clicking on the "Share Account" icon in the Admin panel column of the account you wish to share. Alternatively if you wish to request access to an account, click on the "Request Account Share" icon of the account you wish to have access too. If you cannot see the account you want access too, simply click the "Request Share" button on the right of the page, and search for the account you want access too and follow the on screen instructions.');?></p>
	<p><strong><?=_("Here is a key for the icons in the \"Access Controls\" column, simply click these icons to access the features.");?></strong></p>
	<p>
	<br />
	<div class="acc_sharetab" style="margin-left:10px;"><a class="addLevel" href="javascript:void(0);">Add New Level</a></div>
	<div class="acc_sharetab"><a class="addAccount" href="javascript:void(0);">Add New Account</a></div>
	
	<?php
	$accounts = $accobj->getAccounts();
	?>
	
	<div id="tableHolder">
		<table cellspacing="1" class="tablesorter">
			<thead>
				<tr>
					<th><?=_('Account Name');?></th>
					<th><?=_('Use Custom Limits');?></th>
					<th><?=_('Admin Panel');?></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th><?=_('Account Name');?></th>
					<th><?=_('Use Custom Limits');?></th>
					<th><?=_('Admin Panel');?></th>
				</tr>
			</tfoot>
			<tbody>
				<?php
				foreach($accounts as $accid => $accArr) {
					if($accArr['useAccLimits'] == 1) {
						$acclimitDisplay = 'Yes';
					} else {
						$acclimitDisplay = 'No';
					}
					$adminstring = '';
					$adminstring .= '<a href="javascript:void(0);" class="editAccount" id="editAcc_' . $accid . '"><img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_EditApplication.png" width="16" height="15" /></a>&nbsp;&nbsp;' . "\n";
					echo '<tr>' . "\n";
					echo '<td>' . $accArr['accountname'] . '</td>' . "\n";
					echo '<td>'. $acclimitDisplay . '</td>' . "\n";
					echo '<td><div class="makerelative">' . $adminstring . '</div></td>' . "\n";
					echo '</tr>' . "\n";
				}
				?>
			</tbody>
		</table>
	</div>
	
	<div class="cleaner"></div>
</div>

