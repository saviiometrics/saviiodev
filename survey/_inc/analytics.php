<?php
    $adminobj = new Admin($userobj->uid);

    $adminobj->runTracker('Viewed Page','Analytics', 0, 'PAGEVIEW');

?>
<!-- Overlay Box for Adding / Editing Companies -->
<input type="hidden" name="ETMapid" id="ETMapid" />
<input type="hidden" name="surveyDomain" id="surveyDomain" value="/" />
<input type="hidden" name="currLeft" id="currLeft" />
<input type="hidden" name="currRight" id="currRight" />
<input type="hidden" name="accpath" id="accpath" value="<?=ACCOUNT_PATH;?>" />
<input type="hidden" name="locale" id="locale" value="<?=$locale;?>"/>
<input type="hidden" name="populateError" id="populateError" value="<?=_("Sorry you must always populate the left hand box");?>"/>

 <div id="maincontent">
 <div id="titlefloat">
 <div id="headersq"><img src="<?=ACCOUNT_PATH;?>_images/_header/compare_header.jpg" width="89" height="74" alt="compare header" /></div>
 <div id="titletextfloat"><h2 class="MWxBox"><?=_("MAP Analytics");?></h2>
 <?=_("Compare and rank multiple MAPs against each other.");?>
 </div>
 </div>
 <div class="cleaner"></div>
 <div id="OboxTop">
 <div id="leftMAPheader"><img src="<?=ACCOUNT_PATH;?>_images/_icons/mymap_icon.png" alt="MAP Icon" /></div>
 <div id="rightMAPheader"><img src="<?=ACCOUNT_PATH;?>_images/_icons/talentmap_icon.png" alt="Talent MAP Icon" /></div>
 <a href="javascript:void(0);" id="toggleObox"><img src="<?=ACCOUNT_PATH;?>_images/_map/optionsbox_collapse.jpg" id="oboxswitch" alt="toggle hide" /></a>
 </div>
 <div id="OboxContent">
 	
 	 <div id="MAPSelectTextL"><?=_("Select MAP Type");?>
		 <div id="leftMAPselectABS" class="dropdown">
		  <select id="leftMAPselect" name="leftMAPselect">
				<option value="MAP">MAP</option>
				<option value="TeamMAP">teamMAP</option>
				<option value="TalentMAP" selected="selected">talentMAP</option>
				<option value="idealMAP">idealMAP</option>
			</select>
		 </div>
		</div>
		
		<div id="MAPSelectTextR"><?=_("Select MAP Type");?>
		 <div id="rightMAPselectABS" class="dropdown">
			 <select id="rightMAPselect" name="rightMAPselect">
					<option value="MAP" selected="selected">MAP</option>
					<option value="TeamMAP">teamMAP</option>
					<option value="TalentMAP">talentMAP</option>
					<option value="idealMAP">idealMAP</option>							
			 </select>
		 </div>
		</div>
		<div class="cleaner"></div>
		
		<div id="MAPTextHeaderL"><?= _("Select a MAP"); ?><div id="addMAPBoxLeft"><a href="javascript:void(0);" class="addmap"><?=_("Select");?> (+)</a></div></div>
		<div id="MAPTextHeaderR"><?= _("Compare With"); ?><div id="addMAPBoxRight"><a href="javascript:void(0);" class="addmap"><?=_("Select");?> (+)</a></div></div>

		<div id="leftsortbox">
			<div class="multiselect">
				<ul></ul>
			</div>
			<div id="clearAllLeft"><a class="clearmaps" href="javascript:void(0);" title="Clear Selection"><?=_("Clear All");?> (-)</a></div>
		</div>
		
		<div id="rightsortbox">
			<div class="multiselect">
				<ul></ul>
			</div>
			<div id="clearAllRight"><a href="javascript:void(0);" class="clearmaps" title="Clear Selection"><?=_("Clear All");?> (-)</a></div>
		</div>
		
		<div id="compareBox">
			<div id="leftAvatarBox"></div>
			<div id="vsAvatarBox">Vs</div>
			<div id="rightAvatarBox"></div>
		</div>
		
		<div id="rankByBox">
			<strong>Rank By</strong>
			<select id="rankSort" name="rankSort">
				<option value="TOTAL" selected="selected"><?=_("Total Score");?></option>
				<option value="REL"><?=_("Relationships");?></option>
				<option value="TAP"><?=_("Thinking & Planning");?></option>
				<option value="MAK"><?=_("Making Decisions");?></option>
				<option value="GTD"><?=_("Getting Things Done");?></option>
			</select>
		</div>
 
 </div>
 
 <div id="Oboxbtm"></div>
 <div id="OboxGen"><div id="genMAPbox"><a href="javascript:void(0);" id="generatemap"><?=_("Generate");?></a></div></div>
 <div id="rankholder"></div>
 
 <div id="MainContentHolder">
 <!--MAP & INFO-->
 <div id="mapinfofloat">
 <div id="leftresultbox">
 <div id="leftboxtop"></div>
 <div id="leftboxmid">
 <div id="resultsinfo"><h3 class="MWxLeft"><?=_('Results Information');?></h3></div><div id="resultshelp"><img src="<?=ACCOUNT_PATH;?>_images/_map/float_active.jpg" alt="help" id="changeFloat" style="cursor:pointer;" alt="Start / Stop Float" /></div>
 <div class="cleaner"></div>
 <div id="scoreicon"><img src="<?=ACCOUNT_PATH;?>_images/_map/single_blue.png" alt="left icon" /></div>
 <div id="leftresultsname"></div>
 <div id="vsbox"><h3 class="MWxLeft">Vs</h3></div>
 <div id="rightresultsname"></div>
 <div id="benchicon"><img src="<?=ACCOUNT_PATH;?>_images/_map/cross_grey.png" alt="right icon" /></div>
 <br />
	<div id="legendPad">
		<div id="matchKeyHolder">
			<h3 class="MWxLeft"><?=_("Match Colour Key");?></h3>
			<div class="legendExcel">90-100&#37;</div>
			<div class="legendGood">70-90&#37;</div>
			<div class="cleaner"></div>
			<div class="legendAverage">50-70&#37;</div>
			<div class="legendPoor">0-50&#37;</div>
			<div class="cleaner"></div>
		</div>
		<div id="leftmapswitch"></div>
		<br />
		<div id="rightmapswitch"></div>
 </div>
 <div id="exportreport"><h3 class="MWxLeft"><?=_("Export Report");?></h3>
 <div style="font-size:11px;font-weight:bold; margin:0px 0px 10px 0px;"><input type="checkbox" name="incREP" id="incREP" checked="checked" />&nbsp;&nbsp;<?=_("Include Technical Summary?");?></div>
 <div style="font-size:11px;font-weight:bold; margin:0px 0px 10px 0px;">
	 <?=_("PDF Format");?>: <select id="pdfOrientation">
		 <option value="portrait"><?=_("Portrait");?></option>
		 <option value="landscape"><?=_("Landscape");?></option>
	 </select>
 </div>
 <div class="pdflink"><a href="<?=$userobj->surveyURL;?>pdfgen.php?pagetype=detailed&incR=yes" target="_blank" id="detailedrep"><?=_("Export this Report (Detailed)");?></a></div>
 <div class="pdflink"><a href="<?=$userobj->surveyURL;?>pdfgen.php?incR=yes" target="_blank" id="summaryrep"><?=_("Export this Report (Summary)");?></a></div>
 <div class="pdflink rankReportBox"><a href="<?=$userobj->surveyURL;?>pdfgenrank.php" target="_blank" id="rankreport"><?=_("Export Rank Report");?></a> *</div>
 <div class="pdflink"><a href="<?=$userobj->surveyURL;?>pdfgen_quickview.php" target="_blank" id="quickview"><?=_("Quick View Report");?></a></div>
 <div class="cleaner"></div>
 <div style="font-size:11px;font-weight:bold; margin-top:6px;">* <?=_("Rank report is only available in landscape");?></div>
 </div>
 <div class="cleaner"></div>
 </div>
 <div id="leftboxbtm"></div> 
 </div>
 </div>
 <div id="mapholder">
 <div id="MH_Top"></div>
 <div id="MH_Content">
 <div id="sliders">
<p><strong><?=_("Please Generate a MAP Comparison using the Tools above.");?></strong></p>
 </div>
 </div>
 <div id="MH_Btm"></div>
 </div>
 </div>
 </div>
