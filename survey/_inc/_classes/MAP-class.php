<?php

class MAP {
		public $mapid;
		public $uid;
		public $currentq;
		public $qidstring;
		public $timebegin;
		public $timefinish;
		public $timeissue;
		public $active;
		public $surveyid;
		public $score;
		public $timer;
		public $qid;
		public $factor_group;

		public function __construct(){
			$this->MAPtable = DB_PREFIX . '_maps';
			$this->iMAPtable = DB_PREFIX . '_idealmaps';
			$this->iMAPsurvey = DB_PREFIX . '_imapsurvey';
			$this->TMAPtable = DB_PREFIX . '_tmaps';
			$this->surveytable = DB_PREFIX . '_survey';		
			$this->usertable = DB_PREFIX . '_users';
			$this->companiestable = DB_PREFIX . '_companies';
			$this->tmapaccesstable = DB_PREFIX . '_tmapaccess';
			$this->compshare = DB_PREFIX . '_sharecomp';
			$this->compshareaccess = DB_PREFIX . '_sharecompaccess';
			
			//non customisable
			$this->clustertable = 'system_clusters';
			$this->factortable = 'system_factors';
			$this->facDescTable = 'system_factordesc';
		}

		public function addScore($mapid, $qid, $score, $timer, $factorgroup, $polarize, $currentq, $timefinish = 0) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}

			//alter score if polarize is true
			if($polarize == 1) {
				$score = (10 - $score);
			}
			
			$sql = "INSERT INTO $this->surveytable (mapid, qid, score, timer, factor_group) VALUES ('$mapid', $qid, '$score', '$timer', '$factorgroup')";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " MAP Class " . __LINE__);
			
			if($timefinish == 0) {
				$this->updateCurrentQ($mapid, $currentq);
			} else {
				$timenow = time();
				$this->updateCurrentQ($mapid, $currentq, $timenow);
			}
				
			RETURN TRUE;

			$db->mysqlclose();
		}//end addScore

		
		public function checkCurrentMAP($uid) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
		
			$sql = "SELECT mapid FROM $this->MAPtable WHERE uid = '$uid' AND active = 1 AND archive = 0";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " MAP Class " . __LINE__);
			
			$num_rows = mysql_num_rows($result);
			if($num_rows == 0) {
				return FALSE;
			} else {
				return TRUE;
			}
			
			$db->mysqlclose();
			
		}//end checkCurrentMAP
		
		
		public function setStart($mapid, $timenow) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			
			$sql = "UPDATE $this->MAPtable SET timebegin = '$timenow' WHERE mapid = '$mapid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " MAP Class " . __LINE__);

			RETURN true;
			
			$db->mysqlclose();
			
		}//end updateCurrentQ
		
		public function getMAP($uid) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
		
			$sql = "SELECT * FROM $this->MAPtable WHERE uid = '$uid' AND active = 1 AND archive = 0";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " MAP Class " . __LINE__);
			
			$row = mysql_fetch_object($result);
			$this->mapid = $row->mapid;
			$this->uid = $uid;
			$this->currentq = $row->currentq;
			$this->qidstring = $row->qidstring;
			$this->timeissue = $row->timeissue;
			$this->timebegin = $row->timebegin;
			$this->timefinish = $row->timefinish;
			
			$db->mysqlclose();
			
		}//end getMAP
		
		public function getMAPvid($mapid) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}

			$sql = "SELECT * FROM $this->MAPtable mt LEFT JOIN $this->usertable ut ON mt.uid=ut.uid WHERE mt.mapid = '$mapid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " MAP Class " . __LINE__);
			
			//echo $sql;
			$row = mysql_fetch_object($result);
			return $row;
			
			$db->mysqlclose();
			
		}//end getMAP
		
		public function getiMAPvid($imapid) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}

			$sql = "SELECT * FROM $this->iMAPtable WHERE imapid = '$imapid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " MAP Class " . __LINE__);
			
			//echo $sql;
			$row = mysql_fetch_object($result);
			return $row;
			
			$db->mysqlclose();
			
		}//end getMAP
		
		public function addMAP($uid, $qidstring, $companyid, $mainsite = 0, $accid=0) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}

			//when adding a user you want to update their accID NOT your own. So if you specifiy the accid here it updates that one not the admin who is logged in!
			$admiXobj = new Admin($_SESSION['uid']);
			if($accid == 0) {
				$admiXobj->incrementTotals('map');
			} else {
				$admiXobj->incrementTotals('map', $accid);
			}

			$timenow = time();
			//this basically sets timeissue and timebegin if added from the main page.. or just sets timeissue if from the admin site
			if($mainsite == 0) {
				$sql = "INSERT INTO $this->MAPtable (uid, companyid, currentq, qidstring, timeissue, active, archive) VALUES('$uid', '$companyid', 1, '$qidstring', '$timenow', 1, 0)";
			} else {
				$sql = "INSERT INTO $this->MAPtable (uid, companyid, currentq, qidstring, timeissue, timebegin, active, archive) VALUES('$uid', '$companyid', 1, '$qidstring', '$timenow', '$timenow', 1, 0)";
			}
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " MAP Class " . __LINE__);
	
			RETURN TRUE;

			$db->mysqlclose();
		}//end addMAP
		
				
		public function updateCurrentQ($mapid, $currentq, $endtime = 0) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			
			//the reason for + 2 is because array is from 0 so it minuses 1 at the start.. so to get the actual q.. u have to add one anyway.. to get actual q + 1 u have to add 2!
			$currentq = $currentq + 2;
			if($endtime == 0) {
				$sql = "UPDATE $this->MAPtable SET currentq = '$currentq' WHERE mapid = '$mapid'";
			} else {
				$sql = "UPDATE $this->MAPtable SET timefinish = '$endtime', active = 0 WHERE mapid = '$mapid'";
			}
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " MAP Class " . __LINE__);

			RETURN true;
			
			$db->mysqlclose();
			
		}//end updateCurrentQ
		
		
		public function getfids() {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
		
			$sql = "SELECT * FROM $this->factortable";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " MAP Class " . __LINE__);
				
			$i = 0;
			$fids = array();
			while( $row = mysql_fetch_object( $result ) ) {
				//multidimensional array containing all the information for each factor
				$fids[$row->fid] = array('factor_text' => $row->factor_text, 'clusterid' => $row->clusterid, 'factor_align' => $row->factor_align, 'factor_group' => $row->factor_group);
			}//end while
			
			RETURN $fids;
			
			$db->mysqlclose();
			
		}//end getfids
		
		
		public function getFactDescs() {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
		
			$sql = "SELECT * FROM $this->facDescTable";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " MAP Class " . __LINE__);
				
			$i = 0;
			$fdescs = array();
			while( $row = mysql_fetch_object( $result ) ) {
				//multidimensional array containing all the information for each factor
				$fdescs[$row->fid][$row->type][] = $row->content;
			}//end while
			
			RETURN $fdescs;
			
			$db->mysqlclose();
			
		}//end getFactDescs
		
		public function getMAPList($companyid, $access) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			
		  $userXmoj = new Admin($_SESSION['uid']);
		  $superAccess =  $userXmoj->getLevelAccess('admin');
			if($superAccess['GODADMIN'] == 'TRUE') {
				$sql = "SELECT sm.uid, ut.firstname, ut.surname, ut.email, sm.companyid, ut.avatarURL, MAX(sm.mapid) as mapid, sm.timefinish FROM $this->MAPtable sm, $this->usertable ut WHERE sm.uid=ut.uid AND sm.active=0 GROUP BY sm.uid ORDER BY ut.firstname, ut.surname ASC";
			} else {
			  switch($access['VIEW']) {
					case 'ALL':
					$sql = "SELECT sm.uid, ut.firstname, ut.surname, ut.email, sm.companyid, ut.avatarURL, MAX(sm.mapid) as mapid, sm.timefinish FROM $this->MAPtable sm, $this->usertable ut WHERE sm.uid=ut.uid AND sm.active=0 AND ut.accid = '$_SESSION[accid]' GROUP BY sm.uid ORDER BY ut.firstname, ut.surname ASC";
					break;
					case 'CREATED':
					//$sql = "SELECT sm.uid, ut.firstname, ut.surname, sm.mapid FROM $this->MAPtable sm LEFT JOIN $this->usertable ut ON sm.uid=ut.uid LEFT JOIN $this->companiestable ct ON sm.companyid=ct.companyid, (SELECT max(timefinish) as maxtime FROM $this->MAPtable GROUP BY uid) mr WHERE sm.timefinish = mr.maxtime AND sm.active=0 AND (ut.adminid = $_SESSION[uid] OR ct.uid = $_SESSION[uid]) OR (ut.uid = $_SESSION[uid] AND sm.active=0) GROUP BY sm.uid"; //8 SECONDS LOLLOLZ
					$sql = "SELECT sm.uid, ut.firstname, ut.surname, ut.email, sm.companyid, ut.avatarURL, MAX(sm.mapid) as mapid, sm.timefinish FROM $this->MAPtable sm, $this->usertable ut, $this->companiestable ct WHERE sm.uid=ut.uid AND sm.companyid=ct.companyid AND sm.active=0 AND (ut.adminid = $_SESSION[uid] OR ct.uid = $_SESSION[uid]) OR (ut.uid = $_SESSION[uid] AND sm.active=0) AND ut.accid = '$_SESSION[accid]' GROUP BY sm.uid ORDER BY ut.firstname, ut.surname ASC"; //0.01 seconds.. pr0 m8z
					break;
					case 'MA':
					$sql = "SELECT sm.uid, ut.firstname, ut.surname, ut.email, sm.companyid, ut.avatarURL, MAX(sm.mapid) as mapid, sm.timefinish FROM $this->MAPtable sm, $this->usertable ut, $this->companiestable ct WHERE sm.uid=ut.uid AND sm.companyid=ct.companyid AND sm.active=0 AND ct.headid = '$companyid' AND ut.accid = '$_SESSION[accid]' GROUP BY sm.uid ORDER BY ut.firstname, ut.surname ASC";
					break;
					case 'SA':
					$sql = "SELECT sm.uid, ut.firstname, ut.surname, ut.email, sm.companyid, ut.avatarURL, MAX(sm.mapid) as mapid, sm.timefinish FROM $this->MAPtable sm, $this->usertable ut WHERE sm.uid=ut.uid AND sm.active=0 AND ut.companyid = '$companyid' AND ut.accid = '$_SESSION[accid]' GROUP BY sm.uid ORDER BY ut.firstname, ut.surname ASC";
					break;
				}
			}
			//echo $sql;

			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " MAP Class " . __LINE__);
				
			$i = 0;
			$maplist = array();
			while( $row = mysql_fetch_object( $result ) ) {
				$fullname = $row->firstname . ' ' . $row->surname;
				$maplist[$row->mapid] = array("uid" => $row->uid, "fullname" => $fullname, "email" => $row->email, "companyid"=>$row->companyid, 'avatarurl'=>$row->avatarURL);
			}//end while
			
			/************************************************************************
			*																																				*
			*														SHARED MAPS HERE														*
			*																																				*
			************************************************************************/
			
			//OK so this is for sharing. It checks to see whether the current user has any account shared with them. If they do, it check to see if they are allowed to see the MAPs in this account in the rights db table.
			//If they are allowed to view the MAPs then it grabs all the MAPs from that companyid and then adds it to the MAP list (code above here) ..if not then no MAPs are added.... genius.


			$sql = "SELECT companyid FROM $this->compshare WHERE uid = '$_SESSION[uid]'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " MAP Class " . __LINE__);

			$compids = array();
			$k = 0;
			while( $row = mysql_fetch_object( $result ) ) {
				$compids[$k] = $row->companyid;
				$k++;
			}
			
			//check to see whether any companyids have been returned!
			if(sizeof($compids) != 0) { 
				foreach($compids as $compid) {
					$sql = "SELECT allowaccess FROM $this->compshareaccess WHERE uid = '$_SESSION[uid]' AND companyid = '$compid' AND accesstype = 'VIEW' AND accessgroup = 'map'";
					$result = mysql_query( $sql, $db->mySQLConnection )
						or die(mysql_error() . " MAP Class " . __LINE__);
						
					$row = mysql_fetch_object( $result );
					
					//if they are allowed to view the MAPs add em to the list!
					if($row->allowaccess == 'TRUE') {
						$sql = "SELECT sm.uid, ut.firstname, ut.surname, MAX(sm.mapid) as mapid, sm.timefinish FROM $this->MAPtable sm, $this->usertable ut WHERE sm.uid=ut.uid AND sm.active=0 AND ut.companyid = '$compid' GROUP BY sm.uid";
						//echo $sql;
						$result = mysql_query( $sql, $db->mySQLConnection )
							or die(mysql_error() . " MAP Class " . __LINE__);
		
						while( $row = mysql_fetch_object( $result ) ) {
							$fullname = $row->firstname . ' ' . $row->surname;
							$maplist[$row->mapid] = array("uid" => $row->uid, "fullname" => $fullname);
						}//end while
						
					}
				}
			}

			RETURN $maplist;
			
			$db->mysqlclose();
			
		}//end getResponses
		
		
		public function getCompaniesFA($headid) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}

			$sql = "SELECT * from $this->companiestable WHERE headid = '$headid'";
				
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " MAP Class " . __LINE__);
				
			$i = 0;
			$companies = array();
			while( $row = mysql_fetch_object( $result ) ) {
				$companies[$i] = $row->companyid;
				$i = $i + 1;
			}//end while
			
			RETURN $companies;

			$db->mysqlclose();
		}//end getQids
		
		
		public function getTMAPList($type, $companyid, $access, $location = 'admin', $archive = 0) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
		
		  $uid = $_SESSION['uid'];
		  if($archive == 0) { 
		  	$typestr = 'AND mt.type = \'' . $type . '\' AND';
		  } else {
		  	$typestr = 'AND';
		  }
		  
		  $userXmoj = new Admin($_SESSION['uid']);
		  $superAccess =  $userXmoj->getLevelAccess('admin');
			if($superAccess['GODADMIN'] == 'TRUE') {
				$sql = "SELECT mt.tmapid, mt.companyid, mt.uid, mt.tmapdesc, mt.tmapname, mt.type, mt.mapids, mt.created, ut.uid, ut.firstname, ut.surname FROM $this->TMAPtable mt, $this->usertable ut WHERE mt.uid=ut.uid $typestr mt.archive = $archive ORDER BY mt.tmapname";
			} else {
				switch($access['VIEW']) {
					case 'ALL':
					$sql = "SELECT mt.tmapid, mt.companyid, mt.uid, mt.tmapdesc, mt.tmapname, mt.type, mt.mapids, mt.created, ut.uid, ut.firstname, ut.surname FROM $this->TMAPtable mt, $this->usertable ut WHERE mt.uid=ut.uid $typestr mt.archive = $archive AND ut.accid = '$_SESSION[accid]' ORDER BY mt.tmapname";
					break;
					case 'CREATED':
					$sql = "SELECT mt.tmapid, ct.uid as owneruid, mt.companyid, mt.uid, mt.tmapdesc, mt.tmapname, mt.type, mt.mapids, mt.created, ut.uid, ut.firstname, ut.surname, ct.headid FROM $this->TMAPtable mt, $this->usertable ut, $this->companiestable ct WHERE mt.uid=ut.uid AND mt.companyid=ct.companyid $typestr mt.archive = $archive AND (mt.uid = '$uid' OR ct.uid = '$uid') AND ut.accid = '$_SESSION[accid]' ORDER BY mt.tmapname";
					break;
					case 'MA':
					$sql = "SELECT mt.tmapid, ct.uid as owneruid, mt.companyid, mt.uid, mt.tmapdesc, mt.tmapname, mt.type, mt.mapids, mt.created, ut.uid, ut.firstname, ut.surname, ct.headid FROM $this->TMAPtable mt, $this->usertable ut, $this->companiestable ct WHERE mt.uid=ut.uid AND mt.companyid=ct.companyid $typestr mt.archive = $archive AND ct.headid = '$companyid' AND ut.accid = '$_SESSION[accid]' ORDER BY mt.tmapname";
					break;
					case 'SA':
					$sql = "SELECT mt.tmapid, ct.uid as owneruid, mt.companyid, mt.uid, mt.tmapdesc, mt.tmapname, mt.type, mt.mapids, mt.created, ut.uid, ut.firstname, ut.surname, ct.headid FROM $this->TMAPtable mt, $this->usertable ut WHERE mt.uid=ut.uid $typestr mt.archive = $archive AND mt.companyid = '$companyid' AND ut.accid = '$_SESSION[accid]' ORDER BY mt.tmapname";
					break;		
				}
			}

			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " MAP Class " . __LINE__);
			
			$i = 0;
			$tmaplist = array();
			while( $row = mysql_fetch_object( $result ) ) {
				if($access['VIEW'] != 'MA') {
					$compstring = 'companyid';
					$compvalue = $row->companyid;
				} else {
					$compstring = 'headid';
					$compvalue = $row->headid;
				}
				$tmaplist[$row->tmapid] = array('tmapname' => $row->tmapname, 'created' => $row->created, 'tmapdesc' => $row->tmapdesc, 'mapids' => $row->mapids, 'creator' => $row->firstname . ' ' . $row->surname, 'edit' => 'yes', 'type' => $row->type, 'uid' => $row->uid, 'companyid' => $row->companyid, 'headid' => $row->headid, "owneruid" => $row->owneruid);
			}//end while
			
			//only show the additional ones when not in the admin (read only) otherwise have to distinguish between shared or not.. could add another tag.. but no point at present!
			if($location != 'admin') {
				if($access['VIEW'] != 'ALL') {
					$sql = "SELECT ma.tmapid, ct.uid as owneruid, ma.identid, ma.accesstype, ma.accesslevel, mt.tmapid, mt.companyid, mt.uid, mt.tmapdesc, mt.tmapname, mt.type, mt.mapids, mt.created, ut.uid, ut.firstname, ut.surname, ct.headid FROM $this->tmapaccesstable ma, $this->TMAPtable mt, $this->usertable ut, $this->companiestable ct WHERE ma.tmapid=mt.tmapid AND mt.uid=ut.uid AND mt.companyid=ct.companyid AND (ma.identid = '$companyid' AND ma.accesstype = 'company' AND mt.type = '$type') OR (ma.identid = '$uid' AND ma.accesstype = 'user' AND mt.type = '$type') AND mt.archive = $archive AND ma.tmaptype = '$type' GROUP BY ma.tmapid";
					//echo $sql;
					$result = mysql_query( $sql, $db->mySQLConnection )
						or die(mysql_error() . " MAP Class " . __LINE__);

					while( $row = mysql_fetch_object( $result ) ) {
						$tmaplist[$row->tmapid] = array('tmapname' => $row->tmapname, 'created' => $row->created, 'tmapdesc' => $row->tmapdesc, 'mapids' => $row->mapids, 'creator' => $row->firstname . ' ' . $row->surname, 'edit' => 'no', 'type' => $row->type, 'uid' => $row->uid, 'companyid' => $row->companyid, 'headid' => $row->headid, "owneruid" => $row->owneruid);
					}//end while
				}
			}
			
			
			/************************************************************************
			*																																				*
			*												ACCOUNT SHARED MAPS HERE												*
			*																																				*
			************************************************************************/
			
			//same as for maps, but this time it just checks for viewing rights (here anyway) it checks for editing and adding and shit from the adminobj / website as that is controlled there this is just the list!
		
			$sql = "SELECT companyid FROM $this->compshare WHERE uid = '$_SESSION[uid]'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " MAP Class " . __LINE__);

			$compids = array();
			$k = 0;
			while( $row = mysql_fetch_object( $result ) ) {
				$compids[$k] = $row->companyid;
				$k++;
			}
			
			//check to see whether any companyids have been returned!
			if(sizeof($compids) != 0) { 
				foreach($compids as $compid) {
					$sql = "SELECT allowaccess FROM $this->compshareaccess WHERE uid = '$_SESSION[uid]' AND companyid = '$compid'  AND accesstype = 'VIEW' AND accessgroup = 'tmap'";
					$result = mysql_query( $sql, $db->mySQLConnection )
						or die(mysql_error() . " MAP Class " . __LINE__);
						
					$row = mysql_fetch_object( $result );
					//if they are allowed to view the MAPs add em to the list!
					if($row->allowaccess == 'TRUE') {		
						$sql = "SELECT mt.tmapid, mt.companyid, mt.uid, mt.tmapdesc, mt.tmapname, mt.type, mt.mapids, mt.created, ut.uid, ut.firstname, ut.surname FROM $this->TMAPtable mt, $this->usertable ut WHERE mt.uid=ut.uid $typestr mt.archive = $archive AND mt.companyid = '$compid' ORDER BY mt.tmapname";
						$result = mysql_query( $sql, $db->mySQLConnection )
							or die(mysql_error() . " MAP Class " . __LINE__);
	
						while( $row = mysql_fetch_object( $result ) ) {
							$tmaplist[$row->tmapid] = array('tmapname' => $row->tmapname, 'created' => $row->created, 'tmapdesc' => $row->tmapdesc, 'mapids' => $row->mapids, 'creator' => $row->firstname . ' ' . $row->surname, 'edit' => 'no', 'type' => $row->type, 'uid' => $row->uid, 'companyid' => $row->companyid, 'headid' => $row->headid);
						}//end while
					}
				}
			}
			
			RETURN $tmaplist;
			
			$db->mysqlclose();
			
		}//end getResponses
		
		
		public function addTMAP($type, $companyid, $uid, $tmapname, $tmapdesc, $mapids, $accessids, $accesslevel, $adminids) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			
			$gouser = 'no';
			$tmapname = $db->EscapeMe($tmapname);
			$tmapdesc = $db->EscapeMe($tmapdesc);
			$created = time();
			$sql = "INSERT INTO $this->TMAPtable (companyid, uid, tmapname, tmapdesc, type, mapids, created) VALUES('$companyid', $uid, '$tmapname', '$tmapdesc', '$type', '$mapids', '$created')";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " MAP Class " . __LINE__);
				
			$tmapid = mysql_insert_id();
			$_SESSION['tmapadd'] = $tmapid; 
			$sql = '';
			//this adds special access rights to the select companies based on the access level selected when creating the TMAP!
			if($accessids != 0 || $accessids != "") {
				$gouser = 'yes';
				$i = 1;
				$accessids = explode(',',$accessids);
				$acount = count($accessids);
				
				foreach($accessids as $ACid) {
					if($i < $acount) {
						$sql .= "('$tmapid', '$ACid', 'company', '$accesslevel', '$type'),";
					} else {
						$sql .= "('$tmapid', '$ACid', 'company', '$accesslevel', '$type')";
					}
					$i++;
				}	
			}
			if($adminids != 0 || $adminids != "") {
				$adminids = explode(',',$adminids);
				$usercount = count($adminids);
				$i = 1;
				foreach($adminids as $ACid) {
					$al = 5;
					if(empty($ACid)) {
						break;
					}
					if($gouser != 'yes') {
						if($i == 1) {
							$sql .= "('$tmapid', '$ACid', 'user', '$al', '$type')";
						} else {
							$sql .= ",('$tmapid', '$ACid', 'user', '$al', '$type')";
						}
					} else {
						$sql .= ",('$tmapid', '$ACid', 'user', '$al', '$type')";
					}
					$i++;
				}	
				//echo $sql;
			}
			
			//only run it if at least one array is set!
			if($accessids != 0 || $accessids != "" || $adminids != 0 || $adminids != "") {
				$sqlmain = "INSERT INTO $this->tmapaccesstable (tmapid, identid, accesstype, accesslevel, tmaptype) VALUES ";
				$sqlgo = $sqlmain . $sql;
				$result = mysql_query( $sqlgo, $db->mySQLConnection )
					or die(mysql_error() . " MAP Class " . __LINE__);
			}
			
			//increment totals here
			$admiXobj = new Admin($_SESSION['uid']);
			//type is talent or team!
			$admiXobj->incrementTotals($type);

			RETURN TRUE;
			
			$db->mysqlclose();
			
		}//end getResponses
		
		//this is just to check whether or not the person made the tmap etc.. so you can check whether they can edit it or not.. this is not used for checking viewing rights.
		public function checkTMAPAccess($tmapid, $companyid, $uid, $access) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			if($access['EDIT'] == 'FALSE') {
				RETURN FALSE;
			} else {
				switch($access['EDIT']) {
					case 'ALL':
					$sql = "SELECT tmapid FROM $this->TMAPtable WHERE tmapid = '$tmapid'";
					break;
					case 'CREATED':
					$sql = "SELECT tmapid, companyid FROM $this->TMAPtable WHERE tmapid = '$tmapid' AND uid = '$uid'";
					break;
					case 'MA':
					$sql = "SELECT mt.tmapid, mt.companyid, ct.headid FROM $this->TMAPtable mt, $this->companiestable ct WHERE mt.companyid=ct.companyid AND mt.tmapid = '$tmapid' AND ct.headid = '$companyid'";
					break;
					case 'SA':
					$sql = "SELECT tmapid, companyid FROM $this->TMAPtable WHERE tmapid = '$tmapid' AND companyid = '$companyid'";
					break;	
				}
				
				$result = mysql_query( $sql, $db->mySQLConnection )
					or die(mysql_error() . " MAP Class " . __LINE__);
					
				$count = mysql_num_rows($result);
	
				if($count == 1) {
					RETURN TRUE;
				} else {
					RETURN FALSE;
				}
			}
			
			$db->mysqlclose();
			
		}//end checkTMAPAccess
		
		public function updateTMAPAccess($tmapid, $compaccids, $adminaccids, $level, $type) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}

			if($compaccids != 'delete') {
				$newcompids = explode(',',$compaccids);
			} else {
				$newcompids = array();
			}
			if($adminaccids != 'delete') {
				$newuserids = explode(',',$adminaccids);
			} else {
				$newuserids = array();
			}
			
			$currcompids = $this->getTMAPAccess($tmapid, 'company');
			$curruserids = $this->getTMAPAccess($tmapid, 'user');
			
			/*************************** COMPANIES UPDATE HERE ***********************/
			if($compaccids != 0 || $compaccids == '') {
				foreach($newcompids as $compid) {
					if(!in_array($compid, array_keys($currcompids))) {
						$sql = "INSERT INTO $this->tmapaccesstable (tmapid, identid, accesstype, accesslevel, tmaptype) VALUES ('$tmapid', '$compid', 'company', '$level', '$type')";
						$result = mysql_query( $sql, $db->mySQLConnection )
							or die(mysql_error() . " MAP Class " . __LINE__);
					} else {
						$sql = "UPDATE $this->tmapaccesstable SET accesslevel = '$level', tmaptype = '$type' WHERE tmapid = '$tmapid' AND accesstype = 'company' AND identid = '$compid'";
						$result = mysql_query( $sql, $db->mySQLConnection )
							or die(mysql_error() . " MAP Class " . __LINE__);
					}
				}
	
				foreach($currcompids as $currcompid => $level) {
					if(empty($currcompids)) {
						$sql = "DELETE FROM $this->tmapaccesstable WHERE tmapid = '$tmapid' AND identid = '$currcompid' AND accesstype = 'company'";
						$result = mysql_query( $sql, $db->mySQLConnection )
							or die(mysql_error() . " MAP Class " . __LINE__);	
					} else {
						if(!in_array($currcompid, $newcompids)) {
							$sql = "DELETE FROM $this->tmapaccesstable WHERE tmapid = '$tmapid' AND identid = '$currcompid' AND accesstype = 'company'";
							$result = mysql_query( $sql, $db->mySQLConnection )
								or die(mysql_error() . " MAP Class " . __LINE__);
						}
					}
				}
			}
			
			/*************************** USERS /ADMINS UPDATE HERE ***********************/

			if($adminaccids != 0 || $adminaccids == '') {
				foreach($newuserids as $userid) {
					if(!in_array($userid, array_keys($curruserids))) {
						$sql = "INSERT INTO $this->tmapaccesstable (tmapid, identid, accesstype, accesslevel, tmaptype) VALUES ('$tmapid', '$userid', 'user', '5', '$type')";
						$result = mysql_query( $sql, $db->mySQLConnection )
							or die(mysql_error() . " MAP Class " . __LINE__);
					} else {
						$sql = "UPDATE $this->tmapaccesstable SET accesslevel = '5', tmaptype = '$type' WHERE tmapid = '$tmapid' AND accesstype = 'user' AND identid = '$userid'";
						$result = mysql_query( $sql, $db->mySQLConnection )
							or die(mysql_error() . " MAP Class " . __LINE__);
					}
				}
	
				foreach($curruserids as $curruserid => $level) {
					if(empty($adminaccids)) {
						$sql = "DELETE FROM $this->tmapaccesstable WHERE tmapid = '$tmapid' AND identid = '$curruserid' AND accesstype = 'user'";
						$result = mysql_query( $sql, $db->mySQLConnection )
							or die(mysql_error() . " MAP Class " . __LINE__);
					} else {
						if(!in_array($curruserid, $newuserids)) {
							$sql = "DELETE FROM $this->tmapaccesstable WHERE tmapid = '$tmapid' AND identid = '$curruserid' AND accesstype = 'user'";
							$result = mysql_query( $sql, $db->mySQLConnection )
								or die(mysql_error() . " MAP Class " . __LINE__);
						}
					}
				}
			}
			
			return TRUE;
			
			$db->mysqlclose();
			
		}//end updateTMAPAccess
		
		public function getFullMAPListvUID($uid) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}

			$sql = "SELECT ut.firstname, ut.surname, mt.mapid, mt.uid, mt.timefinish, mt.timeissue, mt.timebegin FROM $this->MAPtable mt, $this->usertable ut WHERE mt.uid=ut.uid AND mt.uid = '$uid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " MAP Class " . __LINE__);
			
			//echo $sql; 
			$i = 0;
			$maplist = array();
			while( $row = mysql_fetch_object( $result ) ) {
				$fullname = $row->firstname . ' ' . $row->surname;
				$maplist[$row->mapid] = array("uid" => $row->uid, "fullname" => $fullname, "finish" => $row->timefinish, 'create'=>$row->timeissue, 'begin'=>$row->timebegin);
			}//end while
			
			RETURN $maplist;
			
			$db->mysqlclose();
		}
		
		public function getMAPListvUID($uid) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}

			$sql = "SELECT ut.firstname, ut.surname, mt.mapid, mt.uid, mt.timefinish FROM $this->MAPtable mt, $this->usertable ut WHERE mt.uid=ut.uid AND mt.uid = '$uid' AND mt.active=0";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " MAP Class " . __LINE__);
			
			//echo $sql; 
			$i = 0;
			$maplist = array();
			while( $row = mysql_fetch_object( $result ) ) {
				$fullname = $row->firstname . ' ' . $row->surname;
				$maplist[$row->mapid] = array("uid" => $row->uid, "fullname" => $fullname, "finish" => $row->timefinish);
			}//end while
			
			RETURN $maplist;
			
			$db->mysqlclose();
			
		}//end getResponses
		
		
		public function getNamesVMAPids($mapids) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			
			$sql = "SELECT ut.firstname, ut.surname, mt.mapid, ut.uid FROM $this->usertable ut, $this->MAPtable mt WHERE ut.uid=mt.uid AND mt.mapid IN ($mapids) GROUP BY ut.uid ORDER BY mt.mapid ASC";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " MAP Class " . __LINE__);
			
			$i = 0;
			$userlist = array();
			while( $row = mysql_fetch_object( $result ) ) {
				$fullname = $row->firstname . ' ' . $row->surname;
				$userlist[$row->uid] = array("fullname" => $fullname);
			}//end while
			
			RETURN $userlist;
			
			$db->mysqlclose();
			
		}//end getResponses
		
		
		public function getmapidsVTMAP($tmapid) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
		
			$sql = "SELECT mapids FROM $this->TMAPtable WHERE tmapid = '$tmapid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " MAP Class " . __LINE__);
				
			$i = 0;

			$row = mysql_fetch_object( $result );
	
			RETURN $row->mapids;
			
			$db->mysqlclose();
			
		}//end getmapidsVTMAP
		
		
		/*****************************************************
		***************** GET UPDATED TMAPIDS ****************
		*****************************************************/
		
		public function getUpdatedTMAPids($oldtmapids) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
		
			$sql = "SELECT uid FROM $this->MAPtable WHERE mapid IN ($oldtmapids) GROUP BY uid";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " MAP Class " . __LINE__);
				
			$i = 0;
			$newuids = array();
			while( $row = mysql_fetch_object( $result ) ) {
				$newuids[$i] = $row->uid;
				$i++;
			}//end while
			
			$i = 0;
			$newmapids = array();
			foreach($newuids as $uid) {
				$sql = "SELECT max(mt.mapid) as maxid FROM $this->MAPtable mt WHERE uid = '$uid' AND mt.active=0";
				$result = mysql_query( $sql, $db->mySQLConnection )
					or die(mysql_error() . " MAP Class " . __LINE__);
					
				$row = mysql_fetch_object($result);
				$newmapids[$i] = $row->maxid;
				$i++;
			}

			RETURN $newmapids;
			
			$db->mysqlclose();
			
		}//end getmapidsVTMAP
		
		public function getTMAP($tmapid) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
		
			$sql = "SELECT * FROM $this->TMAPtable WHERE tmapid = '$tmapid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " MAP Class " . __LINE__);
				
			$row = mysql_fetch_object( $result );
	
			RETURN $row;
			
			$db->mysqlclose();
			
		}//end getTMAP
		
		
		public function deleteTMAP($tmapid, $tmapType) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			
			//delete access for these as no longer needed
			$this->updateTMAPAccess($tmapid, 'delete', 'delete', 1, 'type');
			
			$sql = "DELETE FROM $this->TMAPtable WHERE tmapid = '$tmapid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " MAP Class " . __LINE__);
				
			$admiXobj = new Admin($_SESSION['uid']);
			//0 is for accid i.e. uses user accid and 1 means to decrease total not increase!
			$admiXobj->incrementTotals($tmapType, 0, 1);

			RETURN TRUE;
			
			$db->mysqlclose();
			
		}//end deleteTMAP
		
		public function archiveTMAP($tmapid) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
		
			$sql = "UPDATE $this->TMAPtable SET archive = 1 WHERE tmapid = '$tmapid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " MAP Class " . __LINE__);
	
			RETURN TRUE;
			
			$db->mysqlclose();
			
		}//end archiveTMAP
		
		
	 public function unarchiveTMAP($tmapid) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
		
			$sql = "UPDATE $this->TMAPtable SET archive = 0 WHERE tmapid = '$tmapid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " MAP Class " . __LINE__);
	
			RETURN TRUE;
			
			$db->mysqlclose();
			
		}//end unarchiveTMAP
		
		
		public function updateTMAP($tmapid, $newmapids, $tmapname='', $tmapdesc='') {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}

			if($tmapname == '' || $tmapdesc == '') {
				$sql = "UPDATE $this->TMAPtable SET mapids = '$newmapids' WHERE tmapid = '$tmapid'";
				$result = mysql_query( $sql, $db->mySQLConnection )
					or die(mysql_error() . " MAP Class " . __LINE__);
			} else {
				$sql = "UPDATE $this->TMAPtable SET mapids = '$newmapids', tmapname = '$tmapname', tmapdesc = '$tmapdesc' WHERE tmapid = '$tmapid'";
				$result = mysql_query( $sql, $db->mySQLConnection )
					or die(mysql_error() . " MAP Class " . __LINE__);
			}
			
			return TRUE;
			
			$db->mysqlclose();
			
		}//end getTMAP
		
		
		public function getTMAPAccess($tmapid, $type, $identid='') {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
		
			if($type == 'company') {
				$sql = "SELECT ma.tmapid, ma.identid, ma.accesstype, ma.accesslevel FROM $this->tmapaccesstable ma WHERE ma.tmapid = '$tmapid' AND ma.accesstype = '$type'";
			} else if ($type == 'user') {
				$sql = "SELECT ma.tmapid, ma.identid, ma.accesstype, ma.accesslevel FROM $this->tmapaccesstable ma WHERE ma.tmapid = '$tmapid' AND ma.accesstype = '$type'";
			}
			
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " MAP Class " . __LINE__);
			
			$tmaccess = array();
			while( $row = mysql_fetch_object( $result ) ) {
				$tmaccess[$row->identid] = $row->accesslevel;
			}//end while
	
			RETURN $tmaccess;
			
			$db->mysqlclose();
			
		}//end getTMAP

		public function getClusters() {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
		
			$sql = "SELECT * FROM $this->clustertable ORDER BY corder";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " MAP Class " . __LINE__);
				
			$i = 0;
			$cids = array();
			while( $row = mysql_fetch_object( $result ) ) {
				//multidimensional array containing all the information for each factor
				$cids[$row->clusterid] = $row->clustername;
			}//end while
			
			RETURN $cids;
			
			$db->mysqlclose();
			
		}//end getResponses
		
		
		public function getScores($mapids) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			$sql = "SELECT AVG($this->surveytable.score) as score, STDDEV($this->surveytable.score) as deviation, AVG($this->surveytable.timer) as timer, $this->surveytable.factor_group, $this->factortable.clusterid, $this->surveytable.qid FROM $this->surveytable, $this->factortable WHERE $this->surveytable.factor_group=$this->factortable.factor_group AND $this->surveytable.mapid IN ($mapids) GROUP BY $this->surveytable.qid ORDER BY $this->surveytable.qid ASC";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " MAP Class " . __LINE__);
			
			//echo $sql;
			
			$i = 0;
			$scores = array();
			while( $row = mysql_fetch_object( $result ) ) {
				//multidimensional array containing all the information for each factor
				$scores[$row->qid] = array('score' => $row->score, 'timer' => $row->timer, 'clusterid' => $row->clusterid, 'factorgroup' => $row->factor_group, 'deviation' => $row->deviation);
			}//end while
			
			RETURN $scores;
	
		}//end getMAP
	
		
		public function getFactorGroups() {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
		
			$sql = "SELECT factor_group, clusterid FROM $this->factortable GROUP BY factor_group";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " MAP Class " . __LINE__);
			
			$i = 0;
			$fgs = array();
			while( $row = mysql_fetch_object( $result ) ) {
				$fgs[$row->factor_group] = $row->clusterid;
			}//end while
			
			RETURN $fgs;
			
		}//end getMAP
		
		
		/**************************************** IMAPS ***********************************/
		
		public function addiMAP($resultsObj, $companyid, $uid, $imapname, $parts) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			
			$imapname = $db->EscapeMe($imapname);
			$parts = $db->EscapeMe($parts);
			$created = time();
			
			$sql = "INSERT INTO $this->iMAPtable (companyid, uid, name, time_created, time_edited, participants) VALUES('$companyid', $uid, '$imapname', '$created', '$created', '$parts')";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " MAP Class " . __LINE__);
			
			$imapid = mysql_insert_id();
			$_SESSION['curriMAP'] = $imapid;
			
			$sql = "INSERT INTO $this->iMAPsurvey (imapid, qid, score, factor_group) VALUES";
			foreach($resultsObj as $qid => $resultsarr) {
				//for some reason in the JS in idealMAPs it kept adding a 0 key value as "undefined" to the array (i.e no qid with id 0)... in the end i gave up and just skipped it if qid was 0 :)
				if($qid == 0) {
					continue;
				}
				$score = $resultsarr[0];
				$fg = $resultsarr[1];
				$sql .= "('$imapid', $qid, '$score', '$fg'),";
			}
			
			//remove erroneous comma
			$sql = substr($sql, 0, -1);
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " MAP Class " . __LINE__);
				
			RETURN TRUE;
			
			$db->mysqlclose();
			
		}//end addiMAP
		
		public function deleteiMAP($imapid) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
		
			$sql = "DELETE FROM $this->iMAPtable WHERE imapid = '$imapid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " MAP Class " . __LINE__);
				
			$sql = "DELETE FROM $this->iMAPsurvey WHERE imapid = '$imapid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " MAP Class " . __LINE__);
				
			$admiXobj = new Admin($_SESSION['uid']);
			//0 is for accid i.e. uses user accid and 1 means to decrease total not increase!
			$admiXobj->incrementTotals('ideal', 0, 1);
			
			RETURN TRUE;
			
		}//end deleteiMAP
		
		public function editiMAP($imapid, $imapname, $participants) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			
			$imapname = $db->EscapeMe($imapname);
			$participants = $db->EscapeMe($participants);
		
			$sql = "UPDATE $this->iMAPtable SET name = '$imapname', participants = '$participants' WHERE imapid = '$imapid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " MAP Class " . __LINE__);

			RETURN TRUE;
			
		}//end deleteiMAP
		
		public function getiMAP($imapid) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
		
			$sql = "SELECT name, participants FROM $this->iMAPtable WHERE imapid = '$imapid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " MAP Class " . __LINE__);

			$row = mysql_fetch_object( $result );
			$imaparr = array("imapname" => $row->name, "participants" => $row->participants);
			
			RETURN $imaparr;
			
		}//end deleteiMAP
			
		public function getidealMAPList($companyid, $access) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}

		  $userXmoj = new Admin($_SESSION['uid']);
		  $superAccess =  $userXmoj->getLevelAccess('admin');
			if($superAccess['GODADMIN'] == 'TRUE') {
				$sql = "SELECT im.uid, ct.uid as owneruid, ut.firstname, ut.surname, im.imapid, ct.headid, im.participants, im.time_created, im.name, im.companyid FROM $this->iMAPtable im, $this->usertable ut, $this->companiestable ct WHERE im.uid=ut.uid AND im.companyid=ct.companyid ORDER BY im.name ASC";
			} else {
				//all other SQL statements are based on the accid so that only that account can see it.
			  switch($access['VIEW']) {
					case 'ALL':
					$sql = "SELECT im.uid, ct.uid as owneruid, ut.firstname, ut.surname, im.imapid, ct.headid, im.participants, im.time_created, im.name, im.companyid FROM $this->iMAPtable im, $this->usertable ut, $this->companiestable ct WHERE im.uid=ut.uid AND im.companyid=ct.companyid AND ut.accid = '$_SESSION[accid]' ORDER BY im.name ASC";
					break;
					case 'CREATED':
					$sql = "SELECT im.uid, ct.uid as owneruid, ut.firstname, ut.surname, im.imapid, ct.headid, im.participants, im.time_created, im.name, im.companyid FROM $this->iMAPtable im, $this->usertable ut, $this->companiestable ct WHERE im.uid=ut.uid AND im.companyid=ct.companyid AND im.uid = $_SESSION[uid] OR ct.uid = $_SESSION[uid] AND ut.accid = '$_SESSION[accid]' ORDER BY im.name ASC";
					break;
					case 'MA':
					$sql = "SELECT im.uid, ct.uid as owneruid, ut.firstname, ut.surname, im.imapid, ct.headid, im.participants, im.time_created, im.name, im.companyid FROM $this->iMAPtable im, $this->usertable ut, $this->companiestable ct WHERE im.uid=ut.uid AND im.companyid=ct.companyid AND ct.headid = '$companyid' AND ut.accid = '$_SESSION[accid]' ORDER BY im.name ASC";
					break;
					case 'SA':
					$sql = "SELECT im.uid, ct.uid as owneruid, ut.firstname, ut.surname, im.imapid, ct.headid, im.participants, im.time_created, im.name, im.companyid FROM $this->iMAPtable im, $this->usertable ut, $this->companiestable ct WHERE im.uid=ut.uid AND im.companyid=ct.companyid AND im.companyid = '$companyid' AND ut.accid = '$_SESSION[accid]' ORDER BY im.name ASC";
					break;
				}
			}

			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " MAP Class " . __LINE__);
				
			$i = 0;
			$imaplist = array();
			while( $row = mysql_fetch_object( $result ) ) {
				$imaplist[$row->imapid] = array("imapname" => $row->name, "participants" => $row->participants, "created" => $row->time_created, "creator" => $row->firstname . ' ' . $row->surname, "headid" => $row->headid, "uid" => $row->uid, "companyid" => $row->companyid, "owneruid" => $row->owneruid);
			}//end while
			
			
			/************************************************************************
			*																																				*
			*														SHARED iMAPS HERE														*
			*																																				*
			************************************************************************/
			
			//OK so this is for sharing. It checks to see whether the current user has any account shared with them. If they do, it check to see if they are allowed to see the MAPs in this account in the rights db table.
			//If they are allowed to view the MAPs then it grabs all the MAPs from that companyid and then adds it to the MAP list (code above here) ..if not then no MAPs are added.... genius.


			$sql = "SELECT companyid FROM $this->compshare WHERE uid = '$_SESSION[uid]'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " MAP Class " . __LINE__);

			$compids = array();
			$k = 0;
			while( $row = mysql_fetch_object( $result ) ) {
				$compids[$k] = $row->companyid;
				$k++;
			}
			
			//check to see whether any companyids have been returned!
			if(sizeof($compids) != 0) { 
				foreach($compids as $compid) {
					$sql = "SELECT allowaccess FROM $this->compshareaccess WHERE uid = '$_SESSION[uid]' AND companyid = '$compid' AND accesstype = 'VIEW' AND accessgroup = 'imap'";
					$result = mysql_query( $sql, $db->mySQLConnection )
						or die(mysql_error() . " MAP Class " . __LINE__);
						
					$row = mysql_fetch_object( $result );
					
					//if they are allowed to view the MAPs add em to the list!
					if($row->allowaccess == 'TRUE') {
						//echo 'one here';
						$sql = "SELECT im.uid, ct.uid as owneruid, ut.firstname, ut.surname, im.imapid, ct.headid, im.participants, im.time_created, im.name, im.companyid FROM $this->iMAPtable im, $this->usertable ut, $this->companiestable ct WHERE im.uid=ut.uid AND im.companyid=ct.companyid AND im.companyid = '$compid'";
						//echo $sql;
						$result = mysql_query( $sql, $db->mySQLConnection )
							or die(mysql_error() . " MAP Class " . __LINE__);
		
						while( $row = mysql_fetch_object( $result ) ) {
							$imaplist[$row->imapid] = array("imapname" => $row->name, "participants" => $row->participants, "created" => $row->time_created, "creator" => $row->firstname . ' ' . $row->surname, "headid" => $row->headid, "uid" => $row->uid, "companyid" => $row->companyid, "owneruid" => $row->owneruid);
						}//end while
						
					}
				}
			}

			RETURN $imaplist;
			
			$db->mysqlclose();
			
		}//end getidealMAPList
		
		
		public function getiScores($imapid) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			$sql = "SELECT AVG($this->iMAPsurvey.score) as score, STDDEV($this->iMAPsurvey.score) as deviation, $this->iMAPsurvey.factor_group, $this->factortable.clusterid, $this->iMAPsurvey.qid FROM $this->iMAPsurvey, $this->factortable WHERE $this->iMAPsurvey.factor_group=$this->factortable.factor_group AND $this->iMAPsurvey.imapid IN ($imapid) GROUP BY $this->iMAPsurvey.qid ORDER BY $this->iMAPsurvey.qid ASC";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " MAP Class " . __LINE__);
			
			//echo $sql;
			
			$i = 0;
			$iscores = array();
			while( $row = mysql_fetch_object( $result ) ) {
				//multidimensional array containing all the information for each factor
				$iscores[$row->qid] = array('score' => $row->score, 'clusterid' => $row->clusterid, 'factorgroup' => $row->factor_group, 'deviation' => $row->deviation);
			}//end while
			
			RETURN $iscores;

		}//end getiScores
		
	
}//end class

?>
