
		<div id="innerContent">
	      <h1 class="MWxHeader">Let us introduce ourselves...</h1>
	      <div id="contentSiFR">
	      	<h2 class="MWxBox">We create insight technologies that help you attract, engage, develop and retain the best available people for your organisation<br /><br />Saviio is a software development and management consulting firm that provides insight into the unique combination of human factors that predict success. As part of our service we provide consulting, training, conferencing events and coaching to enable our customers to make the most of the tools we provide.</h2>
	      </div>
	     <div class="cleaner"></div>
	     <div class="contentPadTop">
	     		<span class="contentHeader">An Engaged Workforce</span>
		      <p>Saviio develops "Software as a Service" applications that help our clients improve employee engagement by providing insight into what matters most when people work together.</p>
					<p>Employee engagement is a cost-effective route to achieve business success. It is a very practical way of strengthening your business' productivity, innovation, attendance rate and voluntary turnover.</p>
      		<p><strong>An engaged workforce will make a big difference to your business:</strong><br />
      		They'll each take 5 fewer sick days a year and raise profitability by 12% as well as productivity by 18%. This same workforce will also generate 43% more revenue and, if you asked them, they'd tell you they were 87% less likely to leave.
					<br /><br /><strong><em>Source: MacLeod and Clarke (2009)</em></strong><br />
					"Engaging for Success: Enhancing performance through employee engagement"</p>
      	</div>
	     <div class="infoLeft">
	     	<span class="contentHeader">Our Purpose</span>
				<p>	Saviio's purpose is to help our clients succeed by providing insight into the unique combination of human factors that predict success</p>
				<p>We have two core beliefs:</p>
				<ul>
					<li>Each person has a unique combination of motivations, preferences and skills and has the potential to change any or all three.</li>
					<li>Each person should be able to explain to others their motivations, preferences and skills in order to succeed at work.</li>
				</ul>
				<p>Guided by our beliefs, we offer clients around the world software tools and custom training solutions to help build high-performance teams, develop their managers and leaders, and keep employees highly engaged.</p>
  		</div>	
  	</div>