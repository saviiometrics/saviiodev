<?php

class Auth extends User {
		public $authcode;
		public $authed;
		public $createdon;
		public $expirelag;

		public function __construct($authcode){
			$this->authcode = $authcode;
			//this is 7 days in case you were wondering :)
			//$this->expirelag = 604800;
			//28 days (later)
			$this->expirelag = 99999999999999999999999;
			parent::__construct();
		}
		
		public function getUserviaAC() {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			
			$authcode = $db->EscapeMe($this->authcode);
			$sql = "SELECT ut.uid, ut.email, ut.companyid, ut.authed, ut.authedSent, ut.resetRequest, ut.username FROM $this->usertable ut WHERE ut.userhash = '$authcode'";

			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error());
			
			//if(mysql_num_rows == 0) {
			//	$UserArr = array("error" => true);
			//} else {
				while( $row = mysql_fetch_object( $result ) ) {
					$UserArr = array("uid" => $row->uid, "email" => $row->email, "companyid" => $row->companyid, "authed" => $row->authed, "authedSent" => $row->authedSent, "username" => $row->username, "resetRequest" => $row->resetRequest, "error" => false);
				}//end while
			//}
		
			return $UserArr;
					
			$db->mysqlclose();
			
		}//end getUserviaAC
		
		public function authUser($authcode, $pass) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			$md5pass = md5($pass);
			//resetRequest is 0 as the auth/reset has taken place
			$sql = "UPDATE $this->usertable SET password = '$md5pass', authed = 1, resetRequest = 0 WHERE userhash = '$authcode'";

			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error());

			RETURN true;
			
			$db->mysqlclose();
			
		}//end authUser

}//end class

?>