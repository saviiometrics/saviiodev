	var usrTable;

	$(function() {
		$("#companyform").validationEngine('attach');
		$('.showAddAcc').live("click", function() {
		  $('#newCompanyUserAdd').slideToggle('slow', function() {
		  	if($('#newCompanyUserAdd').is(':visible')) {
					//
		  	} else {
		  		$("#companyform").validationEngine('hideAll');
		  	}
		  });
		});

		$(".savecompany").click(function(){
	  	var valid = $("#companyform").validationEngine('validate');
	  	if(valid == true) {
	  		var Cname = $("#companyN").val();
	  		var Cdesc = $("#companyD").val();
	  		var CCid = $("#CCountry").val();
	  		var comptype = $("#ctype").val();
	  		var xRTG = $("#xRTG").val();
	  		var CurrHeadid;
	  		if(comptype == "head") {
	  			CurrHeadid = "new";
	  		} else {
	  			//this is basically the value set if the person is a sub account admin. Because they can't add main accounts, it uses the headid of their current main account.
	  			if($("#xRTG").val() == 'Y') {
	  				CurrHeadid = $("#compheadid").val();
	  			} else {
	  				CurrHeadid = $("#headID").val();
	  			}
	  		};
				$.get("AjaXCompany.php", { headid:CurrHeadid, addCompany: 'yes', cname:Cname, cdesc:Cdesc, cid:CCid, type:comptype },
				  function(j){
					  $.jGrowl(j.message, {
							header:'<strong>System Message</strong>',
							closeTemplate:'<div class="jGrowlCloseMe">close (x)</div>',
							position:'bottom-right'
						});
					  $("#companyid").append('<option value="' + j.newcompid + '">' + Cname + '</option>');
					  //also add to the select main account list (if it's a main account they added) incase someone decides they want to add an associated account to their newly created main account :)
					  if(CurrHeadid == "new") {
					  	$("#compheadid").append('<option value="' + j.newcompid + '">' + Cname + '</option>');
					  }
					  //trigger click to hide the add account
					  $("#companyN").val('');
	  				$("#companyD").val('');
	  				//set UK as default
	  				$("#CCountry").val(242);
					  $('.showAddAcc').trigger("click");
					}, "json");
			}//end valid check
	  });

		var oCache = {
			iCacheLower: -1
		};

		function fnSetKey( aoData, sKey, mValue )
		{
			for ( var i=0, iLen=aoData.length ; i<iLen ; i++ )
			{
				if ( aoData[i].name == sKey )
				{
					aoData[i].value = mValue;
				}
			}
		};

		function fnGetKey( aoData, sKey )
		{
			for ( var i=0, iLen=aoData.length ; i<iLen ; i++ )
			{
				if ( aoData[i].name == sKey )
				{
					return aoData[i].value;
				}
			}
			return null;
		};

		function fnDataTablesPipeline ( sSource, aoData, fnCallback ) {
			var iPipe = 5; /* Ajust the pipe size */

			var bNeedServer = false;
			var sEcho = fnGetKey(aoData, "sEcho");
			var iRequestStart = fnGetKey(aoData, "iDisplayStart");
			var iRequestLength = fnGetKey(aoData, "iDisplayLength");
			var iRequestEnd = iRequestStart + iRequestLength;
			oCache.iDisplayStart = iRequestStart;

			/* outside pipeline? */
			if ( oCache.iCacheLower < 0 || iRequestStart < oCache.iCacheLower || iRequestEnd > oCache.iCacheUpper )
			{
				bNeedServer = true;
			}

			/* sorting etc changed? */
			if ( oCache.lastRequest && !bNeedServer )
			{
				for( var i=0, iLen=aoData.length ; i<iLen ; i++ )
				{
					if ( aoData[i].name != "iDisplayStart" && aoData[i].name != "iDisplayLength" && aoData[i].name != "sEcho" )
					{
						if ( aoData[i].value != oCache.lastRequest[i].value )
						{
							bNeedServer = true;
							break;
						}
					}
				}
			}

			/* Store the request for checking next time around */
			oCache.lastRequest = aoData.slice();

			if ( bNeedServer )
			{
				if ( iRequestStart < oCache.iCacheLower )
				{
					iRequestStart = iRequestStart - (iRequestLength*(iPipe-1));
					if ( iRequestStart < 0 )
					{
						iRequestStart = 0;
					}
				}

				oCache.iCacheLower = iRequestStart;
				oCache.iCacheUpper = iRequestStart + (iRequestLength * iPipe);
				oCache.iDisplayLength = fnGetKey( aoData, "iDisplayLength" );
				fnSetKey( aoData, "iDisplayStart", iRequestStart );
				fnSetKey( aoData, "iDisplayLength", iRequestLength*iPipe );

				$.getJSON( sSource, aoData, function (json) {
					/* Callback processing */
					oCache.lastJson = jQuery.extend(true, {}, json);

					if ( oCache.iCacheLower != oCache.iDisplayStart )
					{
						json.aaData.splice( 0, oCache.iDisplayStart-oCache.iCacheLower );
					}
					json.aaData.splice( oCache.iDisplayLength, json.aaData.length );

					fnCallback(json);
				} );
			}
			else
			{
				json = jQuery.extend(true, {}, oCache.lastJson);
				json.sEcho = sEcho; /* Update the echo for each response */
				json.aaData.splice( 0, iRequestStart-oCache.iCacheLower );
				json.aaData.splice( iRequestLength, json.aaData.length );
				fnCallback(json);
				return;
			}
		}

		jQuery.fn.dataTableExt.oApi.fnSetFilteringDelay = function ( oSettings, iDelay ) {
			/*
			 * Type:        Plugin for DataTables (www.datatables.net) JQuery plugin.
			 * Name:        dataTableExt.oApi.fnSetFilteringDelay
			 * Version:     2.2.1
			 * Description: Enables filtration delay for keeping the browser more
			 *              responsive while searching for a longer keyword.
			 * Inputs:      object:oSettings - dataTables settings object
			 *              integer:iDelay - delay in miliseconds
			 * Returns:     JQuery
			 * Usage:       $('#example').dataTable().fnSetFilteringDelay(250);
			 * Requires:	  DataTables 1.6.0+
			 *
			 * Author:      Zygimantas Berziunas (www.zygimantas.com) and Allan Jardine (v2)
			 * Created:     7/3/2009
			 * Language:    Javascript
			 * License:     GPL v2 or BSD 3 point style
			 * Contact:     zygimantas.berziunas /AT\ hotmail.com
			 */
			var
				_that = this,
				iDelay = (typeof iDelay == 'undefined') ? 250 : iDelay;

			this.each( function ( i ) {
				$.fn.dataTableExt.iApiIndex = i;
				var
					$this = this,
					oTimerId = null,
					sPreviousSearch = null,
					anControl = $( 'input', _that.fnSettings().aanFeatures.f );

					anControl.unbind( 'keyup' ).bind( 'keyup', function() {
					var $$this = $this;

					if (sPreviousSearch === null || sPreviousSearch != anControl.val()) {
						window.clearTimeout(oTimerId);
						sPreviousSearch = anControl.val();
						oTimerId = window.setTimeout(function() {
							$.fn.dataTableExt.iApiIndex = i;
							_that.fnFilter( anControl.val() );
						}, iDelay);
					}
				});

				return this;
			});
			return this;
		};

		var currLocale;
		if($('#currLocale').val() == '') {
			currLocale = 'en_EN';
		} else {
			currLocale = $('#currLocale').val();
		}

		usrTable = $('.tablesorter').dataTable({
			//"bProcessing": true,
			"bServerSide": true,
			"iDisplayStart": 0,
			"aaSorting": [],
			"oLanguage": {
				"sUrl": "dtblLocale/dataTables." + currLocale + ".txt"
			},
			"aoColumns": [
				null,
				null,
				{ "bSortable": false },
				{ "bSortable": false },
				null,
				{ "bSortable": false },
				{ "bSortable": false }
			],
			"iDisplayLength": 25,
			"sPaginationType": "full_numbers",
			"sAjaxSource": "AjaXSort.php",
			"sDom": '<"filter_container"fl><"top"ip>rt<"bottom"ip<"clear">',
			"bAutoWidth": false,
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
				if ( aData[5] == "Active" )	{
					$('td:eq(5)', nRow).addClass('useractive');
				}
				if ( aData[5] == "Authenticated" )	{
					$('td:eq(5)', nRow).addClass('userauthed');
				}
				if ( aData[5] == "Deactivated" )	{
					$('td:eq(5)', nRow).addClass('userdeactive');
				}
				if($('td:eq(3) > div', nRow).hasClass('nevertaken')) {
					$('td:eq(3)', nRow).addClass('nevertaken');
				}
				if($('td:eq(3) > div', nRow).hasClass('decaygreen')) {
					$('td:eq(3)', nRow).addClass('decaygreen');
				}
				if($('td:eq(3) > div', nRow).hasClass('decayorange')) {
					$('td:eq(3)', nRow).addClass('decayorange');
				}
				if($('td:eq(3) > div', nRow).hasClass('decayred')) {
					$('td:eq(3)', nRow).addClass('decayred');
				}
				return nRow;
			},
			"fnServerData": fnDataTablesPipeline,
		  "fnInitComplete": function() {
 				usrTable.fnSetFilteringDelay(1400);
			}
		});

		$("#ctype").change(function() {
			if($(this).val() == 'sub') {
				$(".compheadlist").show();
			} else {
				$(".compheadlist").hide();
			};
		});

		$(".deacUser, .acUser").live("click", function(){
			if($(this).hasClass('deacUser')) {
				var userid = this.id.substr(11);
				var active = 0;
				var newclass = 'red';
				var newcopy = 'Deactivated';
			} else if($(this).hasClass('acUser')) {
				var userid = this.id.substr(9);
				var active = 1;
				var newclass = 'green';
				var newcopy = 'Re-Activated';
			}
			var clicked = $(this);
			$(this).closest('tr').removeClass('odd even').addClass(newclass);
			var delid = this.id.substr(8);
  		$.get("AjaXUser.php", { uid:userid, active: active },
			  function(data){
					var clTD = clicked.closest('td');
					clTD.html('<strong>' + newcopy + '</strong>');
					if(newclass == 'red') {
						clTD.prev().removeClass().addClass('userdeactive').html('Deactivated');
					} else {
						clTD.prev().removeClass().addClass('useractive').html('Active');
					}
			  });
		});

		// select the overlay element - and "make it an overlay"
	  $('#addUser, .editUser').live("click", function(){
	  	if(this.id == "addUser") {
	  		$('.addonly').show();
	  		$('.editonly, .selWelcomeULang').hide();
			  $("#savuser").val('').attr('disabled', false);
				$("button.saveuser").html($("#adduserstr").val()).attr("id", "addCX");
	  		$("#UserBox div > h2").html($("#adduserstr").val());
	  		$("#savuser").attr("class", 'validate[required,custom[email],ajax[ajaxUser]]');
	  	} else {
	  		$('.addonly, .selWelcomeULang').hide();
	  		$('.editonly').show();
	  		$("#savuser").val('').attr('disabled', true);
	  		$("button.saveuser").html($("#saveuserstr").val()).attr("id", "editCX");
	  		$("#UserBox div > h2").html($("#edituserstr").val());
	  		$("#savuser").attr("class", 'textfield');
	  	}
	  	userobj = $(this);
	  	var mapids = '';
			var MAPapi = $("#UserBox").overlay({
			    top: 'center',
			    expose: {
			        color: '#fff',
			        loadSpeed: 200,
			        opacity: 0.5
			    },
			    onClose: function() {
			    	$("#UserBox").validationEngine('hideAll');
				  },
			    closeOnClick: false,
			    onBeforeLoad: function(event) {
			    	//reset values
			    	if(userobj.attr("id") == 'addUser') {
			    		$('#level').val(4);
			    		var editid = null;
						$('.usercopy').html($("#addusercopy").val());
			    	} else {
			    		var editid = userobj.attr("id").substr(9);
			    		$("#editUserID").val(editid);
							$('.usercopy').html($("#editusercopy").val());
							$('#showmeul').html('');

							//ADD UPLOADER FOR AVATAR UPLOAD
					   	 var AvatarUpload = new AjaxUpload('avaUpload', {
					        action: 'AjaXUser.php',
					        name:'upload',
					        autoSubmit: false,
					        onSubmit : function(file, ext){
						        if (! (ext && /^(jpg|jpeg)$/.test(ext))){
						          // extension is not allowed
						          alert('Error: invalid file extension (Only .jpg, .jpeg is accepted)');
						          // cancel upload
						          return false;
						        } else {
						        	$('.addpad').block({ message: 'Processing' });
						        	this.setData({userid: editid, uploadAvatar: true});
						        }
					        },
								  onComplete: function(file, response) {
									 if (window.console) console.log(response);
								  	var resultData = jQuery.trim(response).split("---");
								  	$('.addpad').unblock();
								    $.jGrowl(resultData[1], {
											header:'<strong>System Message</strong>',
											closeTemplate:'<div class="jGrowlCloseMe">close (x)</div>',
											position:'bottom-right'
										});
										$("#userAvatarHolder").html('<img src="' + resultData[0] + '" />');
								  	//$("#CSVUpload").overlay().close();
								  },
								  onChange: function(file, extension) {
								  	$('#showmeul').html('Image File <span style="color:green;">(Selected - ' + file + ')</span>');
								  }
							});
						  $(".uploadAvaToCDN").click(function(){
								AvatarUpload.submit();
						  });

			    	}
			    	//disable and reset value of the username field (not for editing!)

			  		$("#savfirstname").val('');
			  		$("#savsurname").val('');
			  		$("#savemail").val('');
			    	if(editid != null) {
			    		//THIS NEEDS CHANGING TO JSON OLD AND SHIT IS THIS :(
			    		$.get("AjaXUser.php", { userid: editid, getUser: 'yes' },
							  function(data){
									var resultData = data.split('---');
									$("#savuser").val(jQuery.trim(resultData[0]));
						  		$("#savfirstname").val(jQuery.trim(resultData[1]));
						  		$("#savsurname").val(jQuery.trim(resultData[2]));
						  		$("#savemail").val(jQuery.trim(resultData[3]));
						  		$("#countryid").val(jQuery.trim(resultData[4]));
						  		$("#companyid").val(jQuery.trim(resultData[5]));
						  		$("#level").val(jQuery.trim(resultData[6]));
						  		$("#userAvatarHolder").html('<img src="' + resultData[7] + '" />');
							  });
						}
			    },
			    onLoad: function(event) {
						//nothing yet
			    },
			    api: true
			});
			MAPapi.load();
	  });

	 	// select the overlay element - and "make it an overlay"
	  $("#massUpload").live("click", function(){
	  	CSVobj = $(this);
			var UUapi = $("#CSVUpload").overlay({
			    top: 'center',
			    expose: {
			        color: '#fff',
			        loadSpeed: 200,
			        opacity: 0.5
			    },
			    closeOnClick: false,
			    onBeforeLoad: function(event) {
			    	$('#CSVfSel').html('CSV File');
			    },
			    onLoad: function(event) {
			    	$('#usercsv').html($('#usercsv').html());

				    var upload = new AjaxUpload('usercsv', {
				        action: 'AjaXUser.php',
				        autoSubmit: false,
				        onSubmit : function(file , ext){
					        if (! (ext && /^(xls)$/.test(ext))){
					          // extension is not allowed
					          alert('Error: invalid file extension (Only .xls is accepted)');
					          // cancel upload
					          return false;
					        } else {
					        	$('.addpad').block({ message: 'Processing' });
					        	this.setData({UUCountryid: $("#UUcountryid").val(), UUCompanyid: $("#UUcompanyid").val(), UULevel: $("#UUlevel").val(), eid: $("#MUeid").val(), ulang: $("#MULang").val()});
					        }
				        },
							  name:'usercsv',
							  onComplete: function(file, response) {
							  	$('.addpad').unblock();
							  	alert(response);
							  	$("#CSVUpload").overlay().close();
							  },
							  onChange: function(file, extension) {
							  	$('#CSVfSel').html('CSV File <span style="color:green;">(Selected - ' + file + ')</span>');
							  }
						});
					  $(".UUsaveuser").click(function(){
							upload.submit();
					  });
			    },
			    api: true
			});
			UUapi.load();
	  });

	  // select the overlay element - and "make it an overlay"
	  $(".reSend").live("click", function(){
	  	RSobj = $(this);
			var REapi = $("#reSendEmailBox").overlay({
			    top: 'center',
			    expose: {
			        color: '#fff',
			        loadSpeed: 200,
			        opacity: 0.5
			    },
			    closeOnClick: false,
			    onBeforeLoad: function(event) {
			    		$("#editUserID").val(RSobj.attr("id").substr(7));
			    },
			    onLoad: function(event) {

			    },
			    api: true
			});
			REapi.load();
	  });

		//************************************** SAVE AND EDIT FUNCTIONS ******************************************/

		 $(".saveuser").click(function(){
	  	var valid = $("#userform").validationEngine('validate');
	  	if(valid == true) {
		  	if(this.id == "addCX") {
		  		var savuser = $("#savuser").val();
		  		var savFN = $("#savfirstname").val();
		  		var savSN = $("#savsurname").val();
		  		var savEM = $("#savemail").val();
		  		var CounID = $("#countryid").val();
		  		var CompID = $("#companyid").val();
		  		var level = $("#level").val();
		  		var userLang = $("#ULang").val();
		  		var eid = $("#eid").val();
		  		if(CompID == null || CompID == 0 || CompID == '') {
		  			alert('Sorry, you must select an account before you can add a user');
		  		} else {
					$.get("AjaXUser.php", { username:savuser, firstname: savFN, surname:savSN, companyid:CompID, email:savEM, salutation:$("#salutation").val(), countryid:CounID, eid:eid, level:level, ulang:userLang, addUser:1 },
					  function(data){
					  	oCache.iCacheLower = -1;
					  	usrTable.fnDraw();
					    $.jGrowl(data, {
								header:'<strong>System Message</strong>',
								closeTemplate:'<div class="jGrowlCloseMe">close (x)</div>',
								position:'bottom-right'
							});
					    $("#UserBox").overlay().close();
					  });
					}
				} else if (this.id == "editCX") {
		  		var savFN = $("#savfirstname").val();
		  		var savSN = $("#savsurname").val();
		  		var savEM = $("#savemail").val();
		  		var CounID = $("#countryid").val();
		  		var CompID = $("#companyid").val();
		  		var level = $("#level").val();
		  		var editid = $("#editUserID").val();
		  		var eid = $("#eid").val();
		  		$.get("AjaXUser.php", { userid: editid, firstname: savFN, surname:savSN, companyid:CompID, email:savEM, countryid:CounID, eid:eid, level:level, editUser:1 },
					  function(data){
					  	oCache.iCacheLower = -1;
					  	usrTable.fnDraw();
					    $.jGrowl(data, {
								header:'<strong>System Message</strong>',
								closeTemplate:'<div class="jGrowlCloseMe">close (x)</div>',
								position:'bottom-right'
							});
							$("#UserBox").overlay().close();
					  });
				}
			}//end validation check
	  });

	  $(".resetpass").click(function(){
	  	var editid = $("#editUserID").val();
  		var savFN = $("#savfirstname").val();
  		var savSN = $("#savsurname").val();
  		var savEM = $("#savemail").val();
  		var savuser = $("#savuser").val();
  		$.get("AjaXUser.php", { userid: editid, firstname: savFN, surname:savSN, email:savEM, username:savuser, resetPass:1 },
			  function(data){
			    $.jGrowl(data, {
						header:'<strong>System Message</strong>',
						closeTemplate:'<div class="jGrowlCloseMe">close (x)</div>',
						position:'bottom-right'
					});
			  });
	  });

	  $(".resetpass").confirm({dialogShow:'fadeIn', wrapper:'<div class="confirmdialog"></span>', timeout:10000, msg:'Do you really want to reset the password? &nbsp;', buttons: {ok:'Yes',cancel:'No', separator:' / '}});

	   //resend email!
	  $(".UUResendEmail").live("click", function(){
	  	var editid = $("#editUserID").val();
  		$.get("AjaXUser.php", { userid: editid, eid:$("#MUeidRS").val(), ulang:$("#MULangRS").val(), reSendEmail:1 },
			  function(data){
			  	$('#reSendEmailBox').overlay().close();
			    $.jGrowl(data, {
						header:'<strong>System Message</strong>',
						closeTemplate:'<div class="jGrowlCloseMe">close (x)</div>',
						position:'bottom-right'
					});
			  });
	  });

	    //***********************************************************************
	    //															RETAKE CODE															//
	    //***********************************************************************

		  $(".incretake").live("click", function(){
	  		$.get("AjaXUser.php", { userid: this.id.substr(7), retakeSurvey:1 },
				  function(data){
				    $.jGrowl(data, {
							header:'<strong>System Message</strong>',
							life: 6000,
							closeTemplate:'<div class="jGrowlCloseMe">close (x)</div>',
							position:'bottom-right'
						});
				  });
		  });

	   $(".incretake").confirm({dialogShow:'fadeIn', wrapper:'<div class="confirmdialog"></span>', timeout:10000, msg:'Do you really want this user to retake the survey? &nbsp;', buttons: {ok:'Yes',cancel:'No', separator:' / '}});

		//this is for loading in correct emails based on language selected
		$("#MULang, #ULang, #MULangRS").change(function() {
			if(this.id == 'MULang') {
				var selid = 'MULang';
				var optionid = 'MUeid';
				var langval = $("#MULang").val();
			} else if(this.id == 'ULang') {
				var selid = 'ULang';
				var optionid = 'eid';
				var langval = $("#ULang").val();
			} else if(this.id == 'MULangRS') {
				var selid = 'MULangRS';
				var optionid = 'MUeidRS';
				var langval = $("#MULangRS").val();
			}
		  $.get("AjaXUser.php", { langval: langval, getEmailList:true },
		  function(j){
				var opstring = '';
		    $.each(j, function(eid, infoobj) {
		    	opstring += '<option value="' + eid + '">' + infoobj.name + '</option>';
		    });
		    $("#" + optionid).html(opstring);
			  if($(".selWelcome" + selid).is(':hidden')) {
					$(".selWelcome" + selid).show();
				}
		  }, "json");
		});

    $("#userform").validationEngine('attach');

	});
