	$(function() {
		$("#tabs").tabs();

		var tA = 0;
		var iMapComplete = false;

		$(".IMslider").slider({
			//values: [1, 7, 10],
			value:5,
			min:0,
			max:10,
			step:0.1,
			animate:true,
			slide: function(event, ui) {
				var k = 0;
				var c = 0;
				var infoarr = $(this).attr('id').split('-');
				var key = 'qid_' + infoarr[0].substr(4);
		    var cid = 'cid_' + infoarr[2].substr(4);
		    if(progressObj[cid][key].moved == false) {
	    		progressObj[cid][key].moved = true;
	    		//increment total counter
	    		tA++;
	    		if(tA == 60) {
	    			iMapComplete = true;
	    		}
	    		$.each(progressObj[cid], function(key, value) {
		    		if(value.moved == true) {
		    			 k++;
		    		}
						c++;
					});
					var perc_clus = Math.round((k/c) * 100);
					var perc_total = Math.round((tA/60) * 100);
					if(perc_clus == 100) {
						$('#' + cid).addClass('idealComplete');
					}
					if(perc_total == 100) {
						$('#prog_total').addClass('idealComplete');
					}
					$('#' + cid).html(perc_clus + '%');
					$('#prog_total').html(perc_total + '%');
	    	}
			}
		});

		var progressObj = {};
		progressObj.cid_1 = {};
		progressObj.cid_2 = {};
		progressObj.cid_3 = {};
		progressObj.cid_4 = {};

		$(".IMslider").each(function() {
			var infoarr = $(this).attr('id').split('-');
	    var key = 'qid_' + infoarr[0].substr(4);
	    var cid = 'cid_' + infoarr[2].substr(4);
    	progressObj[cid][key] = {
        moved: false
   		};
		});

		$(".IMslider .ui-slider-handle:nth-child(1)").addClass("handlea");
		$("#idealmap #tabs .IMslider > a").removeClass("ui-corner-all ui-state-default");
		$(".IMslider").removeClass("ui-corner-all");

		$('#addideal').click(function() {
			var valid = $("#idealMAP_FRM").validationEngine('validate');
	  	if(valid == true) {
	  		if(iMapComplete == true) {
					var surveyObj = new Array();
					$(".IMslider").each(function() {
						var infoarr = $(this).attr('id').split('-');
						surveyObj[infoarr[0].substr(4)] = new Array($(this).slider("option", "value"), infoarr[1].substr(3));
					});
					$.post("AjaXMAP.php", { resultsObj: surveyObj, imapname:$('#imapname').val(), parts:$('#participants').val(), addiMAP: true },
					  function(j){
							if(j.error == false) {
								var ai = oTable.fnAddData([
								$('#imapname').val(),
								'<a href="javascript:void(0);" class="userlist">User List</a><div class="tooltip"><div class="tooltiptop"></div><div class="tooltipmid">' + j.participants + '</div><div class="tooltipbtm"></div></div>',
								j.createdBy,
								j.createdOn,
								j.adminString]);
								var n = oTable.fnSettings().aoData[ ai[0] ].nTr;
								$('.userlist', n).tooltip({
									 effect:'slide',
									 bounce:true,
									 offset: [80, 0],
									 position: 'top left',
									 relative: true,
									 predelay:100
							 });
							}
					    $.jGrowl(j.message, {
								header:'<strong>System Message</strong>',
								closeTemplate:'<div class="jGrowlCloseMe">close (x)</div>',
								position:'bottom-right'
							});
					  }, "json");
				} else {
					alert('idealMAP is still not complete');
				}
			}
		});

		$('#resetideal').click(function() {
			tA = 0;
			iMapComplete = false;
			for (i=1;i<=4;i++) {
   			$('#cid_' + i).removeClass('idealComplete');
   			$('#cid_' + i).html('0%');
   		}
			$('#prog_total').removeClass('idealComplete');
			$('#prog_total').html('0%');

			$(".IMslider").each(function() {
				$(this).slider("value", 5);
				//reset true / false values
				var infoarr = $(this).attr('id').split('-');
		    var key = 'qid_' + infoarr[0].substr(4);
		    var cid = 'cid_' + infoarr[2].substr(4);
	    	progressObj[cid][key] = {
	        moved: false
	   		};
			});
		});

		var imapid = '';
		$('.deleteiMAP').live("click", function() {
			var imapid = $(this).attr("id").substr(8);
			$.get("AjaXMAP.php", { imapid:imapid, imapname:$(this).closest('tr').find('td:first').html(), deleteiMAP: true },
			  function(data){
					alert(data);
			  });
			$(this).closest('tr').hide();
		});

		$('.editiMAP').live("click", function() {
			imapobj = $(this);
			var imapapi = $("#IMAPBox").overlay({
		    top: 'center',
		    mask: {
		        color: '#fff',
		        loadSpeed: 200,
		        opacity: 0.5
		    },
		    closeOnClick: false,
		    onBeforeLoad: function(event) {
					imapid = imapobj.attr("id").substr(5);
					$.get("AjaXMAP.php", { imapid:imapid, getiMAP: true },
					  function(j){
					  	$('#editimapname').val(j.imapname);
					  	$('#editimapParts').val(j.participants);
					  }, "json");
		    },
		    onClose: function() {
		    	$("#imapeditform").validationEngine('hideAll');
		    },
		    api: true
			});
			imapapi.load();
		});

		$(".SaveEditIMAP").click(function(){
	  	var valid = $("#imapeditform").validationEngine('validate');
	  	if(valid == true) {
  			$.get("AjaXMAP.php", { imapid:imapid, imapname:$('#editimapname').val(), participants:$('#editimapParts').val(), editiMAP: true },
				  function(data){
						alert(data);
						$("#IMAPBox").overlay().close();
				  });
  		}
  	});

		$('.expcol').toggle(function() {
			$(this).next('.pref_holder').slideUp();
		},function() {
			$(this).next('.pref_holder').slideDown();
		});

		var currLocale;
		if($('#currLocale').val() == '') {
			currLocale = 'en_EN';
		} else {
			currLocale = $('#currLocale').val();
		}

		var oTable = $('.tablesorter').dataTable({
			//"bProcessing": true,
			"bServerSide": false,
			"iDisplayStart": 0,
			"oLanguage": {
				"sUrl": "dtblLocale/dataTables." + currLocale + ".txt"
			},
			"iDisplayLength": 25,
			"sPaginationType": "full_numbers",
			"sDom": '<"filter_container"fl><"top"ip>rt<"bottom"ip<"clear">',
			"bAutoWidth": false
		});

		$('.userlist', oTable.fnGetNodes()).tooltip({
			 effect:'slide',
			 bounce:true,
			 offset: [80, 0],
			 position: 'top left',
			 relative: true,
			 predelay:100
	 });

	});
