	function isArray(a) {
		return Object.prototype.toString.call(a) === '[object Array]';
	}

	var oTable;

	$(function() {

			var currLocale;
			if($('#currLocale').val() == '') {
				currLocale = 'en_EN';
			} else {
				currLocale = $('#currLocale').val();
			}

			oTable = $('.tablesorter').dataTable({
				//"bProcessing": true,
				"iDisplayStart": 0,
				"aaSorting": [],
				"oLanguage": {
					"sUrl": "dtblLocale/dataTables." + currLocale + ".txt"
				},
				"iDisplayLength": 25,
				"sPaginationType": "full_numbers",
				"sDom": '<"filter_container"fl><"top"ip>rt<"bottom"ip<"clear">',
				"bAutoWidth": false
			});

	    jQuery.fn.center = function () {
				this.css("top", ( $(window).height() - this.height() ) / 2+$(window).scrollTop() + "px");
				this.css("left", ( $(window).width() - this.width() ) / 2+$(window).scrollLeft() + "px");
				return this;
			};

			$('.deleteMAP').live("click", function(){
				var clicked = $(this);
				//$(this).closest('tr').removeClass('odd even').addClass('red');
				var delid = this.id.substr(8);
	  		$.get("AjaXMAP.php", { tmapid:delid, deleteTMAP: 'yes', tmapType:$('#tmaptype').val() },
				  function(data){
						//alert(data);
						oTable.fnDeleteRow($(this).parent());
						//clicked.closest('td').html('<strong>Removed</strong>');
				  });
			}).confirm({dialogShow:'fadeIn', wrapper:'<div class="confirmdialog"></span>', timeout:10000, msg:$('#tmapdeletecopy').val() + '? &nbsp;', buttons: {ok:'Yes',cancel:'No', separator:' / '}});

			$('.archiveMAP').live("click", function(){
				var clicked = $(this);
				//$(this).closest('tr').removeClass('odd even').addClass('orange');
				var arcid = this.id.substr(8);
	  		$.get("AjaXMAP.php", { tmapid:arcid, archiveTMAP: 'yes' },
				  function(data){
						//alert(data);
						oTable.fnDeleteRow($(this).parent());
						//clicked.closest('td').html('<strong>Archived</strong>');
				  });
			}).confirm({dialogShow:'fadeIn', wrapper:'<div class="confirmdialog"></span>', timeout:10000, msg:'Are you sure you want to archive this MAP? &nbsp;', buttons: {ok:'Yes',cancel:'No', separator:' / '}});

		 	// select the overlay element - and "make it an overlay"
		  $('#addnewMAP, .editMAP').live("click", function(){
		  	var tmaphead = $("#tmapheader").val();
		  	if(this.id == "addnewMAP") {
		  		$("#addeditTMAP").val('ADD');
					$("button.savemap").html($('#tmapcopyadd').val()).attr("id", "addCX");
		  		$("#MAPbox div > h2").html($('#tmapcopyadd').val());
		  	} else {
		  		$("#addeditTMAP").val('EDIT');
		  		$("button.savemap").html($('#tmapcopysave').val()).attr("id", "editCX");
		  		$("#MAPbox div > h2").html($('#tmapcopyedit').val());
		  	}
		  	mapobj = $(this);
		  	var mapids = '';
				var MAPapi = $("#MAPbox").overlay({
				    top: 'center',
				    mask: {
				        color: '#fff',
				        loadSpeed: 200,
				        opacity: 0.5
				    },
				    closeOnClick: false,
				    onClose: function() {
				    	//closes validation prompts if open when overlay is closed
				    	 $("#tmapform").validationEngine('hideAll');
				    },
				    onBeforeLoad: function(event) {
				    	//THIS IS FUCKING SUPER IMPORTANT OR THE MULTISELECT WILL KEEP OLD VALUES IN FROM THE PREVIOUS EDIT OR ADD!!!
				    	$("#addMAP").html('');
				    	//reset values
				    	if(mapobj.attr("id") == 'addnewMAP') {
				    		var editid = null;
				    	} else {
				    		var editid = mapobj.attr("id").substr(5);
				    		$("#editTMAPID").val(editid);
				    	}
				     	$("#tmapName").val('');
		  				$("#tmapDesc").val('');
				    	if(editid != null) {
				    		//THIS IS FOR COMPANIES IS YOU ARE AN ADMIN
				    		$.get("AjaXMAP.php", { tmapid: editid, getTMAP: 'yes' },
								  function(j){
										$("#tmapName").val(j.tmapname);
							  		$("#tmapDesc").val(j.tmapdesc);
							  		mapids = j.mapids;
							  		var CAScids = j.compids.split(',');
							  		//CALevel not needed anymore for sharing OTT!
										//clear all the values first!
										$('.SelAccessCompanies option').each(function(i) {
											$(this).attr("selected", false);
											$(this).removeProp("selected");
										});

										if(CAScids.length != 0) {
								  		jQuery.each(CAScids, function(i, val) {
								  			$('.SelAccessCompanies option[value=' + val + ']').attr("selected", "selected");
									    });
									  }
								    var UASids = j.adminids.split(',');
									  //this select and clears the user access level box!
										$('.SelectAdmins option').each(function(i) {
											$(this).attr("selected", false);
											$(this).removeProp("selected");
										});
										if(UASids.length != 0) {
								  		jQuery.each(UASids, function(i, val) {
								  			$('.SelectAdmins option[value=' + val + ']').attr("selected", "selected");
									    });
								 	 	}
								  }, "json");
							} else {
								//this select and clears the user access level box!
								$('.SelAccessCompanies option').each(function(i) {
									$(this).attr("selected", false);
								});
								$('.SelectAdmins option').each(function(i) {
									$(this).attr("selected", false);
								});
							}
				    },
				    onLoad: function(event) {
				    	//this is for ie8.. fuck i love ms....
				    	$("#sortoverlay").hide(0);
							var nothing = '';
							if($("#addeditTMAP").val() == 'ADD') {
								$("#addMAP").html('');
								$("#addMAP").multiselect('destroy');
								$("#addMAP").multiselect({
									dividerLocation: 0.5,
									animated: 'fast',
									searchDelay: 1400,
									remoteUrl: "../MSajax.php",
									remoteParams: { maptype: 'MAP' }
								});
							} else {
								$.get("../_inc/ajaXUpdate.php", { tmapid: $("#editTMAPID").val(), getTMAP: 'yes' },
									function(data){
										var resultData = data.split('---');
										mapids = resultData[2];
										$('.edittmap').data("tmapname", {name:resultData[0]});

										//now complete map shiate!
										$("#addMAP").multiselect('destroy');
										$("#addMAP").multiselect({
											dividerLocation: 0.5,
											animated: 'fast',
											remoteUrl: "../MSajax.php",
											remoteParams: { newmapids: mapids, maptype: 'MAP' }
										});
										var currmapids = mapids.split(',');

										$("#addMAP").multiselect('selectNone');
										$('#addMAP').everyTime(500, function(i) {
										  if($('#addMAP').multiselect('isBusy') == false) {
											  $("#addMAP").stopTime();

												$.each(currmapids, function(i, val) {
													$('#addMAP').multiselect('select', $('#addMAP').find('option[value=' + val + ']').text() );
												});

												//this is needed so that it isn't still selecting the options when it trying to do the array difference.
												$('#addMAP').everyTime(100, function(i) {
													 if($('#addMAP').multiselect('isBusy') == false) {
													 	  $("#addMAP").stopTime();

															var diff_arr = array_diff($('#addMAP').multiselect('selectedValues'),currmapids);
															//this is because ie is shit and returns the value "indexOf" from the diff_array function. So this removes it from the array!
															diff_arr = removeByElement(diff_arr,'indexOf');
															var oldmapids = diff_arr.join(',');
															if(diff_arr.length >= 1) {
											  				$.get("../_inc/ajaXUpdate.php", { oldmapids:oldmapids, getnewTMAPids:true },
											  					function(data){
											  						var updatedmapids = data.split(',');
											  						$.each(updatedmapids, function(i, val) {
											  							$('#addMAP').multiselect('select', $('#addMAP').find('option[value=' + val + ']').text() );
											  						});
											  						//$("#addMAP").multiselect('enabled', true);
											  						alert("Some of the respondents have retaken their MAPs since this talentMAP/teamMAP was made. The new MAPs have been automatically added for you. If you want to keep these changes click save, otherwise click cancel to keep the talentMAP/teamMAP in it's original state");
											  					});
										  				}
										  		}
										  	});
							  			}
							  		}); //end everytime
								});
				  		}
				    },
				    onClose: function() {
							$("#addMAPbox .search:input").val('');
							$("#addMAP").multiselect('search', $("#addMAPbox .search:input").val());
						},
				    api: true
				});
				MAPapi.load();
		  });

	    //************************************** SAVE AND EDIT FUNCTIONS ******************************************/

		  $(".savemap").click(function(){
	  		//map list uses the multiselect plugin used in the analytics etc..
		  	var maplist = $("#addMAP").multiselect('selectedValues').join(',');
		  	if(maplist.length >= 1) {
		  		var mapPassed = true;
		  	} else {
				//alert(isArray(maplist));
		  		var mapPassed = false;
		  		alert($('#tmapsnomapcopy').val());
		  	}
	  		var valid = $("#tmapform").validationEngine('validate');
	  		if(valid == true && mapPassed == true) {
			  	if(this.id == "addCX") {
				  		var tmapName = $("#tmapName").val();
				  		var tmapDesc = $("#tmapDesc").val();
				  		var tmaptype = $("#tmaptype").val();
				  		var compid = $("#compid").val();
				  		var companylist = $.map($('.SelAccessCompanies :selected'), function(e) { return $(e).val(); }).join(',');
				  		var accesslevel = 0;
				  		var adminlist = $.map($('.SelectAdmins :selected'), function(e) { return $(e).val(); }).join(',');
							$.get("AjaXMAP.php", { companyid:compid, addTMAP: 'yes', tmapname:tmapName, tmapdesc:tmapDesc, mapids:maplist, type:tmaptype, accessids: companylist, accesslevel:accesslevel, adminAIDs:adminlist },
							  function(j){
							  	if(j.error == false) {
								    var ai = oTable.fnAddData([
										tmapName,
										tmapDesc,
										'<a href="javascript:void(0);" class="userlist">User List</a><div class="tooltip"><div class="tooltiptop"></div><div class="tooltipmid">' + j.userlist + '</div><div class="tooltipbtm"></div></div>',
										j.createdBy,
										j.createdOn,
										j.adminString]);
										var n = oTable.fnSettings().aoData[ ai[0] ].nTr;
										$('.userlist', n).tooltip({
											 effect:'slide',
											 bounce:true,
											 offset: [34, 0],
											 position: 'top left',
											 relative: true,
											 predelay:100
									  });
									}
							    $("#MAPbox").overlay().close();
							    $.jGrowl(j.message, {
										header:'<strong>System Message</strong>',
										closeTemplate:'<div class="jGrowlCloseMe">close (x)</div>',
										position:'bottom-right'
									});
							  }, "json");
					} else if (this.id == "editCX") {
			  		var tmapName = $("#tmapName").val();
			  		var tmapDesc = $("#tmapDesc").val();
			  		var tmaptype = $("#tmaptype").val();
			  		var editid = $("#editTMAPID").val();
						//var maplist = $("#addMAP").multiselect('selectedValues').join(',');
		  			var companylist = $.map($('.SelAccessCompanies :selected'), function(e) { return $(e).val(); }).join(',');
		  			var accesslevel = 0;
			  		//var accesslevel = $("#accesslevel").val();
			  		var adminlist = $.map($('.SelectAdmins :selected'), function(e) { return $(e).val(); }).join(',');
			  		$.get("AjaXMAP.php", { tmapid:editid, tmapname:tmapName, tmapdesc:tmapDesc, newmapids:maplist, companyids: companylist, adminAIDs:adminlist, accesslevel:accesslevel, editTMAP: 'yes', type:tmaptype },
						  function(data){
								alert(data);
								$("#MAPbox").overlay().close();
						  });
					}
				}//end if valid passed
		  });

		  //this works only on current cached TR rows. As TMAPs only uses the cache to display the rows it's fine. However if ever moved to AjaX processing it will need to integrated into the callback on an ajax call for the sorting.
		  $('.userlist', oTable.fnGetNodes()).tooltip({
				 effect:'slide',
				 bounce:true,
				 offset: [34, 0],
				 position: 'top left',
				 relative: true,
				 predelay:100
		 });

	    $("#tmapform").validationEngine('attach');

	});//end doc rdy
