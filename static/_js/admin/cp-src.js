	var pcMAP;
	var pctMAP;
	var chart;
	var surdisplay;

	$(function() {

		//set options for all highchart instances!
 		Highcharts.setOptions({
			colors: [
				"#73a1d2",
				"#e19c46",
				"#e1a0b6",
				"#98c69c",
				"#e8d04d",
				"#df575d"
			]
		});

		/**************** LINE GRAPH OPTIONS *****************/
		var options = {
			chart: {
				renderTo: 'container',
				defaultSeriesType: 'area'
			},
			title: {
				text: 'talentMAP Creation by Date'
			},
			subtitle: {
				text: 'talentMAPs vs Date'
			},
			credits: {
        enabled: false
    	},
			xAxis: {
				type: 'datetime'
			},
			yAxis: {
				title: {
					text: 'Total number of talentMAPs'
				}
			},
			tooltip: {
				formatter: function() {
					return this.series.name +' issued <b>' + Highcharts.numberFormat(this.y, 0, null, ' ') +'</b><br/>talentMAPs in '+ Highcharts.dateFormat('%B %Y', this.x);
				}
			},
			series: []
		};

		/**************** PIE TMAP OPTIONS *****************/
		var pctMAPoptions = {
      chart: {
         renderTo: 'piecontainer',
         margin: [50, 70, 60, -58]
      },
      title: {
         text: 'talentMAP / teamMAP Totals',
         align: 'left',
         x:10,
         y:26
      },
      subtitle: [],
      plotArea: {
         shadow: true,
         borderWidth: null,
         backgroundColor: null
      },
      tooltip: {
         formatter: function() {
            return '<b>'+ this.point.name +'</b>: '+ this.y;
         }
      },
			credits: {
        enabled: false
    	},
      plotOptions: {
         pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
               enabled: false
            },
            showInLegend: true
         }
      },
      legend: {
         layout: 'vertical',
         style: {
            left: 'auto',
            bottom: 'auto',
            right: '22px',
            top: '100px'
         }
      },
      series: []
   };

   	/**************** PIE MAP OPTIONS *****************/
		var pcMAPoptions = {
      chart: {
         renderTo: 'piecontainer_survey',
         margin: [50, 70, 60, -58]
      },
      title: {
         text: 'Survey Usage Results',
         align: 'left',
         x:30,
         y:26
      },
      subtitle: [],
      plotArea: {
         shadow: true,
         borderWidth: null,
         backgroundColor: null
      },
      tooltip: {
         formatter: function() {
            return '<b>'+ this.point.name +'</b>: '+ this.y;
         }
      },
			credits: {
        enabled: false
    	},
      plotOptions: {
         pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
               enabled: false
            },
            showInLegend: true
         }
      },
      legend: {
         layout: 'vertical',
         style: {
            left: 'auto',
            bottom: 'auto',
            right: '22px',
            top: '100px'
         }
      },
      series: []
   };

		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();

   /**************** LOAD LINE CHART *****************/
   //get the value of the uid (xhuid) from the header page hidden field!

   $.get("AjaXCompany.php", { givemejson:true, ms:1, me:12, ys:year, maptype:'talent', ye:year, uidstring:$("#xhuid").val() },
	  function(j){
			$.each(j, function(key, infoobj) {
				options.series.push({
         name: infoobj.gname,
         data: infoobj.data
     	 });
			});
			chart = new Highcharts.Chart(options);
		}, "json");

	 /**************** LOAD TALENT PIE MAP CHART *****************/
	 function loadPieTMAP(iDs, subtitle) {
	 	var currids;
	 	if(iDs == 0) {
	 		currids = $("#userCompID").val();
	 	} else {
	 		currids = iDs;
	 	}
	 	$.get("AjaXCompany.php", { pieTMAP:true, companyid:currids },
	  function(j){
	  	//reset series here from previous values!
	  	pctMAPoptions.series = [];
      var series = {
      		type: 'pie',
          data: []
      };
      var selected;
      //loop through data, pushing values into the data array as 2 new values in an array. Don't you love objects sometimes?!.. ummm
			$.each(j, function(key, value) {
 				series.data.push({name:key, y:parseInt(value)});
			});
			pctMAPoptions.subtitle = ({text:subtitle, align: 'left', x:30, y:42});
			pctMAPoptions.series.push(series);
			pctMAP = new Highcharts.Chart(pctMAPoptions);
		}, "json");
	 };


		/**************** LOAD PIE MAP CHART *****************/
		function loadPieMAP(iDs, subtitle) {
			var currids;
		 	if(iDs == 0) {
		 		currids = $("#userCompID").val();
		 	} else {
		 		currids = iDs;
		 	}
			$.get("AjaXCompany.php", { pieMAP:true, companyid:currids },
		  function(j){
		  	pcMAPoptions.series = [];
	      var series = {
	      		type: 'pie',
	          data: []
	      };
	      var selected;
				$.each(j, function(key, value) {
	 				series.data.push({name:key, y:parseInt(value)});
				});
				pcMAPoptions.subtitle = ({text:subtitle, align: 'left', x:30, y:42});
				pcMAPoptions.series.push(series);
				pcMAP = new Highcharts.Chart(pcMAPoptions);
			}, "json");
		}

		//Load initial graphs
		///the selected value is the users current company name loaded into the graph options lightbox
		loadPieTMAP(0, $("#creationcids :selected").html());
		loadPieMAP(0, $("#surveycids :selected").html());

		$("#g_options").click(function() {
	  	mapobj = $(this);
	  	var mapids = '';
			var Gapi = $("#Graphbox").overlay({
			    top: 'center',
			    mask: {
			        color: '#fff',
			        loadSpeed: 200,
			        opacity: 0.5
			    },
			    closeOnClick: false,
			    onBeforeLoad: function(event) {
						//nout yet biatch :)
			    },
			    onLoad: function(event) {
						//nout yet biatch :)
			    },
			    api: true
			});
			Gapi.load();
		});

		$(".go_gopts").click(function() {
			var maptype;
			if($("#maptype").val() == 'talent') {
				surdisplay = 'talentMAPs';
				maptype = 'talent';
			} else if($("#maptype").val() == 'team') {
				surdisplay = 'teamMAPs';
				maptype = 'team';
			} else if($("#maptype").val() == 'MAP') {
				surdisplay = 'MAPs';
				maptype = 'MAP';
			}

			var MoN_S = $("#TMLD_MonthS").val();
			var MoN_E = $("#TMLD_MonthE").val();
			var YeaR_S = $("#TMLD_YearS").val();
			var YeaR_E = $("#TMLD_YearE").val();

			//this maps the ids of the selected list boxes to an array which is then joined into a string!
			var companylist = $.map($('.g_companies :selected'), function(e) { return $(e).val(); }).join(',');
			var adminlist = $.map($('.g_admins :selected'), function(e) { return $(e).val(); }).join(',');

		   /**************** LOAD LINE CHART *****************/
		   $.get("AjaXCompany.php", { givemejson:true, maptype:maptype, ms:MoN_S, me:MoN_E, ys:YeaR_S, ye:YeaR_E, uidstring:adminlist, cidstring:companylist },
			  function(j){
			  	//reset series data and format based on which type of MAP has been selected!
			  	options.series = [];
			  	options.tooltip = ({formatter: function() {
						return this.series.name +' issued <b>'+Highcharts.numberFormat(this.y, 0, null, ' ') +'</b><br/>' + surdisplay + ' in '+ Highcharts.dateFormat('%B %Y', this.x);
						}
					});
					//set additional options for the graph object
					options.title = ({text: surdisplay + ' Creation by Date'});
					options.subtitle = ({text: surdisplay + ' vs Date'});

					$.each(j, function(key, infoobj) {
						options.series.push({
		         name: infoobj.gname,
		         data: infoobj.data
		     	 });
					});
					chart = new Highcharts.Chart(options);
				}, "json");

			//load the graphs again after the options menu has been changed by running the nice functions above :)
			loadPieMAP($("#creationcids").val(), $("#creationcids :selected").html());
			loadPieTMAP($("#surveycids").val(), $("#surveycids :selected").html());
			$("#Graphbox").overlay().close();
		});

		//cool little function which basically animates the language limit bar when the checkboxes for langs are ticked and unticked. Limits are gotten from hidden fields in page, width of bar is 476px
		$('.langBoxes').click(function() {
			var n = $("input:checked").length;
			var bgTop;
			var langTotal = $('#langLimitVal').val();
			var widthRatio = (n/langTotal);
			var widthNow = Math.round(widthRatio * 476);

			//work out width ratio of bar and adjust bg position accordingly
			if(widthRatio <= 0.7) {
				bgTop = -10;
			} else if(widthRatio < 0.9){
				bgTop = -20;
			} else if(widthRatio <= 1){
				bgTop = -30;
			}

			if(n > langTotal) {
				alert('You have reached your language limit');
				$(this).attr("checked", false);
			} else if(n < 1) {
				alert('You must have at least one language active!');
				$(this).attr("checked", true);
			} else {
				$('#limits_languages').parent().next().find('.limitUsed').html(n);
				$('#limits_languages').animate({width: widthNow + 'px'});
				$('#limits_languages').css('backgroundPosition', '0px ' + bgTop + 'px');
			}
		});

		$('.saveLangs').click(function() {
			 var langArr = $.map($('.langBoxes:checked'), function(e) { return $(e).val(); }).join(',');
		   $.get("AjaXUser.php", { updateLangs: true, langList:langArr },
			  function(j){
					$.jGrowl(j.message, {
						header:'<strong>System Message</strong>',
						closeTemplate:'<div class="jGrowlCloseMe">close (x)</div>',
						position:'bottom-right'
					});
				}, "json");
		});

	});
