 <?php

	if(!isset($_SESSION)) { session_start(); }
	$phpsessid = session_id();
	ini_set("display_errors", 1);
	//include localization and site config files
	require_once("../site.config.php");

	//include DB AND ACCOUNT INFO CLASSES
	include FULL_PATH . '/survey/_inc/_classes/db-class.php';
	include FULL_PATH . '/survey/_inc/_classes/account-class.php';

	$accobj = new Account();

	//include other classes
	include FULL_PATH . '/survey/_inc/_classes/MAP-class.php';
	include FULL_PATH . '/survey/_inc/_classes/question-class.php';
	include FULL_PATH . '/survey/_inc/_classes/user-class.php';
	include FULL_PATH . '/survey/_inc/_classes/admin-class.php';
		
	require_once(FULL_PATH . "/survey/_inc/localization.php");
	require_once(FULL_PATH . "/survey/_inc/scripts.php");
	
	/****************************************************************************
	*																																						*
	*																	ADD ACCOUNT																*
	*																																						*
	****************************************************************************/
	
	if(isset($_POST['addAccount'])) {
		//print_r($_POST['levelObj']);
		if(!$accobj->addAccount($_POST['accName'], $_POST['adminEmail'], $_POST['serviceLevel'], $_POST['salutation'], $_POST['firstname'], $_POST['surname'], $_POST['countryid'])) {
			echo 'There was an error adding the account. Please try again.';
		} else {
			echo 'The account has been added successfully!';
		}
	}
	
?>