
			<div id="innerContent">
			<div id="contentMenuBar">
	      <div id="contentSiFR">
	      	<h2 class="MWxBox">
	      		Saviio creates MAPs to help attract, select and retain the best available talent for any specific job role.<br /><br />
						By extracting motivational preferences from your top performers in a specific job role, Saviio MAPs will help to predict who else will succeed in a similar role.  
					</h2>
	      </div>
	      <div class="cleaner"></div>
	     </div>
	     <div id="contentSideGFX">
	     	<img src="/_images/_sidebar/<?=$sideGFX;?>" width="194" height="150" />
	     </div>
	     <div class="cleaner"></div>
     	<div class="contentPadTop">
    		<p><span class="contentHeader">This will benefit organisations by:</span></p>
				<ul>
					<li>Improving selection and reducing attrition</li>
					<li>Improving retention</li>
					<li>Improving sales and performance</li>
					<li>Helping managers to motivate their teams</li>
				</ul>
    	</div>
	     <div class="featureHighlights">
         <ul>
           <li><strong>Attracting Staff</strong><p>Saviio MAPs is used by organisations as the cornerstone of their selection process and fine-tuning of their recruitment strategies. It is simple and easy to use and is designed to be implemented by managers within the workplace without the need to consult professional psychologists.</p></li>
           <li><strong>Selection</strong><p>Saviio MAPs enables a more disciplined, thorough and objective approach to recruitment decisions. It gives managers and HR professionals specific and objective information about candidates' motivations and preferences that complement traditional application forms and interviews. It also provides an insight into a candidate's current workplace behaviour that is difficult to obtain even from an in-depth interview.</p></li>
           <li class="cleaner"><strong>Motivating People</strong><p>Saviio MAPs gives managers accurate and reliable information about the motivations, behaviours and operating styles of their staff, and can be used to ensure that they manage in an effective and appropriate manner. It also provides managers with the understanding needed to bring about positive productive changes in workplace behaviour.</p></li>
           <li><strong>Building Teams</strong><p>Saviio MAPs gives managers accurate and reliable information about the motivations, behaviours and operating styles of their staff, and can be used to ensure that they manage in an effective and appropriate manner. It also provides managers with the understanding needed to bring about positive productive changes in workplace behaviour.</p></li>
           <li class="cleaner"><strong>Redeployment</strong><p>As an organisation develops, Saviio MAPs helps with future planning by looking objectively at the drives, capabilities and motivations of the management team helping them to prepare for the challenges ahead. It is also a very useful tool with respect to succession planning and the identifying of future leaders. Saviio MAPs can also enable you to describe a future 'space' allowing a fit and gaps analysis.</p></li>
           <li><strong>M & A</strong><p>Research underlines the importance of getting the people related issues right. Retaining key people and maintaining employee morale, despite the usual debilitating effects of major change, are essential to running a successful merger. Saviio MAPs can enable you to assess and evaluate the gaps, fits and challenges associated with mergers and acquisitions.</p></li>
        </ul>
      </div>
	</div>
	
	
	



    

    
    
    




    
    
