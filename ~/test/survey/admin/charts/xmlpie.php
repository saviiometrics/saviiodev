<?php
if(!isset($_SESSION)) { session_start(); }
include '_inc/_objects/db-class.php';
include '_inc/_objects/report-class.php';
include '_inc/_objects/device-class.php';
include '_inc/_objects/module-class.php';

$jobrole = $_GET['jobrole'];

if( !isset( $db->mySQLConnection ) ) {
	$db = new DbaseMySQL();
	$db->mySQLConnect();
}

// create a new XML document
$doc = new DomDocument('1.0');
// create root node
$root = $doc->createElement('pie');
$root = $doc->appendChild($root);

/***************************************************************
****************ALL QUERIES FOR XML GEN HERE********************
****************************************************************/

if($_GET['graphtitle'] != "") {
	$gtitle = $_GET['graphtitle'];
} else {
	$gtitle = 'Training POD Graphs';
}

$labelroot = $doc->createElement('labels');
$labelroot = $root->appendChild($labelroot);
$value = $doc->createElement('label');
$value = $labelroot->appendChild($value);
$value->setAttribute('lid', 0);
$labvals = $doc->createElement('x');
$labvals = $value->appendChild($labvals);
$valuenode = $doc->createTextNode(0);
$valuenode = $labvals->appendChild($valuenode);
$labvals = $doc->createElement('y');
$labvals = $value->appendChild($labvals);
$valuenode = $doc->createTextNode(10);
$valuenode = $labvals->appendChild($valuenode);
$labvals = $doc->createElement('align');
$labvals = $value->appendChild($labvals);
$valuenode = $doc->createTextNode('center');
$valuenode = $labvals->appendChild($valuenode);
$labvals = $doc->createElement('text_size');
$labvals = $value->appendChild($labvals);
$valuenode = $doc->createTextNode(12);
$valuenode = $labvals->appendChild($valuenode);
$labvals = $doc->createElement('text');
$labvals = $value->appendChild($labvals);
$valuenode = $doc->createTextNode('<b>' . $gtitle . '</b>');
$valuenode = $labvals->appendChild($valuenode);

//displays tpods working and broken in %
if($_GET['report'] == 'tpods') {
	
	$deviceobj = new Device($_SESSION['sysid']);
	//total / operational
	$totals = $deviceobj->PODTotals();
	$totalpods = $totals[0];
	$workingpods = $totals[1];
	$brokenpods = $totalpods - $workingpods;
	
	// add node for each row
	$occ = $doc->createElement('slice');
	$occ = $root->appendChild($occ);
	$occ->setAttribute('title', "Working Pods");
	$occ->setAttribute('pull_out', "true");
	$value = $doc->createTextNode($workingpods);
	$value = $occ->appendChild($value);
	
	// add node for each row
	$occ = $doc->createElement('slice');
	$occ = $root->appendChild($occ);
	$occ->setAttribute('title', "Broken Pods");
	$value = $doc->createTextNode($brokenpods);
	$value = $occ->appendChild($value);
}

//displays uncompleted modules for a particular jobrole
if($_GET['report'] == 'uncompleted') {
	
	$reportobj = new Report($_SESSION['sysid']);
	$module = new Module($_SESSION['sysid']);
	$modids = $module->getmodids($_SESSION['sysid']);

	$usertable = 'tpod08_users_sys' . $_SESSION['sysid'];
	$moduletable = 'tpod08_module_status' . $_SESSION['sysid'];
	
	//get object from result
	$k = 0;
	$modidstring = "";
	foreach($modids as $modid) {
		$modidstring .= ", SUM(CASE WHEN $moduletable.ModID = '$modid' then 1 else 0 end) AS modcount" . $modid . "";
	}
	//returns count of the uncompleted modules for each individual module via left join with the usertable and module table on userid
	$sql = "SELECT DISTINCT($moduletable.ModID) $modidstring FROM $usertable LEFT OUTER JOIN $moduletable ON $moduletable.UserID = $usertable.UserID WHERE $moduletable.Status = '1' AND $usertable.JobRole = '$jobrole' GROUP BY $moduletable.ModID ASC";
	
	$result = mysql_query( $sql, $db->mySQLConnection )
		or die( "Invalid query Line 67" );
		
	while($row_fields = mysql_fetch_array($result)) {
		$modname = $reportobj->getModuleName($row_fields[ModID]);
		$occ = $doc->createElement('slice');
		$occ = $root->appendChild($occ);
		$occ->setAttribute('title', $modname);
		$value = $doc->createTextNode($row_fields['modcount' + $row_fields[ModID]]);
		$value = $occ->appendChild($value);
		$k++;
	}

}

//displays completed modules for a particular jobrole
if($_GET['report'] == 'completed') {
	
	$reportobj = new Report($_SESSION['sysid']);
	$module = new Module($_SESSION['sysid']);
	$modids = $module->getmodids($_SESSION['sysid']);

	$usertable = 'tpod08_users_sys' . $_SESSION['sysid'];
	$moduletable = 'tpod08_module_status' . $_SESSION['sysid'];
	
	//get object from result
	$k = 0;
	$modidstring = "";
	foreach($modids as $modid) {
		$modidstring .= ", SUM(CASE WHEN $moduletable.ModID = '$modid' then 1 else 0 end) AS modcount" . $modid . "";
	}
	
	//returns count of the completed modules for each individual module via left join with the usertable and module table on userid
	$sql = "SELECT DISTINCT($moduletable.ModID) $modidstring FROM $usertable LEFT OUTER JOIN $moduletable ON $moduletable.UserID = $usertable.UserID WHERE $moduletable.Status IN('2','3') AND $usertable.JobRole = '$jobrole' GROUP BY $moduletable.ModID ASC";
	
	$result = mysql_query( $sql, $db->mySQLConnection )
		or die( "Invalid query Line 138" );
		
	while($row_fields = mysql_fetch_array($result)) {
		$modname = $reportobj->getModuleName($row_fields[ModID]);
		$occ = $doc->createElement('slice');
		$occ = $root->appendChild($occ);
		$occ->setAttribute('title', $modname);
		$value = $doc->createTextNode($row_fields['modcount' + $row_fields[ModID]]);
		$value = $occ->appendChild($value);
		$k++;
	}

}

//displays % of completed and uncompleted for a particular user
if($_GET['report'] == 'usercompleted') {
	
	$reportobj = new Report($_SESSION['sysid']);
	$module = new Module($_SESSION['sysid']);

	$moduletable = 'tpod08_module_status' . $_SESSION['sysid'];

	$sql = "SELECT SUM(CASE WHEN $moduletable.Status = 1 then 1 else 0 end) AS uncompleted, SUM(CASE WHEN $moduletable.Status IN('2','3') then 1 else 0 end) AS completed FROM $moduletable WHERE UserID = '$_GET[userid]' GROUP BY UserID ASC";
	$result = mysql_query( $sql, $db->mySQLConnection )
		or die( "Invalid query Line 67" );
	
	//place new xml node in for completed and uncompleted to complete the pie chart. no need for a loop as only returns one record!
	$row_fields = mysql_fetch_array($result);
	// add node for each row
	$occ = $doc->createElement('slice');
	$occ = $root->appendChild($occ);
	$occ->setAttribute('title', "Completed Modules");
	$occ->setAttribute('pull_out', "true");
	$value = $doc->createTextNode($row_fields['completed']);
	$value = $occ->appendChild($value);
	
	// add node for each row
	$occ = $doc->createElement('slice');
	$occ = $root->appendChild($occ);
	$occ->setAttribute('title', "Uncompleted Modules");
	$value = $doc->createTextNode($row_fields['uncompleted']);
	$value = $occ->appendChild($value);

}

//shows modules passed by certain job role
if($_GET['report'] == 'passed') {
	
	$reportobj = new Report($_SESSION['sysid']);
	$module = new Module($_SESSION['sysid']);
	$modids = $module->getmodids($_SESSION['sysid']);

	$usertable = 'tpod08_users_sys' . $_SESSION['sysid'];
	$moduletable = 'tpod08_module_status' . $_SESSION['sysid'];
	$passperc = $_GET['passperc'];
	echo $passperc;
	
	//get object from result
	$k = 0;
	$modidstring = "";
	foreach($modids as $modid) {
		$modidstring .= ", SUM(CASE WHEN $moduletable.ModID = '$modid' then 1 else 0 end) AS modcount" . $modid . "";
	}
	
	$sql = "SELECT DISTINCT($moduletable.ModID) $modidstring FROM $usertable LEFT OUTER JOIN $moduletable ON $moduletable.UserID = $usertable.UserID WHERE $moduletable.Status = '3' AND $usertable.JobRole = '$jobrole' AND $moduletable.Percent >= '$passperc' GROUP BY $moduletable.ModID ASC";
	
	$result = mysql_query( $sql, $db->mySQLConnection )
		or die( "Invalid query Line 67" );
		
	while($row_fields = mysql_fetch_array($result)) {
		$modname = $reportobj->getModuleName($row_fields[ModID]);
		$occ = $doc->createElement('slice');
		$occ = $root->appendChild($occ);
		$occ->setAttribute('title', $modname);
		$value = $doc->createTextNode($row_fields['modcount' + $row_fields[ModID]]);
		$value = $occ->appendChild($value);
		$k++;
	}

}

// get completed xml document
$doc->encoding = 'utf-8';
$xml_string = $doc->saveXML();

echo $xml_string;
?>
