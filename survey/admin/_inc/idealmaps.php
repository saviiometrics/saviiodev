<?php
	$allmodules = $adminobj->getALLModules();
	$MAPobj = new MAP();
?>
<div id="mapbar"><div class="iconfloat"><img src="<?=ADMIN_ACCOUNT_PATH;?>_images/_icons/IC_ViewUsage.png" /></div> <div class="floatleft"><?=_("Manage idealMAPs");?></div></div>
<div class="titlebar_shadow"></div>
<div id="content_pad">
	<h1 class="MWxHeader"><?=_("Manage idealMAPs");?></h1>
	<div id="IMAPBox" class="MAPsBox">
		<div class="addpad">
			<div class="cicon" style="padding:10px;"><img src="<?=ADMIN_ACCOUNT_PATH;?>_images/_icons/IC_AddCompany.png" /></div>
			<h2><?=_('Edit idealMAP');?></h2>
			<p>
				<?=_("Currently it is only possible to edit the name and participants involved in the idealMAP.");?>
			</p>
			<div id="addTMAPheader"><?=_('idealMAP Details');?></div>
			<form name="imapeditform" id="imapeditform">
				<table border="0" cellpadding="3" cellspacing="2" width="724">
					<tr>
						<td><?=_("idealMAP Name");?></td>
						<td align="right"><input type="text" name="editimapname" id="editimapname" class="validate[required,length[5,50]] textfield" size="30" /></td>
					</tr>
					<tr>
						<td valign="top"><?=_("idealMAP Participants");?></td>
						<td align="right"><textarea name="editimapParts" id="editimapParts" cols="32" rows="4" class="validate[required,length[5,500]] textfield" /></textarea></td>
					</tr>
				</table>
			</form>
			<p>
				<button class="SaveEditIMAP"> <?=_('Update idealMAP');?> </button>
				<button class="close"> <?=_('Close');?> </button>
			</p>
		</div>
	</div>
	
	<p><?=_("Welcome to the Ideal MAP Page. From here you can add idealMAPs as well as delete and edit existing ones. Like all other types of MAP such as a talentMAP you can share idealMAPs with other administrators. More information on how to share accounts can be found on the accounts page.");?></p>
	<div style="overflow:hidden;">
		<table cellspacing="1" cellpadding="0" class="tablesorter">
			<thead>
				<tr>
					<th><?=_("idealMAP Name");?></th>
					<th><?=_("Participants");?></th>
					<th><?=_("Created By");?></th>
					<th><?=_("Created On");?></th>
					<th><?=_("Access Controls");?></th>
				</tr>
			</thead>
			<tbody>
			<?php
				$access = $adminobj->getLevelAccess('imap');
				if($access['VIEW'] == 'MA') {
					$compid = $adminobj->headid;
				} else {
					$compid = $adminobj->companyid;
				}
				$imaplist = $MAPobj->getidealMAPList($compid, $access);
			
				$shareaccess = $adminobj->getAccountShareAccess('imap');
				function checkAccess($acctype, $html, $imaparr, $adminid, $headid, $companyid, $checktype, $sharerights) {
					switch($acctype) {
						case 'ALL':
						$adminstring = $html;
						break;
						case 'CREATED':
						if($imaparr['uid'] == $adminid || $imaparr['owneruid'] == $adminid) {
								$adminstring = $html;
						} else {
							if($sharerights[$imaparr['companyid']][$checktype] == 'TRUE') {
								$adminstring = $html;
							}
						}
						break;
						case 'MA':
						if($imaparr['headid'] == $headid) {
							$adminstring = $html;
						} else {
							if($sharerights[$imaparr['companyid']][$checktype] == 'TRUE') {
								$adminstring = $html;
							}
						}
						break;
						case 'SA':
						if($imaparr['companyid'] == $companyid) {
							$adminstring = $html;
						} else {
							if($sharerights[$imaparr['companyid']][$checktype] == 'TRUE') {
								$adminstring = $html;
							}
						}
						break;
					}
					return $adminstring;
				}
				
				$tmapheader = 'iMAP';
				if(!empty($imaplist)) {
					foreach($imaplist as $imapid => $imaparr) {

						$adminstring = '';
						$editimapHTML = '<a href="javascript:void(0);" class="editiMAP" id="imap_' . $imapid . '"><img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_EditApplication.png" alt="Edit ' . $tmapheader . '" title="Edit ' . $tmapheader . '" /></a>&nbsp;&nbsp;';
						$deleteimapHTML = '<a href="javascript:void(0);" class="deleteiMAP" id="imapdel_' . $imapid . '"><img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_EraserMinus.png" alt="Delete ' . $tmapheader . '" title="Delete ' . $tmapheader . '" /></a>&nbsp;&nbsp;';
						
						$adminstring .= checkAccess($access['EDIT'], $editimapHTML, $imaparr, $adminobj->uid, $adminobj->headid, $adminobj->companyid, 'EDIT', $shareaccess);
						$adminstring .= checkAccess($access['DELETE'], $deleteimapHTML, $imaparr, $adminobj->uid, $adminobj->headid, $adminobj->companyid, 'DELETE', $shareaccess);

						if($adminstring == '') {
							$adminstring = _('Read Only');
						}
						
						echo '<tr>' . "\n";
						echo '<td>' . $imaparr['imapname'] . '</td>' . "\n";
						echo '<td><a class="userlist">User List</a><div class="tooltip"><div class="tooltiptop"></div><div class="tooltipmid">' . $imaparr['participants'] . '</div><div class="tooltipbtm"></div></div></td>' . "\n";
						echo '<td>' . $imaparr['creator'] . '</td>' . "\n";
						echo '<td>' . date("d/m/Y", $imaparr['created']) . '</td>' . "\n";
						echo '<td><div class="makerelative">' . $adminstring . '</div></td>' . "\n";
						echo '</tr>' . "\n";
					}
				}
			?>
			</tbody>
			<tfoot>
				<tr>
					<th><?=_("idealMAP Name");?></th>
					<th><?=_("Participants");?></th>
					<th><?=_("Created By");?></th>
					<th><?=_("Created On");?></th>
					<th><?=_("Access Controls");?></th>
				</tr>
			</tfoot>
			<tbody>
		</table>
	</div>

	<div id="idealmap" style="overflow:hidden;">
		<h1 class="MWxHeader"><?=_("Create an idealMAP");?></h1>
			<p><?=_("To create an idealMAP simply click each of the four coloured tabs in turn which will display the questions for that particular factor.  Once a factor is shown you will see each of the preferences within that factor. If you would like a more compact view of the questions simply click the preference bar to hide and reveal the questions for that preference. As when taking a real MAP survey, you can then simply drag the icon or click to where you would like to answer the statement pair.");?></p>
		  <div id="idealfrmholder">
      	<div id="idealadd_inner">
      		<form id="idealMAP_FRM" name="idealMAP_FRM">
	          <table width="300" border="0" cellspacing="0" cellpadding="3">
	            <tr>
	              <td valign="top"><strong>idealMAP Name</strong></td>
	            </tr>
	            <tr>
	              <td valign="top"><input type="text" name="imapname" id="imapname" class="validate[required,length[5,50]] textfield" /></td>
	            </tr>
	            <tr>
	              <td valign="top"><strong>Participants</strong></td>
	            </tr>
	            <tr>
	              <td valign="top"><textarea name="participants" id="participants" cols="30" rows="5" class="validate[required,length[5,500]] textfield"></textarea></td>
	            </tr>
	           </table>
	         </form>
    		</div>
    		<div id="idealcontrol">
    			<div id="idealbuttons">
    				<a class="orgtab_30" href="javascript:void(0);" id="addideal">Add idealMAP</a>
    				<a class="orgtab_30" href="javascript:void(0);" id="resetideal">Reset</a>
    			</div>
    			<div class="cleaner"></div>
    			<div style="float:left; width:300px;">
	    			<table border="0" cellpadding="3" cellspacing="0">
	    				<tr>
	    					<td width="264"><strong><?=_("Total Completion");?></strong></td>
	    					<td id="prog_total">0%</td>
	    				</tr>
	    				<tr>
	    					<td class="rel_text"><?=_("Relationships");?></td>
	    					<td id="cid_1">0%</td>
	    				</tr>
	    				<tr>
	    					<td class="tap_text"><?=_("Thinking & Planning");?></td>
	    					<td id="cid_2">0%</td>
	    				</tr>
	    				<tr>
	    					<td class="mkd_text"><?=_("Making Decisions");?></td>
	    					<td id="cid_3">0%</td>
	    				</tr>
	    				<tr>
	    					<td class="gtd_text"><?=_("Getting Things Done");?></td>
	    					<td id="cid_4">0%</td>
	    				</tr>
	    			</table>
    			</div>
    		</div>
      </div>
      <div class="cleaner"></div>

		<div class="demo">
			<div id="tabs">
				<ul>
					<li id="reltab"><a href="#tabs-1"><?=_("Relationships");?></a></li>
					<li id="taptab"><a href="#tabs-2"><?=_("Thinking & Planning");?></a></li>
					<li id="mkdtab"><a href="#tabs-3"><?=_("Making Decisions");?></a></li>
					<li id="gtdtab"><a href="#tabs-4"><?=_("Getting Things Done");?></a></li>
				</ul>
				<?php
				$cids = $MAPobj->getClusters();
				foreach($cids as $cid => $clustername) {
					switch($cid) {
						case 1:
						$st_prefix = 'rel';
						break;
						case 2:
						$st_prefix = 'tap';
						break;
						case 3:
						$st_prefix = 'mkd';
						break;
						case 4:
						$st_prefix = 'gtd';
						break;
					}
					echo '<div id="tabs-' . $cid . '">' . "\n";
					$responses = $adminobj->getClusterResponses($cid);
					foreach($responses as $fg => $factorarr) {
						$i = 0;
						foreach($factorarr as $qid => $responsearr) {
							if($i == 0) {
								echo '<a href="javascript:void(0);" class="expcol"><div id="' . $st_prefix . 'prefbar"><div class="ft_left">' . _($responsearr['l']['factor_text']) . '</div><div class="ft_right">' . _($responsearr['r']['factor_text']) . '</div></div></a>';
								echo '<div class="pref_holder">';
							}
							echo '<div class="left_response">' . _($responsearr['l']['response']) . '</div><div class="IMslider" id="qid_' . $qid . '-fg_' . $fg . '-cid_' . $cid . '"></div><div class="right_response">' .  _($responsearr['r']['response']) . '</div><div class="cleaner"></div>';
							$i++;
						}
						echo '</div>';
					}
					echo '</div>' . "\n";
				}
				?>
			</div>
		
		</div><!-- End demo -->
	</div>
	<div class="cleaner"></div>
</div>