<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=$accobj->pagetitle;?> - V<?=CURR_VERSION;?></title>
<link type="text/css" href="/min/?g=static_css&<?=VER_JSTAG;?>" rel="stylesheet" media="all" />

<?php
if($_SESSION['locale'] != 'ar_SA' && $_SESSION['locale'] != 'zh_CN') {
	$jsGroups = 'static';
} else {
	$jsGroups = 'static';
}
$jsIncMe = false;
$jsFile = '&f=_js/main/header-packed.js';

switch($_GET['saviio']) {
	case 'analytics':
	$jsIncMe = true;
	$jsIncFile = '_js/main/analytics.packed.js';
	break;
	case 'map':
	$jsIncMe = true;
	$jsIncFile = '_js/main/map-min.js';
	break;
	case 'mysettings':
	$jsIncMe = true;
	$jsIncFile = '_js/main/mysettings-min.js';
	break;
}
?>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?=$userobj->staticURL;?>min/?g=<?=$jsGroups;?><?=$jsFile;?>&<?=VER_JSTAG;?>"></script>

<?php
	if($jsIncMe == true) {
		echo '<script type="text/javascript" src="' . $userobj->staticURL . 'min/?f=' . $jsIncFile . '"></script>' . "\n";
	}
	$validation_ab = substr($_SESSION['locale'], 0, -3);
	echo '<script type="text/javascript" src="' . $userobj->staticURL. 'min/?f=_js/jquery.validationEngine-' . $validation_ab . '.js"></script>' . "\n";
?>
<script type="text/javascript" src="http://use.typekit.com/iti7bdl.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-16976883-2']);
  _gaq.push(['_setDomainName', '.saviio.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!-- SNAP A BUG --->
<script type="text/javascript">
document.write(unescape("%3Cscript src='" + ((document.location.protocol=="https:")?"https://snapabug.appspot.com":"http://www.snapengage.com") + "/snapabug.js' type='text/javascript'%3E%3C/script%3E"));</script><script type="text/javascript">
SnapABug.addButton("1eb3fe03-f6bc-4b78-a538-0c20d008e399","0","55%");
</script>

<?php
	//only shows the flash header on the main page or when saviio var isnt set (same as main)
	if(!isset($_GET['saviio']) || $_GET['saviio'] == 'main') {
?>
	<script type="text/javascript">
	  $(function() {
			var flashvars = {};
			var params = {};
			params.wmode = "transparent";
			var attributes = {};				   
			swfobject.embedSWF("kite_standalone.swf", 'kitediv', "900", "188", "9.0.0", false, flashvars, params, attributes);
	  });
	</script>
<?php 
}
?>

</head>
<body>
<input type="hidden" name="currLocale" id="currLocale" value="<?=$_SESSION['locale'];?>" />
<input type="hidden" name="accid" id="accid" value="<?=$userobj->accid;?>" />
<?php if($userobj->accid == 17) {
echo '<form style="display:none;" id="redirect" action="http://samknowhow.saviio.com/" method="post" name="savloginmain">
	<input type="hidden" name="savuser" value="' . $_REQUEST["savuser"] . '">
	<input type="hidden" name="savpass" value="' . $_REQUEST["savpass"] . '">
	<input id="firtlogin" class="loginbutton" type="hidden" name="firstlogin" value="Login">
</form>';
}?>
<div id="sessionWarning" class="bottom-right"><span></span></div>
<div id="loginBox" class="simple_overlay">
	<div class="addpad">
		<h2><?=_('Login');?></h2>
		<p>
			<?=_("You are seeing this box because your sessions have timed out and you have been logged out. To continue, login again here and you will be returned to the exact state in which you left the webpage. Please note as long as the page is not reloaded manually no data will be lost.");?>
		</p>
		<div class="overlayHeader"><?=_('Login Box');?></div>
		<form name="reloginform" id="reloginform">
			<table border="0" cellpadding="3" cellspacing="2" width="740">
				<tr>
					<td><?=_("Username");?></td>
					<td align="right"><input type="text" name="reloguser" id="reloguser" class="validate[required,length[1,50]] textfield" size="30" /></td>
				</tr>
				<tr>
					<td valign="top"><?=_("Password");?></td>
					<td align="right"><input type="password" name="relogpass" id="relogpass" class="validate[required,length[1,50]] textfield" size="30" /></td>
				</tr>
			</table>
		</form>
		<p>
			<button class="reLogNow"> <?=_('Login');?> </button>
		</p>
		<p><strong>Note:</strong> <?php printf(_('If you cannot remeber your login then please proceed to the <a href="%s">Home Page</a> where you can login again.'), $userobj->surveyURL);?></p>
	</div>
</div>

<?php
//had to put this here due to z-index issues. Being above the container means there are no problems with other elements being on top. Think it was mainly an issue with ie7 due to the z-index bug.
if($_GET['saviio'] == 'analytics') {
?>
<div id="mapsDisplay" class="simple_overlay">
	<input type="hidden" id="CurrentColumn" value='1' />
	<input type="hidden" id="MapType" value='' />
	<input type="hidden" id="Target" value='' />
	<input type="hidden" id="Start" value='' />
	<input type="hidden" id="SingleSelect" value="0" />
	<input type="hidden" id="selTalentMAP" value="<?php printf(_("Select %s"), "talentMAPs");?>" />
	<input type="hidden" id="selTeamMAP" value="<?php printf(_("Select %s"), "teamMAPs");?>" />
	<input type="hidden" id="selMAP" value="<?php printf(_("Select %s"), "MAPs");?>" />
	<input type="hidden" id="selIdealMAP" value="<?php printf(_("Select %s"), "idealMAPs");?>" />
	<div class="Search">
		<div id="selectMAPsHeader" class="blueMyriadLarge">
			<?=_("Select MAPs");?>
		</div>
		<div id="mapsDisplaycloseBtn">
			<a href="javascript:void(0);" class="close"><img src="<?=ACCOUNT_PATH;?>_images/_map/closeBtnSelMAP.png" /></a>
		</div>
		<div class="cleaner"></div>
		<div id="viewModeFloat">
			<select id="ViewMode">
				<option value="thumbs">Thumbnail View</option>
				<option value="list">List View</option>
			</select>
			&nbsp;&nbsp;&nbsp;<span style="font-weight:bold; color:#5e9fd6; font-size:14px;"><?=_("Search");?>:</span>&nbsp;<input type="text" id="SearchInput" />
		</div>
	</div>
	<div class="Contents"></div>
	<div id="selCloseMapsDisplay"><a href="javascript:void(0);" class="close blueButtonAL"><?=_("Select and Close");?></a></div>
	<div class="cleaner"></div>
	<div class="CloseDiv">
		<a href="javascript:void(0);" id="viewMoreButton"><?= _("View More") ?></a>
	</div>
</div>
<div id="tooltips"></div>
	<div id="TMAPbox">
		<div class="addpad">
			<div class="cicon" style="padding:10px;"></div>
			<h2><?=_("Edit this MAP");?></h2>
			<br />
			<strong>&nbsp;<?= _("Current People in MAP are selected."); ?></strong>
			<p style="padding:5px;"><?php printf(_("Below is list of MAP's available for you to choose from in order to amend your TalentMAP or TeamMAP. Simply drag and drop from the available list (on the left) to the selected list (on the right). Alternatively you can simply click the + icon on the name tab to add the desired item to the selected list."));?></p>
			<div id="addMAPbox">
			  <select id="editTMAP" class="multiselect" multiple="multiple" name="editTMAP[]"></select>
			</div>
	    <div class="cleaner"></div>
			<p>
				<?=_('To save the new MAP click the "Save" Button, else click "close" or press ESC to close the dialog box.');?>
			</p>
			<!-- yes/no buttons -->
			<p>
				<button class="updatetmap"> <?=_('Update');?> </button>
			</p>
			<p>
				<?=_('Alternatively you can also choose to save this talentMAP or teamMAP under a different name. This will create a brand new talentMAP or teamMAP');?>
			</p>
			<!-- yes/no buttons -->
			<p>
				<input name="ntmapname" id="ntmapname" type="text" /> <button class="newtmap" id="newtmap"> <?=_('Save talentMAP/teamMAP');?> </button>
			</p>
			<p>
				<button class="close"> <?=_('Close');?> </button>
			</p>
		</div>
	</div>
<?php
}
?>

<!--	SETTINGS BOXS FOR AVATAR SELECTION (mysettings) -->
<div class="avatarPBox" id="FB_Box">
	<div class="floatleft medHeader" style="margin-top:1px;">Facebook</div>
	<div id="closeBtnForgot"><a href="javascript:void(0);" class="closeAvatar"></a></div>
	<div class="cleaner"></div>
	<p><?= _("Simply enter in the name of your Facebook user account and click update:"); ?></p>
	<input type="text" name="FBuser" id="FBuser" class="textfield" /><br /><br />
	<a href="javascript:void(0);" class="changeType blueButton" id="goFB"><?=_("Update Avatar");?></a>
</div>
<div class="avatarPBox" id="TW_Box">
	<div class="floatleft medHeader" style="margin-top:1px;">Twitter</div>
	<div class="cleaner"></div>
	<div id="closeBtnForgot"><a href="javascript:void(0);" class="closeAvatar"></a></div>
	<p><?= _("Simply enter in the name of your Twitter user account and click update:"); ?></p>
	<input type="text" name="TWuser" id="TWuser" class="textfield" /><br /><br />
	<a href="javascript:void(0);" class="changeType blueButton" id="goTW"><?=_("Update Avatar");?></a>
</div>
<div class="avatarPBox" id="GR_Box">
	<div class="floatleft medHeader" style="margin-top:1px;">Gravatar</div>
	<div class="cleaner"></div>
	<div id="closeBtnForgot"><a href="javascript:void(0);" class="closeAvatar"></a></div>
	<p><?= str_replace("gravatar.com", "<a href='www.gravatar.com' target='_blank'>gravatar.com</a>", _("Gravatar uses your current email address to generate your image. If you don't have an account you can signup at gravatar.com")); ?></p>
	<a href="javascript:void(0);" class="changeType blueButton" id="goGR"><?=_("Use Gravatar");?></a>
</div>
<div class="avatarPBox" id="UU_Box">
	<div class="floatleft medHeader" style="margin-top:1px;">Image Upload</div>
	<div id="closeBtnForgot"><a href="javascript:void(0);" class="closeAvatar"></a></div>
	<div class="cleaner"></div>
	<p><?= _("Click below to select an image you wish you use as your profile picture:"); ?></p>
	<a href="javascript:void(0);" id="avaUpload"><?=_("Select an Image");?></a>&nbsp;<div id="picShowUL"></div><br />
	<a href="javascript:void(0);" class="uploadAvaToCDN"><?=_("Upload Photo");?></a>
	<a href="javascript:void(0);" class="changeType blueButton" id="goFB"><?=_("Upload Image");?></a>
</div>

<div id="topcloudbg"></div>
<div id="topMenuHolder"></div>
<div id="kitediv"><img src="<?=ACCOUNT_PATH;?>_images/static_kitelogo.png" width="900" height="188"></div>
<div id="sortoverlay"><br /><?=_("Please wait...");?><br /><br /><img src="<?=ACCOUNT_PATH;?>_images/ajax-loader.gif" alt="ajax_loader" /></div>
<div id="container">
	
<div id="forgottenpass_box">
	<strong><?=_('Forgotten Password?');?></strong><br />
	<p><?=_('Please enter your username below and an email will be sent to the email address registered to your account containing your new password. If you cannot remember your username, please contact the administrator who setup your account.');?></p>
	<strong><?=_('Username');?></strong>
	<input type="text" id="lostusername" name="lostusername" class="textfield" /><br /><br />
	<div id="forgotpass_button"><a href="javascript:void(0);" id="resetpass">Reset Password</a></div>
	<div id="closeBtnForgot"><a href="javascript:void(0);" class="closeme"></a></div>
	<br />
</div>

<div id="settingsbox">
	<a href="/saviio/mysettings">My Info</a>
</div>

	<?php 
	/************************************************* GET CURRENT LANG OBJECT BASED ON LOCALE ******************************************/
	$currLangObj = $userobj->langviaLocale($locale);
	if($currLangObj->countryid == '') {
		//default 242 is english (uk flag)
		$currpic = '242.gif';
		$currlangName = _('English');
	} else {
		$currpic = $currLangObj->countryid . '.gif';
		$currlangName = $currLangObj->lang_name;
	}
	?>

		<div id="header">
	   <div id="loginbox">
	  	<form action="?saviio=main" method="post" name="firstlogin" id="firstlogin">
	  		<strong><?=_("Username");?></strong><br /><input type="textfield" name="savuser" id="savuser" class="textfield" style="width:248px;" /><br />
	  		<strong><?=_("Password");?></strong><br /><input type="password" name="savpass" id="savpass" class="textfield" style="width:248px;" /><br /><br />
	  		<input type="submit" value="Login" id="firstlogin" name="firstlogin" class="loginbutton" /> 
	  		<div class="forgPass"><a href="javascript:void(0);" id="forgotpass">Forgotten Password?</a></div>
	  		<div id="closeBtnLogin"><a href="javascript:void(0);" class="closeme"></a></div>
	  	</form>
	  </div>
  	<div id="mainlogo"><a href="<?=$userobj->surveyURL;?>"><img src="<?=ACCOUNT_PATH;?>_images/main_logo.png" alt="Saviio MAPs" /></a></div>
      <div class="cleaner"></div>
      <?php $allowed = false; ?>
      <div id="menu_holder">
        <ul class="savmenu">
        	<?php
        	if(isset($_SESSION['username'])) {
        		?>
            <li><a href="/saviio/map"<?php if($_GET['saviio'] == 'map') { echo ' class="current"'; } ?>><b><?=_("Survey");?></b></a></li>
						<?php
            if(checkLevel('analytics', $userobj)) {
            ?>
            <li><a href="/saviio/analytics"<?php if($_GET['saviio'] == 'analytics') { echo ' class="current"'; } ?>><b><?=_("Analytics");?></b></a></li>
            <?php
          	}
          	?>
          	<?php
            if(checkLevel('admin', $userobj)) {
            ?>
            <li><a href="/admin/"><b><?=_("Admin");?></b></a></li>
            <?php
            }
          }
          ?>
        </ul>
      </div>
      <div id="rightLangSettingsBar">
      	<?php 
      	if(isset($_SESSION['username'])) {
      		echo '<div class="floatleft">' . $userobj->firstname . ' ' . $userobj->surname . ' ( <a href="' . $userobj->surveyURL . '?saviio=main&amp;logout=1">' . _("Logout") . '</a> )</div>' . "\n";
      		echo '<div class="cleaner"></div>' . "\n";
      		echo '<div id="settingsFloat"><a href="javascript:void(0);" class="showSettings">' . _("Settings") . '</a></div>' . "\n";
      	} else {
      		echo '<div class="floatright">&nbsp;<span style="color:#b91f1f; font-weight:bold;">' . $login_error . '</span></div>' . "\n";
      		echo '<div class="cleaner"></div>' . "\n";
      		echo '<div id="loginFloat">' . _("Login") . '</div>' . "\n";
      	}
      	?>
				<div id="helpFloat"><a href="http://help.saviio.com/" target="_blank">FAQ</a></div>
				<?php if(isset($_GET['saviio']) && strtolower($_GET['saviio']) == 'analytics' && isset($_SESSION['username'])) { ?>
				<div id="keepSessionAliveDiv" class="unchecked"><input type="checkbox" id="keepSessionAlive" />
					<div id='keepSessionAliveDivText'><?= _("Keep me logged in"); ?></div>
					<div id='keepSessionAliveDivImg' style="display: none;"><?= _("Logout"); ?></div>
				</div>
				<?php } ?>
			  <div id="lang_select">
			  	<div class="langselectFloat"><img src="<?=ACCOUNT_PATH;?>_images/_flags/<?=$currpic;?>" alt="flag" /></div>
			  	<div class="floatleft"><?=_($currlangName);?></div>
			    <div id="langselbox">
			      <div id="langmenu">
			      	<ul>
			      	<?php 
			      	if(count($_GET) == 0) {
			      		$delim = '';
			      	} else if (count($_GET) == 1) {
			      		if(array_key_exists('locale', $_GET)) {
			      			$delim = '';
			      		} else {
			      			$delim = '&amp;';
			      		}
			      	} else {
			      		$delim = '&amp;';
			      	}
			      	//remember to get all the languages.. you have to remove the accid from the function call. If the accid is there it gets all the langs available for that account.
			      	$langs = $userobj->getLanguages($_SESSION['accid']);
			      	foreach($langs as $langid => $langInfo) {
			      		echo '<li><div class="flag_' . $langInfo['countryid'] . '"></div><a href="/' . $actionurl . $delim . 'locale=' . $langInfo['locale'] . '" id="' . $langInfo['locale'] . '">' . _($langInfo['lang_name']) . '</a></li>' . "\n";
			      	}
			      	?>
						</ul>
						</div>
			    </div>
			  </div>
      </div>
  	</div>
  <div id="content_top"></div>
  <div id="content_mid">
 
  <?php
    //
    //putting this here becasuse I can't think of anywhere else for it to go 
  	if(isset($_SESSION['username'])) {
      		$_SESSION['cuFname'] = $userobj->firstname;
      		$_SESSION['cuLname'] = $userobj->surname;
      	}?>
