<?php
require_once("site.config.php");

//see if this versions
if(!isset($_SESSION)) { session_start(); }
$phpsessid = session_id();
$start_time=microtime(true);
//ini_set('display_errors',1);


/******************************* CHECK SESSION IS SET AND THAT ARR DEV ARRAY IS ACTUALLY AN ARRAY!!!! ******************************/

//get factor scores set from AjaXGen Script!
$arrFactorScores = $_SESSION['arrFactorScores'];
$arrDeviations = $_SESSION['arrDeviations'];

if(!isset($_SESSION['arrFactorScores']) || !is_array($_SESSION['arrDeviations']) || !isset($_SESSION['arrDeviations']) || !is_array($_SESSION['arrFactorScores'])) {
    printf (_("I'm sorry, there seems to be a problem generating your PDF because your sessions have timed out or been reset! Please close this window and re login to your %s account. Thank You"), 'Saviio.');
    exit;
} else {
    //include localization and site config files
	//include DB AND ACCOUNT INFO CLASSES
	include CONTENT_PATH . '/_classes/db-class.php';
	include CONTENT_PATH . '/_classes/account-class.php';
	$accobj = new Account($_SESSION['accid']);
	
	//include other classes
	include FULL_PATH . '/_inc/_classes/question-class.php';
	include FULL_PATH . '/_inc/_classes/MAP-class.php';
	include FULL_PATH . '/_inc/_classes/user-class.php';
	include FULL_PATH . '/_inc/_classes/admin-class.php';
	include FULL_PATH . '/_inc/_classes/pdf-class.php';
	
	//$_GET['pdflocale'] = 'da_DK';
	require_once(FULL_PATH . "/_inc/localization.php");
	require_once(FULL_PATH . "/_inc/scripts.php");
	
	$dispPDFObj = new DisplayPDF();
	$fontn = $dispPDFObj->getFont($_SESSION['locale']);
	$_SESSION['fontn'] = $fontn;
	if(isset($_REQUEST['orientation']) && $_REQUEST['orientation'] == 'landscape') {
		$PDF_PAGE_ORIENTATION = 'L';
		$height = 16.8;
		$width = 55.5;
		$clusttoptenwidth1 = "3.3%";
		$clusttoptenwidth2 = "90.7%";
		$matchedStatements = "90.7%";
		$clusttoptenwidth3 = "5.7%";
		$clusttoptenwidth4 = "0.3%";
		$clusterBar1 = "92.7%";
		$clusterBar2 = "0.8%";
		$clusterBar3 = "5.7%";
	} else {
		$PDF_PAGE_ORIENTATION = 'P';
		$height = 16.8;
		$width = 55.5;
		$clusttoptenwidth1 = "30";
		$clusttoptenwidth2 = "540";
		$matchedStatements = "550";
		$clusttoptenwidth3 = "52.5";
		$clusttoptenwidth4 = "0";
		$clusterBar1 = "582";
		$clusterBar2 = "4";
		$clusterBar3 = "52.5";
	}
	$clusterPredictorCount = array();
		$clusterPredictorCount[1] = 0;
		$clusterPredictorCount[2] = 0;
		$clusterPredictorCount[3] = 0;
		$clusterPredictorCount[4] = 0;
	// Extend the TCPDF class to create custom Header and Footer
	
	// create new PDF document
	$pdf = new SaviioTCPDF($PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false, 16.8, 55.5);
	
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor(ACC_NAME);
	$pdf->SetTitle(ACC_NAME . ' Report');
	$pdf->SetSubject('Analytics Summary Report');
	$pdf->SetKeywords(ACC_NAME . ',analytics, summary, report');
	
	// set default header data
	//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
	
	// set header and footer fonts
	$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
	
	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	
	//set margins
	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
	
	//set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	
	//set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 

    $pdf->SetFont('DroidSansFallback', '', 12, '', false);
	
	$userobj = new Admin($_SESSION['uid']);
	$MAPobj = new MAP();
	$fids = $MAPobj->getfids();
	$fgs = $MAPobj->getFactorGroups();
	$cids = $MAPobj->getClusters();
	$stquestions = $_SESSION['stquestions'];
	
	$access = $userobj->getLevelAccess('tmap');
	if($access['EDIT'] == 'MA') {
		$compid = $userobj->headid;
	} else {
		$compid = $userobj->companyid;
	}
	
	if(!isset($_SESSION['fdescs'])) {
		$fdescs = $MAPobj->getFactDescs();
		$_SESSION['fdescs'] = $fdescs;
	} else {
		$fdescs = $_SESSION['fdescs'];
	}
	//$rankid = $_GET['rankID'];
	if(!isset($_SESSION['rankID'])) {
		$rankid = 0;
	} else {
		$rankid = $_SESSION['rankID'];
	}
	$_SESSION['rightname'] = $arrFactorScores[$rankid]['rightname'];
	$_SESSION['leftname'] = $arrFactorScores[$rankid]['leftname'];
	$_SESSION['rightemail'] = $arrFactorScores[$rankid]['rightemail'];
	$_SESSION['leftemail'] = $arrFactorScores[$rankid]['leftemail'];
	$_SESSION['rightissuedata'] = $arrFactorScores[$rankid]['rightissuedata'];
	$_SESSION['leftissuedata'] = $arrFactorScores[$rankid]['leftissuedata'];
	
	
	$currentUsername = $_SESSION['cuFname'] . ' ' . $_SESSION['cuLname'];
	
	$grey_rbg = $dispPDFObj->getGrey(true);
	
		
	$excel = $dispPDFObj->getExcellentColour();
	$good = $dispPDFObj->getGoodColour();
	$average = $dispPDFObj->getAverageColour();
	$poor = $dispPDFObj->getPoorColour();
	$clus_1 = $dispPDFObj->getCluster1Colour();
	$clus_2 = $dispPDFObj->getCluster2Colour();
	$clus_3 = $dispPDFObj->getCluster3Colour();
	$clus_4 = $dispPDFObj->getCluster4Colour();
	$top10bg = $dispPDFObj->getTop10BackgroundColour();

  // ---------------------------------------------------------
	
	// set font
	if($_SESSION['locale'] == 'ar_SA') {
		$pdf->SetFont($fontn, '', 18);
	} else {
		$pdf->SetFont($fontn, '', 16);
	}
	
	// add a page
	$pdf->AddPage();
	$timenow = time();
	$headerpagehtmlArray = $dispPDFObj->getFirstPage($PDF_PAGE_ORIENTATION, $currentUsername, $_SESSION['username'], $_SESSION['leftTypeDisp'], $_SESSION['leftname'], $_SESSION['leftemail'], $_SESSION['rightTypeDisp'], $_SESSION['rightname'], $_SESSION['rightemail'], $_SESSION['leftissuedata'], $_SESSION['rightissuedata']);
	
	// encode HTML
	// output the HTML content
	$pdf->writeHTMLCell(0, 0, '', 30, $headerpagehtmlArray['main'], 0, 1, 0, true, 'center');
	$pdf->SetFont($fontn, '', 10);
	$pdf->writeHTMLCell(0, 0, '', 130, $headerpagehtmlArray['mapdata'], 0, 1, 0, true, 'center');
	
	if(isset($_GET['incR'])) {
		// add a page
		$pdf->AddPage();
		
		// create some HTML content
		$namereplace = '<em>' . ACC_NAME . '</em>';
		$searchfor = array('%s', '%emS', '%strS', '%emE', '%strE');
		$replacewith = array('<em>' . ACC_NAME. '</em>', '<em>', '<strong>', '</em>', '</strong>');
		$htmlcontent = str_replace($searchfor, $replacewith, '<h1>' . _("Introduction to the %s reports") . '</h1>
		<strong>' . _("Purpose") . '</strong>:<br />
		' . _("Our online survey tools dramatically increase the probability of people succeeding in their role at work.") . "<ul>
		<li>" . _("We help improve employee engagement.") . "</li>
		<li>" . _("We help cut the cost of recruitment.") . "</li>
		<li>" . _("We help organisations make more profit.") . "</li>
		</ul>" .
		
		 "<p><strong>" ._("How?") . "</strong><br />" .
		_("By providing fresh insight to people at work that what really matters is how to create a working climate that plays to each persons' strengths.") . "</p>" .
		
		_("So, how is this approach different from other profiling tools?") . "<br />" .
		_("Most psychometric profiling tools assess the individual and compare the person with a normed benchmark - usually comprising of a collection of anonymous people who have completed the same survey in the past. Somewhere else.") . "<br />" .
		_("Not Saviio.") . "<br />" .
		_("We compare the individual with their manager, their team and their colleagues. Oh - and with people who are rather good in a similar role too. In the here and now - not the there and then.") . "<br />" .
		_("What matters most is providing insight to both manager AND employee so that they can improve mutual understanding around each other's motivations and preferences.") . '<br />
		<br />
		<strong>' . _("The Survey") . '</strong>:<br />
		' . _("The heart of the tool comprises 60 bipolar statements that inform 12 bipolar preference sets - organised into 4 main factors. When completed by the respondent, a MAP (Motivations and Preferences) is created. The MAP may be viewed in several ways:") . '
		<ol start="1" type="1">
		<li>' . _("By itself - without comparing against other individuals, teams or job profiles.") . '</li>
		<li>' . _("Comparison against a previous MAP from the same individual") . '</li>
		<li>' . _("Comparison against another individual, team or job profile.") . '</li>
		</ol>
		<p><strong>' . _("Benchmark Reporting") . '</strong>:<br />
		' . _("A benchmark can be established by generating responses from within an organisation and as such does not depend on normative data to generate comparisons. MAPs may be combined to produce benchmarks against which teams, talent, managers and individuals can all be compared for 'match' of 'fit'. When used for recruitment and selection, an individuals' MAP may be compared with a talentMAP (a job profile) that has been created for a specific job vacancy.") . '<br />
		<br />
		<strong>' . _("Theory:") . '</strong><br />
		' . _("The underlying theory supporting %s is based on NLP (Neuro Linguistic Programming) Meta Programs. Meta Programs are filters that determine how you perceive the world around you, and they have a major influence on how you communicate with others and the behaviours you manifest. <em>Meta</em> means over, beyond, above, or on a different level - i.e. operating at an unconscious level. Meta Programs are deep-rooted mental programs, which automatically filter our experience and guide and direct our thought processes, resulting in significant differences in behaviour from person to person. They define typical patterns in the strategies or thinking styles of an individual, group, company or culture.") . '</p>
		<p><strong>' . _("Report Summary") . '</strong><br />
		' . _("This report is presented as an independent assessment using survey results of the individual respondent named in this report. It has been prepared in good faith, based on the information provided by the individual through psychometric assessment. The survey responses provided by the respondent have been used to produce either an <em>individual</em> MAP or they have been compared with <em>another</em> MAP as a benchmark (e.g. a teamMAP or a talent MAP).") . '</p>
		<p><strong>' . _("To the Survey respondent:") . '</strong><br />
		' . _("The accuracy of this report will reflect the frankness of your responses as well as, to some extent, your self-awareness. You may receive personal feedback of the results contained in this report. Whoever gives this feedback to you is trained to interpret %s surveys and to use the feedback session to validate your profile. The %s advisor should also be able to answer any questions and provide more detailed interpretation and guidance on what the results mean to you.") . '</p>
		<p>' . _("The content of this report is based on the information that was supplied by you on your preferred ways of thinking, understanding information and decision-making when you completed %s. It reflects your preferences in the context stated when you completed the questionnaire - usually this is a work context.") . '</p>
		<p>' . _("The results set out in this report have a usefulness of between six to nine months. In most cases, patterns of individual preferences change only slowly over time, but in times of rapid change in your personal or working life your %s profile may develop more quickly.") . '</p>
		' . _("%s is only part of the overall assessment process alongside other essential elements:") . '
		<ol>
		<li>' . _("Face to face meeting and dialogue.") . '</li>
		<li>' . _("CV with accompanying evidence of achievement and reference sources provided.") . '</li>
		<li>' . _("Competency assessment (in relation to the role).") . '</li>
		<li>' . _("%s - Motivations and Preferences of the individual in <em>relation</em> to the role, the team and the manager.") . '</li>
		</ol>
		<p>' . _("Remember that the results set out here reflect preferences rather than abilities. %s will help you to understand <em>how</em> you prefer to operate - for example when making decisions. It will <strong><em>not</em></strong> assess your <em>ability</em> (for example, how <strong><em>good</em></strong> you are at making decisions).") . '</p>
		<p><strong>' . _("Liability") . '</strong><br />
		' . _("As motivations and preferences in an individual can change, the accuracy of the information contained in this report should be considered to be up to approximately nine months, depending upon the respondent's work role and personal circumstances. No liability is accepted for outcomes resulting from the use of this information contained within the report.") . '</p>
		');
		
		// output the HTML content
		$pdf->writeHTML($htmlcontent, true, 0, true, 0);
		
		//new page here?
		$pdf->AddPage();
		$htmlcontent = str_replace($searchfor, $replacewith, '<h1>' . _("What is %s?") . '</h1>
		<p><strong>' . _("%s - Motivation and Preferences that predict success") . '</strong><br />
		' . _("%s improves your individual, team and organisational performance by providing continuous insight into the unique combination of human factors that predict success.") . '<br />
		<br />
		<strong><em>%s MAPs</em></strong><br />
		' . _("%s MAPs (Motivation And Preferences) is an online \"Software as a Service\" (SaaS) that enables organisations to better understand how people are uniquely motivated in four major areas:") . '<br />
		  <ol>
		  	<li>' . _("Relationships") . '</li>
			<li>' . _("Thinking & Planning") . '</li>
			<li>' . _("Making Decisions") . '</li>
			<li>' . _("Getting Things Done") . '</li>
		  </ol>
		  ' . _("This insight allows individuals, managers and their teams to work more productively and succeed in their roles.") . '</p>
		<strong>' . _("How does %s work?") . '</strong><br />'. _("People make unconscious and conscious judgments about the people they meet during the assessment and selection process.") . '<br />');
		
		// output the HTML content
		$pdf->writeHTML($htmlcontent, true, 0, true, 0);
		
		//to indent had to change it to this.. also add different colour to block.. only way I think for now!
		$cellcontent = sprintf('<span color="#45525f">' . _("%s helps individuals to positively influence these judgments using predictive data to support 'gut feel'") . '</span><br />',$namereplace);
		$pdf->writeHTMLCell(0, 0, 20, '', $cellcontent, 0, 1, 0, true, 'L');
		
		//question / answer 2
		$htmlcontent = _("People want to feel that they will be doing a job that matters to them and where they will have 'significance' - it matters to others too.") . '<br />';
		$pdf->writeHTML($htmlcontent, true, 0, true, 0);
		
		$cellcontent = sprintf('<span color="#45525f">' . _("%s helps managers to understand the motivational preferences of team members and respond to meet those needs - people that feel understood feel valued and are more likely to stay. People generally leave managers - not organisations.") . '</span><br />',$namereplace);
		$pdf->writeHTMLCell(0, 0, 20, '', $cellcontent, 0, 1, 0, true, 'L');
		
		//question / answer 3
		$htmlcontent = _("People want to be part of something and feel that they \"fit in\" and belong.") . '<br />';
		$pdf->writeHTML($htmlcontent, true, 0, true, 0);
		
		$cellcontent = sprintf('<span color="#45525f">' . _("%s helps inform the dialogue between manager, coach and individual in ways that help people feel that they belong to something more than just a means to provide income.") . '</span><br />',$namereplace);
		$pdf->writeHTMLCell(0, 0, 20, '', $cellcontent, 0, 1, 0, true, 'L');
		
		//question / answer 4
		$htmlcontent = _("Once people feel that they 'fit in' and feel part of something that matters to them, they gain more intrinsic motivation from their job, feel more engaged, and are less open to changing their situation.") . '<br />';
		$pdf->writeHTML($htmlcontent, true, 0, true, 0);
		
		$cellcontent = sprintf('<span color="#45525f">' . _("%s helps managers to secure and develop people within their organisation.") . '</span><br />',$namereplace);
		$pdf->writeHTMLCell(0, 0, 20, '', $cellcontent, 0, 1, 0, true, 'L');
		
		//new page here?
		$pdf->AddPage();
		
		$htmlcontent = str_replace($searchfor, $replacewith, '<h1>' . _("Understanding This Report - <em>Factors & Preferences</em>") . '</h1>
		<p>' . _("%s displays the results of the survey you have completed in 12 sets of preferences (opposites) organised into four main factors or groups.") . '</p>
		  ' . $dispPDFObj->getRelationshipsTable());
			
		$pdf->writeHTML($htmlcontent, true, 0, true, 0);
		if($PDF_PAGE_ORIENTATION == 'L') {
			$pdf->AddPage();
		}
		
		$pdf->writeHTML($dispPDFObj->getThinkingAndPlanningTable(), true, 0, true, 0);
		$pdf->AddPage();
		
		$pdf->writeHTML($dispPDFObj->getmakingDecisionsTable(), true, 0, true, 0);
		if($PDF_PAGE_ORIENTATION == 'L') {
			$pdf->AddPage();
		}
		
		$pdf->writeHTML($dispPDFObj->getgettingThingsDoneTable(), true, 0, true, 0);
		$pdf->AddPage();
		
		$interpretingHTML = '<h1>' . _("Understanding This Report - <em>Interpreting The Symbols</em>") . '</h1>
		<p><strong>' . _("Behavioural Range Line") . '</strong><br/>
		' . _("According to NLP, behavioural flexibility is the 'ability to vary one's own behaviour in order to elicit, or secure, a response from another person'.") . ' ' . _("Behavioural flexibility can refer to the development of an entire range of responses to any given stimulus as opposed to having habitual, and therefore limiting responses, which would inhibit performance potential.") . '</p>
		<p>' . _("The behavioural flexibility bar that appears in the Saviio report represents how flexible the behaviour of the individual is within the twelve preference pairs.") . ' ' . _("If the line is short then it suggests that the behavioural flexibility within that preference is very small (less flexible), and if the line is long then the behavioural flexibility of that individual within that preference is large (more flexible).") . '</p>
		<p>' . _("In this example the line is very short, and sits on the scale next to 'perfectionist'.") . ' ' . _("This suggests that this particular person will always be a perfectionist no matter what their mood or environment (i.e. home or work).") . ' ' . _("This is because they have answered always towards perfectionist in the five statements that make up that preference pair.") . '</p>
		<p>' . _("This same person has a broader behavioural range when it comes to having concern for task or concern for people.") . '</p>
		<div align="center"><img height="75" src="' . FULL_PATH . ACCOUNT_PATH . '_images/_pdf/GettingThingsDone.png" width="550"><br />
		<img height="75" src="' . FULL_PATH . ACCOUNT_PATH . '_images/_pdf/Relationships.png" width="550"><br />&nbsp;</div>
		<p>' . _("This means that their behaviour in this area will be more flexible and may change depending on what mood they are in or what situation they're in.") . ' ' . _("For example on one day they may be concerned about their task, and then another day or another situation they may show more concern for people, however, as the figure is more on the side of concern for people, they are more likely to show concern for people.") . '</p>
		<p>' . _("If a survey takers output report shows that they have a very narrow behavioural range that sits in the middle of a preference pair, for example on 'perfectionist/pragmatist', this suggests that their behaviour will always be somewhere in the middle, i.e., they will never be a total perfectionist and will never be a complete pragmatist but will always behave somewhere between the two."). '</p>';
		
		$pdf->writeHTML($interpretingHTML, true, 0, true, 0);
		$pdf->AddPage();
		
		
		$htmlcontent = '<h1>' . _("Left & Right Brain Attributes by Factor") . '</h1>';
		$htmlcontent .= '<table width="100%" border="0" cellspacing="15" cellpadding="2">
		  <tr>
		    <td colspan="2" bgcolor="' . $dispPDFObj->getRelationshipsClusterColour() . '" align="center"><strong><font size="18" color="' . $dispPDFObj->getWhite() . '">' . _("Relationships") . '</font></strong></td>
		    </tr><tr>
		    <td width="50%" bgcolor="' . $dispPDFObj->getRelationshipsClusterColour() . '" align="center"><strong color="' . $dispPDFObj->getWhite() . '">' . _("Left Brain Inventory") . '</strong></td>
		    <td width="50%" bgcolor="' . $dispPDFObj->getRelationshipsClusterColour() . '" align="center"><strong color="' . $dispPDFObj->getWhite() . '">' . _("Right Brain Inventory") . '</strong></td>
		  </tr>
		  <tr>
		    
		    <td bgcolor="' . $dispPDFObj->getClusterTableDescriptionColour() . '">
	        <ul>
	            <li>' . _("Verbal, focusing on words, symbols, numbers") . '</li>
	            <li>' . _("Likely to follow rules without questioning them") . ' </li>
	            <li>' . _("Spelling and mathematical formula easily memorised") . ' </li>
	            <li>' . _("Enjoy observing") . ' </li>
	            <li>' . _("Listen to what is being said") . ' </li>
	            <li>' . _("Rarely use gestures when talking") . ' </li>
	      	</ul>
		    </td>
		    <td bgcolor="' . $dispPDFObj->getClusterTableDescriptionColour() . '">
		    <ul>
		        <li>' . _("Visual, focusing on images, patterns") . '</li>
		        <li>' . _("Like to know why you're doing something or why rules exist (reasons)") . ' </li>
		        <li>' . _("May have trouble with spelling and finding words to express yourself") . ' </li>
		        <li>' . _("Enjoy touching and feeling actual objects (sensory input)") . ' </li>
		        <li>' . _("Listen to how something is being said") . ' </li>
		        <li>' . _("Talk with your hands") . ' </li>
		    </ul>
		    </td>
		  </tr>
		  <tr>
		    <td colspan="2" bgcolor="' . $dispPDFObj->getThinkingAndPlanningClusterColour() . '" align="center"><strong><font size="18" color="' . $dispPDFObj->getWhite() . '">' . _("Thinking & Planning") . '</font></strong></td>
		    </tr><tr>
		    <td width="50%" bgcolor="' . $dispPDFObj->getThinkingAndPlanningClusterColour() . '" align="center"><strong color="' . $dispPDFObj->getWhite() . '">' . _("Left Brain Inventory") . '</strong></td>
		    <td width="50%" bgcolor="' . $dispPDFObj->getThinkingAndPlanningClusterColour() . '" align="center"><strong color="' . $dispPDFObj->getWhite() . '">' . _("Right Brain Inventory") . '</strong></td>
		  </tr>
		  <tr>
		    <td bgcolor="' . $dispPDFObj->getClusterTableDescriptionColour() . '">
		      <ul>
		          <li>' . _("Words used to remember things, remember names rather than faces") . ' </li>
		          <li>' . _("Make logical deductions from information") . ' </li>
		          <li>' . _("Like making lists and planning") . ' </li>
		          <li>' . _("Likely to follow rules without questioning them") . ' </li>
		          <li>' . _("Good at keeping track of time") . ' </li>
		          <li>' . _("Enjoy observing") . ' </li>
		          <li>' . _("Plan ahead") . ' </li>
		      </ul>
		    </td>
		    <td bgcolor="' . $dispPDFObj->getClusterTableDescriptionColour() . '">
		        <ul>
		            <li>' . _("\"Mind photos\" used to remember things, writing things down or illustrating them helps you remember") . ' </li>
		            <li>' . _("Make lateral connections from information") . '</li>
		            <li>' . _("Free association") . ' </li>
		            <li>' . _("Like to know why you're doing something or why rules exist (reasons)") . ' </li>
		            <li>' . _("No sense of time") . ' </li>
		            <li>' . _("Enjoy touching and feeling actual objects (sensory input)") . ' </li>
		            <li>' . _("Trouble prioritising, so often late, impulsive") . ' </li>
		        </ul>
		    </td>
		  </tr>
		  </table>';
		
		$pdf->writeHTML($htmlcontent, true, 0, true, 0);
		$pdf->AddPage();
			
			$htmlcontent = '<h1>' . _("Left & Right Brain Attributes by Factor continued..") . '</h1>';
			$htmlcontent .= '<table width="100%" border="0" cellspacing="15" cellpadding="2">
		  <tr>
		    <td colspan="2" bgcolor="' . $dispPDFObj->getMakingDecisionsClusterColour() . '" align="center"><strong><font size="16" color="' . $dispPDFObj->getWhite() . '">' . _("Making Decisions") . '</font></strong></td>
		    </tr><tr>
		    <td width="50%" bgcolor="' . $dispPDFObj->getMakingDecisionsClusterColour() . '" align="center"><strong color="' . $dispPDFObj->getWhite() . '">' . _("Left Brain Inventory") . '</strong></td>
		    <td width="50%" bgcolor="' . $dispPDFObj->getMakingDecisionsClusterColour() . '" align="center"><strong color="' . $dispPDFObj->getWhite() . '">' . _("Right Brain Inventory") . '</strong></td>
		  </tr>
		  <tr>
		    <td bgcolor="' . $dispPDFObj->getClusterTableDescriptionColour() . '">
		     <ul>
		      <li>' . _("Analytical, led by logic") . '</li>
		      <li>' . _("Process ideas sequentially, step by step") . '</li>
		      <li>' . _("Words used to remember things, remember names rather than faces") . ' </li>
		      <li>' . _("Highly organised") . ' </li>
		      <li>' . _("Good at keeping track of time") . ' </li>
		      <li>' . _("Enjoy observing") . ' </li>
		     </ul>
		    </td>
		    <td bgcolor="' . $dispPDFObj->getClusterTableDescriptionColour() . '">
		    <ul>
		        <li>' . _("Intuitive, led by feelings") . ' </li>
		        <li>' . _("Process ideas simultaneously") . ' </li>
		        <li>' . _("\"Mind photos\" used to remember things, writing things down or illustrating them helps you remember") . ' </li>
		        <li>' . _("Organization tends to be lacking") . ' </li>
		        <li>' . _("No sense of time") . ' </li>
		        <li>' . _("Enjoy touching and feeling actual objects (sensory input)") . ' </li>
		    </ul>
		    </td>
		  </tr>
		   <tr>
		    <td colspan="2" bgcolor="' . $dispPDFObj->getGettingThingsDoneClusterColour() . '" align="center"><strong><font size="16" color="' . $dispPDFObj->getWhite() . '">' . _("Getting Things Done") . '</font></strong></td>
		    </tr><tr>
		    <td width="50%" bgcolor="' . $dispPDFObj->getGettingThingsDoneClusterColour() . '" align="center"><strong color="' . $dispPDFObj->getWhite() . '">' . _("Left Brain Inventory") . '</strong></td>
		    <td width="50%" bgcolor="' . $dispPDFObj->getGettingThingsDoneClusterColour() . '" align="center"><strong color="' . $dispPDFObj->getWhite() . '">' . _("Right Brain Inventory") . '</strong></td>
		  </tr>
		  <tr>
		    <td bgcolor="' . $dispPDFObj->getClusterTableDescriptionColour() . '">
		    <ul>
		      <li>' . _("Process ideas sequentially, step by step") . '</li>
		      <li>' . _("Work up to the whole step by step, focusing on details, information organised") . ' </li>
		      <li>' . _("Highly organised") . ' </li>
		      <li>' . _("Good at keeping track of time") . ' </li>
		      <li>' . _("Enjoy observing") . ' </li>
		      <li>' . _("Likely read an instruction manual before trying") . ' </li>
		    </ul>
		    </td>
		    <td bgcolor="' . $dispPDFObj->getClusterTableDescriptionColour() . '">
		    <ul>
		        <li>' . _("Process ideas simultaneously") . ' </li>
		        <li>' . _("\"Mind photos\" used to remember things, writing things down or illustrating them helps you remember") . ' </li>
		        <li>' . _("See the whole first, then the details") . ' </li>
		        <li>' . _("Organization tends to be lacking") . ' </li>
		        <li>' . _("No sense of time") . ' </li>
		        <li>' . _("Enjoy touching and feeling actual objects (sensory input)") . ' </li>
		        <li>' . _("Unlikely to read instruction manual before trying") . ' </li>
		    </ul>
		    </td>
		  </tr>
		</table>';
		
		$pdf->writeHTML($htmlcontent, true, 0, true, 0);
	}//end inc tech report!
	
	/**********************************************************************
	*																																			*
	*																																			*
	*																	LEGENDS															*
	*																																			*
	*																																			*
	**********************************************************************/
	
	$pdf->AddPage();
	$legendHTML = ' <h1>' . _("Legend and Icon Descriptions") . '</h1>';
  $legendHTML .= '<table width="100%" border="0" cellspacing="0" cellpadding="6">
	  <tr>
	    <td colspan="5"><strong>Factors</strong></td>
	  </tr>
	  <tr>
	    <td width="4%"><img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_map/rel_icon.png" /></td><td width="21%">&nbsp;&nbsp;' . _("Relationships") . '</td>
	    <td width="4%"><img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_map/tap_icon.png" /></td><td width="21%">&nbsp;&nbsp;' . _("Thinking & Planning") . '</td>
	    <td width="4%"><img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_map/mak_icon.png" /></td><td width="21%">&nbsp;&nbsp;' . _("Making Decisions") . '</td>
	    <td width="4%"><img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_map/gtd_icon.png" /></td><td width="21%">&nbsp;&nbsp;' . _("Getting Things Done") . '</td>
	  </tr>
	  <tr><td colspan="8">' . _("The factors are sometimes represented by three letter acronyms (such as in the top 10 matched statements) these are:") . '</td></tr>
	  <tr>
	    <td width="4%">&nbsp;</td><td width="21%">&nbsp;&nbsp;' . _("REL") . '</td>
	    <td width="4%">&nbsp;</td><td width="21%">&nbsp;&nbsp;' . _("TAP") . '</td>
	    <td width="4%">&nbsp;</td><td width="21%">&nbsp;&nbsp;' . _("DEC") . '</td>
	    <td width="4%">&nbsp;</td><td width="21%">&nbsp;&nbsp;' . _("GTD") . '</td>
	  </tr>
	  <tr><td colspan="8">' . _("An asterisk (*) next to one of the above acronyms denotes that inspite of any high quality match the results from the two MAPs are on opposite sides of the scale") . '</td></tr>
	</table>
	<br />
	<table width="100%" border="0" cellspacing="0" cellpadding="6">
	  <tr>
	    <td colspan="4"><strong>MAP Predictor Strength</strong></td>
	  </tr>
	  <tr>
	    <td colspan="4">' . _("The MAP predictor strength shows how predicitive each statement in the team/talentMAP is based upon, the standard deviation of each of the questions between the participants in that team/talentMAP.") . '</td>
	  </tr>
	  <tr>
	    <td width="25%"><img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_map/excellent_bullet.png" />&nbsp;&nbsp;' . _("Excellent") . '</td>
	    <td width="25%"><img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_map/good_bullet.png" />&nbsp;&nbsp;' . _("Good") . '</td>
	    <td width="25%"><img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_map/poor_bullet.png" />&nbsp;&nbsp;' . _("Average") . '</td>
	    <td width="25%"><img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_map/bad_bullet.png" />&nbsp;&nbsp;' . _("Poor") . '</td>
	  </tr>
	</table>
	<br />
	<table width="100%" border="0" cellspacing="0" cellpadding="6">
	  <tr>
	    <td><strong>'._("Behavioural Flexibility") . '</strong></td>
	  </tr>
	  <tr>
	    <td>' . _("This bar shows the behavioural range for both of the MAPs. The top bar shows the 'master' or left hand side MAP and the bottom bar shows the behavioural range for the MAP on the right hand side.") . '</td>
	  </tr>
	  <tr>
	    <td><img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_map/behaviour_bar.png" width="61" height="7" alt="behaviour bar" /></td>
	  </tr>
	</table>
	<br />
	<table width="100%" border="0" cellspacing="0" cellpadding="6">
	  <tr>
	    <td colspan="4"><strong>Match Colour Key</strong></td>
	  </tr>
	  <tr>
	    <td colspan="4">' . _("The colours of the icons and percentages are based upon how well that preference or statement matches with the other MAP or talent/teamMAP. A low percentage (i.e. red) is not considered bad, it just means that the match is far apart.") . '</td>
	  </tr>
	  <tr>
	    <td width="25%"><img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_map/excellent_circle.png" />&nbsp;&nbsp;90-100%</td>
	    <td width="25%"><img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_map/good_circle.png" />&nbsp;&nbsp;70-90%</td>
	    <td width="25%"><img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_map/average_circle.png" />&nbsp;&nbsp;50-70%</td>
	    <td width="25%"><img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_map/poor_circle.png" />&nbsp;&nbsp;0-50%</td>
	  </tr>
	</table>';

	$pdf->writeHTML($legendHTML, true, 0, true, 0);
	
	
	/**********************************************************************
	*																																			*
	*																																			*
	*											PREFERENCES AND SCORES													*
	*																																			*
	*																																			*
	**********************************************************************/
	

	function getDiffBG($intDiff) {
			global $excel;
			global $good;
			global $average;
			global $poor;
			if($intDiff <= 1.01){
				return $excel;
			}
			elseif($intDiff <= 3.01){
				return $good;
			}
			elseif($intDiff <= 5.01){
				return $average;
			}
			else{
				return $poor;
			}
	}
	
	function getDevImg($stddev) {
			if($stddev <= 1.01){
				return "excellent_bullet.png";
			}
			elseif($stddev <= 2.01){
				return "good_bullet.png";
			}
			elseif($stddev <= 3.01){
				return "poor_bullet.png";
			}
			else{
				return "bad_bullet.png";
			}
	}
	function splitString($string) {
		$pieces = str_split(utf8_encode($string));
		$out = join("", $pieces);
		return utf8_decode($out);
	}

	
	/**********************************************************************
	*																																			*
	*																																			*
	*											TOP 10 STATEMENTS & STAR												*
	*																																			*
	*																																			*
	**********************************************************************/
	
	if($_SESSION['rightTypeDisp'] != 'single') {
		$pdf->AddPage();
		$legendhtml = $dispPDFObj->showLegend($_SESSION['leftTypeDisp'], $_SESSION['rightTypeDisp'], $_SESSION['leftname'], $_SESSION['rightname'], $PDF_PAGE_ORIENTATION, $arrFactorScores[$rankid]['leftemail'], $arrFactorScores[$rankid]['rightemail']);
	}
	$pdf->writeHTML($legendhtml, true, 0, true, 0);
	$yscore = $pdf->GetY();
	
	if($_SESSION['rightTypeDisp'] == 'single') {
		uasort($arrDeviations[$rankid]['allinfo'], cmpdiff);
	} else {
		//sort all diffs and devs hehe!
		if(($_SESSION['leftTypeDisp'] != 'talentMAP' && $_SESSION['leftTypeDisp'] != 'teamMAP') && ($_SESSION['rightTypeDisp'] != 'talentMAP' && $_SESSION['rightTypeDisp'] != 'teamMAP')) {
			$matching = 'diff';
			uasort($arrDeviations[$rankid]['allinfo'], cmpdiff);
		} else {
			$matching = 'diff';
			if($_SESSION['leftTypeDisp'] == 'talentMAP' || $_SESSION['leftTypeDisp'] == 'teamMAP') {
				uasort($arrDeviations[$rankid]['allinfo'], cmpdevleft);
				$stdevside = 'stddev_l';
			} else {
				uasort($arrDeviations[$rankid]['allinfo'], cmpdev);
				$stdevside = 'stddev';
			}
		}
	}
	
	if($_SESSION['rightTypeDisp'] != 'single') {
		
		$gcount = 0;
		$avcount = 0;
		$pcount = 0;
		$bcount = 0;
		$qtotal = count($arrDeviations[$rankid]['allinfo']);
		
		$leftmapids = $arrFactorScores[$rankid]['l_mapids'];
		$leftmaparr = explode(',',$leftmapids);
		$lmcount = count($leftmaparr);
		$rightmapids = $arrFactorScores[$rankid]['r_mapids'];
		$rightmaparr = explode(',',$rightmapids);
		$rmcount = count($rightmaparr);
		
		//calc star score for each q!
		function calcStar($nummaps, $stdev) {
			$const = 0.01;
			$rating = ($nummaps / ($stdev + $const));
			return $rating; 
		}
		
		$i = 0;
	
		$toptenhtml = '<table border="0" cellpadding="3">
		  <tr>
		    <td bgcolor="'.$top10bg.'" color="' . $dispPDFObj->getWhite() . '">' . _('Top 10 Matched Statements') . '</td>
		  </tr>
		</table><br /><table border="0" cellpadding="0" cellspacing="2">';
		
		foreach($arrDeviations[$rankid]['allinfo'] as $qid => $value) {
			if($value[$stdevside] <= 2.01) {
				//this increments the number of predictive statements at a per cluster level (i.e less than or equal to 2.01)
				$clusterPredictorCount[$value['clusid']]++;
			}
			//THIS WORKS OUT THE % NUMBERS ON NUM STATEMENTS MIDDLE BOX
			if($value[$matching] <= 1.01) {
				$gcount++;
			} else if ($value[$matching] <= 3.01) {
				$avcount++;
			} else if($value[$matching] <= 5.01) {
				$pcount++;
			} else {
				$bcount++;
			}

			//loop array of q's stored in as customs... so then do foreach of those then do $arrDeviations[$rankid]['allinfo']['KEYHERE_QID']['lscore'] etc to work values and display in order! (as keys will have been moved by sort);
			if($i <= 9) {
				if($value['lscore'] <= 5.0) {
					$qresponse = $stquestions[$qid]['l'];
				} else if($value['lscore'] >= 5.01) {
					$qresponse = $stquestions[$qid]['r'];
				}
				//check if scores are on opposite sides
				if(oppositeSide($value['lscore'],$value['rscore'])) {
					$starSTR = ' *';
				} else {
					$starSTR = '';
				}
				$qscore = round((10 - $value['diff']),1) * 10;
				$diffbg = getDiffBG($value['diff']);
				if($_SESSION['leftTypeDisp'] == 'MAP' && $_SESSION['rightTypeDisp'] == 'MAP') {
					$stddevimg = 'neutral_bullet.png';
				} else {
					$stddevimg = getDevImg($value[$stdevside]);
				}
				$toptenhtml .= '<tr>
				<td width="' . $clusttoptenwidth1 . '"><img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_pdf/' . $stddevimg. '" width="10" height="10" /></td>
				<td width="' . $clusttoptenwidth2 . '">' . _($qresponse) . '</td>
				<td width="' . $clusttoptenwidth3 . '"><table border="0" cellpadding="0" cellspacing="0"><tr>
					<td width="100%" height="14" bgcolor="' . $diffbg. '" color="#FFFFFF" align="center">' . getClusterABRID($value['clusid']) . $starSTR . '</td>
				</tr></table>
				</td><td width="' . $clusttoptenwidth4 . '"> </td></tr>';
			}
			if($i == 9) {
				$toptenhtml .= '</table>';
			}
			if(($_SESSION['leftTypeDisp'] != 'talentMAP' && $_SESSION['leftTypeDisp'] != 'teamMAP') && ($_SESSION['rightTypeDisp'] != 'talentMAP' && $_SESSION['rightTypeDisp'] != 'teamMAP')) {
				if($i == 50) {
					$btmtenhtml .= '<br /><table border="0" cellpadding="3">
					  <tr>
					    <td bgcolor="'.$top10bg.'" color="#FFFFFF">' . _('Top 10 Mismatched Statements') . '</td>
					  </tr>
					</table><br /><table border="0" cellpadding="0" cellspacing="2">';
				}
				
				if($i >= 50) {
					if($value['lscore'] <= 5.0) {
						$qresponse = $stquestions[$qid]['l'];
					} else if($value['lscore'] >= 5.01) {
						$qresponse = $stquestions[$qid]['r'];
					}
					//check if scores are on opposite sides
					if(oppositeSide($value['lscore'],$value['rscore'])) {
						$starSTR = ' *';
					} else {
						$starSTR = '';
					}
					$qscore = round((10 - $value['diff']),1) * 10;
					$diffbg = getDiffBG($value['diff']);
					if($_SESSION['leftTypeDisp'] == 'MAP' && $_SESSION['rightTypeDisp'] == 'MAP') {
						$stddevimg = 'neutral_bullet.png';
					} else {
						$stddevimg = getDevImg($value[$stdevside]);
					}
					$btmtenhtml .= '<tr><td width="' . $clusttoptenwidth1 . '"><img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_pdf/' . $stddevimg. '" width="10" height="10" /></td>
					<td width="' . $matchedStatements . '">' . _($qresponse) . '</td>
					<td width="' . $clusttoptenwidth3 . '"><table border="0" cellpadding="0" cellspacing="0"><tr><td width="100%" height="14" bgcolor="' . $diffbg. '" color="#FFFFFF" align="center">' . getClusterABRID($value['clusid']) .  $starSTR . '</td></tr></table></td>
					<td width="' . $clusttoptenwidth4 . '"> </td></tr>';
				}
				if($i == 59) {
					$btmtenhtml .= '</table>' . "\n";
				}
			}
			$i++;
		}
		$pdf->writeHTMLCell(0, 0, '', 72, $toptenhtml, 0, 1, 0, true, 'L');
	
		
		/* ********************************************************************
		*																																			*
		*																																			*
		*													STARS AND DIFFERENCE												*
		*																																			*
		*																																			*
		******************************************************************** */
		
		$scorehtml = '<table border="0" width="300" cellpadding="0" cellspacing="0">
			<tr>
				<td><img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_pdf/greenscore_bg.jpg" /></td>
				<td><img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_pdf/bluescore_bg.jpg" /></td>
				<td><img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_pdf/orangescore_bg.jpg" /></td>
				<td><img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_pdf/redscore_bg.jpg" /></td>
			</tr>
		</table>';
		
		if($PDF_PAGE_ORIENTATION == 'L') {
			$xmid = 99;
		} else {
			$xmid = 55;
		}
		$pdf->writeHTMLCell(0, 0, $xmid, $yscore+3, $scorehtml, 0, 1, 0, true, 'L');
       
       
        $excel_val = $gcount/$qtotal;
        $good_val = $avcount/$qtotal;
        $average_val = $pcount/$qtotal;
        $poor_val = ($qtotal - ($gcount + $avcount + $pcount)) / $qtotal;
        
        //Display values - format these as you output them!
        $excel_display_val = $excel_val*100;
        $good_display_val = $good_val*100;
        $average_display_val = $average_val*100;
        $poor_display_val = $poor_val*100;
        
        $matchScores = array("excel" => $excel_val, "good" => $good_val, "average" => $average_val, "poor" => $poor_val);
        $totalVal = array_sum($matchScores);                                                       
			
		$scorehtmltext = '<table border="0" width="300" cellpadding="0" cellspacing="0">
			<tr>
				<td align="center" valign="top"><font color="'.$dispPDFObj->getExcellentColour().'" size="16">' . number_format($excel_display_val,1). '%</font></td>
				<td align="center" valign="center"><font color="'.$dispPDFObj->getGoodColour().'" size="16">' . number_format($good_display_val,1). '%</font></td>
				<td align="center" valign="center"><font color="'.$dispPDFObj->getAverageColour().'" size="16">' . number_format($average_display_val,1). '%</font></td>
				<td align="center" valign="center"><font color="'.$dispPDFObj->getPoorColour().'" size="16">' . number_format($poor_display_val,1). '%</font></td>
			</tr>
		</table>';
		
		$pdf->writeHTMLCell(0, 0, $xmid, $yscore+7.4, $scorehtmltext, 0, 1, 0, true, 'L');
		
		if($_SESSION['leftTypeDisp'] == 'MAP' && $_SESSION['rightTypeDisp'] == 'MAP') {
			$pdf->AddPage();
			$legendhtml = $dispPDFObj->showLegend($_SESSION['leftTypeDisp'], $_SESSION['rightTypeDisp'], $_SESSION['leftname'], $_SESSION['rightname'], $PDF_PAGE_ORIENTATION, $arrFactorScores[$rankid]['leftemail'], $arrFactorScores[$rankid]['rightemail']);
			$pdf->writeHTML($legendhtml, true, 0, true, 0);
			$pdf->writeHTMLCell(0, 0, '', '', $btmtenhtml, 0, 1, 0, true, 'L');
		}
		
	}//end if not single!
			
	
	/**********************************************************************
	*																																			*
	*																																			*
	*													CLUSTERS AND SCORING												*
	*																																			*
	*																																			*
	**********************************************************************/
	
	foreach($cids as $cid => $value) {
		//new page for each cluster!
		$pdf->AddPage();
		if($_SESSION['rightTypeDisp'] == 'single') {
			$legendhtml = $dispPDFObj->showLegendSingle($_SESSION['leftTypeDisp'], $_SESSION['leftname'], $PDF_PAGE_ORIENTATION, $arrFactorScores[$rankid]['leftemail']);
		} else {
			$legendhtml = $dispPDFObj->showLegend($_SESSION['leftTypeDisp'], $_SESSION['rightTypeDisp'], $_SESSION['leftname'], $_SESSION['rightname'], $PDF_PAGE_ORIENTATION, $arrFactorScores[$rankid]['leftemail'], $arrFactorScores[$rankid]['rightemail']);
		}
		$pdf->writeHTML($legendhtml, true, 0, true, 0);
		
	  $clusname = $value;
		switch($cid) {
			case 1:
			$clustbg = $clus_1;
			break;
			case 2:
			$clustbg = $clus_2;
			break;
			case 3:
			$clustbg = $clus_3;
			break;
			case 4:
			$clustbg = $clus_4;
			break;
		}
	
		if($_SESSION['rightTypeDisp'] != 'single') {
			  $clustertotal	= round($_SESSION['clustotal_' . $cid],1);
				//which class to use based on clustertotal!
				switch($clustertotal) {
					case ($clustertotal >= 89.50):
					$clustscorebg = $excel;
					break;
					case ($clustertotal >= 69.50):
					$clustscorebg = $good;
					break;
					case ($clustertotal >= 49.50):
					$clustscorebg = $average;
					break;
					case ($clustertotal >= 0):
					$clustscorebg = $poor;
					break;
				}
		}
				
	  if($_SESSION['rightTypeDisp'] != 'single') {
			//cmpdev is deviation - cmpdiff is difference (for instance MAP vs MAP)
			if($_SESSION['leftTypeDisp'] == 'MAP' && $_SESSION['rightTypeDisp'] == 'MAP') {
				uasort($arrDeviations[$rankid][$cid], cmpdiff);
			} else {
				if($_SESSION['leftTypeDisp'] == 'talentMAP' || $_SESSION['leftTypeDisp'] == 'teamMAP') {
					uasort($arrDeviations[$rankid][$cid], cmpdevleft);
					$stdevside = 'stddev_l';
				} else {
					uasort($arrDeviations[$rankid][$cid], cmpdev);
					$stdevside = 'stddev';
				}
			}
		if($_SESSION['leftType'] == 'TalentMAP' || $_SESSION['leftType'] == 'TeamMAP'|| $_SESSION['rightType'] == 'TalentMAP' || $_SESSION['rightType'] == 'TeamMAP') {
	 	 if($clusterPredictorCount[$cid] != 1) {
    			$predictText = sprintf(_("%s statements are predictive"), $clusterPredictorCount[$cid]);
    		} else {
    			$predictText = sprintf(_("%s statement is predictive"), $clusterPredictorCount[$cid]);
    		}
	 	 } else {
	 	 	$predictText = '';
	 	 }
			$clusterhtml = '<table border="0" cellpadding="3" cellspacing="0">
			  <tr>
			    <td bgcolor="' . $clustbg . '" width="' . $clusterBar1 . '"><table width="100%"><tr><td color="' . $dispPDFObj->getWhite() . '"><strong>' . _($value) . '</strong></td><td align="right" color="' . $dispPDFObj->getWhite() . '">' . $predictText . '&nbsp;</td></tr></table></td>
			    <td width="' . $clusterBar2 . '"></td>
			    <td width="' . $clusterBar3 . '" align="center" color="#FFFFFF" bgcolor="' . $clustscorebg . '">' . $clustertotal . '%</td>
			  </tr>
			</table>';
		} else {
			$clusterhtml = '<table border="0" cellpadding="3">
		  <tr>
		    <td color="#FFFFFF" bgcolor="' . $clustbg . '" width="100%"><strong>' . _($value) . '</strong></td>
		  </tr>
			</table>';
			uasort($arrDeviations[$rankid][$cid], cmpdiff);
		}
		
		//cluster repeat here!
		$clusterdetailed = '<table border="0" cellpadding="3">
		  <tr>
		    <td color="#FFFFFF" bgcolor="' . $clustbg . '"><strong>' . _($value) . '</strong></td>
		  </tr>
		</table>';
	
		$pdf->writeHTML($clusterhtml, true, 0, true, 0);
		$morehtml = '<table border="0" cellpadding="0" cellspacing="2">';
		
		foreach($arrDeviations[$rankid][$cid] as $qid => $value) {
			if($value['lscore'] <= 5.0) {
				$qresponse = $stquestions[$qid]['l'];
			} else if($value['lscore'] >= 5.1) {
				$qresponse = $stquestions[$qid]['r'];
			}
			//check if scores are on opposite sides
			if(oppositeSide($value['lscore'],$value['rscore'])) {
				$starSTR = ' *';
			} else {
				$starSTR = '';
			}
			if($_SESSION['rightTypeDisp'] != 'single') {
				$qscore = round((10 - $value['diff']),1) * 10 . '% ' . $starSTR;
				$diffbg = getDiffBG($value['diff']);
			} else {
				$qscore = round($value['lscore'],1);
				$diffbg = '#6EA8D9';
			}
			if($_SESSION['leftTypeDisp'] == 'MAP' && $_SESSION['rightTypeDisp'] == 'MAP') {
				$stddevimg = 'neutral_bullet.png';
			} else {
				$stddevimg = getDevImg($value[$stdevside]);
			}
			$morehtml .= '<tr><td width="' . $clusttoptenwidth1 . '"><img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_pdf/' . $stddevimg. '" width="10" height="10" /></td>
			<td width="' . $matchedStatements . '">' . _($qresponse) . '</td>
			<td width="' . $clusttoptenwidth3 . '"><table border="0" cellpadding="0" cellspacing="0"><tr><td width="100%" height="14" bgcolor="' . $diffbg. '" color="#FFFFFF" align="center">' . $qscore . '</td></tr></table>
			</td><td width="' . $clusttoptenwidth4 . '"> </td></tr>';
		}
		
		$morehtml .= '</table>';
		//write that HTML!
		$pdf->writeHTML($morehtml, true, 0, true, 0);
		
		/********************************************PREF SCORES HERE *********************************/
	
	  $fgcount = 0;
		$clusterscore = 0;
		$k = 1;
		foreach($fgs as $fg => $clusterid) {
			$leftpositive = "";
			$leftnegative = "";
			$leftfacdesc = "";
			$rightpositive = "";
			$rightnegative = "";
			$rightfacdesc = "";
			foreach($fids as $fid => $info) {
				if($info['factor_group'] == $fg) {
					if($info['factor_align'] == 'l') {
						$left = $info['factor_text'];
						
						//get pos and neg for this factor group (left side)
						$thisfactors = $fdescs[$fid];
						foreach($thisfactors as $type => $factdescs) {
							foreach($factdescs as $fact) {
								$translated = _($fact);
								if($type == 'positive') {
									if($_SESSION['locale'] == "zh_CN") {
										$translated = splitString($translated);
									}
									$leftpositive .= '<li>' . $translated . ' </li>';
								} else if($type == 'negative') {
									if($_SESSION['locale'] == "zh_CN") {
										$translated = splitString($translated);
									}
									$leftnegative .= '<li>' . $translated . ' </li>';
								} else if($type == 'desc') {
									if($_SESSION['locale'] == "zh_CN") {
										$translated = splitString($translated);
									}
									$leftfacdesc = $translated. ' ';
								}
							}
						}
					} else if ($info['factor_align'] == 'r') {
						$right = $info['factor_text'];
						
						//get pos and neg for this factor group (right side)
						$thisfactors = $fdescs[$fid];
						foreach($thisfactors as $type => $factdescs) {
							foreach($factdescs as $fact) {
								$translated = _($fact);
								if($type == 'positive') {
									if($_SESSION['locale'] == "zh_CN") {
										$translated = splitString($translated);
									}
									$rightpositive .= '<li>' . $translated . ' </li>';
								} else if($type == 'negative') {
									if($_SESSION['locale'] == "zh_CN") {
										$translated = splitString($translated);
									}
									$rightnegative .= '<li>' . $translated . ' </li>';
								} else if($type == 'desc') {
									$rightfacdesc = $translated . ' ';
								}
							}
						}
						
					}
				}
			}
			if($_SESSION['locale'] == "zh_CN") {
				error_log(print_r(array('lp'=>$leftpositive, 'ln'=>$leftnegative, 'ld'=>$leftfacdesc, 'rp'=>$rightpositive, 'rn'=>$rightnegative, 'rd'=>$rightfacdesc), true) . '
			', 3, "/tmp/my-errors.log");
			}
			if($clusterid == $cid) {
				$fgcount++;
				
				//if you get a detailed page need to write the legend and clustername HTML at top
				if($_GET['pagetype'] == 'detailed') {
						if($_SESSION['rightTypeDisp'] == 'single') {
							$legendhtml = $dispPDFObj->showLegendSingle($_SESSION['leftTypeDisp'], $_SESSION['leftname'], $PDF_PAGE_ORIENTATION, $arrFactorScores[$rankid]['leftemail']);
						} else {
							$legendhtml = $dispPDFObj->showLegend($_SESSION['leftTypeDisp'], $_SESSION['rightTypeDisp'], $_SESSION['leftname'], $_SESSION['rightname'], $PDF_PAGE_ORIENTATION, $arrFactorScores[$rankid]['leftemail'], $arrFactorScores[$rankid]['rightemail']);
						}

						$ktotal = 4;
						if($fgcount != $ktotal) {
							$pdf->AddPage();
							$pdf->writeHTML($legendhtml, true, 0, true, 0);
							$pdf->writeHTML($clusterdetailed, true, 0, true, 0);
						}
				}
				
				//this value is the distance between each point on the scale. so 4.85 width units = 1 survey point
				$WsConstant = 4.85;
				if($PDF_PAGE_ORIENTATION == 'L') {
					$xmargin = 114.2;
				} else {
					$xmargin = 72;
				}
				$intScoreMin = min($arrFactorScores[$rankid][$fg]['lscoreall']);
				$intScoreMax = max($arrFactorScores[$rankid][$fg]['lscoreall']);
				if($_SESSION['rightTypeDisp'] != 'single') {
					$intNumF = $arrFactorScores[$rankid][$fg]['num'];
					$intModF = $arrFactorScores[$rankid][$fg]['diff'];
					$intIdealMin = min($arrFactorScores[$rankid][$fg]['rscoreall']);
					$intIdealMax = max($arrFactorScores[$rankid][$fg]['rscoreall']);
					$scorediff = ($intModF/$intNumF);
					$permatch = (round((10 - $scorediff),2)) * 10;
					$clusterscore += $permatch;
					$intIdealWidth = round((($intIdealMax - $intIdealMin) * $WsConstant),3);
					if($_SESSION['rightTypeDisp'] != 'single') {
						switch($permatch) {
							case ($permatch >= 89.5):
							$matchbg = $excel;
							break;
							case ($permatch >= 69.5):
							$matchbg = $good;
							break;
							case ($permatch >= 49.5):
							$matchbg = $average;
							break;
							case ($permatch >= 0):
							$matchbg = $poor;
							break;
						}
					} else {
						switch($permatch) {
							case ($permatch >= 9.00):
							$matchbg = $excel;
							break;
							case ($permatch >= 7.00):
							$matchbg = $good;
							break;
							case ($permatch >= 5.00):
							$matchbg = $average;
							break;
							case ($permatch >= 0):
							$matchbg = $poor;
							break;
						}
					}
				} else {
					$matchbg = '#6EA8D9';
					$permatch = round(($arrFactorScores[$rankid][$fg]['lscore']/$arrFactorScores[$rankid][$fg]['num']),1);
				}
	
				$intScoreWidth = round((($intScoreMax - $intScoreMin) * $WsConstant),2);
				
				$intScoreWidth = number_format($intScoreWidth,2,'.','');
				$intIdealWidth = number_format($intIdealWidth,2,'.','');
				
				//These functions get colours for the bars and for the icons
				$factscore = round($permatch,0);  //gets factor score as a percentage
				$iconclass = getScoreColour($factscore);
				
				//adding +3 here for additional padding between preference sets
				$y = $pdf->GetY() + 3;
				$naby = $y+4.8;
				if($PDF_PAGE_ORIENTATION == 'L') {
					$titleWidth = 340;
				} else {
					$titleWidth = 190;
				}
				if($_SESSION['rightTypeDisp'] == 'single') {
					$prefTot = round($permatch,1);
				} else {
					$prefTot = round($permatch,0) . '%';
				}
				
				if($_SESSION['rightTypeDisp'] == 'single') {
					$fontSize = 13;
					
				} else {
					$fontSize = 13;
				}
				if(strlen(_($left)) >= 24 || strlen(_($right)) >= 24) {
					$fontSize = 12;
				}
				
				$scoregroup = '<table border="0" cellpadding="8" cellspacing="0" border="0">
					<tr>
					<td width="' . $titleWidth . '" valign="center" align="center"><br /><strong><font size="' . $fontSize . '">' . _($left) . '</font></strong></td>
					<td width="196" align="center"><br /><img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_map/surveybar_new.gif" /></td>
					<td width="' . $titleWidth . '" valign="center" align="center"><br /><strong><font size="' . $fontSize . '">' . _($right) . '</font></strong></td>
					<td width="60"><table border="0" cellpadding="8"><tr><td valign="center" align="center" bgcolor="' . $matchbg. '" color="#FFFFFF" height="10" width="52">  ' . $prefTot . '</td></tr></table></td>
					</tr>
				</table>';
				
				$testDiv = '<table border="0" cellpadding="0" cellspacing="0"><tr><td width="54" height="14"></td></tr></table>';
				$pdf->writeHTMLCell(0, 0, 15, $y+5, $scoregroup, 0, 1, false, true, 'C', true);
				
				//============================================================+
				// BEHAVIOUR FLEXIBILITY BARS                                                 
				//============================================================+
	
	    	switch($_SESSION['leftTypeDisp']) {
			  		case 'MAP':
			  		if($_SESSION['rightTypeDisp'] == 'talentMAP' || $_SESSION['rightTypeDisp'] == 'teamMAP') {
			  			$left_bar_col = $dispPDFObj->getPDFFlexBarColour($factscore);
			  			$right_bar_col = $grey_rbg;
			  		} else if($_SESSION['rightTypeDisp'] == 'MAP') {
			  			$left_bar_col = $grey_rbg;
			  			$right_bar_col = $dispPDFObj->getPDFFlexBarColour($factscore);
			  		} else {
			  			$left_bar_col = $grey_rbg;
			  		}
			  		break;
			  		case 'idealMAP':
			  		if($_SESSION['rightTypeDisp'] == 'talentMAP' || $_SESSION['rightTypeDisp'] == 'teamMAP') {
			  			$left_bar_col = $dispPDFObj->getPDFFlexBarColour($factscore);
			  			$right_bar_col = $grey_rbg;
			  		} else {
			  			$left_bar_col = $grey_rbg;
			  			$right_bar_col = $dispPDFObj->getPDFFlexBarColour($factscore);
			  		}
			  		break;
			  		case 'talentMAP':
			  			$left_bar_col = $grey_rbg;
			  			$right_bar_col = $dispPDFObj->getPDFFlexBarColour($factscore);
			  		break;
			  		case 'teamMAP':
			  		if($_SESSION['rightTypeDisp'] == 'talentMAP') {
			  			$left_bar_col = $dispPDFObj->getPDFFlexBarColour($factscore);
			  			$right_bar_col = $grey_rbg;
			  		} else {
			  			$left_bar_col = $grey_rbg;
			  			$right_bar_col = $dispPDFObj->getPDFFlexBarColour($factscore);
			  		}
			  }
	    			  	
	    	//This writes a HTML cell with the border array defining colours depending on the score and whether it is the benchmark or not.
	    		$intScoreWidthTest = $intScoreWidth == 0 ? 0.1 : $intScoreWidth;
				$pdf->writeHTMLCell($intScoreWidthTest, 0, ($xmargin + round(($WsConstant * $intScoreMin), 0)), $y+3, $testDiv, array('LTR' => array('color' => $left_bar_col, 'width' => 0.5)), 1, false, true);
				if($_SESSION['rightTypeDisp'] != 'single') {
					$intIdealWidth = $intIdealWidth == 0 ? 0.1 : $intIdealWidth;
					$pdf->writeHTMLCell($intIdealWidth, 0, ($xmargin + round(($WsConstant * $intIdealMin), 0)), $y+11, $testDiv, array('LRB' => array('color' => $right_bar_col, 'width' => 0.5)), 1, false, true);
				}
	
				//needed for stupid locale comma shit hahah <3 french!
				$lf_total = number_format($arrFactorScores[$rankid][$fg]['lscore'],2,'.','');
			  $fac_total = number_format($arrFactorScores[$rankid][$fg]['num'],2,'.','');
			  $roundnum_l = $lf_total/$fac_total;
			  $engnum_l = number_format($roundnum_l, 2, '.', '');
	
				if($_SESSION['rightTypeDisp'] != 'single') {
					
		    	/*************************************************
			  	*																								 *
			  	*											RIGHT SIDE 								 *
			  	* 																							 *
			  	*************************************************/
			  	
			  	$rf_total = number_format($arrFactorScores[$rankid][$fg]['rscore'],2,'.','');
				  $roundnum_r = $rf_total/$fac_total;
				  $engnum_r = number_format($roundnum_r, 2, '.', '');
				  
			  	switch($_SESSION['rightTypeDisp']) {
			  		case 'MAP':
						$pdf->Image(FULL_PATH . ACCOUNT_PATH . '_images/_pdf/single_' . $iconclass . '.png', $xmargin + number_format((($engnum_r * $WsConstant)-3),2,'.',''), $naby,6.6,9, '', '', '', false, 300);
			  		break;
			  		case 'idealMAP':
						$pdf->Image(FULL_PATH . ACCOUNT_PATH . '_images/_pdf/ideal_' . $iconclass . '.png', $xmargin + number_format((($engnum_r * $WsConstant)-4.4),2,'.',''), $naby,8.6,8.2, '', '', '', false, 300);
			  		break;
			  		case 'teamMAP':
			  		if($_SESSION['leftTypeDisp'] == 'MAP' || $_SESSION['leftTypeDisp'] == 'idealMAP') {
						$pdf->Image(FULL_PATH . ACCOUNT_PATH . '_images/_pdf/cross_grey.png', $xmargin + number_format((($engnum_r * $WsConstant)-3.2),2,'.',''), $naby,8.2,8.2, '', '', '', false, 300);
			  		} else {
						$pdf->Image(FULL_PATH . ACCOUNT_PATH . '_images/_pdf/cross_' . $iconclass . '.png', $xmargin + number_format((($engnum_r * $WsConstant)-3.2),2,'.',''), $naby,8.2,8.2, '', '', '', false, 300);
			  		}
			  		break;
			  		case 'talentMAP':
			  			if($_SESSION['leftTypeDisp'] == 'talentMAP') {
								$pdf->Image(FULL_PATH . ACCOUNT_PATH . '_images/_pdf/cross_' . $iconclass . '.png', $xmargin + number_format((($engnum_r * $WsConstant)-3.2),2,'.',''), $naby,8.2,8.2, '', '', '', false, 300);
			  			} else {
								$pdf->Image(FULL_PATH . ACCOUNT_PATH . '_images/_pdf/cross_grey.png', $xmargin + number_format((($engnum_r * $WsConstant)-3.2),2,'.',''), $naby,8.2,8.2, '', '', '', false, 300);
			  			}	
			  		break;
			  	}
				}
				
		  	/*************************************************
		  	*																								 *
		  	*											LEFT SIDE 								 *
		  	* 																							 *
		  	*************************************************/
		  	
		  	switch($_SESSION['leftTypeDisp']) {
		  		case 'MAP':
		  		if($_SESSION['rightTypeDisp'] == 'talentMAP' || $_SESSION['rightTypeDisp'] == 'teamMAP') {
		  			$pdf->Image(FULL_PATH . ACCOUNT_PATH . '_images/_pdf/single_' . $iconclass . '.png', $xmargin + number_format((($engnum_l * $WsConstant)-3),2,'.',''), $naby,6.6,9, '', '', '', false, 300);
		  		} else if($_SESSION['rightTypeDisp'] == 'MAP') {
						$pdf->Image(FULL_PATH . ACCOUNT_PATH . '_images/_pdf/single_grey_alt.png', $xmargin + number_format((($engnum_l * $WsConstant)-3),2,'.',''), $naby,6.6,9, '', '', '', false, 300);
		  		} else {
		  			$pdf->Image(FULL_PATH . ACCOUNT_PATH . '_images/_pdf/single_grey.png', $xmargin + number_format((($engnum_l * $WsConstant)-3),2,'.',''), $naby,6.6,9, '', '', '', false, 300);
		  		}
		  		break;
		  		case 'idealMAP':
		  		if($_SESSION['rightTypeDisp'] == 'talentMAP' || $_SESSION['rightTypeDisp'] == 'teamMAP') {
						$pdf->Image(FULL_PATH . ACCOUNT_PATH . '_images/_pdf/ideal_' . $iconclass . '.png', $xmargin + number_format((($engnum_l * $WsConstant)-4.4),2,'.',''), $naby,8.6,8.2, '', '', '', false, 300);
		  		} else if($_SESSION['rightTypeDisp'] == 'idealMAP') {
						$pdf->Image(FULL_PATH . ACCOUNT_PATH . '_images/_pdf/ideal_grey_alt.png', $xmargin + number_format((($engnum_l * $WsConstant)-4.4),2,'.',''), $naby,8.6,8.2, '', '', '', false, 300);
		  		} else {
						$pdf->Image(FULL_PATH . ACCOUNT_PATH . '_images/_pdf/ideal_grey.png', $xmargin + number_format((($engnum_l * $WsConstant)-4.4),2,'.',''), $naby,8.6,8.2, '', '', '', false, 300);
		  		}
		  		break;
		  		case 'teamMAP':
		  		if($_SESSION['rightTypeDisp'] == 'talentMAP') {
						$pdf->Image(FULL_PATH . ACCOUNT_PATH . '_images/_pdf/cross_' . $iconclass . '.png', $xmargin + number_format((($engnum_l * $WsConstant)-3.2),2,'.',''), $naby,8.2,8.2, '', '', '', false, 300);
		  		} else {
		  			if($_SESSION['rightTypeDisp'] == 'teamMAP') {
							$pdf->Image(FULL_PATH . ACCOUNT_PATH . '_images/_pdf/cross_grey_alt.png', $xmargin + number_format((($engnum_l * $WsConstant)-3.2),2,'.',''), $naby,8.2,8.2, '', '', '', false, 300);
		  			} else {
							$pdf->Image(FULL_PATH . ACCOUNT_PATH . '_images/_pdf/cross_grey.png', $xmargin + number_format((($engnum_l * $WsConstant)-3.2),2,'.',''), $naby,8.2,8.2, '', '', '', false, 300);
		  			}
		  		}
		  		break;
		  		case 'talentMAP':
		  			if($_SESSION['rightTypeDisp'] == 'talentMAP') {
							$pdf->Image(FULL_PATH . ACCOUNT_PATH . '_images/_pdf/cross_grey_alt.png', $xmargin + number_format((($engnum_l * $WsConstant)-3.2),2,'.',''), $naby,8.2,8.2, '', '', '', false, 300);
		  			} else {
							$pdf->Image(FULL_PATH . ACCOUNT_PATH . '_images/_pdf/cross_grey.png', $xmargin + number_format((($engnum_l * $WsConstant)-3.2),2,'.',''), $naby,8.2,8.2, '', '', '', false, 300);
		  			}
		  		break;
		  	}
	
	
				//if detailed write that shit here under the bar!
				if($_GET['pagetype'] == 'detailed') {
					$detailedhtml = '<br />&nbsp;<br />&nbsp;<br />&nbsp;<br />&nbsp;<br /><table border="0" cellpadding="12" cellspacing="0"><tr>
					<td width="50%"><strong>' . _($left) . '</strong><br />' . _($leftfacdesc) . '<br />
					<br /><strong>' . _("Constructive Behaviours") . '</strong><ul>' . $leftpositive . '</ul>
					<strong>' . _("Less Constructive Behaviours") . '</strong><ul>' . $leftnegative . '</ul>
					 </td
					 ><td width="20"></td><td width="50%"><strong>' . _($right) . '</strong><br />' . _($rightfacdesc) . '<br />
					 <br /><strong>' . _("Constructive Behaviours") . '</strong><ul>' . $rightpositive . '</ul>
					 <strong>' . _("Less Constructive Behaviours") . '</strong><ul>' . $rightnegative . '</ul>
					 </td></tr></table>';
					$pdf->writeHTML($detailedhtml, true, 0, true, 0);
				}
			
			}//end if clusterid;
		}
	}//for each Cluster!
		
	// ---------------------------------------------------------
	
	//Close and output PDF document
	if($_SESSION['rightTypeDisp'] != 'single') {
		$pdf->Output(ACC_NAME . '_' . $_SESSION['leftname'] . '-vs-' . $_SESSION['rightname'] . '.pdf', 'I');
	} else {
		$pdf->Output(ACC_NAME . '_' . $_SESSION['leftname'] . '.pdf', 'I');
	}
	
}//end check if session exists


//============================================================+
// END OF FILE                                                 
//============================================================+
?>
