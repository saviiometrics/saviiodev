﻿***************************************************************************
*																																					*
*																																					*
*														SAVIIO CHANGE LOG															*
*																																					*
*																																					*
***************************************************************************

! - Bug Fix
+ - New Feature
* - Improvment

v2.37 - 18/06/10

+ Added new PDF format (Quick View) which displays a simple one page summary of the scores
+ Added Dutch to list of email templates
* Sortable columns now activated in Manage Users. You can now sort by Firstname/Surname or Account Name

v2.36 - 15/06/10

+ Added idealMAPs
* Incorporated tinyMCE into main admin js cache
* Changed some German Phrases in .po file
* Amended various Danish translations of survey statement
* Improved time delay on multiselect in Analytics and admin
* Updated jQuery UI to 1.8.2
* Updated ValidationEngine to 1.6.5
* Fixed duplication of text in technical summary for German
! Fixed IE 7 bug in analytics
! Fixed bug with Polish characters when generating PDF's
! Fixed bug in Manage users which incorrectly displayed the amount of users remaining which resulted in incorrect paging of the table view.
+ Added "help" tab to main page which now makes use of new Knowledge base at http://help.samknowhow.com
* Small modifications and improvements made to icon positioning in analytics and PDF's
! Fixed a bug in Manage Users where a ' would cause the JSON to become invalid
* Greatly improved SQL speed for selecting MAP list

v2.3 - 30/05/10

* Upgraded jQuery Tools to 1.2.1
* Admin JS minified
* Added new script to minify and combine all JS and CSS files in both admin / user section. Also includes caching and versioning of releases
* Updated config.php and account-class to include changes to versioning and HTML titles.
* Re-Written manage users in admin section. Now uses datatables.net script which greatly increases performance
* Improved CSS of light window boxes in preparation for white label
* Added left brain / right brain descriptions to analytics
* Improved PDF generation time in Chinese
* Improved identification of users when sharing accounts
+ Added User Panel to intro page of admin section
+ Added Gravatar.com functionality
+ Added the ability to push/pull accounts (inc email alerts)
* Added new Terms and Conditions
* Reworded accounts page introduction text to reflect changes with push / pull sharing

v2.24 - 07/05/10

--- General ---

* Added ability to see when MAPs where issued in admin section
! Fix bug in IE7 where bottom down tooltip would appear behind content when hovered over

v2.22 - 27/04/10

--- General ---

* Updated jQuery to version 1.4.2 (previously 1.4.1)
* Updated all jQuery Plugins to work with 1.4.2 and updated jQuery UI to 1.8.0
+ Added Spanish to the system.
! Sharing not working correctly for headhunters and created accounts
* Changed mass upload feature to use .xls (changed from .csv) BlockUI also implemented to prevent users from closing upload popup whilst file is being uploaded and processed.

v2.2 - 11/04/10

--- Analytics & Front End ---

+ Added top 10 Mistmatch Statements (MAP vs MAP)
+ Made top 10 and top 10 mismatch statments hidden - click title bar to reveal statements
* Changed drag and drop menus to only display top 25 results
* Drag and drop menu items can now be found using the search facility
+ When selecting a new language, if you are logged in it now remembers the language you selected when you log back in.
* Changed how editing talent/team MAP works making it quicker and improving performance.
+ Added the ability to save the talentMAP / teamMAP under a new name whilst in edit mode
+ French Added
+ Added security and privacy statements (English only)
+ Added new PDF format which shows a summary for each ranked MAP/team/talentMAP it can be found as a link under the two existing PDF format links
! Fixed bug where the Technical summary tickbox did not work in IE when clicked the first time.
+ You are now able to stop the legend from moving up and down by clicking on the icon in the top right of the movable window pane.
* Improved layout and spacing in IE
! The progress bar when taking a survey now correctly displays the progress indicator length

--- Admin Section ---

! Fixed an issue with the Swedish error language file in the admin section
+ You are now able to share accounts with other administrators - Please see the LMS for more details on this.
* Account administrator accounts reverted back to "Headhunter" access due to new account sharing system.
+ French Added
+ Resend Email invitation function added
+ Retake survey option added
! Fixed Salutation issue where incorrect salutation was sent in invitation email depending on the current language the admin was using on the site
+ Added the ability to see what accounts are linked with every main account.

--- General ----

! Fixed issue where emails would not be received by some email accounts
! Fixed an issue with German invites not being correctly sent to some users