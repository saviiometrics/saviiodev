		<div id="innerContent">
			<div id="contentMenuBar">
	      <div id="contentSiFR">
	      	<h2 class="MWxBox">Every organisation experiences some degree of staff attrition. But what is sometimes ignored is the fact that the reasons why employees leave can help attract and retain high quality people in the future.</h2>
	      </div>
	      <div class="cleaner"></div>
	     </div>
	     <div id="contentSideGFX">
					<img src="/_images/_sidebar/<?=$sideGFX;?>" width="194" height="150" />
	     </div>
	     <div class="cleaner"></div>
	     <div class="featureHighlights">
	     	<div class="contentPadTop">
	     		<p>No content yet!</p>
      	</div>
      </div>
		</div>
		