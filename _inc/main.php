   	 <script type="text/javascript">
			$(function() {
				$('.homeTab').click(function() {
					var currTabID = this.id;
					var butArr = new Array('engage', 'develop', 'attract');

					$.each(butArr, function(index, value) {
						$('#' + value + '_img').attr('src', '/_images/' + value + '_holder.png');
					});

					$('.homeInfo').each(function(index) {
						$(this).hide();
					});
					$('.infoTab').each(function(index) {
						$(this).removeClass('selected');
					});
					$('#home' + currTabID).show();
					var currID = currTabID.toLowerCase();
					$('#' + currID + '_img').attr('src', '/_images/' + currID + '_holder_on.png');
				});
			});
		 </script>
		
     <div id="mainbox_middle">
	  		<div id="flash_overlay"><a href="#"><img src="/_images/findoutmore.png" /></a></div>
	  		<div id="saviio_flash"><img src="/_images/main_animations/main_maps.jpg" width="924" height="278" /></div>
	  		
	  		<div id="main_content">
		  		<div id="main_cornerHolder">
						<div class="homebox hbmargin"><a href="javascript:void(0);" class="homeTab" id="Engage"><img src="/_images/engage_holder.png" width="224" height="131" id="engage_img" /></a></div>
				    <div class="homebox hbmargin"><a href="javascript:void(0);" class="homeTab" id="Develop"><img src="/_images/develop_holder.png" width="236" height="139" id="develop_img" /></a></div>
				    <div class="homebox"><a href="javascript:void(0);" class="homeTab" id="Attract"><img src="/_images/attract_holder.png" width="234" height="144" id="attract_img" /></a></div>
				    <div class="cleaner"></div><br />
				    <div id="mainPageContent">
			      	<div class="homeInfo" id="homeIntro">
				    		<h1 class="MWxHeader">The Saviio "Elevator" Pitch</h1>
							 		<h2 class="MWxBox">
							 			<ul>
							 				<li>Our online survey tools dramatically increase the probability of people succeeding in their role at work.</li>
							 				<li>We help decrease the risk of non-productive work.</li>
							        <li>We help cut the cost of recruitment.</li>
							        <li>We help organisations make more profit.</li>
							      </ul>
							    </h2>
					       	<span class="contentHeader">How?</span>
					        By providing fresh insight to people at work that what really matters is how to create a working climate that plays to each persons&rsquo; strengths.<br /><br />
					       	<span class="contentHeader">So, how is this approach different from other profiling tools?</span>
					        Most psychometric profiling tools assess the individual and compare the person with a normed benchmark &ndash; usually comprising of a collection of anonymous people who have completed the same survey in the past. Somewhere else. Not Saviio.
					       	<p>We compare the individual with their manager, their team and their colleagues. Oh &ndash; and with people who are rather good in a similar role too. In the &ldquo;here and now&rdquo; &ndash; not the &ldquo;there and then&rdquo; usually associated with normative psychometrics.</p>
					        <p>What matters most is providing insight to both manager AND employee so that they can improve mutual understanding around each other&rsquo;s motivations and preferences.</p>
					        <p>An unashamed &lsquo;steal&rsquo; from a Harvard Business School Press article: <strong><em>&ldquo;Nine steps towards creating a great workplace&rdquo;</em></strong> Frederick Hertzberg:</p>
					       	<span class="contentHeader">Don&rsquo;t Dictate the &ldquo;How&rdquo;</span>
					        <em>So, while great companies set high standards, they&rsquo;re flexible about how they let people meet those standards&hellip; &ldquo;If you standardize the end&rdquo; says Marcus Buckingham from Gallup, &ldquo;you don&rsquo;t need to standardize the means&rdquo;.</em></p>
					        <p>We agree with Marcus.</p>
			    			</div>
			    	
			    			<div class="homeInfo" id="homeEngage">
			    				<div class="homeRightFloat">
			    					<a href="javascript:void(0);" class="homeTab" id="Intro"><img src="/_images/backtohome.png" width="135" height="37" /></a>
			    				</div>
			    				<h1 class="MWxHeader">Engage with your staff</h1>
									<span class="contentHeader topPad">A Global Software Brand</span>
									Engaging with Staff and Clients during stressful times!
									<br /><br />
									<div class="homeHighlights">
						         <ul>
						           <li><strong>The Situation</strong><p>The organisation provides support to it's thousands of clients - small and medium sized accounting professionals. During annual service peaks (the end of a tax year) - things can get really busy and stressful! To manage this rise in demand, an additional 42 technical advisors were required to augment their existing staff. An outsourcing contact centre (a Saviio client) was awarded the contract to undertake the work.</p></li>
						           <li class="rightTab"><strong>The Challenges</strong><p>How does the organisation ensure that the new recruits engage quickly with existing staff, the team culture and provide great service - as if they were the top 'talent' employed within the software company itself?</p></li>
						           <li class="cleaner"><strong>The Solution</strong><p>The contact centre profiled the top performers within the role at their client with Saviio MAPs to create a talentMAP. This was used as an essential part of the selection and recruitment process by benchmarking each individual against this talentMAP.</p></li>
						           <li class="rightTab"><strong>The Outcomes</strong><p>The contact centre was appointed to provide this service because they could demonstrate to their client HOW they would attract, engage, develop and retain the right staff.</p></li>
						        </ul>
						      </div>
 									<div class="cleaner"></div>
 									<div class="homeBtmText">
 									<p>Only one of the 42 people left the induction programme and 41 remained through until the end of the contract (9 months later).</p>
									<p>The Client was delighted with the performance of the "out-sourced" client service advisors and on completion of the contract immediately filled 28 internal posts with 28 people, selected from the contact centre's contracted staff.</p>
			  					<br /><br />
			  					</div>
			  			</div>
			    	
			    	
			    			<div class="homeInfo" id="homeDevelop">
			    				<div class="homeRightFloat">
			    					<a href="javascript:void(0);" class="homeTab" id="Intro"><img src="/_images/backtohome.png" width="135" height="37" /></a>
			    				</div>
			    				<h1 class="MWxHeader">Develop and Motivate staff</h1>
									<span class="contentHeader topPad">A National Call Centre</span>
									 Same People | Same Products | Busted Sales Targets! <strong>How did they do that?</strong>
									 <br /><br />
									 <div class="homeHighlights">
						         <ul>
						           <li><strong>The Situation</strong><p>The organisation provides call centre services to it's clients in London, Brighton and Nottingham, England - specialising in outbound selling through specialist teams dedicated to their clients' products and services (Orange, Sky, Barclaycard and others).</p></li>
						           <li class="rightTab"><strong>The Challenges</strong><p>How do you align new hires so that they quickly absorb their clients' culture, get to grips with the product range and sales process and then hit target? Recruiting people capable of outbound sales calls is part of the challenge. The other main challenge is how to re-energise and motivate existing people to perform well - consistently.</p></li>
						           <li class="cleaner"><strong>The Solution</strong><p>The contact centre profiled the top performers within the role at their client with Saviio MAPs to create a talentMAP. This was used as an essential part of the selection and recruitment process by benchmarking each individual against this talentMAP. Internal Sales Trainers were taught how to interpret Saviio MAPs for their teams and then rewrite the training programmes to cater towards these preferences.</p></li>
						           <li class="rightTab"><strong>The Outcomes</strong><p>This is a direct quotation from a letter written by a Saviio MAPs client:</p>
												<p><em>"55 % of new starters attending this workshop had exceeded their sales target within 4 days of being on the phones. Agents are maintaining their improvements over a longer period indicating that they have really bought in to the ideas and key learning points and not just implemented a quick fix.</em></p>
												<p><em>New agents have maintained a consistent conversion rate of 9.5% and exceed their premium income target in the first month by &pound;17,485 that's up 27%!"</em></p>
											 </li>
						        </ul>
						      </div>
						      <div class="cleaner"></div>
						      <br /><br />
			    			</div>
			
				      	<div class="homeInfo" id="homeAttract">
				      		<div class="homeRightFloat">
			    					<a href="javascript:void(0);" class="homeTab" id="Intro"><img src="/_images/backtohome.png" width="135" height="37" /></a>
			    				</div>
				      		<h1 class="MWxHeader">Attract the right staff for you!</h1>
									<span class="contentHeader topPad">A Major Airline and Tour Operator</span>
									 Attracting staff who can sell and still provide a safe flying experience.
									 <br /><br />
								   <div class="homeHighlights">
						         <ul>
						           <li><strong>The Situation</strong><p>With rising fuel costs, increased competition in the 'value' airline sector and internet-savvy customers selecting on price, the organisation needed to attract and retain cabin crew who can maximise in-flight SALES whilst maintaining SAFE operating procedures.</p></li>
						           <li class="rightTab"><strong>The Challenges</strong><p>To recruit and retain cabin crew who are motivated to sell yet maintain rigid safety procedures - a potential recruitment paradox. To achieve the right balance of over and under-servicing passengers so that in-flight sales are maximised and passengers want to fly again with the airline.</p></li>
						           <li class="cleaner"><strong>The Solution</strong><p>Profile top talent in the role using Saviio TalentMAPs to produce an ideal motivational profile for the job role. Select, interview, recruit and motivate using this profile.</p><p>Re-invent the cabin-experience for passengers and crew by changing the language and behaviour of all cabin crew.</p><p>Investment in Saviio c&pound;50k over 6 months.<br />Further opportunities to provide portable on demand training devices, content and analytics to improve access to training and product information for cabin crew</p></li>
						           <li class="rightTab"><strong>The Outcomes</strong><p>Average increase of 17% of in-flight sales across all routes (where changes have been implemented).</p>
											 </li>
						        </ul>
						       </div>
						       <div class="cleaner"></div>
						     
				      	</div>
      			</div>
				 </div>
		    <div class="cleaner"></div>
		 	</div>
		 </div>
		 
		 
		 
		 
		 
