/**
 * Localization strings for the UI Multiselect widget
 *
 * @locale en, en-US
 */

$.extend($.ui.multiselect.locale, {
	addAll:'Pridať všetky',
	removeAll:'Odstrániť všetky',
	itemsCount:'počet vybraných položiek: #{count}',
	itemsTotal:'počet položiek spolu: #{count}',
	busy:'čakajte, prosím...',
	errorDataFormat:"Nedajú sa pridať možnosti, neznámy formát údajov",
	errorInsertNode:"Vyskytol sa problém pri pokuse o pridanie položky:\n\n\t[#{key}] => #{value}\n\nOperácia bola ukončená.",
	errorReadonly:"Možnosť #{option} je iba na čítanie",
	errorRequest:"Žiaľ, vyskytol sa problém so vzdialeným hovorom. (Type: #{status})"
});