		<div id="innerContent">
			<div id="contentMenuBar">
	      <div id="contentSiFR">
	      	<h2 class="MWxBox">We know that there are many reliable and valid 'instruments' that attempt to measure personality, attitude or preferences.  In 2005 our vision was to combine reliability and validity with 'utility'.</h2>
	      </div>
	      <div class="cleaner"></div>
	     </div>
	     <div id="contentSideGFX">
	     	<img src="/_images/_sidebar/<?=$sideGFX;?>" width="194" height="150" />
	     </div>
	     <div class="cleaner"></div>
     	<div class="contentPadTop">
	      <p>We wanted all of the advantages of robust and researched tools with the ease of use and intuitiveness of Software as a Service applications. We set ourselves six guiding principles when creating and developing all Saviio applications:</p>
    	</div>
	     <div class="featureHighlights">
         <ul>
           <li><strong>Excite</strong><p>Let's capture the imagination of leaders, managers, teams and individuals by creating exciting and intuitive technologies that display relevant and highly graphical data on demand.</p></li>
           <li><strong>Engage</strong><p>	Let's stimulate conversations that matter most between people at work by showing the areas where they are likely to agree or disagree in accomplishing their goals.</p></li>
           <li class="cleaner"><strong>Evaluate</strong><p>Let's improve decision-making by revealing human factors that will make the difference between potential success or failure.</p></li>
           <li><strong>Execute</strong><p>Let's enable rapid enterprise-wide decision making by sharing relevant information with all interested parties - wherever they are or whatever language they speak.</p></li>
           <li class="cleaner"><strong>Easy</strong><p>Let's empower people "on the move", working from home or the office, to concentrate on their business goals - and not on learning new systems or software.</p></li>
           <li><strong>Effective</strong><p>Let's provide affordable, reliable and valid technologies that rapidly add measurable value to the individual and the organisation.</p></li>
        </ul>
      </div>
	</div>
	
 
 













 





















