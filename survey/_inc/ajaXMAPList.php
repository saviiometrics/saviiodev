	  <?php
	  //header('Content-Type: text/html; charset=ISO-8859-1');
	  if(!isset($_SESSION)) { session_start(); }
		$phpsessid = session_id();
	
		//include localization and site config files
		require_once("../site.config.php");
		//include DB AND ACCOUNT INFO CLASSES
		include CONTENT_PATH . '/_classes/db-class.php';
		include CONTENT_PATH . '/_classes/account-class.php';
		$accobj = new Account($_SESSION['accid']);
		
		//include other classes
		include FULL_PATH . '/_inc/_classes/MAP-class.php';
		include FULL_PATH . '/_inc/_classes/user-class.php';
		include FULL_PATH . '/_inc/_classes/admin-class.php';
		
		$MAPobj = new MAP();
		$userobj = new Admin($_SESSION['uid']);
		
		require_once(FULL_PATH . "/_inc/localization.php");
		require_once(FULL_PATH . "/_inc/scripts.php");

		if($_GET['listType'] == 'MAP') {
			$access = $userobj->getLevelAccess('map');
			if($access['VIEW'] == 'MA') {
				$compid = $userobj->headid;
			} else {
				$compid = $userobj->companyid;
			}
			$maplist = $MAPobj->getMAPList($compid, $access);
			foreach($maplist as $mapid => $userarr) {
			  echo '<option value="' . $mapid . '">' . $userarr['fullname'] . '</option>' . "\n";
			}
		} else {
			if($_GET['listType'] == 'TeamMAP') {
				$strMAP = 'team';
			} else if($_GET['listType'] == 'TalentMAP') {
				$strMAP = 'talent';
			}
			$access = $userobj->getLevelAccess('tmap');
			if($access['VIEW'] == 'MA') {
				$compid = $userobj->headid;
			} else {
				$compid = $userobj->companyid;
			}
			$tmaplist = $MAPobj->getTMAPList($strMAP, $compid, $access, 'analytics');
			foreach($tmaplist as $tmapid => $tmaparr) {
				if($tmaparr['edit'] == 'no') {
					$class = 'noedit';
				} else if ($tmaparr['edit'] == 'yes') {
					$class = 'okedit';
				}
			  echo '<option value="' . $tmapid . '" class="' . $class . '">' . $tmaparr['tmapname'] . '</option>' . "\n";
			}
		}
    ?>