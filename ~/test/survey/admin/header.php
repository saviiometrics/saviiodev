<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=ACC_NAME;?> Admin</title>
<link type="text/css" href="/min/?g=admin_css&<?=VER_JSTAG;?>" rel="stylesheet" media="all" />

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"></script>
<script type="text/javascript" src="_js/tiny_mce/jquery.tinymce.js"></script>

<?php
	//jsMinFile set in index.php
	$jsGroups = 'static,admin';

	if($jsMinFile != '') {
		$jsInsertFile = $jsMinFile;
	} else {
		$jsInsertFile = '';
	}
	//set validation engine lang file tag
	$validation_ab = substr($_SESSION['locale'], 0, -3);

	/***********************************************************
	 *																												 *
	 *													DEV JS												 *
	 *																											   *
	 ***********************************************************/

	 /*
	<script type="text/javascript" src="http://static.saviio.com/min/?g=<?=$jsGroups;?>&<?=VER_JSTAG;?>"></script>
	<script type="text/javascript" src="http://static.saviio.com/_js/jquery.validationEngine-<?=$validation_ab;?>.js"></script>
	
	if($jsInsertFile != '') {
		echo '<script type="text/javascript" src="http://c0430539.cdn2.cloudfiles.rackspacecloud.com/' . $jsInsertFile . '"></script>' . "\n";
	}
	*/
	
	/***********************************************************
	 *																												 *
	 *										PRODUCTION JS												 *
	 *																											   *
	 ***********************************************************/
	 
	 /*
	 <script type="text/javascript" src="http://c0430539.cdn2.cloudfiles.rackspacecloud.com/admin-all.min.js"></script>
	 <script type="text/javascript" src="http://c0430539.cdn2.cloudfiles.rackspacecloud.com/jquery.validationEngine-<?=$validation_ab;?>.js"></script>
	 	if($jsInsertFile != '') {
		echo '<script type="text/javascript" src="http://static.saviio.com/min/?f=_js/admin/' . $jsInsertFile . '"></script>' . "\n";
		}
	  */
	
	?>
	<script type="text/javascript" src="<?=$userobj->staticURL;?>min/?g=<?=$jsGroups;?>&<?=VER_JSTAG;?>"></script>
	<script type="text/javascript" src="<?=$userobj->staticURL;?>_js/jquery.validationEngine-<?=$validation_ab;?>.js"></script>
	
	<?php 
		if($jsInsertFile != '') {
			echo '<script type="text/javascript" src="' . $userobj->staticURL . 'min/?f=_js/admin/' . $jsInsertFile . '"></script>' . "\n";
		}
	?>

<script type="text/javascript" src="http://use.typekit.com/iti7bdl.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-16976883-2']);
  _gaq.push(['_setDomainName', '.saviio.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
 
</script>

</head>
<body>
<?php
//3 is free Trial
if($accobj->serviceid == 3) {
	//5184000 is 60 days!
  $calculation = (((ACC_TIME_ADDED + 5184000) - time(void))/3600);
  $hours = (int)$calculation;
  $days  = (int)($hours/24);
  if($days < 1) {
  	$timeshow = $hours . ' hours';
  } else {
  	$timeshow = $days . ' days';
  }
	?>
	<div id="freetrial_box">
		<span class="freeTrialHeader">Free Trial</span><br />
		<span class="freeTrialDays">Time Remaining: </span><span class="freeTrialDays_ok"><?=$timeshow;?></span>
		<p><?=_("To upgrade your account please contact us at:");?> <a href="mailto:accounts@saviio.com">accounts@saviio.com</a></p>
	</div>
	<?php
}
?>

<div id="sessionWarning" class="bottom-right"><span></span></div>
<div id="loginBox" class="MAPsBox">
	<div class="addpad">
		<div class="cicon" style="padding:10px;"><img src="<?=ADMIN_ACCOUNT_PATH;?>_images/_icons/IC_AddCompany.png" /></div>
		<h2><?=_('Login');?></h2>
		<p>
			<?=_("You are seeing this box because your sessions have timed out and you have been logged out. To continue, login again here and you will be returned to the exact state in which you left the webpage. Please note as long as the page is not reloaded manually no data will be lost.");?>
		</p>
		<div id="addTMAPheader"><?=_('Login Box');?></div>
		<form name="reloginform" id="reloginform">
			<table border="0" cellpadding="3" cellspacing="2" width="740">
				<tr>
					<td><?=_("Username");?></td>
					<td align="right"><input type="text" name="reloguser" id="reloguser" class="validate[required,length[1,50]] textfield" size="30" /></td>
				</tr>
				<tr>
					<td valign="top"><?=_("Password");?></td>
					<td align="right"><input type="password" name="relogpass" id="relogpass" class="validate[required,length[1,50]] textfield" size="30" /></td>
				</tr>
			</table>
		</form>
		<p>
			<button class="reLogNow"> <?=_('Login');?> </button>
		</p>
		<p><strong>Note:</strong> <?=_('If you cannot remeber your login then please proceed to the <a href="http://survey.saviio.com/admin/">Admin Home Page</a> where you can login again.');?></p>
	</div>
</div>

<input type="hidden" name="xhuid" id="xhuid" value="<?=$adminobj->uid;?>" />
<input type="hidden" name="currLocale" id="currLocale" value="<?=$_SESSION['locale'];?>" />
<input type="hidden" name="accid" id="accid" value="<?=$adminobj->accid;?>" />

<!----------- Userpanel Box ----------->
<div id="UserPanelBox" class="panelBox">
	<div class="addpad">
		<div class="cicon" style="padding:10px;"><img src="<?=ADMIN_ACCOUNT_PATH;?>_images/_icons/IC_AddCompany.png" /></div>
		<h2><?=_("User Panel");?></h2>
		<p><?=_('From here you can edit your own details as well as change your password');?>
		<div id="addTMAPheader"><?php echo _("Your Details");?></div>
		<form id="UPForm" name="UPForm">
			<table border="0" cellpadding="3" cellspacing="2" width="740">
				<tr>
					<td><?=_("Firstname");?></td>
					<td align="right"><input type="text" name="UPfn" id="UPfn" class="validate[required,length[2,30]] textfield" size="30" value="<?=$adminobj->firstname;?>" /></td>
				</tr>
				<tr>
					<td><?=_("Surname");?></td>
					<td align="right"><input type="text" name="UPsn" id="UPsn" class="validate[required,length[2,30]] textfield" size="30" value="<?=$adminobj->surname;?>" /></td>
				</tr>
				<tr>
					<td><?=_("Email");?></td>
					<td align="right"><input type="text" name="UPemail" id="UPemail" class="validate[required,custom[email]] textfield" size="30" value="<?=$adminobj->email;?>" /></td>
				</tr>
				<tr>
					<td><?=_("New Password");?></td>
					<td align="right"><input type="text" name="UPpassword" id="UPpassword" class="validate[optional,length[6,22]] textfield" size="30" /></td>
				</tr>
				<tr>
					<td><?=_("Repeat New Password");?></td>
					<td align="right"><input type="text" name="UPpasswordC" id="UPpasswordC" class="validate[optional,length[6,22],equals[UPpassword]] textfield" size="30" /></td>
				</tr>
			</table>
		</form>
		<br />
		<!-- yes/no buttons -->
		<p>
			<button class="UPSave"> Save Details </button>
			<button class="close"> Close </button>
		</p>
		<div class="cleaner"></div>
	</div>
</div>

<div id="sortoverlay"><br />Please wait...<br /><br /><img src="..<?=ACCOUNT_PATH;?>_images/ajax-loader.gif" width="24" height="24" /></div>

<!-- same tooltip for each entry --> 
<div id="tt_useraccess">User Access Control</div>

<?php 
$cid = $adminobj->cidviaLocale($locale);
$currpic = $cid . '.gif';
?>
  
<div id="lang_select"> <div id="currFlagFloat"><img src="..<?=ACCOUNT_PATH;?>_images/_flags/<?=$currpic;?>" alt="flag" /></div><div class="floatRight"><?=_("Current Language");?></div>
  <div id="langselbox">
    <div id="langmenu">
    	<ul>
    	<?php 
    	if(count($_GET) == 0) {
    		$delim = '';
    	} else if (count($_GET) == 1) {
    		if(array_key_exists('locale', $_GET)) {
    			$delim = '';
    		} else {
    			$delim = '&amp;';
    		}
    	} else {
    		$delim = '&amp;';
    	}
  	  $allLangs = $adminobj->getLanguages();
  	  $langs = $adminobj->getLanguages($adminobj->accid);
    	foreach($langs as $langid => $langInfo) {
    		echo '<li><div class="flag_' . $langInfo['countryid'] . '"></div><a href="' . $actionurl . $delim . 'locale=' . $langInfo['locale'] . '"  id="' . $langInfo['locale'] . '">' . _($langInfo['lang_name']) . '</a></li>' . "\n";
    	}
    	?>
		</ul>
		</div>
  </div>
</div>

<div id="adminlogo"><a href="http://<?=$_SERVER['HTTP_HOST'];?>/admin/"><img src="<?=ADMIN_ACCOUNT_PATH;?>_images/admin_logo.png" width="114" height="166" /></a></div>

<div id="adminpaneltext">
		<h3 class="MWxAdmin"><?=_("Admin Panel")?></h3>
</div>
<div class="clearRight"></div>
<div id="adminpanel_float">
	<div id="adminpanel">
   	<div class="flpad"><img src="<?=ADMIN_ACCOUNT_PATH;?>_images/_icons/IC_AddCompany.png" /></div> <div class="floatleft"><a href="http://<?=$_SERVER['SERVER_NAME'];?>"><?=_("Back to Main Site");?></a></div>
		<div class="flpad"><img src="<?=ADMIN_ACCOUNT_PATH;?>_images/_icons/IC_AddCompany.png" /></div> <div class="floatleft"><a href="javascript:void(0);" id="userpanel"><?=_("User Panel");?></a></div>
		<div class="flpad"><img src="<?=ADMIN_ACCOUNT_PATH;?>_images/_icons/IC_AddCompany.png" /></div> <div class="floatleft"><a href="http://help.saviio.com/" target="_blank"><?=_("Help");?></a></div>
		<?php 
			if($_GET['admin'] == 'companies') {
				$access = $adminobj->getLevelAccess('company');
				if($access['ADD'] == 'TRUE') {
					echo '<div class="flpad"><img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_AddCompany.png" /></div> <div class="floatleft"><a href="javascript:void(0);" id="addcompany">' . _("Add Account") . '</a></div>' . "\n";
				}
			} else if($_GET['admin'] == 'tmaps') {
				$access = $adminobj->getLevelAccess('tmap');
					if($access['ADD'] == 'TRUE') {
						echo '<div class="flpad"><img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_AddCompany.png" /></div> <div class="floatleft"><a href="javascript:void(0);" id="addnewMAP">';
						if($_GET['tmap'] == "teammap") { 
							echo _("Add Team MAP"); 
						} else {
						  echo _("Add Talent MAP");
						}
						echo '</a></div>' . "\n";
					}
			} else if($_GET['admin'] == 'users') {
				$access = $adminobj->getLevelAccess('user');
				if($access['ADD'] == 'TRUE') {
					echo '<div class="flpad"><img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_AddCompany.png" /></div> <div class="floatleft"><a href="javascript:void(0);" id="addUser">' . _("Add User") . '</a></div><div style="float:left; padding:0px 5px 0px 0px;margin-left:10px;"><img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_AddCompany.png" /></div> <div style="float:left;"><a href="javascript:void(0);" id="massUpload">' . _("Multiple User Upload") . '</a></div>' . "\n";
				}
			} else if($_GET['admin'] == 'emails') {
				echo '<div class="flpad"><img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_AddMail.png" /></div> <div class="floatleft"><a href="javascript:void(0);" class="addEmail">' . _("Add Email Template") . '</a></div>' . "\n";
			}
		?>
		<div class="flpad"><img src="<?=ADMIN_ACCOUNT_PATH;?>_images/_icons/IC_AddCompany.png" /></div>
		<div id="keepSessionAliveDiv" class="unchecked floatleft" style="float:right; cursor:pointer; color:#555"><input style="display:none;" type="checkbox" id="keepSessionAlive" />
			<div id='keepSessionAliveDivText'><?= _("Keep me logged in"); ?></div>
			<div id='keepSessionAliveDivImg' style="display: none;"><?= _("Logout"); ?></div>
		</div>
	</div>
</div>
<div class="cleaner"></div>