<?php
$allowLang = false;
//some pages use the adminobj not the userobj. Just check which is set here and use accordingly!
if(isset($userobj)) {
	$currLangs = $userobj->getLanguages($_SESSION['accid']);
} else if(isset($adminobj)) {
	$currLangs = $adminobj->getLanguages($_SESSION['accid']);
}
if(isset($_GET['locale'])) {
	//this is to prevent people setting locale in get string they don't have access too :)
	foreach($currLangs as $langid => $langArr) {
		if(in_array($_GET['locale'], $langArr)) {
			$allowLang = true;
		}
	}
	if($allowLang == true) {
		$locale = $_GET['locale'];
		$_SESSION['locale'] = $_GET['locale'];
		if(isset($_SESSION['uid'])) {
			$localeobj = new Admin($_SESSION['uid']);
			$localeobj->setUserLocale($_SESSION['uid'], $_GET['locale']);
		}
	} else {
		if(isset($_SESSION['locale'])) {
			$locale = $_SESSION['locale'];
		} else {
			$locale = "en_EN";
		}
	}
} else {
	if(isset($_POST['firstlogin'])) {
			$localeobj = new Admin($_SESSION['uid']);
			$locale = $localeobj->getLocale($_SESSION['uid']);
			$_SESSION['locale'] = $locale;
	} else {
		if(isset($_SESSION['locale'])) {
			$locale = $_SESSION['locale'];
		} else {
			//used when logged in via AJAX
			if(isset($_GET['relogLocale'])) {
				$locale = $_GET['relogLocale'];
				$_SESSION['locale'] = $locale;
			} else {
				$locale = "en_EN";
				$_SESSION['locale'] = $locale;
			}
		}
	}
}

if(isset($_GET['pdflocale'])) {
	$locale = $_GET['pdflocale'];
}

//putenv("LC_ALL=$locale");
setlocale(LC_ALL, $locale . '.UTF-8');
bindtextdomain("messages", FULL_PATH . "/locale");
textdomain("messages");
?>
