	<?php
			echo '<div style="height:68px; width:560px;">' . "\n";
				echo '<div id="lb_box"><div id="lb_box_desc" class="ST_TT">' . _("Left Brain") . '</div><div class="ST_TT_Box" style="width:550px; padding:20px;">' . "\n";
				echo '<strong>' . _("Left Brain Inventory - An inventory of the different ways the left brain processes information.") . '</strong><br /><br />
						  ' . _("While we have a natural tendency towards one way of thinking, the two sides of our brain work together in our everyday lives. The focus of the <strong>left brain</strong> is verbal, processing information in an analytical and sequential way, looking first at the pieces then putting them together to get the whole.") . '
							<br /><br /><strong>' . _("Left Brain Inventory") . '</strong><br />
							<ul>
							<li>' . _("Verbal, focusing on words, symbols, numbers") . '</li>
						  <li>' . _("Analytical, led by logic") . '</li>
						  <li>' . _("Process ideas sequentially, step by step") . '</li>
						  <li>' . _("Words used to remember things, remember names rather than faces") . ' </li>
						  <li>' . _("Make logical deductions from information") . ' </li>
						  <li>' . _("Work up to the whole step by step, focusing on details, information organized") . ' </li>
						  <li>' . _("Highly organized") . ' </li>
						  <li>' . _("Like making lists and planning") . ' </li>
						  <li>' . _("Likely to follow rules without questioning them") . ' </li>
						  <li>' . _("Good at keeping track of time") . ' </li>
						  <li>' . _("Spelling and mathematical formula easily memorized") . ' </li>
						  <li>' . _("Enjoy observing") . ' </li>
						  <li>' . _("Plan ahead") . ' </li>
						  <li>' . _("Likely read an instruction manual before trying") . ' </li>
						  <li>' . _("Listen to what is being said") . ' </li>
						  <li>' . _("Rarely use gestures when talking") . ' </li>
						  <li>' . _("Likely to believe you\'re not creative, need to be willing to try and take risks to develop your potential") . '</li></ul>';
				echo '</div></div>' . "\n";
				echo '<div id="rb_box"><div id="rb_box_desc" class="ST_TT">' . _("Right Brain") . '</div><div class="ST_TT_Box" style="width:550px; padding:20px; text-align:left;">' . "\n";
				echo '<strong>' . _("Right Brain Inventory -  An inventory of the different ways the right brain processes information.") . '</strong><br /><br />
							' . _("While we have a natural tendency towards one way of thinking, the two sides of our brain work together in our everyday lives. The <strong>right brain</strong> of the brain focuses on the visual, and processes information in an intuitive and simultaneous way, looking first at the whole picture then the details.") . '<br />
							<br />
							<strong>' . _("Right Brain Inventory") . '</strong><br />
							<ul>
							<li>' . _("Visual, focusing on images, patterns") . '</li>
							<li>' . _("Intuitive, led by feelings") . ' </li>
							<li>' . _("Process ideas simultaneously") . ' </li>
							<li>' . _("\"Mind photos\" used to remember things, writing things down or illustrating them helps you remember") . ' </li>
							<li>' . _("Make lateral connections from information") . '</li>
							<li>' . _("See the whole first, then the details") . ' </li>
							<li>' . _("Organization ends to be lacking") . ' </li>
							<li>' . _("Free association") . ' </li>
							<li>' . _("Like to know why you're doing something or why rules exist (reasons)") . ' </li>
							<li>' . _("No sense of time") . ' </li>
							<li>' . _("May have trouble with spelling and finding words to express yourself") . ' </li>
							<li>' . _("Enjoy touching and feeling actual objects (sensory input)") . ' </li>
							<li>' . _("Trouble prioritizing, so often late, impulsive") . ' </li>
							<li>' . _("Unlikely to read instruction manual before trying") . ' </li>
							<li>' . _("Listen to how something is being said") . ' </li>
							<li>' . _("Talk with your hands") . ' </li>
							<li>' . _("Likely to think you\'re naturally creative, but need to apply yourself to develop your potential") . ' </li></ul>' . "\n";	
				echo '</div></div>' . "\n";
			echo '</div>' . "\n";
			
			?>