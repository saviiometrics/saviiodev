/**
 * Localization strings for the UI Multiselect widget
 *
 * @locale pl, pl_PL
 */

$.extend($.ui.multiselect.locale, {
	addAll: 'Dodaj wszystko',
	removeAll:'Usun wszystko',
	itemsCount:'#{count} wybrane pozycje',
	itemsTotal:'#{count} pozycje ogólem',
	busy: 'prosze czekac...',
	errorDataFormat: 'Nie mozna dodac opcji, nieznany format danych',
	errorInsertNode:"Przy próbie dodania wystapil problem item:\n\n\t[#{key}] => #{value}\n\n Operacja zostala przerwana.",
	errorReadonly:"Opcja #{option} tylko do odczytu",
	errorRequest:"Przepraszamy! Wydawalo sie, ze wystapil problem ze zdalnym wywolaniem. (Type: #{status})"
}); 