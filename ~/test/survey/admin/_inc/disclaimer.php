

<div id="surveybar"><div style="float:left; margin:6px 6px 0px 0px;"><img src="<?=ADMIN_ACCOUNT_PATH;?>_images/_icons/IC_ViewUsage.png" /></div> <div style="float:left;"><?= _("Legal Disclaimer");?></div></div>
<div style="height:15px; background:url(_images/shadow_grad.jpg) repeat-x;"></div>

<div id="content_pad">
    <h1 class="MWxHeader"><?= _("Legal Disclaimer");?></h1>

  <p>The information contained in this website is provided for   general information purposes only. Bespoke legal advice must be taken   for each individual query as every situation is different. While we do   our best to ensure that the content of this site is accurate, errors or   omissions may occur, and we do not accept any liability in respect of   them. SAVIIO LTD. accepts no responsibility for loss which may arise from   any reliance which is placed on any information contained in this site   or from access to, or inability to access, this site.</p>
  <p>Any links provided to external web sites are provided for   convenience and we are not responsible for the content or operation of   such sites.</p>
  <p>&copy;  The content of this website is the property of SAVIIO LTD. unless there  is an indication to the contrary. The name SAVIIO and the SAVIIO logo  are trade marks.</p>
  Permission is given for the downloading of the content of this   website for the purpose of viewing on a personal computer or monitor.   No further reproduction of the content is permitted without the approval   of SAVIIO.
<p class="MWxMedium">&nbsp;</p>
</div>
<div class="cleaner"></div>
