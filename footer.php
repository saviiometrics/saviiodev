
		<div class="cleaner"></div>
		<div id="mainbox_btm"></div>
		<div id="footer">Copyright © Saviio <?=date("Y", time());?></div>
		<div id="btmInfo">
			Copyright © Saviio Ltd. 2005-<?=date("Y", time());?>. All rights reserved.<br />
			1200 Century Way, Thorpe Park, Leeds, LS15 8ZA<br />
			+44 113 347 0123 | Email: <a href="mailto:info@saviio.com" target="_blank">info@saviio.com</a> | Twitter: <a href="http://www.twitter.com/saviio" target="_blank">@saviio</a><br />
		</div>
	</div>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-28190248-1']);
  _gaq.push(['_setDomainName', 'saviio.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 
'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 
'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; 
s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
</html>
