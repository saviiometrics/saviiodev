  		<?php
  		require_once('_inc/_classes/auth-class.php');
  		$authobj = new Auth($_GET['authcode']);
  		$userArr = $authobj->getUserviaAC();
  		$diff = time() - $userArr['authedSent'];
  		?>
  		
  		<script type="text/javascript">
			$(function() {
				$('#savpassXN, #savpassRE').pstrength();
				$('.authRegNow').click(function() {
					if($('#savpassXN').val() == $('#savpassRE').val()) {
						if (digitalspaghetti.password.totalscore >= 20) {
							$.get("/_inc/AjaXUser.php", { completeAuth:true, username:$('#xRAuthUser').val(), authCode:$('#xRAuthCode').val(), newPass:$('#savpassRE').val()},
							function(j){
								if(j.error == true) {
									alert(j.message);
								} else {
									window.location = $('#surveyDomain').val();
								}
							}, "json");
						} else {
							alert("You must have a password rating of at least normal to register successfully.");
						}
					} else {
						alert('Passwords do not match!');
					}
				});
				//kept filling in password fields for some reason, saving from submit... lol stupid!
				$('#savpassXN, #savpassRE').val('');
			});
  		</script>
  		<input type="hidden" name="surveyDomain" id="surveyDomain" value="/" />
  		<input type="hidden" name="xRAuthCode" id="xRAuthCode" value="<?=$_GET['authcode'];?>" />
  		<input type="hidden" name="xRAuthUser" id="xRAuthUser" value="<?=$userArr['username'];?>" />
  		<div id="mainContentHolder">
		    <div id="titleBoxHolder">
       	  <div id="iconHolder"><img src="<?=ACCOUNT_PATH;?>_images/_header/mymap_header.jpg" width="89" height="74" /></div>
       	  <?php 
       	  	if($userArr['resetRequest'] == 0) {
            echo '<div class="MWxTitleHolder"><h2 class="MWxBox">' . _("Authenticate your Account") . '</h2>';
            	echo _("Please follow the steps below to authenticate your account");
            echo '</div>';
          } else {
          	 echo '<div class="MWxTitleHolder"><h2 class="MWxBox">' . _("Reset Password") . '</h2>';
            	echo _("Please follow the steps below to reset your password");
            echo '</div>';
          }
          ?>
        </div>
        <div class="cleaner"></div>
        <div id="authContentBox">
        	<?php
	  			$showAuth = 0;
	  			if($userArr['resetRequest'] == 0) {
	  				$linkStr = _("Complete Registration");
	        	if($userArr['authed'] == 1) {
	        		$showAuth = 0;
	        		echo '<p>'. _("I'm sorry this account has already been authenticated. If you have forgotten your password please click on the 'forgotten password' link in the login box above"); 
	        	} else if($diff > $authobj->expirelag) {
	        		$showAuth = 0;
	        		echo '<p>'. _("I'm sorry this authorisation code has expired. Please contact the adminstrator who sent you this authorisation code (their details are at the bottom of your invitation email) to recieve a new one.");
	        	} else {
	        		$showAuth = 1;
	        		echo '<p>' . _("Welcome to Saviio. From this page you can authenticate your account, pick the password you would like, then login and get started with our system straight away, it's that simple. The username (normally your email address) is also shown.") . '</p>';
	        	}
	        } else if($userArr['resetRequest'] == 1) {
	        	$showAuth = 1;
	        	$linkStr = _("Reset Password");
	        	echo '<p>' . _("You have requested to reset your password. Please fill out your new password below.") . '</p>';
	        }
	        
	        if($showAuth == 1) {
        	?>
	  			<form name="authUser" id="authUser">
		  			<table border="0" cellpadding="0" cellspacing="0">
		  				<tr>
		  					<td width="160">
		  						<h3 class="MWxLogin">Username</h3>
		  					</td>
		  					<td width="300"><h3 class="MWxLogin"><strong><?=$userArr['username'];?></strong></h3></td>
		  				</tr>
		  				<tr>
		  					<td width="160" valign="top">
		  						<h3 class="MWxLogin">Password</h3>
		  					</td>
		  					<td width="300"><input id="savpassXN" name="savpassXN" class="bigTBAuth" type="password" /></td>
		  				</tr>
		  				<tr>
		  					<td width="160" valign="top">
		  						<h3 class="MWxLogin">Repeat Password</h3>
		  					</td>
		  					<td width="300"><input id="savpassRE" name="savpassRE" class="bigTBAuth" type="password" /></td>
		  				</tr>
		  				<tr>
		  					<td width="160">&nbsp;</td>
		  					<td width="300"><br /><a href="javascript:void(0);" class="authRegNow" id="authBoxGo"><?=$linkStr;?></a></td>
		  				</tr>
		  			</table>
		  		</form>
	  			<br /><br />
	  			<p><?=_("Your Authorisation code is displayed below. If you have any problems in registering with the service please go to <a href=\"http://help.saviio.com\" target=\"_blank\">http://help.saviio.com</a> and create a thread with your auth code copied into the message. This will enable us to deal with your query as quickly as possible.");?><p>
	  			<p><strong><?=_("Authorisation Code");?>:</strong> <?=$_GET['authcode']; ?></p>
	  				
	  		  <?php
	  			}//end if showAuth
					?>
	  			
	  		</div>
      </div>


     	 

        

  
