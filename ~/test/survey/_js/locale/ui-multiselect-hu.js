/**
 * Localization strings for the UI Multiselect widget
 *
 * @locale hu, hu-HU
 */

$.extend($.ui.multiselect.locale, {
	addAll:'Összes hozzáadása',
	removeAll:'Összes eltávolítása',
	itemsCount:'#{count} kiválasztott elem',
	itemsTotal:'#{count} összes elem',
	busy:'Kérjük, várjon...',
	errorDataFormat:"Opció nem adható hozzá, ismeretlen adatformátum",
	errorInsertNode:"Hiba történt az alábbi elem hozzáadása során:\n\n\t[#{key}] => #{value}\n\nA művelet megszakadt.",
	errorReadonly:"A következő opció: #{option} csak olvasható formátumú",
	errorRequest:"Sajnáljuk! A távoli hívás során hiba jelentkezett. (Type: #{status})"
});
