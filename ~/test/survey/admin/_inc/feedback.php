<script type="text/javascript">

	$(function() {
			$(".tablesorter")
				.tablesorter({widthFixed: true, widgets: ['zebra']})
				.tablesorterPager({container: $("#pager"), positionFixed: false})
	      .tablesorterFilter({filterContainer: "#filter-box",
	                    filterClearContainer: "#filter-clear-button",
	                    filterColumns: [0,1,2,5,6]}).bind("filterStart",function(){positionOverlay(); $("#sortoverlay").show()}).bind("filterEnd",function(){$("#sortoverlay").hide()});
	                    
	 		$("table").bind("sortStart",function() {
					positionOverlay(); 
	        $("#sortoverlay").show(); 
	    }).bind("sortEnd",function() { 
	        $("#sortoverlay").hide(); 
	    }); 

			$(".assignadmin").live('change', function() {
				if($(this).val() != 0) {
		  		$.get("AjaXUser.php", { uid:$(this).val(), bugid:this.id.substr(8), assignAdminTK: true },
					  function(data){
							alert(data);
					  });
				} else {
					alert('Please select a real name! ;)');
				}
			});
			
			$(".updateStatus").live('change', function() {
				if($(this).val() != 0) {
		  		$.get("AjaXUser.php", { newStatus:$(this).val(), bugid:this.id.substr(8), uid:$(this).attr('name').substr(5), updateStatus: true },
					  function(data){
							alert(data);
					  });
				} else {
					alert('An error occured changing the ticket status');
				}
			});
			
			$('.deleteMAP').live("click", function(){
				var clicked = $(this);
				$(this).closest('tr').removeClass('odd even').addClass('red');
				var delid = this.id.substr(8);
	  		$.get("AjaXMAP.php", { tmapid:delid, deleteTMAP: 'yes' },
				  function(data){
						//alert(data);
						clicked.closest('td').html('<strong>Removed</strong>');
				  });
			}).confirm({dialogShow:'fadeIn', wrapper:'<div class="confirmdialog"></span>', timeout:10000, msg:$('#tmapdeletecopy').val() + '? &nbsp;', buttons: {ok:'Yes',cancel:'No', separator:' / '}});
				
			$('.unarchiveMAP').live("click", function(){
				var clicked = $(this);
				$(this).closest('tr').removeClass('odd even').addClass('green');
				var arcid = this.id.substr(8);
	  		$.get("AjaXMAP.php", { tmapid:arcid, archiveTMAP: 'reverse' },
				  function(data){
						//alert(data);
						clicked.closest('td').html('<strong>Un Archived</strong>');
				  });
			}).confirm({dialogShow:'fadeIn', wrapper:'<div class="confirmdialog"></span>', timeout:10000, msg:$('#tmaprestorecopy').val() + '? &nbsp;', buttons: {ok:'Yes',cancel:'No', separator:' / '}});


	    
	});//end doc rdy

</script>

<div id="mapbar"><div class="iconfloat"><img src="<?=ADMIN_ACCOUNT_PATH;?>_images/_icons/IC_Structure.png" /></div> <div class="floatleft"><?=_("Manage Feedback");?></div></div>
<div class="titlebar_shadow"></div>
<div id="content_pad">
<h1 class="MWxHeader"><?=_("Manage Feedback & Tickets")?></h1>
<p><?=_("This page enables you to manage all of the feedback and tickets submitted. It also allows you to easily change the status of a ticket, assign it to an admin to fix and view pictures submitted along with the ticket. You can also communicate directly with users and give them an update on the ticket they submitted, as well as merge tickets together which contain a common problem.");?></p>
<br />

<div id="filter_container">
<div id="pager" class="pager">
	<form>
		<img src="<?=ADMIN_ACCOUNT_PATH;?>_css/blue/first.png" class="first"/>
		<img src="<?=ADMIN_ACCOUNT_PATH;?>_css/blue/prev.png" class="prev"/>
		<input type="text" class="pagedisplay textfield" />
		<img src="<?=ADMIN_ACCOUNT_PATH;?>_css/blue/next.png" class="next"/>
		<img src="<?=ADMIN_ACCOUNT_PATH;?>_css/blue/last.png" class="last"/>
		<select class="pagesize">
			<option selected="selected"  value="10">10</option>
			<option value="20">20</option>
			<option value="30">30</option>
			<option  value="40">40</option>
		</select>
	</form>
</div>

<div class="searchicon"><img src="<?=ADMIN_ACCOUNT_PATH;?>_images/_icons/IC_Search.png" /></div><div class="floatleft"><strong><?=_("Search");?>:</strong> &nbsp;<input name="filter" id="filter-box" value="" maxlength="30" size="30" type="text" class="textfield"></div>
<input id="filter-clear-button" type="submit" value="" /><br />

</div>
<?php
$feedbacklist = $adminobj->getFeedback();
?>

<table cellspacing="1" class="tablesorter">
	<thead>
		<tr>
			<th width="50"><?=_("Ticket");?></th>
			<th><?=_("Ticket Name");?></th>
			<th><?=_("Ticket Summary");?></th>
            <th><?=_("Status");?></th>
			<th width="120"><?=_("Admin Assigned");?></th>
            <th><?=_("Priority");?></th>
			<th width="120"><?=_("Created By");?></th>
			<th width="90"><?=_("Created On");?></th>
			<th width="70"><?=_("Admin");?></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<th><?=_("Ticket");?></th>
			<th><?=_("Ticket Name");?></th>
			<th><?=_("Ticket Summary");?></th>
			<th><?=_("Status");?></th>
            <th><?=_("Admin Assigned");?></th>
             <th><?=_("Priority");?></th>
			<th><?=_("Created By");?></th>
			<th><?=_("Created On");?></th>
			<th><?=_("Admin");?></th>
		</tr>
	</tfoot>
	<tbody>
		<?php 
		if(!empty($feedbacklist)) {
			foreach($feedbacklist as $ticketid => $feedbackarr) {
				echo '</ul>' . "\n";
				echo '<tr>' . "\n";
				echo '<td>#' . $ticketid . '</td>' . "\n";
				echo '<td>' . $feedbackarr['title'] . '</td>' . "\n";
				echo '<td>' . $feedbackarr['comments'] . '</td>' . "\n";
				$admin = '';
				if($feedbackarr['adminid'] == 0) {
					$admin .= '<select name="assignadmin" id="as_tick_' . $ticketid . '" class="assignadmin">';
					$admin .= '<option value="0">--No Admin Assigned--</option>';
					$admin .= '<option value="1">Richard Thompson</option>';
					$admin .= '</select>';
				} else {
					if($feedbackarr['adminid'] == 1) {
						$selected = ' selected="selected"';
					} else {
						$selected = '';
					}
					$admin .= '<select name="assignadmin" id="as_tick_' . $ticketid . '" class="assignadmin">';
					$admin .= '<option value="0">--Please Select--</option>';
					$admin .= '<option value="1"' . $selected . '>Richard Thompson</option>';
					$admin .= '</select>';
				}
				$userdetails = $adminobj->getUserVID($feedbackarr['uid']);
				
				$selectednew = '';
				$selectedopen = '';
				$selectedclosed = '';
				$selectedfixed = '';
				$status = '';
				switch($feedbackarr['status']) {
					case 'new':
					$selectednew = ' selected="selected"';
					break;
					case 'open':
					$selectedopen = ' selected="selected"';
					break;
					case 'closed':
					$selectedclosed = ' selected="selected"';
					break;
					case 'fixed':
					$selectedfixed = ' selected="selected"';
					break;
				
				}
				
				$status .= '<select name="Suid_' . $feedbackarr['uid'] . '" id="US_tick_' . $ticketid . '" class="updateStatus">';
				$status .= '<option value="new"' . $selectednew . '>New</option>';
				$status .= '<option value="open"' . $selectedopen . '>Open</option>';
				$status .= '<option value="closed"' . $selectedclosed . '>Closed</option>';
				$status .= '<option value="fixed"' . $selectedfixed . '>Fixed</option>';
				$status .= '</select>';
				
				echo '<td>' . $status . '</td>' . "\n";
				echo '<td>' . $admin . '</td>' . "\n";
				echo '<td>' . $feedbackarr['priority'] . '</td>' . "\n";
				echo '<td>' . $userdetails->firstname . ' ' . $userdetails->surname . '</td>' . "\n";
				echo '<td>' . date("d/m/Y", $feedbackarr['dateadded']) . '</td>' . "\n";
				echo '<td><div class="makerelative"><a href="javascript:void(0);" class="deleteMAP" id="tmapdel_' . $tmapid . '"><img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_EraserMinus.png" alt="Delete ' . ucwords($feedbackarr['type']) . ' MAP" title="Delete ' . ucwords($feedbackarr['type']) . ' MAP" /></a>&nbsp;
				<a href="javascript:void(0);" class="unarchiveMAP" id="tmaparc_' . $tmapid . '"><img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_UnArchive.png" alt="UnArchive ' . ucwords($feedbackarr['type']) . ' MAP" title="UnArchive ' . ucwords($feedbackarr['type']) . ' MAP" /></a></div></td>' . "\n";
				echo '</tr>' . "\n";
			} 
		} else {
			echo '<tr>' . "\n"; 
			echo '<td>' . _("Sorry no records found") . '</td>' . "\n";
			echo '<td></td>' . "\n";
			echo '<td></td>' . "\n";
			echo '<td></td>' . "\n";
			echo '<td></td>' . "\n";
			echo '<td></td>' . "\n";
			echo '<td></td>' . "\n";
			echo '</tr>' . "\n"; 
		}
		?>
	</tbody>
</table>
</div>

